// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "VFX/S_UI_Trail"
{
	Properties
	{
		[PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
		_Color ("Tint", Color) = (1,1,1,1)
		
		_StencilComp ("Stencil Comparison", Float) = 8
		_Stencil ("Stencil ID", Float) = 0
		_StencilOp ("Stencil Operation", Float) = 0
		_StencilWriteMask ("Stencil Write Mask", Float) = 255
		_StencilReadMask ("Stencil Read Mask", Float) = 255

		_ColorMask ("Color Mask", Float) = 15

		[Toggle(UNITY_UI_ALPHACLIP)] _UseUIAlphaClip ("Use Alpha Clip", Float) = 0
		_Trail("Trail", 2D) = "white" {}
		_UVTrail01("UVTrail01", Vector) = (0,0,0,0)
		_TrailOffset01("TrailOffset01", Vector) = (0,0,0,0)
		_IntensityTrail01("IntensityTrail01", Float) = 0.5
		_UVTrail02("UVTrail02", Vector) = (0,0,0,0)
		_TrailOffset02("TrailOffset02", Vector) = (0,0,0,0)
		_IntensityTrail02("IntensityTrail02", Float) = 0.5
		_HDR("HDR", Float) = 1
		_Opacity("Opacity", Float) = 1
		_Edges("Edges", Float) = 2
		[Toggle(_BOOLGM_ON)] _BoolGM("BoolGM", Float) = 0
		_GradientMap("GradientMap", 2D) = "white" {}
		_Mask("Mask", 2D) = "white" {}
		[HideInInspector] _texcoord( "", 2D ) = "white" {}

	}

	SubShader
	{
		LOD 0

		Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" "PreviewType"="Plane" "CanUseSpriteAtlas"="True" }
		
		Stencil
		{
			Ref [_Stencil]
			ReadMask [_StencilReadMask]
			WriteMask [_StencilWriteMask]
			CompFront [_StencilComp]
			PassFront [_StencilOp]
			FailFront Keep
			ZFailFront Keep
			CompBack Always
			PassBack Keep
			FailBack Keep
			ZFailBack Keep
		}


		Cull Off
		Lighting Off
		ZWrite Off
		ZTest [unity_GUIZTestMode]
		Blend One OneMinusSrcAlpha, One OneMinusSrcAlpha
		ColorMask [_ColorMask]

		
		Pass
		{
			Name "Default"
		CGPROGRAM
			
			#ifndef UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX
			#define UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input)
			#endif
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0

			#include "UnityCG.cginc"
			#include "UnityUI.cginc"

			#pragma multi_compile __ UNITY_UI_CLIP_RECT
			#pragma multi_compile __ UNITY_UI_ALPHACLIP
			
			#include "UnityShaderVariables.cginc"
			#define ASE_NEEDS_FRAG_COLOR
			#pragma shader_feature_local _BOOLGM_ON

			
			struct appdata_t
			{
				float4 vertex   : POSITION;
				float4 color    : COLOR;
				float2 texcoord : TEXCOORD0;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				
			};

			struct v2f
			{
				float4 vertex   : SV_POSITION;
				fixed4 color    : COLOR;
				half2 texcoord  : TEXCOORD0;
				float4 worldPosition : TEXCOORD1;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
				
			};
			
			uniform fixed4 _Color;
			uniform fixed4 _TextureSampleAdd;
			uniform float4 _ClipRect;
			uniform sampler2D _MainTex;
			uniform sampler2D _Trail;
			uniform float4 _TrailOffset01;
			uniform float4 _UVTrail01;
			uniform float _IntensityTrail01;
			uniform float4 _TrailOffset02;
			uniform float4 _UVTrail02;
			uniform float _IntensityTrail02;
			uniform sampler2D _Mask;
			uniform float4 _Mask_ST;
			uniform float _Edges;
			uniform float _HDR;
			uniform sampler2D _GradientMap;
			uniform float _Opacity;

			
			v2f vert( appdata_t IN  )
			{
				v2f OUT;
				UNITY_SETUP_INSTANCE_ID( IN );
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(OUT);
				UNITY_TRANSFER_INSTANCE_ID(IN, OUT);
				OUT.worldPosition = IN.vertex;
				
				
				OUT.worldPosition.xyz +=  float3( 0, 0, 0 ) ;
				OUT.vertex = UnityObjectToClipPos(OUT.worldPosition);

				OUT.texcoord = IN.texcoord;
				
				OUT.color = IN.color * _Color;
				return OUT;
			}

			fixed4 frag(v2f IN  ) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX( IN );

				float2 appendResult25 = (float2(_TrailOffset01.x , _TrailOffset01.y));
				float2 appendResult19 = (float2(_UVTrail01.x , _UVTrail01.y));
				float2 appendResult17 = (float2(_UVTrail01.z , _UVTrail01.w));
				float2 texCoord26 = IN.texcoord.xy * appendResult19 + appendResult17;
				float2 panner30 = ( 1.0 * _Time.y * appendResult25 + texCoord26);
				float4 tex2DNode35 = tex2D( _Trail, panner30 );
				float4 TrailLayer0146 = ( tex2DNode35 * _IntensityTrail01 * tex2DNode35.a );
				float2 appendResult27 = (float2(_TrailOffset02.x , _TrailOffset02.y));
				float2 appendResult18 = (float2(_UVTrail02.x , _UVTrail02.y));
				float2 appendResult14 = (float2(_UVTrail02.z , _UVTrail02.w));
				float2 texCoord28 = IN.texcoord.xy * appendResult18 + appendResult14;
				float2 panner34 = ( 1.0 * _Time.y * appendResult27 + texCoord28);
				float4 tex2DNode38 = tex2D( _Trail, panner34 );
				float4 TrailLayer0245 = ( tex2DNode38 * _IntensityTrail02 * tex2DNode38.a );
				float2 uv_Mask = IN.texcoord.xy * _Mask_ST.xy + _Mask_ST.zw;
				float4 temp_output_52_0 = ( ( TrailLayer0146 + TrailLayer0245 ) * tex2D( _Mask, uv_Mask ) * _Edges );
				float3 appendResult63 = (float3(IN.color.r , IN.color.g , IN.color.b));
				float4 temp_output_62_0 = ( saturate( ( temp_output_52_0 * float4( appendResult63 , 0.0 ) ) ) * _HDR );
				#ifdef _BOOLGM_ON
				float4 staticSwitch60 = tex2D( _GradientMap, temp_output_62_0.rg );
				#else
				float4 staticSwitch60 = temp_output_62_0;
				#endif
				float4 break124 = saturate( staticSwitch60 );
				float4 break53 = temp_output_52_0;
				float4 appendResult123 = (float4(break124.r , break124.g , break124.b , saturate( ( IN.color.a * _Opacity * ( break53.r * break53.a ) ) )));
				
				half4 color = appendResult123;
				
				#ifdef UNITY_UI_CLIP_RECT
                color.a *= UnityGet2DClipping(IN.worldPosition.xy, _ClipRect);
                #endif
				
				#ifdef UNITY_UI_ALPHACLIP
				clip (color.a - 0.001);
				#endif

				return color;
			}
		ENDCG
		}
	}
	CustomEditor "ASEMaterialInspector"
	
	
}
/*ASEBEGIN
Version=18800
1920;73;1920;928;7659.273;1953.498;4.097001;True;False
Node;AmplifyShaderEditor.CommentaryNode;6;-4352.174,-398.1154;Inherit;False;2284.323;706.6891;Trail ;29;46;45;42;41;36;38;37;35;33;34;30;112;27;26;25;28;14;16;18;19;11;17;9;8;111;113;109;105;110;;1,1,1,1;0;0
Node;AmplifyShaderEditor.Vector4Node;8;-4333.019,-348.1154;Inherit;False;Property;_UVTrail01;UVTrail01;1;0;Create;True;0;0;0;False;0;False;0,0,0,0;0,0,0,0;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.Vector4Node;9;-4329.872,-49.47139;Inherit;False;Property;_UVTrail02;UVTrail02;4;0;Create;True;0;0;0;False;0;False;0,0,0,0;0,0,0,0;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.Vector4Node;11;-3958.511,-173.4942;Inherit;False;Property;_TrailOffset01;TrailOffset01;2;0;Create;True;0;0;0;False;0;False;0,0,0,0;0,0,0,0;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.DynamicAppendNode;14;-4092.873,65.3905;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;18;-4094.873,-21.60951;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;17;-4107.019,-232.5949;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.Vector4Node;16;-3974.011,125.5059;Inherit;False;Property;_TrailOffset02;TrailOffset02;5;0;Create;True;0;0;0;False;0;False;0,0,0,0;0,0,0,0;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.DynamicAppendNode;19;-4109.019,-319.595;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;28;-3911.535,19.83165;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TextureCoordinatesNode;26;-3871.813,-310.0059;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.DynamicAppendNode;27;-3809.011,153.5058;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;25;-3786.511,-145.4942;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TexturePropertyNode;33;-3626.835,-172.3799;Inherit;True;Property;_Trail;Trail;0;0;Create;True;0;0;0;False;0;False;643eb5f8d5ee7b840924d45338569098;643eb5f8d5ee7b840924d45338569098;False;white;Auto;Texture2D;-1;0;2;SAMPLER2D;0;SAMPLERSTATE;1
Node;AmplifyShaderEditor.PannerNode;34;-3615.573,17.59059;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.PannerNode;30;-3609.018,-298.595;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SamplerNode;38;-2913.65,10.46854;Inherit;True;Property;_TextureSample1;Texture Sample 1;0;0;Create;True;0;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;36;-2620.202,-189.0823;Inherit;False;Property;_IntensityTrail01;IntensityTrail01;3;0;Create;True;0;0;0;False;0;False;0.5;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;35;-2916.194,-301.8167;Inherit;True;Property;_TextureSample0;Texture Sample 0;0;0;Create;True;0;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;37;-2635.492,117.0051;Inherit;False;Property;_IntensityTrail02;IntensityTrail02;6;0;Create;True;0;0;0;False;0;False;0.5;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;42;-2437.327,-296.6362;Inherit;False;3;3;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;41;-2462.526,15.37615;Inherit;False;3;3;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;45;-2291.965,10.74791;Inherit;False;TrailLayer02;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;46;-2269.905,-300.8958;Inherit;False;TrailLayer01;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.CommentaryNode;7;-4358.793,307.3903;Inherit;False;2287.259;767.541;Edge fade;18;43;40;44;39;31;32;21;20;29;22;23;24;15;12;13;10;85;86;;1,1,1,1;0;0
Node;AmplifyShaderEditor.GetLocalVarNode;47;-1823.266,141.6152;Inherit;False;46;TrailLayer01;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;49;-1823.266,205.6151;Inherit;False;45;TrailLayer02;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;69;-1812.912,370.5679;Inherit;True;Property;_Mask;Mask;16;0;Create;True;0;0;0;False;0;False;-1;29523ae44dbb9534aa96adfc02449c9e;29523ae44dbb9534aa96adfc02449c9e;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;12;-4005.326,612.5165;Inherit;False;Property;_Edges;Edges;9;0;Create;True;0;0;0;False;0;False;2;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.VertexColorNode;55;-1621.332,-100.8634;Inherit;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;51;-1609.822,144.4478;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;52;-1460.389,144.0158;Inherit;False;3;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.DynamicAppendNode;63;-1476.828,-76.63386;Inherit;False;FLOAT3;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;61;-1264.251,-158.1857;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT3;0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;66;-1178.693,-37.03267;Inherit;False;Property;_HDR;HDR;7;0;Create;True;0;0;0;False;0;False;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;68;-1149.206,-157.421;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.TexturePropertyNode;64;-1221.651,-340.8382;Inherit;True;Property;_GradientMap;GradientMap;15;0;Create;True;0;0;0;False;0;False;None;None;False;white;Auto;Texture2D;-1;0;2;SAMPLER2D;0;SAMPLERSTATE;1
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;62;-1012.763,-54.1931;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.BreakToComponentsNode;53;-1288.182,145.2271;Inherit;False;COLOR;1;0;COLOR;0,0,0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.SamplerNode;65;-818.6917,-198.1419;Inherit;True;Property;_TextureSample2;Texture Sample 2;7;0;Create;True;0;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.StaticSwitch;60;-495.8449,-32.3373;Inherit;False;Property;_BoolGM;BoolGM;14;0;Create;True;0;0;0;False;0;False;0;0;0;True;;Toggle;2;Key0;Key1;Create;True;True;9;1;COLOR;0,0,0,0;False;0;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;4;COLOR;0,0,0,0;False;5;COLOR;0,0,0,0;False;6;COLOR;0,0,0,0;False;7;COLOR;0,0,0,0;False;8;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;54;-1164.284,144.2534;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;56;-1062.786,58.37257;Inherit;False;Property;_Opacity;Opacity;8;0;Create;True;0;0;0;False;0;False;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;58;-182.2731,-27.16468;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;57;-830.8859,38.87257;Inherit;True;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;121;-7677.178,-691.8606;Inherit;False;2462.216;712.1156;Distortion;19;95;94;91;120;119;117;101;102;103;90;116;114;97;99;115;93;92;88;104;;1,1,1,1;0;0
Node;AmplifyShaderEditor.BreakToComponentsNode;124;64.15696,-36.66513;Inherit;False;COLOR;1;0;COLOR;0,0,0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.SaturateNode;59;-179.5557,37.03426;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;113;-3621.875,216.3195;Inherit;False;Property;_LerpB;LerpB;20;0;Create;True;0;0;0;False;0;False;0;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;105;-3425.423,-229.4573;Inherit;False;104;NoiseDistortion;1;0;OBJECT;;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;110;-3401.77,-135.8334;Inherit;False;Property;_LerpA;LerpA;19;0;Create;True;0;0;0;False;0;False;0.1632353;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;112;-3545.709,126.2813;Inherit;False;104;NoiseDistortion;1;0;OBJECT;;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;88;-5676.646,-576.2057;Inherit;True;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.PannerNode;92;-5891.198,-527.9034;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.LerpOp;91;-6053.672,-529.0565;Inherit;False;3;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;93;-6484.781,-618.2894;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;95;-6258,-278.5035;Inherit;False;Property;_DistortionLerp;DistortionLerp;18;0;Create;True;0;0;0;False;0;False;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;115;-6847.771,-641.8606;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.PannerNode;97;-6689.664,-572.0381;Inherit;False;3;0;FLOAT2;1,1;False;2;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SamplerNode;90;-6855.478,-291.0042;Inherit;True;Property;_DistortionNoise;DistortionNoise;17;0;Create;True;0;0;0;False;0;False;-1;3f21621536ac6e74c810d23a828d590a;3f21621536ac6e74c810d23a828d590a;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TextureCoordinatesNode;103;-7067.778,-260.9778;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.DynamicAppendNode;116;-6850.655,-551.391;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleTimeNode;99;-6879.643,-444.3465;Inherit;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.Vector4Node;114;-7038.898,-628.3608;Inherit;False;Property;_NeutralPanner;NeutralPanner;21;0;Create;True;0;0;0;False;0;False;1,1,0,0;0,0,1,1;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.PannerNode;101;-7266.903,-213.218;Inherit;False;3;0;FLOAT2;1,1;False;2;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;119;-7407.755,-277.7855;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;94;-6542,-308.5035;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RegisterLocalVarNode;104;-5445.501,-581.012;Inherit;False;NoiseDistortion;-1;True;1;0;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.LerpOp;109;-3128.77,-295.8334;Inherit;False;3;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;120;-7408.696,-188.1404;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;23;-3689.594,363.7583;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.Vector4Node;117;-7627.178,-266.47;Inherit;False;Property;_DistortionPanner;DistortionPanner;22;0;Create;True;0;0;0;False;0;False;1,1,0,0;1,1,0,0;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LerpOp;111;-3248.128,44.18759;Inherit;False;3;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;20;-3695.931,679.2557;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;44;-2273.836,547.9221;Inherit;False;EdgeFade;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;10;-4308.793,550.3588;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.OneMinusNode;13;-3829.594,449.7582;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;22;-3689.594,449.7582;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;24;-3461.594,404.7582;Inherit;False;Constant;_Power01;Power01;6;0;Create;True;0;0;0;False;0;False;2;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;15;-3835.931,678.2557;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;32;-3295.594,361.7583;Inherit;True;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleTimeNode;102;-7444.837,-90.745;Inherit;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;50;-1646.929,280.4835;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;29;-3694.931,596.2557;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;21;-3454.931,637.2557;Inherit;False;Constant;_Power02;Power02;6;0;Create;True;0;0;0;False;0;False;2;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;31;-3294.931,595.2559;Inherit;True;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StaticSwitch;85;-3064.424,401.6878;Inherit;False;Property;_BothLeftRight;Both Left Right;12;0;Create;True;0;0;0;False;0;False;0;0;0;True;;Toggle;2;Key0;Key1;Create;True;True;9;1;FLOAT;0;False;0;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT;0;False;7;FLOAT;0;False;8;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StaticSwitch;40;-2811.323,423.89;Inherit;False;Property;_EdgeFadeAlignment;Edge Fade Alignment;10;0;Create;True;0;0;0;False;0;False;0;0;0;True;;Toggle;2;Key0;Key1;Create;True;True;9;1;FLOAT;0;False;0;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT;0;False;7;FLOAT;0;False;8;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;39;-2966.111,522.8414;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StaticSwitch;43;-2540.185,511.5043;Inherit;False;Property;_UseBothEdgeFade;Use Both Edge Fade;13;0;Create;True;0;0;0;False;0;False;0;0;0;True;;Toggle;2;Key0;Key1;Create;True;True;9;1;FLOAT;0;False;0;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT;0;False;7;FLOAT;0;False;8;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StaticSwitch;86;-3288.322,820.4875;Inherit;False;Property;_LeftorRight;Left or Right;11;0;Create;True;0;0;0;False;0;False;0;0;0;True;;Toggle;2;Key0;Key1;Create;True;True;9;1;FLOAT;0;False;0;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT;0;False;7;FLOAT;0;False;8;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;123;231.8832,-90.12803;Inherit;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.GetLocalVarNode;48;-1821.603,276.8527;Inherit;False;44;EdgeFade;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;122;343.3705,49.57349;Float;False;True;-1;2;ASEMaterialInspector;0;6;VFX/S_UI_Trail;5056123faa0c79b47ab6ad7e8bf059a4;True;Default;0;0;Default;2;True;3;1;False;-1;10;False;-1;3;1;False;-1;10;False;-1;False;False;False;False;False;False;False;False;True;2;False;-1;True;True;True;True;True;0;True;-9;False;False;False;True;True;0;True;-5;255;True;-8;255;True;-7;0;True;-4;0;True;-6;1;False;-1;1;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;True;2;False;-1;True;0;True;-11;False;True;5;Queue=Transparent=Queue=0;IgnoreProjector=True;RenderType=Transparent=RenderType;PreviewType=Plane;CanUseSpriteAtlas=True;False;0;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;2;0;;0;0;Standard;0;0;1;True;False;;False;0
WireConnection;14;0;9;3
WireConnection;14;1;9;4
WireConnection;18;0;9;1
WireConnection;18;1;9;2
WireConnection;17;0;8;3
WireConnection;17;1;8;4
WireConnection;19;0;8;1
WireConnection;19;1;8;2
WireConnection;28;0;18;0
WireConnection;28;1;14;0
WireConnection;26;0;19;0
WireConnection;26;1;17;0
WireConnection;27;0;16;1
WireConnection;27;1;16;2
WireConnection;25;0;11;1
WireConnection;25;1;11;2
WireConnection;34;0;28;0
WireConnection;34;2;27;0
WireConnection;30;0;26;0
WireConnection;30;2;25;0
WireConnection;38;0;33;0
WireConnection;38;1;34;0
WireConnection;35;0;33;0
WireConnection;35;1;30;0
WireConnection;42;0;35;0
WireConnection;42;1;36;0
WireConnection;42;2;35;4
WireConnection;41;0;38;0
WireConnection;41;1;37;0
WireConnection;41;2;38;4
WireConnection;45;0;41;0
WireConnection;46;0;42;0
WireConnection;51;0;47;0
WireConnection;51;1;49;0
WireConnection;52;0;51;0
WireConnection;52;1;69;0
WireConnection;52;2;12;0
WireConnection;63;0;55;1
WireConnection;63;1;55;2
WireConnection;63;2;55;3
WireConnection;61;0;52;0
WireConnection;61;1;63;0
WireConnection;68;0;61;0
WireConnection;62;0;68;0
WireConnection;62;1;66;0
WireConnection;53;0;52;0
WireConnection;65;0;64;0
WireConnection;65;1;62;0
WireConnection;60;1;62;0
WireConnection;60;0;65;0
WireConnection;54;0;53;0
WireConnection;54;1;53;3
WireConnection;58;0;60;0
WireConnection;57;0;55;4
WireConnection;57;1;56;0
WireConnection;57;2;54;0
WireConnection;124;0;58;0
WireConnection;59;0;57;0
WireConnection;88;1;92;0
WireConnection;92;0;91;0
WireConnection;91;0;93;0
WireConnection;91;1;94;0
WireConnection;91;2;95;0
WireConnection;93;0;115;0
WireConnection;93;1;97;0
WireConnection;115;0;114;1
WireConnection;115;1;114;2
WireConnection;97;2;116;0
WireConnection;97;1;99;0
WireConnection;90;1;103;0
WireConnection;103;0;119;0
WireConnection;103;1;101;0
WireConnection;116;0;114;3
WireConnection;116;1;114;4
WireConnection;101;2;120;0
WireConnection;101;1;102;0
WireConnection;119;0;117;1
WireConnection;119;1;117;2
WireConnection;94;0;90;0
WireConnection;104;0;88;0
WireConnection;109;0;30;0
WireConnection;109;1;105;0
WireConnection;109;2;110;0
WireConnection;120;0;117;3
WireConnection;120;1;117;4
WireConnection;23;0;10;1
WireConnection;23;1;12;0
WireConnection;111;0;34;0
WireConnection;111;1;112;0
WireConnection;111;2;113;0
WireConnection;20;0;15;0
WireConnection;20;1;12;0
WireConnection;44;0;43;0
WireConnection;13;0;10;1
WireConnection;22;0;13;0
WireConnection;22;1;12;0
WireConnection;15;0;10;2
WireConnection;32;0;23;0
WireConnection;32;1;22;0
WireConnection;32;2;24;0
WireConnection;50;0;48;0
WireConnection;29;0;10;2
WireConnection;29;1;12;0
WireConnection;31;0;29;0
WireConnection;31;1;20;0
WireConnection;31;2;21;0
WireConnection;85;1;86;0
WireConnection;85;0;32;0
WireConnection;40;1;85;0
WireConnection;40;0;31;0
WireConnection;39;0;32;0
WireConnection;39;1;31;0
WireConnection;43;1;40;0
WireConnection;43;0;39;0
WireConnection;86;1;23;0
WireConnection;86;0;22;0
WireConnection;123;0;124;0
WireConnection;123;1;124;1
WireConnection;123;2;124;2
WireConnection;123;3;59;0
WireConnection;122;0;123;0
ASEEND*/
//CHKSM=47AAEA2DB67EF2E6F16DC3DD91C652B307B9DEF7