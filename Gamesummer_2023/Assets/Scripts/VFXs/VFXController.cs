using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.Events;
using GDC;

namespace GDC.Managers
{
    public class VFXController : MonoBehaviour
    {
        [SerializeField] ShaderModifierEffect lowHealthVfx;

        private ShaderModifierEffect lowHealthVfxInstance; 
        // Start is called before the first frame update
        void Start()
        {
            GameEvents.TOGGLE_LOW_HEALTH_VFX += HandleToggleLowHealthVfx;
        }
        void OnDestroy()
        {
            GameEvents.TOGGLE_LOW_HEALTH_VFX -= HandleToggleLowHealthVfx;
        }

        void HandleToggleLowHealthVfx(bool isOn)
        {
            if (isOn)
            {
                if (this.lowHealthVfxInstance != null)
                {
                    Destroy(this.lowHealthVfxInstance.gameObject);
                }
                this.lowHealthVfxInstance = Instantiate(this.lowHealthVfx, transform);
                this.lowHealthVfxInstance.Show();
            }
            else
            {
                if (this.lowHealthVfxInstance == null)
                    return;
                this.lowHealthVfxInstance.Hide();
            }
        }
    }
}
