using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NaughtyAttributes;
using DG.Tweening;

namespace GDC
{
    public class ShaderModifierEffect : MonoBehaviour
    {
        public Image image;
        private Material myMat;
        Material cloneMat;
        public float minEdgesVal;
        public float maxEdgesVal;
        [SerializeField] private string edgesString = "_Edges";
        public float tweenTime = 0.5f;
        public float endTweenTime = 0.5f;
        private float fadeValue = 0;
        private Tween effectTweenLoop;
        // Start is called before the first frame update
        void Init()
        {
            myMat = image.material;
            CacheMaterials();
        }
        [Button]
        public void Show()
        {
            Init();
            this.fadeValue = this.minEdgesVal;
            this.effectTweenLoop = DOTween
                .To(() => this.fadeValue, value => this.fadeValue = value, this.maxEdgesVal, this.tweenTime)
                .SetLoops(-1, LoopType.Yoyo)
                .OnUpdate(() => 
                {
                    myMat.SetFloat(edgesString, this.fadeValue);
                })
                .OnKill(() =>
                {
                    DOTween
                        .To(() => this.fadeValue, valueEnd => this.fadeValue = valueEnd, 0, this.endTweenTime)
                        .OnUpdate(() => 
                        {
                            myMat.SetFloat(edgesString, this.fadeValue);
                        })
                        .SetEase(Ease.InOutSine)
                        .OnComplete(() => Destroy(gameObject));
                });
        }
        [Button]
        public void Hide()
        {
            this.effectTweenLoop.Kill();
        }
        public void CacheMaterials()
        {
            cloneMat = new Material(myMat);
        }
    }
}
