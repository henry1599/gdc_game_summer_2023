using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using RotaryHeart.Lib.SerializableDictionary;
using GDC.Enums;

namespace GDC.Configuration
{
    [System.Serializable] 
    public class EffectDictPath : SerializableDictionaryBase<ePlayerEffectStatus, string> {}
    [CreateAssetMenu(fileName = "Player Stat Config", menuName = "Scriptable Objects/Player")]
    public class PlayerStatConfig : ScriptableObject
    {
        [Foldout("Init stats")] public int BaseSpeed = 10;
        [Foldout("Init stats")] public int Speed;
        [Foldout("Init stats")] public float SpeedScale;
        [Foldout("Init stats")] public int Attack;
        [Foldout("Init stats")] public int Defense;
        [Foldout("Init stats")] public int MaxHealth;
        [Foldout("Init stats")] public int MaxStamina;
        [Foldout("Init stats")] public float DashDistance;

        [Foldout("Effects")] public string EffectsPath;
        [Foldout("Effects")] public EffectDictPath EffectPath;
        public GameObject LoadEffectObject(ePlayerEffectStatus effectStatus)
        {
            string path = EffectsPath + EffectPath[effectStatus];
            return Resources.Load(path) as GameObject;
        }
        public Dictionary<ePlayerEffectStatus, GameObject> LoadAllEffectInstances(Transform parent = null)
        {
            Dictionary<ePlayerEffectStatus, GameObject> result = new();
            foreach (var (key, value) in EffectPath)
            {
                var statusObject = LoadEffectObject(key);
                var statusInstance = Instantiate(statusObject, parent);
                statusInstance.SetActive(false);
                result.TryAdd(key, statusInstance);
            }
            return result;
        }
#if UNITY_EDITOR
        [Button("Test Load All Effects")]
        public void TestLoadAllEffects()
        {
            LoadAllEffectInstances();
        }
#endif
    }
}
