using GDC.Enums;
using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GDC.Configuration
{
    [CreateAssetMenu(menuName = "Scriptable Object/SO Pet", fileName = "SO Pet")]
    public class SO_Pet : ScriptableObject
    {
         public PetType PetType;
         public float Speed, Cooldown;
         public float HealAmount;
         public float Damage;
         public PetID PetID;

         public string PetInfo;
    }
}