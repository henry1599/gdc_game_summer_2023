using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.Enums;

namespace GDC.Configuration
{
    [CreateAssetMenu(menuName = "Scriptable Object/SO Item", fileName = "SO Item")]
    public class SO_Item : ScriptableObject
    {
        public ItemType itemType;

        [Header("If ItemType == WEAPON, choose WeaponType:")]
        public WeaponType weaponType;

        [Header("If ItemType == WEAPON or CLOTHES, choose EquipmentType")]
        public EquipmentType EquipmentType;

        [Space(10)]
        public Sprite sprite;
        public string itemName, detail, ID;
        public int cost;

        [Header("Additional stat when EquipmentType != null only:")]
        public int Attack;
        public int Defense, Speed, Stamina;

        [Header("Additional stat immediately:")]
        public int HealthIncrease;
        public int StaminaIncrease;
    }
}
