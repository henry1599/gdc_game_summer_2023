using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.Enums;

namespace GDC.Configuration
{
    [CreateAssetMenu(menuName = "Scriptable Object/SO Craft", fileName = "SO Craft")]
    public class SO_Craft : ScriptableObject
    {
        public SO_Item SO_item;

        [Header("ItemRequire")]
        public SO_Item[] ItemRequires;
        public int[] numberRequires;
    }
}
