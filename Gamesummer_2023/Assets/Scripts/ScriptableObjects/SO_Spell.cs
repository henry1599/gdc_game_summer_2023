using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.Enums;
using GDC.Managers;

namespace GDC.Configuration
{
    [CreateAssetMenu(menuName = "Scriptable Object/SO Spell", fileName = "SO Spell")]
    public class SO_Spell : ScriptableObject
    {
        public SpellType SpellType;
        //public LayerMask LayerMask;
        public string Name;
        public string Explain;
        public Sprite Icon;
        public float BaseSpellAtk;
        public float BaseStaminaUsed;
        public float BaseEffectDuration;

        public int Index;

        [Header("ItemRequire (maximum 3 item)")]
        public SO_Item[] ItemRequires;
        public int[] numberRequires;

        public float SpellAtk()
        {
            return BaseSpellAtk + (SaveLoadManager.Instance.GameData.SpellLevelList[Index] - 1) * BaseSpellAtk * 0.05f;
        }

        public float StaminaUsed()
        {
            return BaseStaminaUsed + (SaveLoadManager.Instance.GameData.SpellLevelList[Index] - 1) * BaseStaminaUsed * 0.05f;
        }

        public float EffectDuration()
        {
            if (SaveLoadManager.Instance.GameData.SpellLevelList[Index] > 5) return BaseEffectDuration * 1.5f;
            return BaseEffectDuration;
        }    
    }
}
