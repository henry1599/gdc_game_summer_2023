using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.Managers;
using GDC.Enums;
using GDC.Gameplay.UI;
using GDC.Configuration;
using GDC.PlayerManager;

public class NewBehaviourScript : MonoBehaviour
{
    [SerializeField] SO_Item SO_item1, SO_item2, SO_weaponItem, SO_weaponItem2, SO_useableItem1, SO_useableItem2;
    [SerializeField] SO_Craft SO_useableItemCraft1;

    [SerializeField] List<bool> getSpellList;

    [SerializeField] int getSpellListLength;

    // Update is called once per frame
    void Update()
    {
        if (SaveLoadManager.Instance.GameData.SpellLevelList != null)
        {
            getSpellListLength = SaveLoadManager.Instance.GameData.GetSpellList.Count;
        }
        //if (Input.GetKeyDown(KeyCode.Z))
        //{
        //    GameManager.Instance.LoadSceneManually(SceneType.AREA_01_CHECKER, TransitionType.RIGHT);
        //}

        if (Input.GetKeyDown(KeyCode.N))
        {
            SaveLoadManager.Instance.Save();
        }    
        if (Input.GetKeyDown(KeyCode.M))
        {
            SaveLoadManager.Instance.Load();
        }

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            SaveLoadManager.Instance.GameData.AddItem(SO_item1);
            Debug.Log("Add ingredient to data success");
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            SaveLoadManager.Instance.GameData.AddItem(SO_item2);
            Debug.Log("Add ingredient to data success");
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            SaveLoadManager.Instance.GameData.AddItem(SO_weaponItem);
            Debug.Log("Add weapon to data success");
        }

        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            SaveLoadManager.Instance.GameData.AddItem(SO_useableItem1);
            Debug.Log("Add useable to data success");
        }
        if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            SaveLoadManager.Instance.GameData.AddItem(SO_useableItem2);
            Debug.Log("Add useable to data success");
        }

        if (Input.GetKeyDown(KeyCode.Alpha6))
        {
            SaveLoadManager.Instance.GameData.AddItem(SO_weaponItem2);
            Debug.Log("Add weapon to data success");
        }

        if (Input.GetKeyDown(KeyCode.Alpha0))
        {
            SaveLoadManager.Instance.GameData.AddCraft(SO_useableItemCraft1);
            Debug.Log("Add useable item craft to data success");
        }

        if (Input.GetKeyDown(KeyCode.C))
        {
            InventoryManager.Instance.LoadIngredientItem();
            Debug.Log("Load Ingredient Inventory");
        }
        if (Input.GetKeyDown(KeyCode.V))
        {
            InventoryManager.Instance.LoadWeaponItem();
            Debug.Log("Load Weapon Inventory");
        }
    }

    [NaughtyAttributes.Button]
    public void SetGetSpellList()
    {
        SaveLoadManager.Instance.GameData.GetSpellList = getSpellList;
        SpellUpgradeManager.Instance.Load();
    }

    [NaughtyAttributes.Button]
    void TestObtain()
    {
        Player.Instance.ObtainItem(SO_item1);
    }    
}
