using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class TestLowHealthVfx : MonoBehaviour
{
    [Button]
    public void Show()
    {
        GDC.Events.GameEvents.TOGGLE_LOW_HEALTH_VFX?.Invoke(true);
    }
    [Button]
    public void Hide()
    {
        GDC.Events.GameEvents.TOGGLE_LOW_HEALTH_VFX?.Invoke(false);
    }
}
