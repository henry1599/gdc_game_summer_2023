using GDC.Managers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DovaFishingRopeEvent : DialogueEvent
{
    public override void ActiveDialogue()
    {
        if (SaveLoadManager.Instance.GameData.CutsceneInHouse) return;
        base.ActiveDialogue();
    }
}
