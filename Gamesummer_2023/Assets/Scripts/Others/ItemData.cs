using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GDC.Configuration
{
    public class ItemData
    {
        public SO_Item SO_item;
        public int Amount = 0;
    }
}
