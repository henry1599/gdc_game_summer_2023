using GDC.Managers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss1Gate : MonoBehaviour
{
    [SerializeField] GameObject BossGate, BossGateCollapse, transitionZone;
    // Start is called before the first frame update
    void Start()
    {
        if (SaveLoadManager.Instance.GameData.CutsceneDefeatBoss1)
        {
            BossGateCollapse.SetActive(true);
            BossGate.SetActive(false);
            transitionZone.SetActive(false);
        }    
        else
        {
            BossGate.SetActive(true);
            BossGateCollapse.SetActive(false);
            transitionZone.SetActive(true);
        }
    }
}
