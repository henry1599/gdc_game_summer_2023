using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveEvent : MonoBehaviour
{
    [SerializeField] GameObject[] activeObject;
    [SerializeField] GameObject[] inactiveObjects;
    
    public void Setup()
    {
        foreach(var obj in activeObject)
        {
            obj.SetActive(true);
        }
        foreach(var obj in inactiveObjects)
        {
            obj.SetActive(false);
        }
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            Setup();
        }
    }
}
