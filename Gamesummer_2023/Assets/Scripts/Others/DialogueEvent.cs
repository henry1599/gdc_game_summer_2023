using GDC.Gameplay;
using GDC.Gameplay.UI;
using GDC.PlayerManager;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueEvent : MonoBehaviour
{
    [SerializeField] Dialogue dialogue;
    [SerializeField] Animator npcAnim;

    public virtual void ActiveDialogue()
    {
        StartCoroutine(Cor_ActiveDialogue());
    }
    IEnumerator Cor_ActiveDialogue()
    {
        yield return new WaitUntil(() => Player.Instance.InCutScene == false);
        Player.Instance.InCutScene = true;
        Player.Instance.UseHandMovement = false;
        yield return new WaitUntil(() => TriggerDialogue.Instance.isEndDialogue);
        yield return new WaitForSeconds(0.5f);
        if (npcAnim != null)
            npcAnim.Play("talk");
        TriggerDialogue.Instance.SetDialogue(dialogue);
        yield return new WaitUntil(() => TriggerDialogue.Instance.isEndDialogue);
        if (npcAnim != null)
            npcAnim.Play("idle");
        Player.Instance.InCutScene = false;
        Player.Instance.UseHandMovement = true;
    }
}
