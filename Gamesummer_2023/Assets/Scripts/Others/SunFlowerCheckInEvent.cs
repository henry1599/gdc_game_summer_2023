using DG.Tweening;
using GDC.Gameplay;
using GDC.Managers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SunFlowerCheckInEvent : MonoBehaviour
{
    [SerializeField] List<Transform> grasses;
    [SerializeField] Chest chest;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Cor_Init());
    }
    IEnumerator Cor_Init()
    {
        yield return new WaitUntil(() => GameManager.Instance.isLoadSceneComplete);
        if (SaveLoadManager.Instance.GameData.EventSunFlowerCheckIn)
        {
            foreach(var grass in grasses)
                grass.gameObject.SetActive(true);
        }
        else
        {
            foreach (var grass in grasses)
                grass.gameObject.SetActive(false);
        }
    }
    void ChestAppear()
    {
        SaveLoadManager.Instance.GameData.EventSunFlowerCheckIn = true;
        foreach (var grass in grasses)
        {
            grass.gameObject.SetActive(true);
            grass.localScale = Vector2.zero;
            grass.DOScale(1, 0.5f).SetEase(Ease.OutBack).OnComplete(()=>SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_LOOT_SPECIAL));
            chest.Appear();
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            if (SaveLoadManager.Instance.GameData.EventSunFlowerCheckIn == false)
            {
                ChestAppear();
            }
        }
    }
}
