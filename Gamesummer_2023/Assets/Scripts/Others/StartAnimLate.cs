using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartAnimLate : MonoBehaviour
{
    [SerializeField] Animator anim;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Cor_StartAnim());
    }

    IEnumerator Cor_StartAnim()
    {
        yield return new WaitForSeconds(Random.Range(0, 2f));
        anim.enabled = true;
    }
}
