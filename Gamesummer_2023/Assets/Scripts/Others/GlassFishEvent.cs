using DG.Tweening;
using GDC.Enemies;
using GDC.Gameplay;
using GDC.Gameplay.UI;
using GDC.Managers;
using GDC.PlayerManager;
using GDC.PlayerManager.Hand;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlassFishEvent : MonoBehaviour
{
    bool isFishIn;
    [SerializeField] Animator glassFishAnim, waterSplashAnim;
    [SerializeField] Dialogue aquariumHaveFishDialogue;
    Dialogue aquariumNotFishDialogue;
    [SerializeField] NPC fish, aquarium, deadFish;
    [SerializeField] GameObject vfx;
    [SerializeField] SpriteRenderer deadFishSpr;
    // Start is called before the first frame update
    void Start()
    {
        aquariumNotFishDialogue = aquarium.GetDialogue();
        StartCoroutine(Cor_Init());
    }

    public void FishJumpOut()
    {
        StartCoroutine(Cor_FishJumpOut());
    }
    public void FishJumpIn()
    {
        StartCoroutine(Cor_FishJumpIn());
    }
    public void DeadFishVanish()
    {
        SaveLoadManager.Instance.GameData.EventDeadFishVanish = true;
        deadFish.enabled = false;
        deadFishSpr.DOFade(0, 2).OnComplete(() => deadFish.gameObject.SetActive(false));
    }
    IEnumerator Cor_Init()
    {
        yield return new WaitUntil(() => GameManager.Instance.isLoadSceneComplete);
        if (SaveLoadManager.Instance.GameData.EventDeadGlassFish)
        {
            vfx.gameObject.SetActive(false);
            if (SaveLoadManager.Instance.GameData.EventDeadFishVanish == false)
                deadFish.gameObject.SetActive(true);
            isFishIn = false;
        }
        else
        {
            vfx.gameObject.SetActive(true);
            aquarium.SetDialogue(aquariumHaveFishDialogue);
            isFishIn = true;
        }
    }
    IEnumerator Cor_FishJumpOut()
    {
        isFishIn = false;
        vfx.SetActive(false);
        SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_WATER_SPLASH);
        aquarium.SetDialogue(aquariumNotFishDialogue);
        waterSplashAnim.gameObject.SetActive(true);
        glassFishAnim.gameObject.SetActive(true);
        glassFishAnim.Play("jumpOut");
        yield return new WaitForSeconds(1.5f);
        fish.gameObject.SetActive(true);
        waterSplashAnim.gameObject.SetActive(false);
        SaveLoadManager.Instance.GameData.EventDeadGlassFish = true;
    }
    IEnumerator Cor_FishJumpIn()
    {
        fish.gameObject.SetActive(false);
        //Player.Instance.InCutScene = true;
        //Player.Instance.UseHandMovement = false;
        yield return new WaitUntil(() => !HandMovement.Instance.gameObject.activeInHierarchy);
        Player.Instance.graphicOfPlayer.Play("pickUp");
        yield return new WaitForSeconds(0.3f);
        glassFishAnim.Play("jumpIn");
        yield return new WaitForSeconds(1.2f);
        Player.Instance.ToIdle();
        waterSplashAnim.gameObject.SetActive(true);
        SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_WATER_SPLASH);
        yield return new WaitForSeconds(0.5f);
        waterSplashAnim.gameObject.SetActive(false);
        aquarium.SetDialogue(aquariumHaveFishDialogue);
        glassFishAnim.gameObject.SetActive(false);
        vfx.SetActive(true);     
        isFishIn = true;
        SaveLoadManager.Instance.GameData.EventDeadGlassFish = false;
        yield return new WaitUntil(() => TriggerDialogue.Instance.isEndDialogue == true);
        Player.Instance.InCutScene = false;
        Player.Instance.UseHandMovement = true;
        Player.Instance.SetIsCanGetHit();
        Player.Instance.coll.enabled = true;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("PlayerAttack") && isFishIn)
        {
            FishJumpOut();
        }
    }
}
