using AudioPlayer;
using DG.Tweening;
using Gameplay;
using GDC.Configuration;
using GDC.Enemies;
using GDC.Enums;
using GDC.Gameplay.UI;
using GDC.Managers;
using GDC.PlayerManager;
using GDC.PlayerManager.Hand;
using System.Collections;
using UnityEngine;
using UnityEngine.Rendering.Universal;

namespace GDC.Gameplay.Cutscene
{
    public class CutsceneManager : MonoBehaviour
    {
        public bool wasComeInBookStore, wasFirstMeetBook;
        public static CutsceneManager Instance { get; private set; }

        private void Awake()
        {
            //if (Instance != null)
            //{
            //    Destroy(gameObject);
            //    return;
            //}
            Instance = this;
        }

        public void ComeInBookStore(Dialogue dialogue, GameObject[] tutorialPanels)
        {
            if (wasComeInBookStore) return;
            wasComeInBookStore = true;

            StartCoroutine(Cor_ComeInBookStore(dialogue, tutorialPanels));
        }
        IEnumerator Cor_ComeInBookStore(Dialogue dialogue, GameObject[] tutorialPanels)
        {
            yield return new WaitUntil(() => GameManager.Instance.isLoadSceneComplete);
            //UIGameplay.Instance.HideBottomLeftButton(0);
            UIGameplay.Instance.LoadBottomLeftButton();
            UIGameplay.Instance.HideSliders(0);
            Player.Instance.InCutScene = true;
            Player.Instance.UseHandMovement = false;
            Player.Instance.transform.position = new Vector3(3.6f, -9.5f);
            Player.Instance.MoveTo(new Vector2(3.6f, -7.5f), 2);
            yield return new WaitForSeconds(2);
            Player.Instance.graphicOfPlayer.transform.localScale = new Vector3(1, 1, 1);
            TriggerDialogue.Instance.SetDialogue(dialogue);
            yield return new WaitUntil(() => TriggerDialogue.Instance.isEndDialogue);

            foreach (var tutorialPanel in tutorialPanels)
            {
                SoundManager.Instance.PlaySound(SoundID.SFX_SHOW_CARRY_SLOT);
                tutorialPanel.SetActive(true);
                yield return new WaitUntil(() => tutorialPanel.activeSelf == false);
            }

            Player.Instance.InCutScene = false;
            Player.Instance.UseHandMovement = false;
        }

        public void FirstMeetBook(GameObject vfx_inhale, SceneType sceneType, Animator bubbleChatAnim, Animator bookAnim)
        {
            if (wasFirstMeetBook) return;
            wasFirstMeetBook = true;

            StartCoroutine(Cor_FirstMeetBook(vfx_inhale, sceneType, bubbleChatAnim, bookAnim));
        }
        IEnumerator Cor_FirstMeetBook(GameObject vfx_inhale, SceneType sceneType, Animator bubbleChatAnim, Animator bookAnim)
        {
            Player.Instance.InCutScene = true;
            Player.Instance.UseHandMovement = false;
            Player.Instance.UseWeapon = false;
            Player.Instance.MoveTo(new Vector2(23f, -1.5f), 2);
            yield return new WaitForSeconds(2);

            Player.Instance.graphicOfPlayer.transform.localScale = new Vector3(1, 1, 1);
            yield return new WaitForSeconds(1);

            SoundManager.Instance.PlaySound(SoundID.SFX_FAIRY_DUST);
            bookAnim.Play("Blink");
            yield return new WaitForSeconds(0.5f);

            SoundManager.Instance.PlaySound(SoundID.SFX_EXCLAMATION);
            bubbleChatAnim.Play("Surprise");
            bubbleChatAnim.transform.DOScaleX(0.6f, 0.15f);
            bubbleChatAnim.transform.DOScaleY(1.4f, 0.15f).OnComplete(() =>
            {
                bubbleChatAnim.transform.DOScale(1f, 0.1f);
            });
            yield return new WaitForSeconds(1);

            bubbleChatAnim.transform.DOScaleX(0.6f, 0.1f);
            bubbleChatAnim.transform.DOScaleY(1.4f, 0.1f).OnComplete(() =>
            {
                bubbleChatAnim.transform.DOScale(0f, 0.15f);
            });
            SoundManager.Instance.PlaySound(SoundID.SFX_BOOK_PAGE);
            bookAnim.Play("Open");
            vfx_inhale.SetActive(true);
            vfx_inhale.transform.DOScale(1, 0.5f);
            SoundManager.Instance.PlaySound(SoundID.SFX_INHALE);
            yield return new WaitForSeconds(0.5f);

            SoundManager.Instance.PlayMusic(SoundID.MUSIC_SURPRISE);
            SoundManager.Instance.PlaySound(SoundID.SFX_SURPRISE);
            Player.Instance.graphicOfPlayer.Play("surprise");
            Player.Instance.transform.DOLocalMove(new Vector2(24.5f, -1.5f), 2).SetEase(Ease.Linear);
            yield return new WaitForSeconds(2);

            SoundManager.Instance.PlaySound(SoundID.SFX_INHALE);
            Player.Instance.graphicOfPlayer.transform.DOScale(0, 0.5f);
            Camera.main.DOOrthoSize(2f, 4f);
            yield return new WaitForSeconds(1.5f);

            SoundManager.Instance.PauseSFX(SoundID.SFX_INHALE);
            SoundManager.Instance.PlaySound(SoundID.SFX_BOOK_PAGE);
            bookAnim.Play("Close");
            vfx_inhale.transform.DOScale(0, 0.5f);
            yield return new WaitForSeconds(2f);

            SaveLoadManager.Instance.CacheData.isChangeCameraBound = false;
            SaveLoadManager.Instance.CacheData.playerInitPos = Vector2.zero;
            //GameManager.Instance.LoadSceneManually(sceneType, TransitionType.FADE, SoundType.BEGIN_AREA);
            GameManager.Instance.LoadSceneAsyncManually(
                        sceneType,
                        TransitionType.FADE,
                        SoundType.BEGIN_AREA,
                        cb: () =>
                        {
                            GameManager.Instance.UnloadSceneManually(
                                SaveLoadManager.Instance.GameData.CurrentSceneType,
                                cb: () => GameManager.Instance.SetInitData()
                            );
                        });
            Player.Instance.InCutScene = false;
        }

        public void FirstComeBookWorld()
        {
            if (SaveLoadManager.Instance.GameData.CutsceneFirstComeBookWorld) return;
            StartCoroutine(Cor_FirstComeBookWorld());
        }
        IEnumerator Cor_FirstComeBookWorld()
        {
            yield return new WaitUntil(() => GameManager.Instance.isLoadSceneComplete);
            //yield return new WaitUntil(() => UIGameplay.Instance != null);
            //UIGameplay.Instance.HideBottomLeftButton(0);
            UIGameplay.Instance.LoadBottomLeftButton();
            UIGameplay.Instance.HideSliders(0);

            yield return new WaitUntil(() => Player.Instance != null);
            Player.Instance.InCutScene = true;
            Player.Instance.UseWeapon = false;
            Player.Instance.UseHandMovement = false;
            Player.Instance.LieDown();
            yield return new WaitForSeconds(3);

            SoundManager.Instance.PlaySound(SoundID.SFX_JUMP);
            Player.Instance.StandUpSurprise();
            yield return new WaitForSeconds(2.5f);

            SoundManager.Instance.PlaySound(SoundID.SFX_SURPRISE);
            Player.Instance.graphicOfPlayer.Play("surprise");
            for (int i=0; i<3; i++)
            {
                Camera.main.DOOrthoSize(3.7f, 0.3f).OnComplete(()=>
                {
                    Camera.main.DOOrthoSize(4f, 0.3f);
                });
                yield return new WaitForSeconds(0.6f);
            }
            yield return new WaitForSeconds(0.5f);
            Player.Instance.graphicOfPlayer.Play("idle(noWeapon)");
            Player.Instance.InCutScene = false;
            UIGameplay.Instance.ShowSliders();
            SaveLoadManager.Instance.GameData.CutsceneFirstComeBookWorld = true;
        }

        public void FirstMeetMysteriousMan(NPC mysteriousMan, Dialogue dialogue1, Dialogue dialogue2, Dialogue dialogue3, Following mysteriousFollowing)
        {
            if (SaveLoadManager.Instance.GameData.CutsceneFirstMeetMysteriousMan) return;
            StartCoroutine(Cor_FirstMeetMysteriousMan(mysteriousMan, dialogue1, dialogue2, dialogue3, mysteriousFollowing));
        }
        IEnumerator Cor_FirstMeetMysteriousMan(NPC mysteriousMan, Dialogue dialogue1, Dialogue dialogue2, Dialogue dialogue3, Following mysteriousFollowing)
        {
            yield return new WaitUntil(() => Player.Instance != null);
            Player player = Player.Instance;
            player.InCutScene = true;
            player.UseWeapon = false;
            Player.Instance.UseHandMovement = false;
            Player.Instance.ToIdle();
            yield return new WaitUntil(() => !HandMovement.Instance.gameObject.activeInHierarchy);
            Player.Instance.graphicOfPlayer.transform.localScale = new Vector3(1, 1, 1);
            mysteriousMan.anim.Play("surprise");
            SoundManager.Instance.PlaySound(SoundID.SFX_JUMP);
            yield return mysteriousMan.graphicTrans.DOJump(mysteriousMan.transform.position, 1f, 1, 0.6f).WaitForCompletion();
            
            TriggerDialogue.Instance.SetDialogue(dialogue1);
            yield return new WaitUntil(() => TriggerDialogue.Instance.isEndDialogue);

            mysteriousMan.MoveToPos(Player.Instance.transform.position + Vector3.right * 2, 2);
            yield return new WaitForSeconds(1.25f);
            SoundManager.Instance.PlaySound(SoundID.SFX_SURPRISE);
            Player.Instance.graphicOfPlayer.Play("surprise");
            yield return new WaitForSeconds(1);

            mysteriousMan.anim.Play("talk");
            TriggerDialogue.Instance.SetDialogue(dialogue2);
            yield return new WaitUntil(() => TriggerDialogue.Instance.isEndDialogue);

            mysteriousMan.anim.Play("idle");
            Player.Instance.graphicOfPlayer.Play("pickUp");
            yield return new WaitForSeconds(1.5f);

            Player.Instance.ToIdle();
            mysteriousMan.anim.Play("talk");
            TriggerDialogue.Instance.SetDialogue(dialogue3);
            yield return new WaitUntil(() => TriggerDialogue.Instance.isEndDialogue);

            mysteriousFollowing.isFollowing = true;
            SaveLoadManager.Instance.GameData.isMysteriousManFollowing = true;
            SaveLoadManager.Instance.GameData.CutsceneFirstMeetMysteriousMan = true;
            Player.Instance.InCutScene = false;
        }

        public void FirstMeetSlime(NPC mysteriousMan, Dialogue dialogue1, Dialogue dialogue2, Dialogue dialogue3, Following mysteriousFollowing, SO_Item so_item, GameObject tutorialPanel, GameObject wall)
        {
            if (SaveLoadManager.Instance.GameData.CutsceneFirstBattleSlime) return;
            StartCoroutine(Cor_FirstMeetSlime(mysteriousMan, dialogue1, dialogue2, dialogue3, mysteriousFollowing, so_item, tutorialPanel, wall));
        }
        IEnumerator Cor_FirstMeetSlime(NPC mysteriousMan, Dialogue dialogue1, Dialogue dialogue2, Dialogue dialogue3, Following mysteriousFollowing, SO_Item so_item, GameObject tutorialPanel, GameObject wall)
        {
            Player.Instance.InCutScene = true;
            Player.Instance.ToIdle();
            mysteriousMan.anim.Play("surprise");
            mysteriousFollowing.isFollowing = false;
            wall.SetActive(true);
            SoundManager.Instance.PlaySound(SoundID.SFX_JUMP);
            yield return mysteriousMan.graphicTrans.DOJump(mysteriousMan.transform.position, 1f, 1, 0.6f).WaitForCompletion();

            TriggerDialogue.Instance.SetDialogue(dialogue1);
            yield return new WaitUntil(() => TriggerDialogue.Instance.isEndDialogue);

            SoundManager.Instance.PlaySound(SoundID.SFX_START_UI);
            CameraController.Instance.isFollowPlayer = false;
            CameraController.Instance.transform.DOMoveX(27, 1);
            yield return new WaitForSeconds(2);

            SoundManager.Instance.PlaySound(SoundID.SFX_END_UI);
            CameraController.Instance.isFollowPlayer = true;
            Player.Instance.graphicOfPlayer.transform.localScale = new Vector3(-1, 1, 1);
            mysteriousMan.anim.Play("talk");
            TriggerDialogue.Instance.SetDialogue(dialogue2);
            yield return new WaitUntil(() => TriggerDialogue.Instance.isEndDialogue);

            mysteriousMan.anim.Play("give");
            yield return new WaitForSeconds(0.5f);
            //SoundManager.Instance.PlaySound(SoundID.SFX_LOOT_SPECIAL);
            Player.Instance.ObtainItem(so_item);
            SaveLoadManager.Instance.GameData.AddItem(so_item);
            GetItemDisplayManager.Instance.ShowItem(so_item.sprite, so_item.itemName);
            yield return new WaitForSeconds(3.5f);

            Player.Instance.UseWeapon = true;
            SaveLoadManager.Instance.GameData.isPlayerHaveWeapon = true;
            SaveLoadManager.Instance.GameData.isPlayerHaveMap = true;
            SaveLoadManager.Instance.GameData.TargetSceneType = SceneType.BEGIN_AREA_3;
            UIGameplay.Instance.LoadBottomLeftButton();
            SoundManager.Instance.PlaySound(SoundID.SFX_SHOW_CARRY_SLOT);
            tutorialPanel.SetActive(true);
            yield return new WaitUntil(() => tutorialPanel.activeSelf == false);

            mysteriousMan.anim.Play("talk");
            TriggerDialogue.Instance.SetDialogue(dialogue3);
            yield return new WaitUntil(() => TriggerDialogue.Instance.isEndDialogue);
            mysteriousMan.anim.Play("idle");
            Player.Instance.UseHandMovement = true;
            Player.Instance.UseWeapon = true;
            Player.Instance.InCutScene = false;
        }

        public void RunFromFirstSlime(NPC mysteriousMan, Dialogue dialogue)
        {
            StartCoroutine(Cor_RunFromFirstSlime(mysteriousMan, dialogue));
        }
        IEnumerator Cor_RunFromFirstSlime(NPC mysteriousMan, Dialogue dialogue)
        {
            Player.Instance.InCutScene = true;
            Player.Instance.ToIdle();

            Following follow = null;
            bool isFollowing = false;
            if (mysteriousMan != null)
            {
                follow = mysteriousMan.gameObject.GetComponent<Following>();
                if (follow != null)
                {
                    isFollowing = follow.isFollowing;
                    follow.isFollowing = false;
                }

                SoundManager.Instance.PlaySound(SoundID.SFX_JUMP);
                yield return mysteriousMan.graphicTrans.DOJump(mysteriousMan.transform.position, 1f, 1, 0.6f).WaitForCompletion();
                mysteriousMan.anim.Play("talk");
            }
            TriggerDialogue.Instance.SetDialogue(dialogue);
            yield return new WaitUntil(() => TriggerDialogue.Instance.isEndDialogue);
            Player.Instance.InCutScene = false;
            if (follow != null)
            {
                follow.isFollowing = isFollowing;
            }
        }

        public void FirstBattle(EnemyRoom enemyRoom, NPC mysteriousMan, Following mysteriousFollow, Dialogue dialogue, GameObject wall)
        {
            if (SaveLoadManager.Instance.GameData.CutsceneFirstBattleSlime) return;
            StartCoroutine(Cor_FirstBattle(enemyRoom, mysteriousMan, mysteriousFollow, dialogue, wall));
        }
        IEnumerator Cor_FirstBattle(EnemyRoom enemyRoom, NPC mysteriousMan, Following mysteriousFollow, Dialogue dialogue, GameObject wall)
        {
            SoundManager.Instance.PlayMusicWithIntro(SoundID.MUSIC_MINIBOSS_BEGIN, SoundID.MUSIC_MINIBOSS);
            enemyRoom.gameObject.SetActive(true);
            enemyRoom.StartEnemyRoom();
            CameraController.Instance.coll.enabled = true;
            yield return new WaitUntil(() => enemyRoom.gameObject.activeSelf == false);

            SoundManager.Instance.PlaySound(SoundID.SFX_LOOT_SPECIAL);
            SoundManager.Instance.PlayMusic(SoundID.MUSIC_BIRD_MEADOW);
            Player.Instance.InCutScene = true;
            Player.Instance.UseHandMovement = false;
            yield return new WaitForSeconds(0.5f);

            Player.Instance.ToIdle();
            Player.Instance.graphicOfPlayer.transform.localScale = new Vector3(-1, 1, 1);
            mysteriousMan.MoveToPos(Player.Instance.transform.position - Vector3.right * 2, 2);
            yield return new WaitForSeconds(2f);

            mysteriousMan.anim.Play("talk");
            TriggerDialogue.Instance.SetDialogue(dialogue);
            yield return new WaitUntil(() => TriggerDialogue.Instance.isEndDialogue);
            
            mysteriousMan.anim.Play("idle");
            mysteriousFollow.isFollowing = true;
            Player.Instance.UseHandMovement = true;
            Player.Instance.InCutScene = false;
            wall.SetActive(false);

            SaveLoadManager.Instance.GameData.CutsceneFirstBattleSlime = true;
        }

        public void MeetHouse(NPC mysteriousMan, Dialogue dialogue, Dialogue dialogue2, GameObject walls)
        {
            if (SaveLoadManager.Instance.GameData.CutsceneMeetHouse) return;
            StartCoroutine(Cor_MeetHouse(mysteriousMan, dialogue, dialogue2, walls));
        }
        IEnumerator Cor_MeetHouse(NPC mysteriousMan, Dialogue dialogue, Dialogue dialogue2, GameObject walls)
        {
            Player.Instance.InCutScene = true;
            Player.Instance.ToIdle();
            mysteriousMan.anim.Play("talk");
            TriggerDialogue.Instance.SetDialogue(dialogue);
            yield return new WaitUntil(() => TriggerDialogue.Instance.isEndDialogue);
            mysteriousMan.anim.Play("idle");

            SoundManager.Instance.PlaySound(SoundID.SFX_START_UI);
            CameraController.Instance.isFollowPlayer = false;
            CameraController.Instance.transform.DOMove(new Vector3(14, 0, -10), 1);
            yield return new WaitForSeconds(2);

            SoundManager.Instance.PlaySound(SoundID.SFX_END_UI);
            CameraController.Instance.isFollowPlayer = true;
            yield return new WaitForSeconds(0.7f);

            mysteriousMan.anim.Play("talk");
            TriggerDialogue.Instance.SetDialogue(dialogue2);
            yield return new WaitUntil(() => TriggerDialogue.Instance.isEndDialogue);
            mysteriousMan.anim.Play("idle");

            Player.Instance.InCutScene = false;
            walls.SetActive(true);
            SaveLoadManager.Instance.GameData.CutsceneMeetHouse = true;
        }

        public void InHouse(NPC mysteriousMan, Following mysteriousFollow, SpriteRenderer[] storyBGs, SpriteRenderer bossShadow, Dialogue[] dialogues, CircleCollider2D coll)
        {
            if (SaveLoadManager.Instance.GameData.CutsceneInHouse) return;
            StartCoroutine(Cor_InHouse(mysteriousMan, mysteriousFollow, storyBGs, bossShadow, dialogues, coll));
        }
        IEnumerator Cor_InHouse(NPC mysteriousMan, Following mysteriousFollow, SpriteRenderer[] storyBGs, SpriteRenderer bossShadow, Dialogue[] dialogues, CircleCollider2D coll)
        {
            yield return new WaitUntil(() => GameManager.Instance.isLoadSceneComplete);
            Player.Instance.InCutScene = true;           
            Player.Instance.UseHandMovement = false;
            coll.enabled = false;
            //mysteriousFollow.isFollowing = false;
            mysteriousFollow.isNotLookAtTarget = true;
            mysteriousMan.transform.position = Player.Instance.transform.position + Vector3.left;
            yield return new WaitForSeconds(1);
            mysteriousFollow.isFollowing = false;

            mysteriousMan.anim.Play("talk");
            TriggerDialogue.Instance.SetDialogue(dialogues[0]);
            yield return new WaitUntil(() => TriggerDialogue.Instance.isEndDialogue);
            mysteriousMan.anim.Play("idle");

            CameraController.Instance.isFollowPlayer = false;
            CameraController.Instance.transform.DOMove(new Vector3(-2.5f, 1, -10), 3);
            Player.Instance.MoveTo(new Vector2(-0.5f, 0.2f), 2);
            mysteriousMan.MoveToPos(new Vector3(-4.5f, -1.5f, 0), 1.3f);
            yield return new WaitForSeconds(1.5f);
            mysteriousMan.MoveToPos(new Vector3(-4.5f, 0.2f, 0), 0.8f);
            yield return new WaitForSeconds(0.8f);

            Player.Instance.graphicOfPlayer.transform.localScale = new Vector3(-1, 1, 1);
            mysteriousMan.SetDirect(Player.Instance.transform.position);        
            yield return new WaitForSeconds(1.5f);

            mysteriousMan.anim.Play("talk");
            TriggerDialogue.Instance.SetDialogue(dialogues[1]);
            yield return new WaitUntil(() => TriggerDialogue.Instance.isEndDialogue);
            mysteriousMan.anim.Play("idle");

            Player.Instance.graphicOfPlayer.Play("pickUp");
            yield return new WaitForSeconds(1.5f);

            mysteriousMan.anim.Play("talk");
            TriggerDialogue.Instance.SetDialogue(dialogues[2]);
            yield return new WaitUntil(() => TriggerDialogue.Instance.isEndDialogue);
            mysteriousMan.anim.Play("idle");

            SoundManager.Instance.PlayMusic(SoundID.MUSIC_SCARY_STORY);
            foreach (var storyBG in storyBGs)
            {
                storyBG.gameObject.SetActive(true);
                storyBG.transform.position = new Vector2(storyBG.transform.position.x, CameraController.Instance.transform.position.y);
                storyBG.color = Color.clear;
                storyBG.DOColor(Color.white, 2);
            }
            yield return new WaitForSeconds(2);

            mysteriousMan.anim.Play("talk");
            TriggerDialogue.Instance.SetDialogue(dialogues[3]);
            yield return new WaitForSeconds(3);
            bossShadow.gameObject.SetActive(true);
            bossShadow.transform.position = new Vector2(CameraController.Instance.transform.position.x, CameraController.Instance.transform.position.y + 2);
            bossShadow.color = Color.clear;
            bossShadow.DOColor(Color.white, 2).OnComplete(()=>
            {
                bossShadow.DOFade(0.6f, 1).SetLoops(-1, LoopType.Yoyo);
            });
            yield return new WaitUntil(() => TriggerDialogue.Instance.isEndDialogue);
            mysteriousMan.anim.Play("idle");

            SoundManager.Instance.PlayMusic(SoundID.MUSIC_BEGIN_AREA, 1, 1);
            foreach (var storyBG in storyBGs)
            {
                storyBG.DOColor(Color.clear, 2);
            }
            DOTween.Kill(bossShadow);
            bossShadow.DOColor(Color.clear, 2);
            yield return new WaitForSeconds(2);

            mysteriousMan.anim.Play("talk");
            TriggerDialogue.Instance.SetDialogue(dialogues[4]);
            yield return new WaitUntil(() => TriggerDialogue.Instance.isEndDialogue);
            mysteriousMan.anim.Play("idle");

            CameraController.Instance.isFollowPlayer = true;
            Player.Instance.InCutScene = false;
            Player.Instance.UseHandMovement = true;
            coll.enabled = true;
            SaveLoadManager.Instance.GameData.TargetSceneType = SceneType.BEGIN_AREA_4;
            SaveLoadManager.Instance.GameData.CutsceneInHouse = true;
            SaveLoadManager.Instance.GameData.isMysteriousManFollowing = false;
        }

        public void LearnSpell(Dialogue[] dialogues, Animator bubbleChatAnim, Animator book, Transform dot, GameObject vfx, GameObject tutorialPanel)
        {
            if (SaveLoadManager.Instance.GameData.CutsceneLearnSpell) return;
            StartCoroutine(Cor_LearnSpell(dialogues, bubbleChatAnim, book, dot, vfx, tutorialPanel));
        }    
        IEnumerator Cor_LearnSpell(Dialogue[] dialogues, Animator bubbleChatAnim, Animator book, Transform dot, GameObject vfx, GameObject tutorialPanel)
        {
            Player.Instance.InCutScene = true;
            Player.Instance.UseHandMovement = false;
            Player.Instance.ToIdle();
            TriggerDialogue.Instance.SetDialogue(dialogues[0]);
            yield return new WaitUntil(() => TriggerDialogue.Instance.isEndDialogue);

            SoundManager.Instance.PlaySound(SoundID.SFX_EXCLAMATION);
            bubbleChatAnim.transform.position = Player.Instance.transform.position + new Vector3(0, 1.5f, 0);
            bubbleChatAnim.Play("Surprise");
            bubbleChatAnim.transform.DOScaleX(0.6f, 0.15f);
            bubbleChatAnim.transform.DOScaleY(1.4f, 0.15f).OnComplete(() =>
            {
                bubbleChatAnim.transform.DOScale(1f, 0.1f);
            });
            yield return new WaitForSeconds(1);

            bubbleChatAnim.transform.DOScaleX(0.6f, 0.1f);
            bubbleChatAnim.transform.DOScaleY(1.4f, 0.1f).OnComplete(() =>
            {
                bubbleChatAnim.transform.DOScale(0f, 0.15f);
            });

            yield return new WaitForSeconds(0.5f);
            Player.Instance.graphicOfPlayer.transform.localScale = new Vector3(-1, 1, 1);
            yield return new WaitForSeconds(0.2f);
            Player.Instance.graphicOfPlayer.transform.localScale = new Vector3(1, 1, 1);
            yield return new WaitForSeconds(0.2f);
            Player.Instance.graphicOfPlayer.transform.localScale = new Vector3(-1, 1, 1);
            yield return new WaitForSeconds(0.2f);
            Player.Instance.graphicOfPlayer.transform.localScale = new Vector3(1, 1, 1);
            yield return new WaitForSeconds(1f);

            TriggerDialogue.Instance.SetDialogue(dialogues[1]);
            yield return new WaitUntil(() => TriggerDialogue.Instance.isEndDialogue);

            SoundManager.Instance.PlaySound(SoundID.SFX_FAIRY_DUST);
            dot.gameObject.SetActive(true);
            dot.position = Player.Instance.transform.position + new Vector3(0.5f, 0, 0);
            dot.DOMove(Player.Instance.transform.position + new Vector3(1, -0.5f, 0), 1);
            yield return new WaitForSeconds(1.5f);
            SoundManager.Instance.PlaySound(SoundID.SFX_FAIRY_DUST);
            dot.DOMove(Player.Instance.transform.position + new Vector3(1.2f, 1f, 0), 1);
            yield return new WaitForSeconds(1.5f);
            SoundManager.Instance.PlaySound(SoundID.SFX_FAIRY_DUST);
            dot.DOMove(Player.Instance.transform.position + new Vector3(1.5f, 0.5f, 0), 1);
            yield return new WaitForSeconds(1.5f);

            SoundManager.Instance.PlaySound(SoundID.SFX_APPEAR);
            Instantiate(vfx, dot.transform.position, Quaternion.identity);
            SoundManager.Instance.PlaySound(SoundID.SFX_SURPRISE);
            Player.Instance.graphicOfPlayer.Play("surprise");
            book.gameObject.SetActive(true);
            book.transform.position = dot.transform.position;
            dot.gameObject.SetActive(false);
            SoundManager.Instance.PlaySound(SoundID.SFX_BOOK_PAGE);
            book.Play("Open");
            yield return new WaitForSeconds(0.55f);
            book.Play("Fly");
            yield return new WaitForSeconds(1);
            Player.Instance.ToIdle();

            TriggerDialogue.Instance.SetDialogue(dialogues[2]);
            yield return new WaitUntil(() => TriggerDialogue.Instance.isEndDialogue);

            SoundManager.Instance.PlaySound(SoundID.SFX_START_UI);
            CameraController.Instance.isFollowPlayer = false;
            CameraController.Instance.transform.DOMove(new Vector3(10, 20, -10), 1);
            yield return new WaitForSeconds(2);
            SoundManager.Instance.PlaySound(SoundID.SFX_END_UI);
            CameraController.Instance.isFollowPlayer = true;
            yield return new WaitForSeconds(0.7f);

            TriggerDialogue.Instance.SetDialogue(dialogues[3]);
            yield return new WaitUntil(() => TriggerDialogue.Instance.isEndDialogue);

            SoundManager.Instance.PlaySound(SoundID.SFX_SHOW_CARRY_SLOT);
            tutorialPanel.SetActive(true);
            yield return new WaitUntil(() => tutorialPanel.activeSelf == false);

            SoundManager.Instance.PlaySound(SoundID.SFX_BOOK_PAGE);
            book.Play("Close");
            yield return new WaitForSeconds(0.55f);
            SoundManager.Instance.PlaySound(SoundID.SFX_APPEAR);
            Instantiate(vfx, dot.transform.position, Quaternion.identity);
            SoundManager.Instance.PlaySound(SoundID.SFX_SURPRISE);
            Player.Instance.graphicOfPlayer.Play("surprise");
            dot.gameObject.SetActive(true);
            book.gameObject.SetActive(false);
            yield return new WaitForSeconds(1);

            SoundManager.Instance.PlaySound(SoundID.SFX_FAIRY_DUST);
            dot.DOMove(Player.Instance.transform.position + new Vector3(0, 0.5f, 0), 1);
            yield return new WaitForSeconds(1f);
            dot.GetComponent<SpriteRenderer>().DOColor(Color.clear, 0.25f).SetEase(Ease.Linear);            
            yield return new WaitForSeconds(0.5f);
            dot.gameObject.SetActive(false);
            Player.Instance.ToIdle();
            Player.Instance.InCutScene = false;
            Player.Instance.UseHandMovement = true;
            SaveLoadManager.Instance.GameData.isPlayerHaveSpell = true;
            SaveLoadManager.Instance.GameData.CutsceneLearnSpell = true;
            UIGameplay.Instance.LoadBottomLeftButton();
        }

        public void SeeBoss1Gate(Dialogue[] dialogues, Animator bubbleChatAnim, Animator book, Transform dot, GameObject vfx)
        {
            if (SaveLoadManager.Instance.GameData.CutsceneSeeBoss1Gate) return;
            StartCoroutine(Cor_SeeBoss1Gate(dialogues, bubbleChatAnim, book, dot, vfx));
        }    
        IEnumerator Cor_SeeBoss1Gate(Dialogue[] dialogues, Animator bubbleChatAnim, Animator book, Transform dot, GameObject vfx)
        {
            Player.Instance.InCutScene = true;
            Player.Instance.UseHandMovement = false;
            Player.Instance.ToIdle();
            Player.Instance.graphicOfPlayer.transform.localScale = new Vector3(-1, 1, 1);

            SoundManager.Instance.PlaySound(SoundID.SFX_FAIRY_DUST);
            dot.gameObject.SetActive(true);
            dot.position = Player.Instance.transform.position + new Vector3(0.5f, 0, 0);
            dot.DOMove(Player.Instance.transform.position + new Vector3(-1.5f, 0.5f, 0), 1);
            SoundManager.Instance.PlaySound(SoundID.SFX_EXCLAMATION);
            bubbleChatAnim.transform.position = Player.Instance.transform.position + new Vector3(0, 1.5f, 0);
            bubbleChatAnim.Play("Surprise");
            bubbleChatAnim.transform.DOScaleX(0.6f, 0.15f);
            bubbleChatAnim.transform.DOScaleY(1.4f, 0.15f).OnComplete(() =>
            {
                bubbleChatAnim.transform.DOScale(1f, 0.1f);
            });
            yield return new WaitForSeconds(1);

            bubbleChatAnim.transform.DOScaleX(0.6f, 0.1f);
            bubbleChatAnim.transform.DOScaleY(1.4f, 0.1f).OnComplete(() =>
            {
                bubbleChatAnim.transform.DOScale(0f, 0.15f);
            });

            SoundManager.Instance.PlaySound(SoundID.SFX_APPEAR);
            Instantiate(vfx, dot.transform.position, Quaternion.identity);
            book.gameObject.SetActive(true);
            book.transform.position = dot.transform.position;
            dot.gameObject.SetActive(false);
            SoundManager.Instance.PlaySound(SoundID.SFX_BOOK_PAGE);
            book.Play("Open");
            yield return new WaitForSeconds(0.55f);
            book.Play("Fly");
            yield return new WaitForSeconds(1);
            TriggerDialogue.Instance.SetDialogue(dialogues[0]);
            yield return new WaitUntil(() => TriggerDialogue.Instance.isEndDialogue);

            SoundManager.Instance.PlaySound(SoundID.SFX_START_UI);
            CameraController.Instance.isFollowPlayer = false;
            CameraController.Instance.transform.DOMove(new Vector3(8, -27, -10), 1);
            yield return new WaitForSeconds(2);
            SoundManager.Instance.PlaySound(SoundID.SFX_END_UI);
            CameraController.Instance.isFollowPlayer = true;
            yield return new WaitForSeconds(0.7f);

            TriggerDialogue.Instance.SetDialogue(dialogues[1]);
            yield return new WaitUntil(() => TriggerDialogue.Instance.isEndDialogue);

            SoundManager.Instance.PlaySound(SoundID.SFX_BOOK_PAGE);
            book.Play("Close");
            yield return new WaitForSeconds(0.55f);
            SoundManager.Instance.PlaySound(SoundID.SFX_APPEAR);
            Instantiate(vfx, dot.transform.position, Quaternion.identity);
            dot.gameObject.SetActive(true);
            book.gameObject.SetActive(false);
            yield return new WaitForSeconds(1);

            SoundManager.Instance.PlaySound(SoundID.SFX_FAIRY_DUST);
            dot.DOMove(Player.Instance.transform.position + new Vector3(0, 0.5f, 0), 1);
            yield return new WaitForSeconds(1f);
            dot.GetComponent<SpriteRenderer>().DOColor(Color.clear, 0.25f).SetEase(Ease.Linear);
            yield return new WaitForSeconds(0.5f);
            dot.gameObject.SetActive(false);
            Player.Instance.InCutScene = false;
            Player.Instance.UseHandMovement = true;
            SaveLoadManager.Instance.GameData.CutsceneSeeBoss1Gate = true;
            SaveLoadManager.Instance.GameData.TargetSceneType = SceneType.AREA1_4;
        }

        public void SeeDashRing(Dialogue[] dialogues, Animator bubbleChatAnim, Animator book, Transform dot, GameObject vfx, Boxy boxy, EnemyRoom enemyRoom)
        {
            if (SaveLoadManager.Instance.GameData.CutsceneSeeDashRing) return;
            StartCoroutine(Cor_SeeDashRing(dialogues, bubbleChatAnim, book, dot, vfx, boxy, enemyRoom));
        }
        IEnumerator Cor_SeeDashRing(Dialogue[] dialogues, Animator bubbleChatAnim, Animator book, Transform dot, GameObject vfx, Boxy boxy, EnemyRoom enemyRoom)
        {
            Player.Instance.InCutScene = true;
            Player.Instance.UseHandMovement = false;
            Player.Instance.ToIdle();
            SoundManager.Instance.PlaySound(SoundID.SFX_EXCLAMATION);
            bubbleChatAnim.transform.position = Player.Instance.transform.position + new Vector3(0, 1.5f, 0);
            bubbleChatAnim.Play("Surprise");
            bubbleChatAnim.transform.DOScaleX(0.6f, 0.15f);
            bubbleChatAnim.transform.DOScaleY(1.4f, 0.15f).OnComplete(() =>
            {
                bubbleChatAnim.transform.DOScale(1f, 0.1f);
            });
            yield return new WaitForSeconds(1);

            bubbleChatAnim.transform.DOScaleX(0.6f, 0.1f);
            bubbleChatAnim.transform.DOScaleY(1.4f, 0.1f).OnComplete(() =>
            {
                bubbleChatAnim.transform.DOScale(0f, 0.15f);
            });
            Player.Instance.MoveTo(new Vector2(18f, -2.3f), 2);
            //Player.Instance.graphicOfPlayer.transform.localScale = new Vector3(-1, 1, 1);
            yield return new WaitForSeconds(2);

            SoundManager.Instance.PlaySound(SoundID.SFX_FAIRY_DUST);
            dot.gameObject.SetActive(true);
            dot.position = Player.Instance.transform.position + new Vector3(0.5f, 0, 0);
            dot.DOMove(Player.Instance.transform.position + new Vector3(-1.5f, 0.5f, 0), 1);
            yield return new WaitForSeconds(1);

            SoundManager.Instance.PlaySound(SoundID.SFX_APPEAR);
            Instantiate(vfx, dot.transform.position, Quaternion.identity);
            book.gameObject.SetActive(true);
            book.transform.position = dot.transform.position;
            dot.gameObject.SetActive(false);
            SoundManager.Instance.PlaySound(SoundID.SFX_BOOK_PAGE);
            book.Play("Open");
            yield return new WaitForSeconds(0.55f);
            book.Play("Fly");
            yield return new WaitForSeconds(0.7f);
            TriggerDialogue.Instance.SetDialogue(dialogues[0]);
            yield return new WaitUntil(() => TriggerDialogue.Instance.isEndDialogue);

            SoundManager.Instance.PlaySound(SoundID.SFX_START_UI);
            CameraController.Instance.isFollowPlayer = false;
            CameraController.Instance.transform.DOMove(new Vector3(14.5f, -2, -10), 1);
            yield return new WaitForSeconds(2);
            //CameraController.Instance.isFollowPlayer = true;
            //yield return new WaitForSeconds(0.7f);

            TriggerDialogue.Instance.SetDialogue(dialogues[1]);
            yield return new WaitUntil(() => TriggerDialogue.Instance.isEndDialogue);

            boxy.JumpToArea();
            yield return new WaitForSeconds(1.5f);
            SoundManager.Instance.PlaySound(SoundID.SFX_SURPRISE);
            Player.Instance.graphicOfPlayer.Play("surprise");
            SoundManager.Instance.PlaySound(SoundID.SFX_BOOK_PAGE);
            book.Play("Close");
            yield return new WaitForSeconds(0.55f);
            SoundManager.Instance.PlaySound(SoundID.SFX_APPEAR);
            Instantiate(vfx, dot.transform.position, Quaternion.identity);
            dot.gameObject.SetActive(true);
            book.gameObject.SetActive(false);
            yield return new WaitForSeconds(1);
            SoundManager.Instance.PlaySound(SoundID.SFX_FAIRY_DUST);
            dot.DOMove(Player.Instance.transform.position + new Vector3(0, 0.5f, 0), 0.5f);
            yield return new WaitForSeconds(0.5f);
            dot.GetComponent<SpriteRenderer>().DOColor(Color.clear, 0.25f).SetEase(Ease.Linear);
            yield return new WaitForSeconds(0.5f);
            dot.gameObject.SetActive(false);
            yield return new WaitForSeconds(1.5f);

            enemyRoom.gameObject.SetActive(true);
            enemyRoom.StartEnemyRoom();
            CameraController.Instance.isFollowPlayer = true;
            Player.Instance.InCutScene = false;
            Player.Instance.UseHandMovement = true;
            SaveLoadManager.Instance.GameData.CutsceneSeeDashRing = true;
            SoundManager.Instance.PlayMusicWithIntro(SoundID.MUSIC_MINIBOSS_BEGIN, SoundID.MUSIC_MINIBOSS);
        }

        public void GetDashRing(Dialogue[] dialogues, Animator book, Transform dot, GameObject vfx, GameObject vfxDirt, Animator acrobanditAnim, Transform acrobandit, GameObject tutorialPanel)
        {
            StartCoroutine(Cor_GetDashRing(dialogues, book, dot, vfx, vfxDirt, acrobanditAnim, acrobandit, tutorialPanel));
        }
        IEnumerator Cor_GetDashRing(Dialogue[] dialogues, Animator book, Transform dot, GameObject vfx, GameObject vfxDirt, Animator acrobanditAnim, Transform acrobandit, GameObject tutorialPanel)
        {
            yield return new WaitUntil(() => Player.Instance.InCutScene == false);
            Player.Instance.InCutScene = true;
            Player.Instance.UseHandMovement = false;

            tutorialPanel.SetActive(true);
            yield return new WaitUntil(() => tutorialPanel.activeSelf == false);

            Player.Instance.MoveTo(new Vector2(19.5f, -2.5f), 2);
            yield return new WaitForSeconds(2);

            Player.Instance.graphicOfPlayer.transform.localScale = new Vector3(1, 1, 1);
            dot.gameObject.SetActive(true);
            dot.GetComponent<SpriteRenderer>().color = Color.white;
            SoundManager.Instance.PlaySound(SoundID.SFX_FAIRY_DUST);
            dot.position = Player.Instance.transform.position + new Vector3(0, 0.5f, 0);
            dot.DOMove(Player.Instance.transform.position + new Vector3(1.5f, 0.5f, 0), 0.7f);
            yield return new WaitForSeconds(0.7f);

            SoundManager.Instance.PlaySound(SoundID.SFX_APPEAR);
            Instantiate(vfx, dot.transform.position, Quaternion.identity);
            book.gameObject.SetActive(true);
            book.transform.position = dot.transform.position;
            dot.gameObject.SetActive(false);
            SoundManager.Instance.PlaySound(SoundID.SFX_BOOK_PAGE);
            book.Play("Open");
            yield return new WaitForSeconds(0.55f);
            book.Play("Fly");
            yield return new WaitForSeconds(0.7f);

            acrobandit.transform.position = new Vector3(15, 0, 0);

            if (SaveLoadManager.Instance.GameData.CutsceneSeeBoss1Gate)
            {
                TriggerDialogue.Instance.SetDialogue(dialogues[0]);
            }
            else
            {
                TriggerDialogue.Instance.SetDialogue(dialogues[1]);
            }
            yield return new WaitForSeconds(3);
            acrobandit.gameObject.SetActive(true);
            acrobanditAnim.Play("appear");
            yield return new WaitForSeconds(2f);
            acrobanditAnim.Play("hide");
            yield return new WaitForSeconds(0.83f);
            acrobandit.gameObject.SetActive(false);
            yield return new WaitForSeconds(1.5f);
            acrobandit.gameObject.SetActive(true);
            acrobandit.transform.localPosition = new Vector3(-5.25f, 1.5f, 0);
            bool isAcrobanditAppear = false;
            acrobanditAnim.Play("appear");
            //acrobanditAnim.GetComponent<SpriteRenderer>().enabled = true;
            yield return new WaitForSeconds(0.5f);
            isAcrobanditAppear = true;
            yield return new WaitUntil(() => TriggerDialogue.Instance.isEndDialogue && isAcrobanditAppear);

            SoundManager.Instance.PlaySound(SoundID.SFX_DIG);
            acrobanditAnim.Play("jumpout");
            acrobanditAnim.speed = 2.5f;
            vfxDirt.gameObject.SetActive(true);
            yield return new WaitForSeconds(0.75f);

            acrobanditAnim.Play("move");
            acrobandit.transform.localScale = new Vector3(-1, 1, 1);
            acrobandit.DOMove(book.transform.position, 0.75f).SetEase(Ease.Linear).OnComplete(()=>
            {
                SoundManager.Instance.PlaySound(SoundID.SFX_SURPRISE);
                Player.Instance.graphicOfPlayer.Play("surprise");
                SaveLoadManager.Instance.GameData.isPlayerHaveSpell = false;
                UIGameplay.Instance.LoadBottomLeftButton();
                book.transform.SetParent(acrobandit);
                book.transform.localPosition = new Vector3(0, 0.5f, 0);
                acrobandit.DOMove(new Vector3(23.5f, -2.5f, 0), 0.75f).SetEase(Ease.Linear).OnComplete(()=>
                {
                    acrobandit.DOMove(new Vector3(40, -2.5f, 0), 3f).SetEase(Ease.Linear);
                });
            });
            yield return new WaitForSeconds(0.7f);
            for (int i = 0; i < 2; i++)
            {
                Camera.main.DOOrthoSize(3.6f, 0.25f).OnComplete(() =>
                {
                    Camera.main.DOOrthoSize(3.9f, 0.25f);
                });
                yield return new WaitForSeconds(0.5f);
            }
            Camera.main.DOOrthoSize(4f, 0.25f);

            yield return new WaitForSeconds(1);

            TriggerDialogue.Instance.SetDialogue(dialogues[2]);
            yield return new WaitUntil(() => TriggerDialogue.Instance.isEndDialogue);

            SoundManager.Instance.PlaySound(SoundID.SFX_START_UI);
            CameraController.Instance.isFollowPlayer = false;
            CameraController.Instance.transform.DOMove(new Vector3(37, 12, -10), 1);
            yield return new WaitForSeconds(0.8f);
            acrobandit.transform.localPosition = new Vector3(17, 7, 0);
            acrobandit.DOLocalMove(new Vector3(17, 19, 0), 2f).SetEase(Ease.Linear);
            yield return new WaitForSeconds(1.4f);
            SoundManager.Instance.PlaySound(SoundID.SFX_END_UI);
            CameraController.Instance.isFollowPlayer = true;
            yield return new WaitForSeconds(0.7f);

            acrobandit.gameObject.SetActive(false);
            Player.Instance.ToIdle();
            Player.Instance.InCutScene = false;
            Player.Instance.UseHandMovement = true;
            SaveLoadManager.Instance.GameData.isPlayerHaveDash = true;
        }
        
        public void TalkBlackCat(Animator blackCatAnim, Transform spiralBall, GameObject vfx, GameObject chest, Light2D globalLight, Light2D spotLight, SpriteRenderer catBubbleChat)
        {
            if (SaveLoadManager.Instance.GameData.CutsceneTalkBlackCat) return;
            StartCoroutine(Cor_TalkBlackCat(blackCatAnim, spiralBall, vfx, chest, globalLight, spotLight, catBubbleChat));
        }
        IEnumerator Cor_TalkBlackCat(Animator blackCatAnim, Transform spiralBall, GameObject vfx, GameObject chest, Light2D globalLight, Light2D spotLight, SpriteRenderer catBubbleChat)
        {
            Player.Instance.InCutScene = true;
            Player.Instance.UseHandMovement = false;
            CameraController.Instance.isFollowPlayer = false;
            CameraController.Instance.transform.DOMove(new Vector3(32, 36, -10), 3f).SetEase(Ease.Linear);
            catBubbleChat.color = Color.clear;
            yield return new WaitForSeconds(3);
            TriggerDialogue.Instance.EndDialogue();
            Player.Instance.InCutScene = true;
            Player.Instance.UseHandMovement = false;          
            blackCatAnim.Play("WakeUp");

            yield return new WaitForSeconds(4);
            SoundManager.Instance.PlayMusic(SoundID.MUSIC_SCARY_STORY, 1, 1);
            globalLight.gameObject.SetActive(true);
            spotLight.gameObject.SetActive(true);
            globalLight.intensity = 1;
            spotLight.intensity = 0;
            for (int i=0; i<10; i++)
            {
                globalLight.intensity -= 0.05f;
                spotLight.intensity += 0.08f;
                yield return new WaitForSeconds(0.07f);
            }
            yield return new WaitForSeconds(0.6f);
            CameraEffects.Instance.ShakeOnce(1f, 4, Vector2.one);
            SoundManager.Instance.PlaySound(SoundID.SFX_SURPRISE);
            Player.Instance.graphicOfPlayer.Play("surprise");
            yield return new WaitForSeconds(3f);

            SoundManager.Instance.PlaySound(SoundID.SFX_ENEMY_ELECTRIC);
            blackCatAnim.Play("Charing");
            spiralBall.gameObject.SetActive(true);
            spiralBall.DOScale(1, 1);

            yield return new WaitForSeconds(3);
            Player.Instance.transform.DOMove(new Vector3(32, 33.4f, 0), 0.5f);
            spiralBall.DOMove(chest.transform.position + new Vector3(0,0.5f,0), 1).SetEase(Ease.Linear).OnComplete(()=>
            {
                SoundManager.Instance.PlaySound(SoundID.SFX_APPEAR);
                CameraEffects.Instance.ShakeOnce(0.5f, 3, Vector2.one);
                Transform vfxTrans = Instantiate(vfx, spiralBall.position, Quaternion.identity).transform;
                vfxTrans.localScale = Vector3.one * 1.5f;
                spiralBall.gameObject.SetActive(false);
                chest.gameObject.SetActive(true);
                SoundManager.Instance.PauseSFX(SoundID.SFX_ENEMY_ELECTRIC);
            });            
            yield return new WaitForSeconds(2);
            for (int i = 0; i < 10; i++)
            {
                globalLight.intensity += 0.05f;
                spotLight.intensity -= 0.08f;
                yield return new WaitForSeconds(0.07f);
            }
            globalLight.gameObject.SetActive(false);
            spotLight.gameObject.SetActive(false);
            yield return new WaitForSeconds(1);
            Player.Instance.ToIdle();
            blackCatAnim.Play("SleepDown");
            yield return new WaitForSeconds(3f);
            blackCatAnim.Play("SleepIdle");
            SoundManager.Instance.PlayMusic(SoundID.MUSIC_AREA_1);

            Player.Instance.InCutScene = false;
            Player.Instance.UseHandMovement = true;
            CameraController.Instance.isFollowPlayer = true;
            SaveLoadManager.Instance.GameData.CutsceneTalkBlackCat = true;
        }
        
        public void BackageCat(Dialogue[] dialogues, Animator backageCatAnim, SO_Item rewardItem, ParticleSystem[] vfxs, SpriteRenderer[] backageCatGraphic, SpriteRenderer pinkBG, SpriteRenderer bubbleChat)
        {
            StartCoroutine(Cor_BackageCat(dialogues, backageCatAnim, rewardItem, vfxs, backageCatGraphic,pinkBG, bubbleChat));
        }    
        IEnumerator Cor_BackageCat(Dialogue[] dialogues, Animator backageCatAnim, SO_Item rewardItem, ParticleSystem[] vfxs, SpriteRenderer[] backageCatGraphic, SpriteRenderer pinkBG, SpriteRenderer bubbleChat)
        {
            Player.Instance.InCutScene = true;
            Player.Instance.UseHandMovement = false;

            bubbleChat.color = Color.clear;
            backageCatAnim.Play("talk");
            SoundManager.Instance.PlaySound(SoundID.SFX_EATING);
            TriggerDialogue.Instance.SetDialogue(dialogues[0]);
            yield return new WaitUntil(() => TriggerDialogue.Instance.isEndDialogue);

            backageCatAnim.speed = 0;
            yield return new WaitForSeconds(1);

            SoundManager.Instance.PauseSFX(SoundID.MUSIC_LAKE);
            backageCatAnim.speed = 1;
            backageCatAnim.Play("surprise");
            TriggerDialogue.Instance.SetDialogue(dialogues[1]);
            yield return new WaitUntil(() => TriggerDialogue.Instance.isEndDialogue);

            Camera.main.orthographicSize = 3.5f;
            Camera.main.transform.rotation = Quaternion.Euler(0, 0, -5);
            TriggerDialogue.Instance.SetDialogue(dialogues[2]);
            yield return new WaitUntil(() => TriggerDialogue.Instance.isEndDialogue);

            Camera.main.orthographicSize = 2.75f;
            Camera.main.transform.rotation = Quaternion.Euler(0, 0, 15);
            TriggerDialogue.Instance.SetDialogue(dialogues[3]);
            yield return new WaitUntil(() => TriggerDialogue.Instance.isEndDialogue);

            Camera.main.orthographicSize = 2f;
            Camera.main.transform.rotation = Quaternion.Euler(0, 0, 0);
            TriggerDialogue.Instance.SetDialogue(dialogues[4]);
            yield return new WaitUntil(() => TriggerDialogue.Instance.isEndDialogue);

            SoundManager.Instance.PlayMusic(SoundID.MUSIC_HAPPY);
            backageCatAnim.Play("satisfy");
            Camera.main.DOOrthoSize(4, 1f);
            foreach(var spr in backageCatGraphic)
            {
                spr.sortingLayerName = "UI_World";
            }
            pinkBG.gameObject.SetActive(true);
            pinkBG.DOFade(1, 1);
            foreach(var vfx in vfxs)
            {
                vfx.gameObject.SetActive(true);
            }
            yield return new WaitForSeconds(3);

            TriggerDialogue.Instance.SetDialogue(dialogues[5]);
            yield return new WaitUntil(() => TriggerDialogue.Instance.isEndDialogue);

            foreach (var vfx in vfxs)
            {
                var main = vfx.main;
                main.loop = false;
            }
            yield return new WaitForSeconds(3);
            pinkBG.DOFade(0, 1);
            yield return new WaitForSeconds(1);
            foreach (var spr in backageCatGraphic)
            {
                spr.sortingLayerName = "Ground";
            }

            SoundManager.Instance.PlayMusic(SoundID.MUSIC_LAKE, 1, 1);
            backageCatAnim.Play("talk");
            TriggerDialogue.Instance.SetDialogue(dialogues[6]);
            yield return new WaitUntil(() => TriggerDialogue.Instance.isEndDialogue);
            pinkBG.gameObject.SetActive(false);
            backageCatAnim.Play("idle");

            Player.Instance.ObtainItem(rewardItem);
            SaveLoadManager.Instance.GameData.AddItem(rewardItem);
            GetItemDisplayManager.Instance.ShowItem(rewardItem.sprite, rewardItem.itemName);
            yield return new WaitForSeconds(3.5f);

            bubbleChat.color = Color.white;

            Player.Instance.InCutScene = false;
            Player.Instance.UseHandMovement = true;
            SaveLoadManager.Instance.GameData.CutsceneBackageCat = true;
        }

        public void TakeBackBook(Dialogue[] dialogues, Animator book, Transform fakeBook, Transform dot, GameObject vfx)
        {
            StartCoroutine(Cor_TakeBackBook(dialogues, book, fakeBook, dot, vfx));
        }    
        IEnumerator  Cor_TakeBackBook(Dialogue[] dialogues, Animator book, Transform fakeBook, Transform dot, GameObject vfx)
        {
            SoundManager.Instance.PlaySound(SoundID.SFX_LOOT_SPECIAL);
            Player.Instance.InCutScene = true;
            Player.Instance.UseHandMovement = false;
            Player.Instance.ToIdle();

            book.transform.parent = null;
            book.gameObject.SetActive(true);
            book.transform.position = fakeBook.position;
            fakeBook.gameObject.SetActive(false);
            book.Play("Fly");
            book.transform.DOMoveY(book.transform.position.y + 0.5f, 1f);
            yield return new WaitForSeconds(1);

            TriggerDialogue.Instance.SetDialogue(dialogues[0]);
            yield return new WaitUntil(() => TriggerDialogue.Instance.isEndDialogue);

            SoundManager.Instance.PlaySound(SoundID.SFX_BOOK_PAGE);
            book.Play("Close");
            yield return new WaitForSeconds(0.55f);
            SoundManager.Instance.PlaySound(SoundID.SFX_APPEAR);
            Instantiate(vfx, book.transform.position, Quaternion.identity);
            dot.gameObject.SetActive(true);
            dot.position = book.transform.position;
            book.gameObject.SetActive(false);
            yield return new WaitForSeconds(1);

            SoundManager.Instance.PlaySound(SoundID.SFX_FAIRY_DUST);
            dot.DOMove(Player.Instance.transform.position + new Vector3(0, 0.5f, 0), 0.6f);
            yield return new WaitForSeconds(0.6f);
            dot.GetComponent<SpriteRenderer>().DOColor(Color.clear, 0.25f).SetEase(Ease.Linear);
            yield return new WaitForSeconds(0.5f);
            dot.gameObject.SetActive(false);
            SaveLoadManager.Instance.GameData.isPlayerHaveSpell = true;
            UIGameplay.Instance.LoadBottomLeftButton();
            Player.Instance.InCutScene = false;
            Player.Instance.UseHandMovement = true;
            SaveLoadManager.Instance.GameData.TargetSceneType = SceneType.AREA1_DUNGEON;
        }    

        public void MeetBoss1(Dialogue[] dialogues, Animator book, Animator boss1, Animator bubbleChatAnim, Transform dot, GameObject vfx, UIBossIntroPanel uiBossIntroPanel, Boss1 bossScr)
        {
            if (SaveLoadManager.Instance.GameData.CutsceneMeetBoss1) return;
            StartCoroutine(Cor_MeetBoss1(dialogues, book, boss1, bubbleChatAnim, dot, vfx, uiBossIntroPanel, bossScr));
        }   
        IEnumerator Cor_MeetBoss1(Dialogue[] dialogues, Animator book, Animator boss1, Animator bubbleChatAnim, Transform dot, GameObject vfx, UIBossIntroPanel uiBossIntroPanel, Boss1 bossScr)
        {
            Player.Instance.InCutScene = true;
            Player.Instance.UseHandMovement = false;
            Player.Instance.MoveTo(new Vector2(1, 56f), 1);
            yield return new WaitForSeconds(1);

            dot.gameObject.SetActive(true);
            dot.position = Player.Instance.transform.position + new Vector3(0, 0.5f, 0);
            SoundManager.Instance.PlaySound(SoundID.SFX_FAIRY_DUST);
            dot.DOMove(Player.Instance.transform.position + new Vector3(1, 0.5f, 0), 0.5f);
            bubbleChatAnim.transform.position = Player.Instance.transform.position + new Vector3(0, 1.5f, 0);
            SoundManager.Instance.PlaySound(SoundID.SFX_EXCLAMATION);
            bubbleChatAnim.Play("Surprise");
            bubbleChatAnim.transform.DOScaleX(0.6f, 0.15f);
            bubbleChatAnim.transform.DOScaleY(1.4f, 0.15f).OnComplete(() =>
            {
                bubbleChatAnim.transform.DOScale(1f, 0.1f);
            });
            yield return new WaitForSeconds(1);

            bubbleChatAnim.transform.DOScaleX(0.6f, 0.1f);
            bubbleChatAnim.transform.DOScaleY(1.4f, 0.1f).OnComplete(() =>
            {
                bubbleChatAnim.transform.DOScale(0f, 0.15f);
            });

            SoundManager.Instance.PlaySound(SoundID.SFX_APPEAR);
            Instantiate(vfx, dot.transform.position, Quaternion.identity);
            book.gameObject.SetActive(true);
            book.transform.position = dot.transform.position;
            dot.gameObject.SetActive(false);
            SoundManager.Instance.PlaySound(SoundID.SFX_BOOK_PAGE);
            book.Play("Open");
            yield return new WaitForSeconds(0.55f);
            book.Play("Fly");
            yield return new WaitForSeconds(1);
            TriggerDialogue.Instance.SetDialogue(dialogues[0]);
            yield return new WaitUntil(() => TriggerDialogue.Instance.isEndDialogue);

            SoundManager.Instance.PlaySound(SoundID.SFX_BOSS_ROAR);
            boss1.Play("roar");
            yield return new WaitForSeconds(0.4f);
            Tween cameraTween = Camera.main.DOOrthoSize(3.7f, 0.15f).SetLoops(-1, LoopType.Yoyo);
            yield return new WaitForSeconds(1.5f);
            cameraTween.Kill();
            Camera.main.DOOrthoSize(4.5f, 0.15f);
            yield return new WaitForSeconds(0.5f);          
            boss1.Play("idleStraight");

            TriggerDialogue.Instance.SetDialogue(dialogues[1]);
            yield return new WaitUntil(() => TriggerDialogue.Instance.isEndDialogue);

            SoundManager.Instance.PlaySound(SoundID.SFX_BOSS_ROAR);
            boss1.Play("roar");
            yield return new WaitForSeconds(0.4f);
            cameraTween = Camera.main.DOOrthoSize(3.7f, 0.15f).SetLoops(-1, LoopType.Yoyo);
            yield return new WaitForSeconds(1.5f);
            cameraTween.Kill();
            Camera.main.DOOrthoSize(4.5f, 0.15f);
            yield return new WaitForSeconds(0.5f);
            boss1.Play("idleStraight");

            TriggerDialogue.Instance.SetDialogue(dialogues[2]);
            yield return new WaitUntil(() => TriggerDialogue.Instance.isEndDialogue);

            yield return new WaitForSeconds(0.5f);
            boss1.Play("talk");
            TriggerDialogue.Instance.SetDialogue(dialogues[3]);
            yield return new WaitUntil(() => TriggerDialogue.Instance.isEndDialogue);
            boss1.Play("idleStraight");

            yield return new WaitForSeconds(0.5f);
            TriggerDialogue.Instance.SetDialogue(dialogues[4]);
            yield return new WaitUntil(() => TriggerDialogue.Instance.isEndDialogue);

            SoundManager.Instance.PlaySound(SoundID.SFX_BOOK_PAGE);
            book.Play("Close");
            yield return new WaitForSeconds(0.55f);
            Instantiate(vfx, dot.transform.position, Quaternion.identity);
            dot.gameObject.SetActive(true);
            book.gameObject.SetActive(false);
            yield return new WaitForSeconds(0.3f);

            dot.DOMove(Player.Instance.transform.position + new Vector3(0, 0.5f, 0), 0.5f);
            yield return new WaitForSeconds(0.5f);
            dot.GetComponent<SpriteRenderer>().DOColor(Color.clear, 0.25f).SetEase(Ease.Linear);
            yield return new WaitForSeconds(0.25f);
            dot.gameObject.SetActive(false);

            CameraController.Instance.isFollowPlayer = false;
            CameraController.Instance.isNotCheckBound = true;
            CameraController.Instance.transform.DOMove(new Vector3(1,60,-10), 0.5f);
            SoundManager.Instance.PlaySound(SoundID.SFX_BOSS_ROAR);
            boss1.Play("roar");
            SoundManager.Instance.PlaySound(SoundID.SFX_START_UI);
            uiBossIntroPanel.Show();
            SoundManager.Instance.PlayMusicWithIntro(SoundID.MUSIC_BOSS_1_BEGIN, SoundID.MUSIC_BOSS_1, 1, 1);
            yield return new WaitForSeconds(0.4f);
            cameraTween = Camera.main.DOOrthoSize(3.7f, 0.15f).SetLoops(-1, LoopType.Yoyo);
            yield return new WaitForSeconds(1.5f);
            cameraTween.Kill();
            Camera.main.DOOrthoSize(4.5f, 0.15f);
            yield return new WaitForSeconds(0.5f);
            boss1.Play("idleStraight");
            yield return new WaitForSeconds(1.6f);
            uiBossIntroPanel.Hide();
            yield return new WaitForSeconds(1.5f);
            CameraController.Instance.transform.DOMove(Player.Instance.transform.position, 0.5f);
            CameraController.Instance.isFollowPlayer = true;
            CameraController.Instance.isNotCheckBound = false;

            Player.Instance.InCutScene = false;
            Player.Instance.UseHandMovement = true;
            SaveLoadManager.Instance.GameData.CutsceneMeetBoss1 = true;
            bossScr.StartBoss();      
        }

        public void DefeatBoss1(Dialogue[] dialogues, Animator book, Animator bubbleChatAnim, Transform boss1, Transform energyOrb, Transform energyOrbContain, 
            Transform dot, GameObject vfx, GameObject vfx_inhale, GameObject vfx_fly, GameObject bgSky, SO_Item energyOrbItem)
        {
            StartCoroutine(Cor_DefeatBoss1(dialogues, book, bubbleChatAnim, boss1, energyOrb, energyOrbContain, dot, vfx, vfx_inhale, vfx_fly, bgSky, energyOrbItem));
        }
        IEnumerator Cor_DefeatBoss1(Dialogue[] dialogues, Animator book, Animator bubbleChatAnim, Transform boss1, Transform energyOrb, Transform energyOrbContain, 
            Transform dot, GameObject vfx, GameObject vfx_inhale, GameObject vfx_fly, GameObject bgSky, SO_Item energyOrbItem)
        {
            SoundManager.Instance.PauseSFX(SoundID.MUSIC_BOSS_1);
            Player.Instance.InCutScene = true;
            Player.Instance.UseHandMovement = false;
            Player.Instance.ToIdle();

            dot.gameObject.SetActive(true);
            dot.position = Player.Instance.transform.position + new Vector3(0, 0.5f, 0);
            SoundManager.Instance.PlaySound(SoundID.SFX_FAIRY_DUST);
            dot.DOMove(Player.Instance.transform.position + new Vector3(0, 1f, 0), 0.7f);
            yield return new WaitForSeconds(0.7f);

            SoundManager.Instance.PlaySound(SoundID.SFX_APPEAR);
            Instantiate(vfx, dot.transform.position, Quaternion.identity);
            book.gameObject.SetActive(true);
            book.transform.position = dot.transform.position;
            dot.gameObject.SetActive(false);
            SoundManager.Instance.PlaySound(SoundID.SFX_BOOK_PAGE);
            book.Play("Open");
            yield return new WaitForSeconds(0.55f);
            book.Play("Fly");
            yield return new WaitForSeconds(1);
            TriggerDialogue.Instance.SetDialogue(dialogues[0]);
            yield return new WaitUntil(() => TriggerDialogue.Instance.isEndDialogue);

            // insert qu� tr�nh h�t boss
            SoundManager.Instance.PlaySound(SoundID.SFX_INHALE);
            vfx_inhale.SetActive(true);
            vfx_inhale.transform.position = book.transform.position;
            vfx_inhale.transform.DOScale(1, 0.5f);
            boss1.DOMove(book.transform.position, 2);
            boss1.DOScale(0, 2);
            yield return new WaitForSeconds(2);
            SoundManager.Instance.PauseSFX(SoundID.SFX_INHALE);
            SoundManager.Instance.PlaySound(SoundID.SFX_ABSORB);
            vfx_inhale.transform.DOScale(0, 0.5f).OnComplete(() => vfx_inhale.SetActive(false));
            yield return new WaitForSeconds(1f);

            SoundManager.Instance.PlaySound(SoundID.SFX_APPEAR);
            Instantiate(vfx, book.transform.position, Quaternion.identity);
            energyOrbContain.gameObject.SetActive(true);
            energyOrbContain.transform.position = Player.Instance.transform.position;
            energyOrb.gameObject.SetActive(true);
            energyOrb.position = book.transform.position;
            SoundManager.Instance.PlaySound(SoundID.SFX_FAIRY_DUST);
            energyOrb.DOMove(book.transform.position + new Vector3(0, 2), 1);
            SoundManager.Instance.PlaySound(SoundID.SFX_EXCLAMATION);
            bubbleChatAnim.transform.position = Player.Instance.transform.position + new Vector3(0, 1.5f, 0);
            bubbleChatAnim.Play("Surprise");
            bubbleChatAnim.transform.DOScaleX(0.6f, 0.15f);
            bubbleChatAnim.transform.DOScaleY(1.4f, 0.15f).OnComplete(() =>
            {
                bubbleChatAnim.transform.DOScale(1f, 0.1f);
            });
            yield return new WaitForSeconds(1);
            bubbleChatAnim.transform.DOScaleX(0.6f, 0.1f);
            bubbleChatAnim.transform.DOScaleY(1.4f, 0.1f).OnComplete(() =>
            {
                bubbleChatAnim.transform.DOScale(0f, 0.15f);
            });
            yield return new WaitForSeconds(2);

            TriggerDialogue.Instance.SetDialogue(dialogues[1]);
            yield return new WaitUntil(() => TriggerDialogue.Instance.isEndDialogue);

            SoundManager.Instance.PlaySound(SoundID.SFX_FAIRY_DUST);
            energyOrbContain.DORotate(new Vector3(0, 0, 1800), 6, RotateMode.FastBeyond360).SetEase(Ease.InOutSine)
                .OnComplete(()=>
                {
                    energyOrb.DOMove(Player.Instance.transform.position + new Vector3(0, 10, 0), 1f).SetEase(Ease.InExpo);
                    SoundManager.Instance.PlaySound(SoundID.SFX_FAIRY_DUST);
                    SoundManager.Instance.PlaySound(SoundID.SFX_START_UI);
                });
            yield return new WaitForSeconds(3);

            SoundManager.Instance.PlaySound(SoundID.SFX_FAIRY_DUST);
            vfx_fly.SetActive(true);
            vfx_fly.transform.SetParent(Player.Instance.graphicContainTransform);
            vfx_fly.transform.localPosition = new Vector3(0,0.5f,0);
            Player.Instance.Heal(100, 100, false);
            book.transform.DOMove(Player.Instance.transform.position + new Vector3(-1, 0.5f, 0), 0.5f);
            Player.Instance.graphicContainTransform.DOMove(Player.Instance.transform.position + new Vector3(0,1,0), 1).SetEase(Ease.Linear);
            yield return new WaitForSeconds(0.5f);
            SoundManager.Instance.PlaySound(SoundID.SFX_FAIRY_DUST);
            Player.Instance.graphicOfPlayer.Play("surprise");
            yield return new WaitForSeconds(3.5f);

            SoundManager.Instance.PlaySound(SoundID.SFX_FAIRY_DUST);
            SoundManager.Instance.PlaySound(SoundID.SFX_START_UI);
            Player.Instance.graphicContainTransform.DOMove(Player.Instance.transform.position + new Vector3(0, 10, 0), 1.5f).SetEase(Ease.InExpo);
            SaveLoadManager.Instance.GameData.PlayerHp = Player.Instance.CurrentHealth;
            SaveLoadManager.Instance.GameData.PlayerStamina = Player.Instance.Stamina;
            yield return new WaitForSeconds(1.5f);

            //------------------PHAN CANH BAY LEN TROI------------------------------------
            GameManager.Instance.ReLoadSceneManually(TransitionType.UP);

            yield return new WaitUntil(() => GameManager.Instance.isLoadSceneComplete);
            SoundManager.Instance.PlaySound(SoundID.SFX_START_UI);
            CameraController.Instance.isFollowPlayer = false;
            Camera.main.orthographicSize = 4;
            CameraController.Instance.maxY = 100;
            CameraController.Instance.transform.position = new Vector3(0, 80, 0);
            bgSky.SetActive(true);
            Player.Instance.InCutScene = true;
            Player.Instance.UseHandMovement = false;
            yield return new WaitUntil(() => !HandMovement.Instance.gameObject.activeInHierarchy);
            Player.Instance.graphicOfPlayer.Play("surprise");
            Player.Instance.transform.position = new Vector3(30, 80, 0);
            Player.Instance.graphicContainTransform.position = new Vector3(0, 80, 0);
            energyOrb.position = new Vector3(0, 82, 0);           
            Player.Instance.graphicContainTransform.DOMove(new Vector3(0, 100, 0),3);
            CameraController.Instance.transform.DOMove(new Vector3(0, 100, 0), 3);
            energyOrb.DOMove(new Vector3(0, 102, 0),3);
            yield return new WaitForSeconds(3.5f);

            SoundManager.Instance.PlaySound(SoundID.SFX_FAIRY_DUST);
            energyOrb.DOMove(Player.Instance.graphicContainTransform.position + new Vector3(2,0), 1);
            yield return new WaitForSeconds(2f);

            SoundManager.Instance.PlaySound(SoundID.SFX_FAIRY_DUST);
            energyOrb.DOMove(Player.Instance.graphicContainTransform.position, 1f).OnComplete(()=>
            {
                energyOrb.gameObject.SetActive(false);
                SoundManager.Instance.PlaySound(SoundID.SFX_LOOT_SPECIAL);
                SaveLoadManager.Instance.GameData.AddItem(energyOrbItem);
                GetItemDisplayManager.Instance.ShowItem(energyOrbItem.sprite, energyOrbItem.itemName);
            });
            yield return new WaitForSeconds(1);

            vfx_fly.SetActive(false);
            yield return new WaitForSeconds(0.9f);
            SoundManager.Instance.PlaySound(SoundID.SFX_EXCLAMATION);
            Player.Instance.graphicOfPlayer.speed = 0;
            bubbleChatAnim.transform.position = Player.Instance.graphicContainTransform.position + new Vector3(0, 1.5f, 0);
            bubbleChatAnim.Play("Surprise");
            bubbleChatAnim.transform.DOScaleX(0.6f, 0.15f);
            bubbleChatAnim.transform.DOScaleY(1.4f, 0.15f).OnComplete(() =>
            {
                bubbleChatAnim.transform.DOScale(1f, 0.1f);
            });
            yield return new WaitForSeconds(1);
            bubbleChatAnim.transform.DOScaleX(0.6f, 0.1f);
            bubbleChatAnim.transform.DOScaleY(1.4f, 0.1f).OnComplete(() =>
            {
                bubbleChatAnim.transform.DOScale(0f, 0.15f);
            });
            yield return new WaitForSeconds(2);
            Player.Instance.graphicOfPlayer.speed = 1;

            SoundManager.Instance.PlaySound(SoundID.SFX_SURPRISE);
            Player.Instance.graphicContainTransform.DOMove(new Vector3(0, 80, 0), 2).SetEase(Ease.InExpo);
            yield return new WaitForSeconds(0.5f);
            SoundManager.Instance.PlaySound(SoundID.SFX_FALL);
            yield return new WaitForSeconds(1.5f);
            SaveLoadManager.Instance.GameData.CutsceneDefeatBoss1 = true;
            //GameManager.Instance.LoadSceneManually(SceneType.AREA1_BONUS, TransitionType.DOWN);
            SaveLoadManager.Instance.CacheData.isChangeCameraBound = false;
            GameManager.Instance.LoadSceneAsyncManually(
                        SceneType.AREA1_BONUS,
                        TransitionType.DOWN,
                        cb: () =>
                        {
                            GameManager.Instance.UnloadSceneManually(
                                SaveLoadManager.Instance.GameData.CurrentSceneType,
                                cb: () => GameManager.Instance.SetInitData()
                            );
                        });
        }

        public void EndFalling(Animator book, Animator vodaNPCAnim, SpriteRenderer vodaNPC, GameObject vodaFishing, GameObject fishingRopeIdle, GameObject vfx_waterSplash, Sprite surpriseVodaSprite)
        {
            if (SaveLoadManager.Instance.GameData.CutsceneDefeatBoss1 && SaveLoadManager.Instance.GameData.CutsceneEndChapter1 == false)
            {
                StartCoroutine(Cor_EndFalling(book, vodaNPCAnim, vodaNPC, vodaFishing, fishingRopeIdle, vfx_waterSplash, surpriseVodaSprite));
            }
        }    
        IEnumerator Cor_EndFalling(Animator book, Animator vodaNPCAnim, SpriteRenderer vodaNPC, GameObject vodaFishing, GameObject fishingRopeIdle, GameObject vfx_waterSplash, Sprite surpriseVodaSprite)
        {
            yield return new WaitUntil(() => GameManager.Instance.isLoadSceneComplete);

            Player.Instance.InCutScene = true;
            Player.Instance.UseHandMovement = false;
            Player.Instance.coll.enabled = false;
            vodaFishing.gameObject.SetActive(true);
            fishingRopeIdle.gameObject.SetActive(false);
            CameraController.Instance.transform.position = new Vector3(21, -11, -10);
            CameraController.Instance.isFollowPlayer = false;
            yield return new WaitForSeconds(2);

            Player.Instance.transform.position = new Vector3(23, -3, 0);
            Player.Instance.graphicOfPlayer.Play("surprise");
            Player.Instance.graphicContainTransform.localRotation = Quaternion.Euler(0, 0, 60);
            SoundManager.Instance.PlaySound(SoundID.SFX_FALL);
            Player.Instance.transform.DOMoveY(-12, 1f).SetEase(Ease.Linear).OnComplete(()=>
            {
                Player.Instance.graphicContainTransform.localScale = Vector3.zero;
                SoundManager.Instance.PlaySound(SoundID.SFX_WATER_SPLASH);
                vfx_waterSplash.gameObject.SetActive(true);
                vfx_waterSplash.transform.position = Player.Instance.transform.position;
            });
            yield return new WaitForSeconds(1f);

            //vodaNPCAnim.enabled = false;
            vodaNPC.sprite = surpriseVodaSprite;
            SoundManager.Instance.PlaySound(SoundID.SFX_JUMP);
            vodaNPC.transform.DOJump(vodaNPC.transform.position, 1f, 1, 0.6f);
            yield return new WaitForSeconds(1.5f);

            book.gameObject.SetActive(true);
            book.Play("Fly");
            book.transform.position = new Vector3(30, -3, 0);
            SoundManager.Instance.PlaySound(SoundID.SFX_FAIRY_DUST);
            book.transform.DOMove(new Vector3(24, -10, 0), 2);
            yield return new WaitForSeconds(2.5f);

            //GameManager.Instance.LoadSceneManually(SceneType.BEGIN_AREA_HOUSE, TransitionType.FADE);
            GameManager.Instance.LoadSceneAsyncManually(
                        SceneType.BEGIN_AREA_HOUSE,
                        TransitionType.FADE,
                        cb: () =>
                        {
                            GameManager.Instance.UnloadSceneManually(
                                SaveLoadManager.Instance.GameData.CurrentSceneType,
                                cb: () => GameManager.Instance.SetInitData()
                            );
                        });
        }

        public void WakeUpEndChapter1(Dialogue[] dialogues, NPC voda, Following vodaFollowing, Animator book, Transform dot, GameObject vfx, SO_Item wishPiece1, Beta betaPanel)
        {
            if (SaveLoadManager.Instance.GameData.CutsceneDefeatBoss1 && SaveLoadManager.Instance.GameData.CutsceneEndChapter1 == false)
            {
                StartCoroutine(Cor_WakeUpEndChapter1(dialogues, voda, vodaFollowing, book, dot, vfx, wishPiece1, betaPanel));
            }
        }
        IEnumerator Cor_WakeUpEndChapter1(Dialogue[] dialogues, NPC voda, Following vodaFollowing, Animator book, Transform dot, GameObject vfx, SO_Item wishPiece1, Beta betaPanel)
        {
            yield return new WaitUntil(() => GameManager.Instance.isLoadSceneComplete);
            Player.Instance.InCutScene = true;
            Player.Instance.UseWeapon = false;
            Player.Instance.UseHandMovement = false;
            Player.Instance.coll.enabled = false;
            vodaFollowing.isFollowing = false;
            CameraController.Instance.transform.position = new Vector3(8.7f, -2.3f, -10);
            //Player.Instance.transform.position = new Vector3(9, -2.3f);
            Player.Instance.LieDown(new Vector2(8.7f,-2.2f));
            yield return new WaitForSeconds(3);

            SoundManager.Instance.PlaySound(SoundID.SFX_JUMP);
            Player.Instance.StandUpSurprise();
            yield return new WaitForSeconds(2.5f);

            Player.Instance.transform.position = new Vector3(8.7f, -1.6f, 0);
            yield return new WaitForSeconds(1.5f);

            book.gameObject.SetActive(true);
            book.transform.position = new Vector3(-2.5f, 1, 0);
            book.Play("Fly");
            CameraController.Instance.isFollowPlayer = false;
            CameraController.Instance.transform.DOMove(new Vector3(-2.5f, 0, -10),0.75f);
            yield return new WaitForSeconds(0.75f);

            voda.anim.Play("talk");
            TriggerDialogue.Instance.SetDialogue(dialogues[0]);
            yield return new WaitUntil(() => TriggerDialogue.Instance.isEndDialogue);
            voda.anim.Play("idle");

            Player.Instance.MoveTo(new Vector2(0, -2),3f);
            yield return new WaitForSeconds(3f);
            Player.Instance.MoveTo(new Vector2(-0.5f, -0.2f), 1f);
            yield return new WaitForSeconds(1f);

            TriggerDialogue.Instance.SetDialogue(dialogues[1]);
            yield return new WaitUntil(() => TriggerDialogue.Instance.isEndDialogue);
            yield return new WaitForSeconds(0.5f);

            voda.anim.Play("talk");
            TriggerDialogue.Instance.SetDialogue(dialogues[2]);
            yield return new WaitUntil(() => TriggerDialogue.Instance.isEndDialogue);
            voda.anim.Play("idle");
            yield return new WaitForSeconds(0.5f);

            SoundManager.Instance.PlaySound(SoundID.SFX_LOOT_SPECIAL);
            Player.Instance.ObtainItem(wishPiece1);
            yield return new WaitForSeconds(3.2f);
            Player.Instance.UseWeapon = false;
            Player.Instance.UseHandMovement = false;
            yield return new WaitUntil(() => !HandMovement.Instance.gameObject.activeInHierarchy);
            Player.Instance.graphicOfPlayer.transform.localScale = new Vector3(-1, 1, 1);

            voda.anim.Play("talk");
            TriggerDialogue.Instance.SetDialogue(dialogues[3]);
            yield return new WaitUntil(() => TriggerDialogue.Instance.isEndDialogue);
            voda.anim.Play("idle");
            yield return new WaitForSeconds(0.5f);

            TriggerDialogue.Instance.SetDialogue(dialogues[4]);
            yield return new WaitUntil(() => TriggerDialogue.Instance.isEndDialogue);

            SoundManager.Instance.PlaySound(SoundID.SFX_BOOK_PAGE);
            book.Play("Close");
            yield return new WaitForSeconds(0.55f);
            SoundManager.Instance.PlaySound(SoundID.SFX_APPEAR);
            Instantiate(vfx, dot.transform.position, Quaternion.identity);
            dot.gameObject.SetActive(true);
            book.gameObject.SetActive(false);
            yield return new WaitForSeconds(0.3f);

            SoundManager.Instance.PlaySound(SoundID.SFX_FAIRY_DUST);
            dot.DOMove(Player.Instance.transform.position + new Vector3(0, 0.5f, 0), 0.5f);
            yield return new WaitForSeconds(0.5f);
            dot.GetComponent<SpriteRenderer>().DOColor(Color.clear, 0.25f).SetEase(Ease.Linear);
            yield return new WaitForSeconds(0.25f);
            dot.gameObject.SetActive(false);

            betaPanel.OpenBetaPanel();
            yield return new WaitUntil(() => betaPanel.gameObject.activeSelf == false);

            Player.Instance.coll.enabled = true;
            Player.Instance.InCutScene = false;
            Player.Instance.UseWeapon = true;
            Player.Instance.UseHandMovement = true;
            SaveLoadManager.Instance.GameData.TargetSceneType = SceneType.BEGIN_AREA_3;
            SaveLoadManager.Instance.GameData.CutsceneEndChapter1 = true;
        }
    }
}
