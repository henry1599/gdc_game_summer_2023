using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.Managers;
using GDC.Enums;
using GDC.Configuration;

namespace GDC.Managers
{
    public class SpellManager : MonoBehaviour
    {
        public static SpellManager Instance { get; private set; }
        public List<SO_Spell> SO_SpellList;

        [SerializeField] private string path;

        private void Awake()
        {
            if (Instance != null)
            {
                Destroy(gameObject);
                return;
            }
            Instance = this;
            DontDestroyOnLoad(gameObject);

            LoadSpellList();
            LoadSpellLevelData();
        }

        void LoadSpellList()
        {
            SO_SpellList.Clear();
            SO_SpellList = new List<SO_Spell>();
            var gos = Resources.LoadAll(path);
            if (gos == null || gos.Length == 0) return;
            foreach(var go in gos)
            {
                SO_SpellList.Add(go as SO_Spell);
            }
            Debug.Log("Load spell list completed!");
        }    
        void LoadSpellLevelData() 
        {
            //load spell level from SaveLoadManager.GameData
        }
    }
}
