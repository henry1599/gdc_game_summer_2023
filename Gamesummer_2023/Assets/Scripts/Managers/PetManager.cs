﻿using GDC.Enums;
using GDC.Pets;
using System.Collections;
using System.Collections.Generic;
using RotaryHeart.Lib.SerializableDictionary;
using UnityEngine;
using NaughtyAttributes;
using DG.Tweening;
using UnityEngine.SceneManagement;
using GDC.Gameplay.UI;
using GDC.PlayerManager;

namespace GDC.Managers
{
    [System.Serializable]
    public class PetInfoByType : SerializableDictionaryBase<PetID, PetInfo> { }

    [System.Serializable]
    public class PetInfo
    {
        public string Name;
        public string Color;
        public Pet Pet;
        [ShowAssetPreview] public Sprite[] Leaf;
        [ShowAssetPreview] public Sprite Head;
    }
    public class PetManager : MonoBehaviour
    {
        [SerializeField,ReadOnly] Pet currentPet = null,capturePet = null;
        public Pet CurrentPet
        {
            get => currentPet;
            set => currentPet = value;
        }

        [SerializeField] SceneType petScene;
        [SerializeField] List<SceneType> minigameScene = new List<SceneType>();
        [SerializeField] List<PetID> petAtHome = new List<PetID>();

        public PetInfoByType PetDict;

        public static PetManager Instance { get; private set; }

        private void Awake()
        {
            if (Instance != null)
                Destroy(gameObject);
            else
                Instance = this;
        }

        public void AddPet(Pet pet,bool inGarden = false)
        {
            if (!inGarden)
                SaveLoadManager.Instance.GameData.AddPetAtHome(pet.PetID);
            capturePet = pet;

            if (currentPet!=null)
            {
                string text = "Sử dụng \n<color=" + PetDict[capturePet.PetID].Color + ">" + PetDict[capturePet.PetID].Name + "</color>";
                PetPopupPanel.Instance.OpenPanel(text, PetDict[capturePet.PetID].Head, PetDict[capturePet.PetID].Leaf);
                return;
            }

            UsePet();
        }

        public void UsePet()
        {
            string curSceneName = SceneManager.GetActiveScene().name;
            SceneType curSceneType = GameSceneManager.TranslateToSceneType(curSceneName);
            if (curSceneType != petScene)

            {
                currentPet?.Disappear();
            }
            else
            {
                if (currentPet!=null)
                    currentPet.FollowPlayer = false;
                PetInfoPanel.Instance?.UpdatePetInUse(capturePet.PetID);
            }

            currentPet = capturePet;
            capturePet = null;
            SaveLoadManager.Instance.GameData.SetPetFollowing(currentPet.PetID);
            currentPet.FollowPlayer = true;
        }

        public void UnusePet(Collider2D range)
        {
            if (currentPet == null)
                return;
            print(1);
            currentPet.SetUpRange(range.bounds.center, range.bounds.extents);
            currentPet.FollowPlayer = false;
            currentPet.Wander();
            currentPet = null;
            PetInfoPanel.Instance?.UpdatePetInUse(PetID.NONE);
            SaveLoadManager.Instance.GameData.SetPetFollowing(PetID.NONE);
        }

        public void StorePet()
        {
            string curSceneName = SceneManager.GetActiveScene().name;
            SceneType curSceneType = GameSceneManager.TranslateToSceneType(curSceneName);
            if (curSceneType != petScene)
            {
                capturePet?.Disappear();
            }
            else
                capturePet?.Wander();
            capturePet = null;
        }

        public void LoadPet()
        {
            SceneType currentScene = SaveLoadManager.Instance.GameData.CurrentSceneType;
            foreach (SceneType scene in minigameScene)
            {
                if (currentScene == scene)
                    return;
            }
            if (currentPet != null)
                return;
            petAtHome = SaveLoadManager.Instance.GameData.GetPetAtHomeList();
            PetID currentPetID = SaveLoadManager.Instance.GameData.GetPetFollowing();
            if (currentPetID == PetID.NONE)
                return;
            if (!petAtHome.Remove(currentPetID))
                print("Player doesnt have this pet");
            if (currentScene == SceneType.BEGIN_AREA_3)
                GreenHouse.Instance.GeneratePetGrass(petAtHome);
            Pet tempPet = Instantiate(PetDict[currentPetID].Pet);
            tempPet.transform.localScale = Vector2.zero;
            tempPet.transform.DOScale(1, 0.5f);
            tempPet.transform.position = Player.Instance.transform.position;
            tempPet.FollowPlayer = true;
            currentPet = tempPet;
        }    
    }
}