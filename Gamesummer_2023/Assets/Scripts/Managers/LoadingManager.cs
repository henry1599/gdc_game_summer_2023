using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using GDC.Events;
using GDC.Enums;
using NaughtyAttributes;
using UnityEngine.UI;
using DG.Tweening;

namespace GDC.Managers
{
    public class LoadingManager : MonoBehaviour
    {
        public static LoadingManager Instance {get; private set;}
        [SerializeField] GameObject loadingCanvas;
        [SerializeField] Image loadingIcon;
        [SerializeField] Sprite slime, bigSlime, spikeSlime, fireSlime, poisonSlime, metalSlime, goldenSlime, iceSlime;
        void Awake()
        {
            if (Instance != null)
            {
                Destroy(gameObject);
                return;
            }
            Instance = this;
        }
        // Start is called before the first frame update
        void Start()
        {
            GameEvents.ON_LOADING += HandleLoading;   
        }
        void OnDestroy()
        {
            GameEvents.ON_LOADING -= HandleLoading;   
        }
        void HandleLoading(bool isLoading)
        {
            if (isLoading)
                Debug.Log("start loading");
            else Debug.Log("end loading");
            this.loadingCanvas.SetActive(isLoading);
            if (isLoading)
            {
                AreaType curAreaType = SaveLoadManager.Instance.GameData.CurrentAreaType;
                SceneType curSceneType = SaveLoadManager.Instance.GameData.CurrentSceneType;
                if (curAreaType == AreaType.REAL_WORLD_AREA || curAreaType == AreaType.BEGIN_AREA)
                {
                    loadingIcon.sprite = slime;
                }
                else if (curAreaType == AreaType.AREA_1)
                {
                    int rand = Random.Range(0, 100);
                    if (rand < 30) loadingIcon.sprite = slime;
                    else if (rand < 50) loadingIcon.sprite = bigSlime;
                    else if (rand < 70) loadingIcon.sprite = spikeSlime;
                    else if (rand < 80) loadingIcon.sprite = fireSlime;
                    else if (rand < 90) loadingIcon.sprite = poisonSlime;
                    else if (rand < 95) loadingIcon.sprite = metalSlime;
                    else loadingIcon.sprite = goldenSlime; ;
                }
                else if(curAreaType==AreaType.AREA_2)
                {
                    loadingIcon.sprite = slime;
                }
                else if (curAreaType == AreaType.AREA_3)
                {
                    loadingIcon.sprite = slime;
                }
                else if (curAreaType == AreaType.AREA_4)
                {
                    loadingIcon.sprite = slime;
                }
                else if (curAreaType == AreaType.AREA_5)
                {
                    loadingIcon.sprite = slime;
                }

                loadingIcon.SetNativeSize();
                loadingIcon.rectTransform.sizeDelta *= 1.333f;
                DOTween.Kill(loadingIcon);
                loadingIcon.rectTransform.localScale = Vector3.one;
                loadingIcon.rectTransform.DOScale(new Vector3(1.2f, 0.8f, 1), 1).SetLoops(-1, LoopType.Yoyo).SetUpdate(true);
            }    
        }
    }
}
