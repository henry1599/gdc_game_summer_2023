using System.Collections.Generic;
using UnityEngine;
using System;
using GDC.Configuration;
using GDC.Enums;
using GDC.Constants;
using GDC.PlayerManager;
using GDC.Gameplay.UI;
using GDC.Gameplay;

namespace GDC.Managers
{

    [Serializable]
    public struct GameData
    {
        public bool IsSaveLoadProcessing;

        public bool IsHaveSaveData;
        public int Coin;
        public string PlayerName;

        #region stat
        public float PlayerHp, PlayerStamina;
        public Vector2 PlayerPosition;
        public int PlayerLevel, PlayerExp;
        public int AtkBonus, DefBonus, HpBonus, StamBonus;
        #endregion

        public float CamMinX, CamMaxX, CamMinY, CamMaxY;

        #region item
        public List<ItemData> PlayerWeaponList;
        public List<ItemData> PlayerIngredientList;
        public List<ItemData> PlayerKeyItemList;
        public List<ItemData> PlayerClothesList;
        public List<ItemData> PlayerUseableItemList;

        public List<string> SeenItemIDList; //Hien thi nhung item da duoc nhin thay
        #endregion

        #region craft
        public List<SO_Craft> PlayerWeaponCraftList;
        public List<SO_Craft> PlayerIngredientCraftList;
        public List<SO_Craft> PlayerKeyItemCraftList;
        public List<SO_Craft> PlayerClothesCraftList;
        public List<SO_Craft> PlayerUseableItemCraftList;

        public List<string> SeenCraftIDList; //Hien thi nhung item craft da duoc nhin thay
        #endregion

        #region carry
        public List<SO_Item> CarrySlotList;
        #endregion

        #region equip
        public SO_Item HelmetEquipItem;
        public SO_Item ArmorEquipItem;
        public SO_Item WeaponEquipItem;
        public SO_Item ShoeEquipItem;
        #endregion

        #region spell
        public List<int> SpellLevelList;
        public List<bool> SeenSpellList;
        public List<bool> GetSpellList;
        public SpellType CurrentSpell;
        #endregion

        #region cutscene
        public bool CutsceneFirstComeBookWorld, CutsceneFirstMeetMysteriousMan;
        public bool CutsceneFirstBattleSlime;
        public bool CutsceneMeetHouse, CutsceneInHouse;
        public bool CutsceneLearnSpell;
        public bool CutsceneSeeBoss1Gate;
        public bool CutsceneSeeDashRing;
        public bool CutsceneTalkBlackCat, CutsceneBackageCat;
        public bool CutsceneMeetBoss1;
        public bool CutsceneDefeatBoss1, CutsceneEndChapter1;
        public bool EventSunFlowerCheckIn, EventDeadGlassFish, EventDeadFishVanish;
        public bool CutsceneBreakStoneToArea2, CutsceneFirstHearForestEcho, CutsceneSecondHearForestEcho, CutsceneGetBow, CutsceneCureGreatTree;
        #endregion cutscene

        #region pet
        public int PetFollowIntID;
        public List<int> PetAtHomes;
        #endregion

        public bool isPlayerHaveWeapon, isMysteriousManFollowing, isPlayerHaveMap;
        public bool isPlayerHaveSpell, isPlayerHaveDash;

        #region scene
        public SceneType CurrentSceneType, TargetSceneType;
        public List<SceneType> SceneTypePassList;
        public AreaType CurrentAreaType;
        #endregion

        #region chest and door
        bool isInitChestAndDoorList;
        public List<bool> ChestListBegin;
        public List<bool> ChestListArea1;
        public List<bool> ChestListArea2;
        public List<bool> ChestListArea3;
        public List<bool> ChestListArea4;
        public List<bool> ChestListArea5;

        public List<bool> DoorListBegin;
        public List<bool> DoorListArea1;
        public List<bool> DoorListArea2;
        public List<bool> DoorListArea3;
        public List<bool> DoorListArea4;
        public List<bool> DoorListArea5;
        #endregion
        public void SetupData() //load
        {
            IsSaveLoadProcessing = true;
            GameDataOrigin gameDataOrigin = SaveLoadManager.Instance.GameDataOrigin;

            IsHaveSaveData = gameDataOrigin.IsHaveSaveData;
            //if (gameDataOrigin.IsHaveSaveData == false)
            //{
            //    SaveLoadManager.Instance.Save();
            //    return;
            //}
            Coin = gameDataOrigin.Coin;
            PlayerName = gameDataOrigin.PlayerName;

            #region load stat
            PlayerHp = gameDataOrigin.PlayerHp;
            PlayerStamina = gameDataOrigin.PlayerStamina;
            if (PlayerHp <=0)
            {
                //yield return new WaitUntil(() => GDC.Managers.ConfigManager.Instance != null);
                var playerStatConfig = GDC.Managers.ConfigManager.Instance.PlayerStatConfig;
                PlayerHp = playerStatConfig.MaxHealth;
                PlayerStamina = playerStatConfig.MaxStamina;
            }
            PlayerPosition = new Vector2(gameDataOrigin.PlayerPosX, gameDataOrigin.PlayerPosY);
            PlayerLevel = gameDataOrigin.PlayerLevel;
            if (PlayerLevel < 1) PlayerLevel = 1;
            PlayerExp = gameDataOrigin.PlayerExp;
            AtkBonus = gameDataOrigin.AtkBonus;
            DefBonus = gameDataOrigin.DefBonus;
            StamBonus = gameDataOrigin.StamBonus;
            HpBonus = gameDataOrigin.HpBonus;
            #endregion

            CamMaxX = gameDataOrigin.CamMaxX;
            CamMinX = gameDataOrigin.CamMinX;
            CamMaxY = gameDataOrigin.CamMaxY;
            CamMinY = gameDataOrigin.CamMinY;
            #region load item
            if (gameDataOrigin.PlayerWeaponIDList == null)
            {
                gameDataOrigin.PlayerWeaponIDList = new List<string>();
                gameDataOrigin.WeaponAmountList = new List<int>();
            }
            if (gameDataOrigin.PlayerClothesIDList == null)
            {
                gameDataOrigin.PlayerClothesIDList = new List<string>();
                gameDataOrigin.ClothesAmountList = new List<int>();
            }
            if (gameDataOrigin.PlayerUseableItemIDList == null)
            {
                gameDataOrigin.PlayerUseableItemIDList = new List<string>();
                gameDataOrigin.UseableItemAmountList = new List<int>();
            }
            if (gameDataOrigin.PlayerIngredientIDList == null)
            {
                gameDataOrigin.PlayerIngredientIDList = new List<string>();
                gameDataOrigin.IngredientAmountList = new List<int>();
            }
            if (gameDataOrigin.PlayerKeyItemIDList == null)
            {
                gameDataOrigin.PlayerKeyItemIDList = new List<string>();
                gameDataOrigin.KeyItemAmountList = new List<int>();
            }

            PlayerWeaponList = new List<ItemData>();
            PlayerIngredientList = new List<ItemData>();
            PlayerClothesList = new List<ItemData>();
            PlayerUseableItemList = new List<ItemData>();
            PlayerKeyItemList = new List<ItemData>();

            int weaponIndex = 0;
            foreach (var data in gameDataOrigin.PlayerWeaponIDList)
            {
                ItemData itemData = new ItemData();
                itemData.Amount = gameDataOrigin.WeaponAmountList[weaponIndex];
                itemData.SO_item = findSO_ItemWithID(data);
                PlayerWeaponList.Add(itemData);
                weaponIndex++;
            }

            int ingredientIndex = 0;
            foreach (var data in gameDataOrigin.PlayerIngredientIDList)
            {
                ItemData itemData = new ItemData();
                itemData.Amount = gameDataOrigin.IngredientAmountList[ingredientIndex];
                itemData.SO_item = findSO_ItemWithID(data);
                PlayerIngredientList.Add(itemData);
                ingredientIndex++;
            }

            int clothesIndex = 0;
            foreach (var data in gameDataOrigin.PlayerClothesIDList)
            {
                ItemData itemData = new ItemData();
                itemData.Amount = gameDataOrigin.ClothesAmountList[clothesIndex];
                itemData.SO_item = findSO_ItemWithID(data);
                PlayerClothesList.Add(itemData);
                clothesIndex++;
            }

            int useableItemIndex = 0;
            foreach (var data in gameDataOrigin.PlayerUseableItemIDList)
            {
                ItemData itemData = new ItemData();
                itemData.Amount = gameDataOrigin.UseableItemAmountList[useableItemIndex];
                itemData.SO_item = findSO_ItemWithID(data);
                PlayerUseableItemList.Add(itemData);
                useableItemIndex++;
            }

            int keyItemIndex = 0;
            foreach (var data in gameDataOrigin.PlayerKeyItemIDList)
            {
                ItemData itemData = new ItemData();
                itemData.Amount = gameDataOrigin.KeyItemAmountList[keyItemIndex];
                itemData.SO_item = findSO_ItemWithID(data);
                PlayerKeyItemList.Add(itemData);
                keyItemIndex++;
            }

            SeenItemIDList = gameDataOrigin.SeenItemIDList;
            #endregion

            #region load craft
            if (gameDataOrigin.PlayerWeaponCraftIDList == null)
            {
                gameDataOrigin.PlayerWeaponCraftIDList = new List<string>();
            }
            if (gameDataOrigin.PlayerClothesCraftIDList == null)
            {
                gameDataOrigin.PlayerClothesCraftIDList = new List<string>();
            }
            if (gameDataOrigin.PlayerUseableItemCraftIDList == null)
            {
                gameDataOrigin.PlayerUseableItemCraftIDList = new List<string>();
            }
            if (gameDataOrigin.PlayerIngredientCraftIDList == null)
            {
                gameDataOrigin.PlayerIngredientCraftIDList = new List<string>();
            }
            if (gameDataOrigin.PlayerKeyItemCraftIDList == null)
            {
                gameDataOrigin.PlayerKeyItemCraftIDList = new List<string>();
            }

            PlayerWeaponCraftList = new List<SO_Craft>();
            PlayerClothesCraftList = new List<SO_Craft>();
            PlayerUseableItemCraftList = new List<SO_Craft>();
            PlayerIngredientCraftList = new List<SO_Craft>();
            PlayerKeyItemCraftList = new List<SO_Craft>();

            int craftIndex = 0;
            foreach (var data in gameDataOrigin.PlayerWeaponCraftIDList)
            {
                PlayerWeaponCraftList.Add(findSO_CraftWithID(gameDataOrigin.PlayerWeaponCraftIDList[craftIndex]));
                craftIndex++;
            }
            craftIndex = 0;
            foreach (var data in gameDataOrigin.PlayerClothesCraftIDList)
            {
                PlayerClothesCraftList.Add(findSO_CraftWithID(gameDataOrigin.PlayerClothesCraftIDList[craftIndex]));
                craftIndex++;
            }
            craftIndex = 0;
            foreach (var data in gameDataOrigin.PlayerUseableItemCraftIDList)
            {
                PlayerUseableItemCraftList.Add(findSO_CraftWithID(gameDataOrigin.PlayerUseableItemCraftIDList[craftIndex]));
                craftIndex++;
            }
            craftIndex = 0;
            foreach (var data in gameDataOrigin.PlayerIngredientCraftIDList)
            {
                PlayerIngredientCraftList.Add(findSO_CraftWithID(gameDataOrigin.PlayerIngredientCraftIDList[craftIndex]));
                craftIndex++;
            }
            craftIndex = 0;
            foreach (var data in gameDataOrigin.PlayerKeyItemCraftIDList)
            {
                PlayerKeyItemCraftList.Add(findSO_CraftWithID(gameDataOrigin.PlayerKeyItemCraftIDList[craftIndex]));
                craftIndex++;
            }

            SeenCraftIDList = gameDataOrigin.SeenCraftIDList;
            if (SeenCraftIDList == null)
            {
                SeenCraftIDList = new List<string>();
            }
            #endregion

            #region load carry
            CarrySlotList = new List<SO_Item>();
            for (int i=0; i<GameConstants.MAX_CARRY_SLOT; i++)
            {
                CarrySlotList.Add(null);
            }
            //CarrySlotList = new List<SO_Item>(GameConstants.MAX_CARRY_SLOT);
            
            if (gameDataOrigin.CarrySlotIDList == null || gameDataOrigin.CarrySlotIDList.Count == 0)
            {
                gameDataOrigin.CarrySlotIDList = new List<string>();
                for (int i = 0; i < GameConstants.MAX_CARRY_SLOT; i++)
                {
                    gameDataOrigin.CarrySlotIDList.Add(null);
                }
            }
            //foreach (var data in gameDataOrigin.CarrySlotIDList)
            //{
            //    CarrySlotList.Add(findSO_ItemWithID(data));
            //}
            for (int i =0; i<GameConstants.MAX_CARRY_SLOT; i++)
            {
                CarrySlotList[i] = findSO_ItemWithID(gameDataOrigin.CarrySlotIDList[i]);
            }
            #endregion

            #region load equip
            HelmetEquipItem = findSO_ItemWithID(gameDataOrigin.HelmetEquipID);
            ArmorEquipItem = findSO_ItemWithID(gameDataOrigin.ArmorEquipID);
            WeaponEquipItem = findSO_ItemWithID(gameDataOrigin.WeaponEquipID);
            ShoeEquipItem = findSO_ItemWithID(gameDataOrigin.ShoeEquipID);
            #endregion

            #region load spell
            if (gameDataOrigin.GetSpellList == null || gameDataOrigin.GetSpellList.Count == 0)
            {
                gameDataOrigin.GetSpellList = new List<bool>();
                for (int i = 0; i < GameConstants.NUMBER_OF_SPELL; i++)
                {
                    gameDataOrigin.GetSpellList.Add(false);
                }
            }
            if (gameDataOrigin.SpellLevelList == null || gameDataOrigin.SpellLevelList.Count == 0)
            {
                gameDataOrigin.SpellLevelList = new List<int>();
                gameDataOrigin.SeenSpellList = new List<bool>();
                for (int i = 0; i < GameConstants.NUMBER_OF_SPELL; i++)
                {
                    gameDataOrigin.SpellLevelList.Add(1);
                    gameDataOrigin.SeenSpellList.Add(false);
                }
            }
            SpellLevelList = gameDataOrigin.SpellLevelList;
            SeenSpellList = gameDataOrigin.SeenSpellList;
            GetSpellList = gameDataOrigin.GetSpellList;

            if (Enum.IsDefined(typeof(SpellType), gameDataOrigin.CurrentSpellID))
            {
                CurrentSpell = (SpellType)gameDataOrigin.CurrentSpellID;
                // if enum is defined
            }
            else
            {
                // if enum is not defined
                CurrentSpell = SpellType.NONE;
            }
            #endregion

            #region load cutscene
            CutsceneFirstComeBookWorld = gameDataOrigin.CutsceneFirstComeBookWorld;
            CutsceneFirstMeetMysteriousMan = gameDataOrigin.CutsceneFirstMeetMysteriousMan;
            CutsceneFirstBattleSlime = gameDataOrigin.CutsceneFirstBattleSlime;
            CutsceneMeetHouse = gameDataOrigin.CutsceneMeetHouse;
            CutsceneInHouse = gameDataOrigin.CutsceneInHouse;
            CutsceneLearnSpell = gameDataOrigin.CutsceneLearnSpell;
            CutsceneSeeBoss1Gate = gameDataOrigin.CutsceneSeeBoss1Gate;
            CutsceneSeeDashRing = gameDataOrigin.CutsceneSeeDashRing;
            CutsceneTalkBlackCat = gameDataOrigin.CutsceneTalkBlackCat;
            CutsceneBackageCat = gameDataOrigin.CutsceneBackageCat;
            CutsceneMeetBoss1 = gameDataOrigin.CutsceneMeetBoss1;
            CutsceneDefeatBoss1 = gameDataOrigin.CutsceneDefeatBoss1;
            CutsceneEndChapter1 = gameDataOrigin.CutsceneEndChapter1;
            EventSunFlowerCheckIn = gameDataOrigin.EventSunFlowerCheckIn;
            EventDeadGlassFish = gameDataOrigin.EventDeadGlassFish;
            EventDeadFishVanish = gameDataOrigin.EventDeadFishVanish;
            CutsceneBreakStoneToArea2 = gameDataOrigin.CutsceneBreakStoneToArea2;
            CutsceneFirstHearForestEcho = gameDataOrigin.CutsceneFirstHearForestEcho;
            CutsceneSecondHearForestEcho = gameDataOrigin.CutsceneSecondHearForestEcho;
            CutsceneGetBow = gameDataOrigin.CutsceneGetBow;
            CutsceneCureGreatTree = gameDataOrigin.CutsceneCureGreatTree;
            #endregion cutscene

            #region load pet
            PetFollowIntID = gameDataOrigin.PetFollowIntID;
            if (gameDataOrigin.PetAtHomes == null || gameDataOrigin.PetAtHomes.Count == 0)
            {
                gameDataOrigin.PetAtHomes = new List<int>();
                for (int i = 0; i < GameConstants.NUMBER_OF_PET; i++)
                {
                    gameDataOrigin.PetAtHomes.Add(0);
                }
            }
            //Debug.Log(gameDataOrigin.PetAtHomes.Count);
            PetAtHomes = gameDataOrigin.PetAtHomes;
            //Debug.Log("A" + PetAtHomes.Count);
            #endregion

            isPlayerHaveWeapon = gameDataOrigin.isPlayerHaveWeapon;
            isMysteriousManFollowing = gameDataOrigin.isMysteriousManFollowing;
            isPlayerHaveMap = gameDataOrigin.isPlayerHaveMap;
            isPlayerHaveSpell = gameDataOrigin.isPlayerHaveSpell;
            isPlayerHaveDash = gameDataOrigin.isPlayerHaveDash;

            #region load scene
            if (Enum.IsDefined(typeof(SceneType), gameDataOrigin.CurrentSceneTypeID))
            {
                CurrentSceneType = (SceneType)gameDataOrigin.CurrentSceneTypeID;
                // if enum is defined
            }
            else
            {
                // if enum is not defined
                CurrentSceneType = SceneType.REAL_WORLD_AREA_01;
            }

            if (Enum.IsDefined(typeof(SceneType), gameDataOrigin.TargetSceneTypeID))
            {
                TargetSceneType = (SceneType)gameDataOrigin.TargetSceneTypeID;
            }
            else
            {
                TargetSceneType = SceneType.MAIN;
            }

            if (Enum.IsDefined(typeof(AreaType), gameDataOrigin.CurrentAreaTypeID))
            {
                CurrentAreaType = (AreaType)gameDataOrigin.CurrentAreaTypeID;
            }
            else
            {
                CurrentAreaType = AreaType.REAL_WORLD_AREA;
            }

            if (gameDataOrigin.SceneTypePassIDList == null)
            {
                gameDataOrigin.SceneTypePassIDList = new List<int>();
            }
            SceneTypePassList = new List<SceneType>();
            foreach(var sceneTypeID in gameDataOrigin.SceneTypePassIDList)
            {
                if (Enum.IsDefined(typeof(SceneType), sceneTypeID))
                {
                    SceneTypePassList.Add((SceneType)sceneTypeID);
                }
            }
            #endregion

            #region chest and door
            ChestListBegin = gameDataOrigin.ChestListBegin;
            ChestListArea1 = gameDataOrigin.ChestListArea1;
            ChestListArea2 = gameDataOrigin.ChestListArea2;
            ChestListArea3 = gameDataOrigin.ChestListArea3;
            ChestListArea4 = gameDataOrigin.ChestListArea4;
            ChestListArea5 = gameDataOrigin.ChestListArea5;

            DoorListBegin = gameDataOrigin.DoorListBegin;
            DoorListArea1 = gameDataOrigin.DoorListArea1;
            DoorListArea2 = gameDataOrigin.DoorListArea2;
            DoorListArea3 = gameDataOrigin.DoorListArea3;
            DoorListArea4 = gameDataOrigin.DoorListArea4;
            DoorListArea5 = gameDataOrigin.DoorListArea5;

            isInitChestAndDoorList = false;
            InitChestAndDoorList();
            #endregion

            SaveLoadManager.Instance.GameDataOrigin = gameDataOrigin;

            //This part just for dev:
            //if (UIGameplay.Instance!=null)
            //{
            //    CurrentSceneType = UIGameplay.Instance.sceneType;
            //}    

            IsSaveLoadProcessing = false;
        }
        public GameDataOrigin ConvertToGameDataOrigin() //save
        {
            IsSaveLoadProcessing = true;
            GameDataOrigin gameDataOrigin = new GameDataOrigin();

            gameDataOrigin.IsHaveSaveData = true;
            gameDataOrigin.Coin = Coin;
            gameDataOrigin.PlayerName = PlayerName;

            #region convert stat
            if (Player.Instance != null)
            {
                PlayerHp = Player.Instance.CurrentHealth;
                PlayerStamina = Player.Instance.Stamina;
                PlayerPosition = Player.Instance.transform.position;
                gameDataOrigin.PlayerHp = PlayerHp;
                gameDataOrigin.PlayerStamina = PlayerStamina;
                gameDataOrigin.PlayerPosX = PlayerPosition.x;
                gameDataOrigin.PlayerPosY = PlayerPosition.y;
            }
            gameDataOrigin.PlayerLevel = PlayerLevel;
            gameDataOrigin.PlayerExp = PlayerExp;
            gameDataOrigin.AtkBonus = AtkBonus;
            gameDataOrigin.DefBonus = DefBonus;
            gameDataOrigin.StamBonus = StamBonus;
            gameDataOrigin.HpBonus = HpBonus;
            #endregion

            gameDataOrigin.CamMaxX = CamMaxX;
            gameDataOrigin.CamMaxY = CamMaxY;
            gameDataOrigin.CamMinX = CamMinX;
            gameDataOrigin.CamMinY = CamMinY;

            #region convert item
            if (PlayerWeaponList == null)
            {
                PlayerWeaponList = new List<ItemData>();
            }
            gameDataOrigin.PlayerWeaponIDList = new List<string>();
            gameDataOrigin.WeaponAmountList = new List<int>();
            foreach (var data in PlayerWeaponList)
            {
                gameDataOrigin.PlayerWeaponIDList.Add(data.SO_item.ID);
                gameDataOrigin.WeaponAmountList.Add(data.Amount);
            }

            if (PlayerIngredientList == null)
            {
                PlayerIngredientList = new List<ItemData>();
            }
            gameDataOrigin.PlayerIngredientIDList = new List<string>();
            gameDataOrigin.IngredientAmountList = new List<int>();
            foreach (var data in PlayerIngredientList)
            {
                gameDataOrigin.PlayerIngredientIDList.Add(data.SO_item.ID);
                gameDataOrigin.IngredientAmountList.Add(data.Amount);
            }

            if (PlayerClothesList == null)
            {
                PlayerClothesList = new List<ItemData>();
            }
            gameDataOrigin.PlayerClothesIDList = new List<string>();
            gameDataOrigin.ClothesAmountList = new List<int>();
            foreach (var data in PlayerClothesList)
            {
                gameDataOrigin.PlayerClothesIDList.Add(data.SO_item.ID);
                gameDataOrigin.ClothesAmountList.Add(data.Amount);
            }

            if (PlayerUseableItemList == null)
            {
                PlayerUseableItemList = new List<ItemData>();
            }
            gameDataOrigin.PlayerUseableItemIDList = new List<string>();
            gameDataOrigin.UseableItemAmountList = new List<int>();
            foreach (var data in PlayerUseableItemList)
            {
                gameDataOrigin.PlayerUseableItemIDList.Add(data.SO_item.ID);
                gameDataOrigin.UseableItemAmountList.Add(data.Amount);
            }

            if (PlayerKeyItemList == null)
            {
                PlayerKeyItemList = new List<ItemData>();
            }
            gameDataOrigin.PlayerKeyItemIDList = new List<string>();
            gameDataOrigin.KeyItemAmountList = new List<int>();
            foreach (var data in PlayerKeyItemList)
            {
                gameDataOrigin.PlayerKeyItemIDList.Add(data.SO_item.ID);
                gameDataOrigin.KeyItemAmountList.Add(data.Amount);
            }

            gameDataOrigin.SeenItemIDList = SeenItemIDList;
            #endregion

            #region convert craft
            if (PlayerWeaponCraftList == null)
            {
                PlayerWeaponCraftList = new List<SO_Craft>();
            }
            if (PlayerClothesCraftList == null)
            {
                PlayerClothesCraftList = new List<SO_Craft>();
            }
            if (PlayerIngredientCraftList == null)
            {
                PlayerIngredientCraftList = new List<SO_Craft>();
            }
            if (PlayerUseableItemCraftList == null)
            {
                PlayerUseableItemCraftList = new List<SO_Craft>();
            }
            if (PlayerKeyItemCraftList == null)
            {
                PlayerKeyItemCraftList = new List<SO_Craft>();
            }

            gameDataOrigin.PlayerWeaponCraftIDList = new List<string>();
            gameDataOrigin.PlayerClothesCraftIDList = new List<string>();
            gameDataOrigin.PlayerUseableItemCraftIDList = new List<string>();
            gameDataOrigin.PlayerIngredientCraftIDList = new List<string>();
            gameDataOrigin.PlayerKeyItemCraftIDList = new List<string>();

            foreach(var data in PlayerWeaponCraftList)
            {
                gameDataOrigin.PlayerWeaponCraftIDList.Add(data.SO_item.ID);
            }
            foreach(var data in PlayerClothesCraftList)
            {
                gameDataOrigin.PlayerClothesCraftIDList.Add(data.SO_item.ID);
            }
            foreach (var data in PlayerUseableItemCraftList)
            {
                gameDataOrigin.PlayerUseableItemCraftIDList.Add(data.SO_item.ID);
            }
            foreach (var data in PlayerIngredientCraftList)
            {
                gameDataOrigin.PlayerIngredientCraftIDList.Add(data.SO_item.ID);
            }
            foreach (var data in PlayerKeyItemCraftList)
            {
                gameDataOrigin.PlayerKeyItemCraftIDList.Add(data.SO_item.ID);
            }

            gameDataOrigin.SeenCraftIDList = SeenCraftIDList;
            #endregion

            #region convert carry
            gameDataOrigin.CarrySlotIDList = new List<string>();

            foreach (var data in CarrySlotList)
            {
                if (data != null)
                {
                    gameDataOrigin.CarrySlotIDList.Add(data.ID);
                }
                else
                {
                    gameDataOrigin.CarrySlotIDList.Add(null);
                }
            }
            #endregion

            #region convert equip
            if (HelmetEquipItem != null)
                gameDataOrigin.HelmetEquipID = HelmetEquipItem.ID;
            else
                gameDataOrigin.HelmetEquipID = "";
            if (ArmorEquipItem != null)
                gameDataOrigin.ArmorEquipID = ArmorEquipItem.ID;
            else
                gameDataOrigin.ArmorEquipID = "";
            if (WeaponEquipItem != null)
                gameDataOrigin.WeaponEquipID = WeaponEquipItem.ID;
            else
                gameDataOrigin.WeaponEquipID = "";
            if (ShoeEquipItem != null)
                gameDataOrigin.ShoeEquipID = ShoeEquipItem.ID;
            else
                gameDataOrigin.ShoeEquipID = "";
            #endregion

            #region convert spell
            if (GetSpellList == null)
            {
                gameDataOrigin.GetSpellList = new List<bool>();
                for (int i = 0; i < GameConstants.NUMBER_OF_SPELL; i++)
                {
                    gameDataOrigin.GetSpellList.Add(false);
                }
            }
            else
            {
                gameDataOrigin.GetSpellList = GetSpellList;
            }
            if (SpellLevelList == null || SpellLevelList.Count == 0)
            {
                gameDataOrigin.SpellLevelList = new List<int>();
                gameDataOrigin.SeenSpellList = new List<bool>();
                for (int i = 0; i < GameConstants.NUMBER_OF_SPELL; i++)
                {
                    gameDataOrigin.SpellLevelList.Add(1);
                    gameDataOrigin.SeenSpellList.Add(false);
                }
            }
            else
            {
                gameDataOrigin.SpellLevelList = SpellLevelList;
                gameDataOrigin.SeenSpellList = SeenSpellList;              
            }

            gameDataOrigin.CurrentSpellID = (int)CurrentSpell;
            #endregion

            #region convert cutscene
            gameDataOrigin.CutsceneFirstComeBookWorld = CutsceneFirstComeBookWorld;
            gameDataOrigin.CutsceneFirstMeetMysteriousMan = CutsceneFirstMeetMysteriousMan;
            gameDataOrigin.CutsceneFirstBattleSlime = CutsceneFirstBattleSlime;
            gameDataOrigin.CutsceneMeetHouse = CutsceneMeetHouse;
            gameDataOrigin.CutsceneInHouse = CutsceneInHouse;
            gameDataOrigin.CutsceneLearnSpell = CutsceneLearnSpell;
            gameDataOrigin.CutsceneSeeBoss1Gate = CutsceneSeeBoss1Gate;
            gameDataOrigin.CutsceneSeeDashRing = CutsceneSeeDashRing;
            gameDataOrigin.CutsceneTalkBlackCat = CutsceneTalkBlackCat;
            gameDataOrigin.CutsceneBackageCat = CutsceneBackageCat;
            gameDataOrigin.CutsceneMeetBoss1 = CutsceneMeetBoss1;
            gameDataOrigin.CutsceneDefeatBoss1 = CutsceneDefeatBoss1;
            gameDataOrigin.CutsceneEndChapter1 = CutsceneEndChapter1;
            gameDataOrigin.EventSunFlowerCheckIn = EventSunFlowerCheckIn;
            gameDataOrigin.EventDeadGlassFish = EventDeadGlassFish;
            gameDataOrigin.EventDeadFishVanish = EventDeadFishVanish;
            gameDataOrigin.CutsceneBreakStoneToArea2 = CutsceneBreakStoneToArea2; //area 2
            gameDataOrigin.CutsceneFirstHearForestEcho = CutsceneFirstHearForestEcho;
            gameDataOrigin.CutsceneSecondHearForestEcho = CutsceneSecondHearForestEcho;
            gameDataOrigin.CutsceneGetBow = CutsceneGetBow;
            gameDataOrigin.CutsceneCureGreatTree = CutsceneCureGreatTree;
            #endregion

            #region convert pet
            gameDataOrigin.PetFollowIntID = PetFollowIntID;
            if (PetAtHomes == null || PetAtHomes.Count==0)
            {
                gameDataOrigin.PetAtHomes = new List<int>();
                for (int i = 0; i < GameConstants.NUMBER_OF_PET; i++)
                {
                    gameDataOrigin.PetAtHomes.Add(0);
                }
            }
            else
            {
                gameDataOrigin.PetAtHomes = PetAtHomes;
            }
            #endregion

            gameDataOrigin.isPlayerHaveWeapon = isPlayerHaveWeapon;
            gameDataOrigin.isMysteriousManFollowing = isMysteriousManFollowing;
            gameDataOrigin.isPlayerHaveMap = isPlayerHaveMap;
            gameDataOrigin.isPlayerHaveSpell = isPlayerHaveSpell;
            gameDataOrigin.isPlayerHaveDash = isPlayerHaveDash;

            #region convert scene
            gameDataOrigin.CurrentSceneTypeID = (int)CurrentSceneType;
            gameDataOrigin.TargetSceneTypeID = (int)TargetSceneType;
            if (SceneTypePassList == null)
            {
                SceneTypePassList = new List<SceneType>();
            }
            gameDataOrigin.SceneTypePassIDList = new List<int>();
            foreach(var sceneTypePass in SceneTypePassList)
            {
                gameDataOrigin.SceneTypePassIDList.Add((int)sceneTypePass);
            }
            gameDataOrigin.CurrentAreaTypeID = (int)CurrentAreaType;
            #endregion

            #region chest and door
            
            gameDataOrigin.ChestListBegin = ChestListBegin;
            gameDataOrigin.ChestListArea1 = ChestListArea1;
            gameDataOrigin.ChestListArea2 = ChestListArea2;
            gameDataOrigin.ChestListArea3 = ChestListArea3;
            gameDataOrigin.ChestListArea4 = ChestListArea4;
            gameDataOrigin.ChestListArea5 = ChestListArea5;

            gameDataOrigin.DoorListBegin = DoorListBegin;
            gameDataOrigin.DoorListArea1 = DoorListArea1;
            gameDataOrigin.DoorListArea2 = DoorListArea2;
            gameDataOrigin.DoorListArea3 = DoorListArea3;
            gameDataOrigin.DoorListArea4 = DoorListArea4;
            gameDataOrigin.DoorListArea5 = DoorListArea5;
            #endregion

            IsSaveLoadProcessing = false;
            return gameDataOrigin;
        }

        #region support function
        public int GetAmountOfItem(SO_Item SO_item)
        {
            if (SO_item.itemType == ItemType.WEAPON && PlayerWeaponList != null)
            {
                for (int i = 0; i < PlayerWeaponList.Count; i++)
                {
                    if (SO_item.Equals(PlayerWeaponList[i].SO_item))
                    {
                        return PlayerWeaponList[i].Amount;
                    }
                }
            }
            else if (SO_item.itemType == ItemType.INGREDIENT && PlayerIngredientList != null)
            {
                for (int i = 0; i < PlayerIngredientList.Count; i++)
                {
                    if (SO_item.Equals(PlayerIngredientList[i].SO_item))
                    {
                        return PlayerIngredientList[i].Amount;
                    }
                }
            }
            else if (SO_item.itemType == ItemType.CLOTHES && PlayerClothesList != null)
            {
                for (int i = 0; i < PlayerClothesList.Count; i++)
                {
                    if (SO_item.Equals(PlayerClothesList[i].SO_item))
                    {
                        return PlayerClothesList[i].Amount;
                    }
                }
            }
            else if (SO_item.itemType == ItemType.USEABLE_ITEM && PlayerUseableItemList != null)
            {
                for (int i = 0; i < PlayerUseableItemList.Count; i++)
                {
                    if (SO_item.Equals(PlayerUseableItemList[i].SO_item))
                    {
                        return PlayerUseableItemList[i].Amount;
                    }
                }
            }
            else if (SO_item.itemType == ItemType.KEY_ITEM && PlayerKeyItemList != null)
            {
                for (int i = 0; i < PlayerKeyItemList.Count; i++)
                {
                    if (SO_item.Equals(PlayerKeyItemList[i].SO_item))
                    {
                        return PlayerKeyItemList[i].Amount;
                    }
                }
            }
            return 0;
        }
        SO_Item findSO_ItemWithID(string ID)
        {
            if (ID == null || ID == "") return null;
            string path = "";
            if (ID[0] == 'w')
            {
                path = "ScriptableObjects\\SO_Item\\Weapon";
            }
            if (ID[0] == 'i' && ID[1] == 'n')
            {
                path = "ScriptableObjects\\SO_Item\\Ingredient";
            }
            if (ID[0] == 'u')
            {
                path = "ScriptableObjects\\SO_Item\\UseableItem";
            }
            if (ID[0] == 'c')
            {
                path = "ScriptableObjects\\SO_Item\\Clothes";
            }
            if (ID[0] == 'k')
            {
                path = "ScriptableObjects\\SO_Item\\KeyItem";
            }

            if (path == "") Debug.Log("Can't find direction to load scriptable object with ID = " + ID);

            var gos = Resources.LoadAll(path);
            if (gos == null || gos.Length == 0) return null;
            foreach (var go in gos)
            {
                SO_Item so_item = go as SO_Item;
                if (so_item.ID == ID)
                {
                    return so_item;
                }
            }
            return null;
        }
        SO_Craft findSO_CraftWithID(string ID)
        {
            if (ID == null || ID == "") return null;
            string path = "";
            if (ID[0] == 'w')
            {
                path = "ScriptableObjects\\SO_Craft\\Weapon";
            }
            if (ID[0] == 'i' && ID[1] == 'n')
            {
                path = "ScriptableObjects\\SO_Craft\\Ingredient";
            }
            if (ID[0] == 'u')
            {
                path = "ScriptableObjects\\SO_Craft\\UseableItem";
            }
            if (ID[0] == 'c')
            {
                path = "ScriptableObjects\\SO_Craft\\Clothes";
            }
            if (ID[0] == 'k')
            {
                path = "ScriptableObjects\\SO_Craft\\KeyItem";
            }

            if (path == "") Debug.Log("Can't find direction to load scriptable object with ID = " + ID);

            var gos = Resources.LoadAll(path);
            if (gos == null || gos.Length == 0) return null;
            foreach (var go in gos)
            {
                SO_Craft so_craft = go as SO_Craft;
                if (so_craft.SO_item.ID == ID)
                {
                    return so_craft;
                }
            }
            return null;
        }
        public void AddItem(SO_Item SO_item)
        {
            if (SO_item.itemType == ItemType.WEAPON)
            {
                if (PlayerWeaponList == null)
                {
                    PlayerWeaponList = new List<ItemData>();
                }
                foreach (var data in PlayerWeaponList)
                {
                    if (SO_item.ID == data.SO_item.ID)
                    {
                        data.Amount++;
                        return;
                    }
                }
                ItemData itemData = new ItemData();
                itemData.SO_item = SO_item;
                itemData.Amount = 1;

                PlayerWeaponList.Add(itemData);
            }
            else if (SO_item.itemType == ItemType.INGREDIENT)
            {
                if (PlayerIngredientList == null)
                {
                    PlayerIngredientList = new List<ItemData>();
                }
                foreach (var data in PlayerIngredientList)
                {
                    if (SO_item.ID == data.SO_item.ID)
                    {
                        data.Amount++;
                        return;
                    }
                }
                ItemData itemData = new ItemData();
                itemData.SO_item = SO_item;
                itemData.Amount = 1;

                PlayerIngredientList.Add(itemData);
            }
            else if (SO_item.itemType == ItemType.CLOTHES)
            {
                if (PlayerClothesList == null)
                {
                    PlayerClothesList = new List<ItemData>();
                }
                foreach (var data in PlayerClothesList)
                {
                    if (SO_item.ID == data.SO_item.ID)
                    {
                        data.Amount++;
                        return;
                    }
                }
                ItemData itemData = new ItemData();
                itemData.SO_item = SO_item;
                itemData.Amount = 1;

                PlayerClothesList.Add(itemData);
            }
            else if (SO_item.itemType == ItemType.USEABLE_ITEM)
            {
                if (PlayerUseableItemList == null)
                {
                    PlayerUseableItemList = new List<ItemData>();
                }
                foreach (var data in PlayerUseableItemList)
                {
                    if (SO_item.ID == data.SO_item.ID)
                    {
                        data.Amount++;
                        return;
                    }
                }
                ItemData itemData = new ItemData();
                itemData.SO_item = SO_item;
                itemData.Amount = 1;

                PlayerUseableItemList.Add(itemData);
            }
            else if (SO_item.itemType == ItemType.KEY_ITEM)
            {
                if (PlayerKeyItemList == null)
                {
                    PlayerKeyItemList = new List<ItemData>();
                }
                foreach (var data in PlayerKeyItemList)
                {
                    if (SO_item.ID == data.SO_item.ID)
                    {
                        data.Amount++;
                        return;
                    }
                }
                ItemData itemData = new ItemData();
                itemData.SO_item = SO_item;
                itemData.Amount = 1;

                PlayerKeyItemList.Add(itemData);
            }
        }
        public void AddCraft(SO_Craft SO_craft)
        {
            if (SO_craft.SO_item.itemType == ItemType.WEAPON)
            {
                if (PlayerWeaponCraftList == null)
                {
                    PlayerWeaponCraftList = new List<SO_Craft>();
                }

                foreach(var data in PlayerWeaponCraftList)
                {
                    if (data.Equals(SO_craft))
                        return;
                }

                PlayerWeaponCraftList.Add(SO_craft);
            }
            else if (SO_craft.SO_item.itemType == ItemType.INGREDIENT)
            {
                if (PlayerIngredientCraftList == null)
                {
                    PlayerIngredientCraftList = new List<SO_Craft>();
                }

                foreach (var data in PlayerIngredientCraftList)
                {
                    if (data.Equals(SO_craft))
                        return;
                }

                PlayerIngredientCraftList.Add(SO_craft);
            }
            else if (SO_craft.SO_item.itemType == ItemType.CLOTHES)
            {
                if (PlayerClothesCraftList == null)
                {
                    PlayerClothesCraftList = new List<SO_Craft>();
                }

                foreach (var data in PlayerClothesCraftList)
                {
                    if (data.Equals(SO_craft))
                        return;
                }

                PlayerClothesCraftList.Add(SO_craft);
            }
            else if (SO_craft.SO_item.itemType == ItemType.USEABLE_ITEM)
            {
                if (PlayerUseableItemCraftList == null)
                {
                    PlayerUseableItemCraftList = new List<SO_Craft>();
                }

                foreach (var data in PlayerUseableItemCraftList)
                {
                    if (data.Equals(SO_craft))
                        return;
                }

                PlayerUseableItemCraftList.Add(SO_craft);
            }
            else if (SO_craft.SO_item.itemType == ItemType.KEY_ITEM)
            {
                if (PlayerKeyItemCraftList == null)
                {
                    PlayerKeyItemCraftList = new List<SO_Craft>();
                }

                foreach (var data in PlayerKeyItemCraftList)
                {
                    if (data.Equals(SO_craft))
                        return;
                }

                PlayerKeyItemCraftList.Add(SO_craft);
            }
        }    
        public void DeleteItem(ItemType itemType, int index)
        {
            if (itemType == ItemType.WEAPON)
            {
                PlayerWeaponList.RemoveAt(index);
            }
            else if (itemType == ItemType.CLOTHES)
            {
                PlayerClothesList.RemoveAt(index);
            }
            else if (itemType == ItemType.INGREDIENT)
            {
                PlayerIngredientList.RemoveAt(index);
            }
            else if (itemType == ItemType.USEABLE_ITEM)
            {
                PlayerUseableItemList.RemoveAt(index);
            }
            else if (itemType == ItemType.KEY_ITEM)
            {
                PlayerKeyItemList.RemoveAt(index);
            }
        }
        public void ReduceItem(SO_Item SO_item, int reduceValue)
        {
            if (SO_item.itemType == ItemType.INGREDIENT)
            {
                for (int i=0; i<PlayerIngredientList.Count; i++)
                {
                    if (SO_item.Equals(PlayerIngredientList[i].SO_item))
                    {
                        PlayerIngredientList[i].Amount -= reduceValue;
                        if (PlayerIngredientList[i].Amount <= 0) PlayerIngredientList.RemoveAt(i);
                    }
                }
            }
            else if (SO_item.itemType == ItemType.WEAPON)
            {
                for (int i = 0; i < PlayerWeaponList.Count; i++)
                {
                    if (SO_item.Equals(PlayerWeaponList[i].SO_item))
                    {
                        PlayerWeaponList[i].Amount -= reduceValue;
                        if (PlayerWeaponList[i].Amount <= 0) PlayerWeaponList.RemoveAt(i);
                    }
                }
            }
            else if (SO_item.itemType == ItemType.CLOTHES)
            {
                for (int i = 0; i < PlayerClothesList.Count; i++)
                {
                    if (SO_item.Equals(PlayerClothesList[i].SO_item))
                    {
                        PlayerClothesList[i].Amount -= reduceValue;
                        if (PlayerClothesList[i].Amount <= 0) PlayerClothesList.RemoveAt(i);
                    }
                }
            }
            else if (SO_item.itemType == ItemType.USEABLE_ITEM)
            {
                for (int i = 0; i < PlayerUseableItemList.Count; i++)
                {
                    if (SO_item.Equals(PlayerUseableItemList[i].SO_item))
                    {
                        PlayerUseableItemList[i].Amount -= reduceValue;
                        if (PlayerUseableItemList[i].Amount <= 0) PlayerUseableItemList.RemoveAt(i);
                    }
                }
            }
            else if (SO_item.itemType == ItemType.KEY_ITEM)
            {
                for (int i = 0; i < PlayerKeyItemList.Count; i++)
                {
                    if (SO_item.Equals(PlayerKeyItemList[i].SO_item))
                    {
                        PlayerKeyItemList[i].Amount -= reduceValue;
                        if (PlayerKeyItemList[i].Amount <= 0) PlayerKeyItemList.RemoveAt(i);
                    }
                }
            }
        }
        public void AddToSeenItemIDList(string id)
        {
            foreach(var data in SeenItemIDList)
            {
                if (data.Equals(id)) return;
            }
            SeenItemIDList.Add(id);
        }
        public void AddToSeenCraftIDList(string id)
        {
            foreach (var data in SeenCraftIDList)
            {
                if (data.Equals(id)) return;
            }
            SeenCraftIDList.Add(id);
        }

        public void SetPetFollowing(PetID petID)
        {
            if (PetAtHomes == null || PetAtHomes.Count == 0)
            {
                PetAtHomes = new List<int>();
                for (int i = 0; i < GameConstants.NUMBER_OF_PET; i++)
                {
                    PetAtHomes.Add(0);
                }
            }

            if (petID == PetID.NONE)
            {
                PetFollowIntID = 0;
            }
            else if (petID == PetID.BLUE_FOLEY)
            {
                PetFollowIntID = 1;
            }
            else if (petID == PetID.GREEN_FOLEY)
            {
                PetFollowIntID = 2;
            }
            else if (petID == PetID.PINK_FOLEY)
            {
                PetFollowIntID = 3;
            }
            else if (petID == PetID.RED_FOLEY)
            {
                PetFollowIntID = 4;
            }
            else if (petID == PetID.YELLOW_FOLEY)
            {
                PetFollowIntID = 5;
            }

            //if (PetFollowIntID != 0)
            //{
            //    PetAtHomes[PetFollowIntID - 1]--;
            //    if (PetAtHomes[PetFollowIntID - 1] < 0)
            //        PetAtHomes[PetFollowIntID - 1] = 0;
            //}
        } //Set data pet di theo Player
        public PetID GetPetFollowing()
        {
            if (PetFollowIntID == 1) return PetID.BLUE_FOLEY;
            else if (PetFollowIntID == 2) return PetID.GREEN_FOLEY;
            else if (PetFollowIntID == 3) return PetID.PINK_FOLEY;
            else if (PetFollowIntID == 4) return PetID.RED_FOLEY;
            else if (PetFollowIntID == 5) return PetID.YELLOW_FOLEY;
            else return PetID.NONE;
        } //Get data pet di theo Player, neu khong co tra ve PetID.NONE
        public void AddPetAtHome(PetID petID)
        {
            if (PetAtHomes == null || PetAtHomes.Count==0)
            {
                PetAtHomes = new List<int>();
                for (int i = 0; i < GameConstants.NUMBER_OF_PET; i++)
                {
                    PetAtHomes.Add(0);
                }
            }

            if (petID == PetID.BLUE_FOLEY)
            {
                PetAtHomes[0]++;
            }
            else if (petID == PetID.GREEN_FOLEY)
            {
                PetAtHomes[1]++;
            }
            else if (petID == PetID.PINK_FOLEY)
            {
                PetAtHomes[2]++;
            }
            else if (petID == PetID.RED_FOLEY)
            {
                PetAtHomes[3]++;
            }
            else if (petID == PetID.YELLOW_FOLEY)
            {
                PetAtHomes[4]++;
            }
        }   //Add data pet cat tru o nha (ko tinh pet di theo)
        public List<PetID> GetPetAtHomeList()
        {
            List<PetID> petList = new List<PetID>();
            if (PetAtHomes == null || PetAtHomes.Count == 0) return petList;

            for (int i = 0; i < PetAtHomes[0]; i++)
            {
                petList.Add(PetID.BLUE_FOLEY);
            }
            for (int i = 0; i < PetAtHomes[1]; i++)
            {
                petList.Add(PetID.GREEN_FOLEY);
            }
            for (int i = 0; i < PetAtHomes[2]; i++)
            {
                petList.Add(PetID.PINK_FOLEY);
            }
            for (int i = 0; i < PetAtHomes[3]; i++)
            {
                petList.Add(PetID.RED_FOLEY);
            }
            for (int i = 0; i < PetAtHomes[4]; i++)
            {
                petList.Add(PetID.YELLOW_FOLEY);
            }
            return petList;
        }   //Get list data pet cat tru o nha, neu khong co pet nao thi tra ve List<PetID> co count = 0, ko tra ve null
        public void ReleasePet(PetID petID)
        {
            int petReleaseIntID = 0;
            if (petID == PetID.NONE)
            {
                petReleaseIntID = 0;
            }
            else if (petID == PetID.BLUE_FOLEY)
            {
                petReleaseIntID = 1;
            }
            else if (petID == PetID.GREEN_FOLEY)
            {
                petReleaseIntID = 2;
            }
            else if (petID == PetID.PINK_FOLEY)
            {
                petReleaseIntID = 3;
            }
            else if (petID == PetID.RED_FOLEY)
            {
                petReleaseIntID = 4;
            }
            else if (petID == PetID.YELLOW_FOLEY)
            {
                petReleaseIntID = 5;
            }

            if (petReleaseIntID != 0)
            {
                PetAtHomes[petReleaseIntID - 1]--;
                if (PetAtHomes[petReleaseIntID - 1] < 0)
                    PetAtHomes[petReleaseIntID - 1] = 0;
            }
        }
        public int GetPetNumber(PetID petID)
        {
            if (petID == PetID.BLUE_FOLEY)
            {
                return PetAtHomes[0];
            }
            else if (petID == PetID.GREEN_FOLEY)
            {
                return PetAtHomes[1];
            }
            else if (petID == PetID.PINK_FOLEY)
            {
                return PetAtHomes[2];
            }
            else if (petID == PetID.RED_FOLEY)
            {
                return PetAtHomes[3];
            }
            else if (petID == PetID.YELLOW_FOLEY)
            {
                return PetAtHomes[4];
            }
            else
            {
                Debug.Log("Invalid pet id");
                return -1;
            }
        }

        public void AddCoin(int val)
        {
            Coin += val;
            if (Coin<0)
            {
                Coin = 0;
            }
            CoinDisplay.Instance?.UpdateCoin();
        }
        public void SetCoin(int val)
        {
            Coin = val;
            CoinDisplay.Instance?.UpdateCoin();
        }

        public void AddSceneTypePass(SceneType sceneType)
        {
            if (FindSceneTypePass(sceneType)) return;
            SceneTypePassList.Add(sceneType);
        }
        public bool FindSceneTypePass(SceneType sceneType)
        {
            foreach (var sceneTypePass in SceneTypePassList)
            {
                if (sceneType == sceneTypePass) return true;
            }
            return false;
        }

        void CheckNull(ref List<bool> chestList, int num)
        {
            if (chestList == null || chestList.Count == 0)
            {
                chestList = new List<bool>();
                for (int i = 0; i <num; i++)
                {
                    chestList.Add(false);
                }
            }
        }
        public void InitChestAndDoorList()
        {
            if (isInitChestAndDoorList) return;
            isInitChestAndDoorList = true;

            CheckNull(ref ChestListBegin, GameConstants.NUM_CHEST_BEGIN_AREA);
            CheckNull(ref ChestListArea1, GameConstants.NUM_CHEST_AREA_1);
            CheckNull(ref ChestListArea2, GameConstants.NUM_CHEST_AREA_2);
            CheckNull(ref ChestListArea3, GameConstants.NUM_CHEST_AREA_3);
            CheckNull(ref ChestListArea4, GameConstants.NUM_CHEST_AREA_4);
            CheckNull(ref ChestListArea5, GameConstants.NUM_CHEST_AREA_5);

            CheckNull(ref DoorListBegin, GameConstants.NUM_DOOR_BEGIN_AREA);
            CheckNull(ref DoorListArea1, GameConstants.NUM_DOOR_AREA_1);
            CheckNull(ref DoorListArea2, GameConstants.NUM_DOOR_AREA_2);
            CheckNull(ref DoorListArea3, GameConstants.NUM_DOOR_AREA_3);
            CheckNull(ref DoorListArea4, GameConstants.NUM_DOOR_AREA_4);
            CheckNull(ref DoorListArea5, GameConstants.NUM_DOOR_AREA_5);
        }
        public void SetChest(AreaType areaType, int id, bool isOpen = true)
        {
            InitChestAndDoorList();

            if (areaType == AreaType.BEGIN_AREA)
            {
                ChestListBegin[id] = isOpen;
            }
            else if (areaType == AreaType.AREA_1)
            {
                if (ChestListArea1 == null)
                {
                    ChestListArea1 = new List<bool>();
                    for (int i = 0; i < 10; i++)
                    {
                        ChestListArea1.Add(false);
                    }
                }
                ChestListArea1[id] = isOpen;
            }
            else if (areaType == AreaType.AREA_2)
            {
                ChestListArea2[id] = isOpen;
            }
            else if (areaType == AreaType.AREA_3)
            {
                ChestListArea3[id] = isOpen;
            }
            else if (areaType == AreaType.AREA_4)
            {
                ChestListArea4[id] = isOpen;
            }
            else if (areaType == AreaType.AREA_5)
            {
                ChestListArea5[id] = isOpen;
            }
        }
        public bool GetOpenChest(AreaType areaType, int id)
        {
            InitChestAndDoorList();

            if (areaType == AreaType.BEGIN_AREA)
            {
                return ChestListBegin[id];
            }
            else if (areaType == AreaType.AREA_1)
            {
                return ChestListArea1[id];
            }
            else if (areaType == AreaType.AREA_2)
            {
                return ChestListArea2[id];
            }
            else if (areaType == AreaType.AREA_3)
            {
                return ChestListArea3[id];
            }
            else if (areaType == AreaType.AREA_4)
            {
                return ChestListArea4[id];
            }
            else if (areaType == AreaType.AREA_5)
            {
                return ChestListArea5[id];
            }
            return false;
        }
        public void SetDoor(AreaType areaType, int id, bool isOpen = true)
        {
            InitChestAndDoorList();

            if (areaType == AreaType.BEGIN_AREA)
            {
                DoorListBegin[id] = isOpen;
            }
            else if (areaType == AreaType.AREA_1)
            {
                DoorListArea1[id] = isOpen;
            }
            else if (areaType == AreaType.AREA_2)
            {
                DoorListArea2[id] = isOpen;
            }
            else if (areaType == AreaType.AREA_3)
            {
                DoorListArea3[id] = isOpen;
            }
            else if (areaType == AreaType.AREA_4)
            {
                DoorListArea4[id] = isOpen;
            }
            else if (areaType == AreaType.AREA_5)
            {
                DoorListArea5[id] = isOpen;
            }
        }
        public bool GetOpenDoor(AreaType areaType, int id)
        {
            InitChestAndDoorList();

            if (areaType == AreaType.BEGIN_AREA)
            {
                return DoorListBegin[id];
            }
            else if (areaType == AreaType.AREA_1)
            {
                return DoorListArea1[id];
            }
            else if (areaType == AreaType.AREA_2)
            {
                return DoorListArea2[id];
            }
            else if (areaType == AreaType.AREA_3)
            {
                return DoorListArea3[id];
            }
            else if (areaType == AreaType.AREA_4)
            {
                return DoorListArea4[id];
            }
            else if (areaType == AreaType.AREA_5)
            {
                return DoorListArea5[id];
            }
            return false;
        }
        public SoundType CurrentSoundMapType()
        {
            SoundType soundType = SoundType.MAIN_MENU;
            if (CurrentAreaType == AreaType.REAL_WORLD_AREA)
            {
                soundType = SoundType.REAL_WORLD_AREA;
            }
            else if (CurrentAreaType == AreaType.BEGIN_AREA)
            {
                soundType = SoundType.BEGIN_AREA;
            }
            else if (CurrentAreaType == AreaType.AREA_1)
            {
                soundType = SoundType.AREA_1;
            }
            else if (CurrentAreaType == AreaType.AREA_2)
            {
                soundType = SoundType.AREA_2;
            }
            else if (CurrentAreaType == AreaType.AREA_3)
            {
                soundType = SoundType.AREA_3;
            }
            else if (CurrentAreaType == AreaType.AREA_4)
            {
                soundType = SoundType.AREA_4;
            }
            else if (CurrentAreaType == AreaType.AREA_5)
            {
                soundType = SoundType.AREA_5;
            }
            return soundType;
        }
        public void SetTrueSpell(SpellType spellType)
        {
            int index = (int)spellType - 1;
            if (index < 0) return;
            GetSpellList[index] = true;
        }
        #endregion
    }

    [Serializable]
    public struct GameDataOrigin
    {
        public bool IsHaveSaveData;
        public int Coin;
        public string PlayerName;

        #region stat
        public float PlayerHp, PlayerStamina;
        public float PlayerPosX, PlayerPosY;
        public int PlayerLevel, PlayerExp;
        public int AtkBonus, DefBonus, HpBonus, StamBonus;
        #endregion

        public float CamMinX, CamMaxX, CamMinY, CamMaxY;

        #region item
        public List<string> PlayerWeaponIDList;
        public List<string> PlayerIngredientIDList;
        public List<string> PlayerKeyItemIDList;
        public List<string> PlayerClothesIDList;
        public List<string> PlayerUseableItemIDList;

        public List<int> WeaponAmountList;
        public List<int> ClothesAmountList;
        public List<int> UseableItemAmountList;
        public List<int> IngredientAmountList;
        public List<int> KeyItemAmountList;

        public List<string> SeenItemIDList; //Hien thi nhung item khong con "new"
        #endregion

        #region craft
        public List<string> PlayerWeaponCraftIDList;
        public List<string> PlayerIngredientCraftIDList;
        public List<string> PlayerKeyItemCraftIDList;
        public List<string> PlayerClothesCraftIDList;
        public List<string> PlayerUseableItemCraftIDList;

        public List<string> SeenCraftIDList; //Hien thi nhung item craft khong con "new"
        #endregion

        #region carry
        public List<string> CarrySlotIDList;
        #endregion

        #region equip
        public string HelmetEquipID;
        public string ArmorEquipID;
        public string WeaponEquipID;
        public string ShoeEquipID;
        #endregion

        #region spell
        public List<int> SpellLevelList;
        public List<bool> SeenSpellList; // List cac spell khong con "new"
        public List<bool> GetSpellList; // List cac spell da duoc nhin thay
        public int CurrentSpellID;
        #endregion

        #region cutscene
        public bool CutsceneFirstComeBookWorld, CutsceneFirstMeetMysteriousMan, CutsceneFirstBattleSlime;
        public bool CutsceneMeetHouse, CutsceneInHouse;
        public bool CutsceneLearnSpell;
        public bool CutsceneSeeBoss1Gate;
        public bool CutsceneSeeDashRing;
        public bool CutsceneTalkBlackCat, CutsceneBackageCat;
        public bool CutsceneMeetBoss1;
        public bool CutsceneDefeatBoss1, CutsceneEndChapter1;
        public bool EventSunFlowerCheckIn, EventDeadGlassFish, EventDeadFishVanish;
        public bool CutsceneBreakStoneToArea2, CutsceneFirstHearForestEcho, CutsceneSecondHearForestEcho, CutsceneGetBow, CutsceneCureGreatTree;
        #endregion

        #region pet
        public int PetFollowIntID;
        public List<int> PetAtHomes;
        #endregion

        public bool isPlayerHaveWeapon, isMysteriousManFollowing, isPlayerHaveMap;
        public bool isPlayerHaveSpell, isPlayerHaveDash;

        #region scene
        public int CurrentSceneTypeID, TargetSceneTypeID;
        public List<int> SceneTypePassIDList;
        public int CurrentAreaTypeID;
        #endregion

        #region chest and door
        public List<bool> ChestListBegin;
        public List<bool> ChestListArea1;
        public List<bool> ChestListArea2;
        public List<bool> ChestListArea3;
        public List<bool> ChestListArea4;
        public List<bool> ChestListArea5;

        public List<bool> DoorListBegin;
        public List<bool> DoorListArea1;
        public List<bool> DoorListArea2;
        public List<bool> DoorListArea3;
        public List<bool> DoorListArea4;
        public List<bool> DoorListArea5;
        #endregion
    }

    [Serializable]
    public struct CacheData
    {
        public Vector2 playerInitPos;
        public bool isChangeCameraBound;
        public float camMinX, camMaxX, camMinY, camMaxY;
    }
}
