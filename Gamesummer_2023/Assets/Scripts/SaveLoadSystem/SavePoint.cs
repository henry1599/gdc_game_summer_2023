using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;
using Gameplay;

namespace GDC.Managers
{
    public class SavePoint : MonoBehaviour
    {
        [SerializeField] Animator bubbleChatAnim;
        [SerializeField] RectTransform savePanel, savingPanel;
        [SerializeField] GameObject ball;
        bool isPlayerNear, isBallUp, stop;
        void Start()
        {
            var thisCam = CameraController.Instance.gameObject.GetComponent<Camera>();
            GetComponentInChildren<Canvas>().worldCamera = thisCam;
        }
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.F))
            {
                CheckSave();
            }
            if (!isBallUp)
            {
                isBallUp = true;
                ball.transform.DOLocalMoveY(0.33f, 1.5f).OnComplete(() => 
                    ball.transform.DOLocalMoveY(0.45f, 1.5f).OnComplete(() => isBallUp = false)
                );
            }
        }
        void CheckSave()
        {
            if (isPlayerNear && !stop)
            {
                SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_START_UI);
                savePanel.DOAnchorPosY(0, 0.5f).SetUpdate(true);
                Time.timeScale = 0;
                stop = true;
            }
        }
        void BubbleChatShow()
        {
            bubbleChatAnim.gameObject.SetActive(true);
            bubbleChatAnim.Play("PressF");
            bubbleChatAnim.transform.DOScaleX(0.6f, 0.15f);
            bubbleChatAnim.transform.DOScaleY(1.4f, 0.15f).OnComplete(() =>
            {
                bubbleChatAnim.transform.DOScale(1f, 0.1f);
            });
        }
        void BubbleChatHide()
        {
            bubbleChatAnim.transform.DOScaleX(0.6f, 0.1f);
            bubbleChatAnim.transform.DOScaleY(1.4f, 0.1f).OnComplete(() =>
            {
                bubbleChatAnim.transform.DOScale(0f, 0.15f).OnComplete(() =>
                {
                    if (!isPlayerNear)
                    {
                        bubbleChatAnim.transform.gameObject.SetActive(false);
                    }
                });
            });
        }
        void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.gameObject.CompareTag("Player"))
            {
                isPlayerNear = true;
                BubbleChatShow();
            }
        }
        void OnCollisionExit2D(Collision2D collision)
        {
            if (collision.gameObject.CompareTag("Player"))
            {
                BubbleChatHide();
                isPlayerNear = false;
            }
        }
        public void ExitSavePanel()
        {
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_END_UI);
            Time.timeScale = 1;
            savePanel.DOAnchorPosY(-1000, 0.5f).SetUpdate(true);
            stop = false;
        }
        public void Save()
        {
            // print("SAVE");
            SaveLoadManager.Instance.Save();
            savePanel.DOAnchorPosY(-1000, 0.5f).SetUpdate(true);
            SavingAnim();
            savingPanel.DOAnchorPosX(-150, 0.5f).SetUpdate(true).OnComplete(
                () => savingPanel.DOAnchorPosX(500, 0.5f).SetUpdate(true).SetDelay(1.5f).SetEase(Ease.OutBounce)
                    .OnComplete(() => 
                    {
                        Time.timeScale = 1;
                        SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_SAVE);
                        // SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_END_UI);
                        stop = false;
                    }
                )
            );
        }
        void SavingAnim()
        {
            float delay = 0.3f;
            for (int i=0; i<4; i++)
            {
                for (int j=0; j<3; j++)
                {
                    DOTween.To(() => 0, value => {}, 0, 0).OnComplete(
                        () => savingPanel.GetComponentInChildren<TMP_Text>().text += "."
                    ).SetDelay(delay).SetUpdate(true);

                    delay += 0.3f;
                }

                DOTween.To(() => 0, value => {}, 0, 0).OnComplete(
                        () => savingPanel.GetComponentInChildren<TMP_Text>().text = "Đang lưu "
                    ).SetDelay(delay).SetUpdate(true);

                delay += 0.3f;
            }
        }
    }
}
