using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetMovingBG : MonoBehaviour
{
    [SerializeField] float speedX, speedY, xMinReset, xMaxReset, yMinReset, yMaxReset;

    // Update is called once per frame
    void Update()
    {
        transform.Translate(new Vector3(speedX, speedY, 0) * Time.deltaTime);

        if (transform.position.x > xMaxReset)
        {
            transform.position = new Vector3(xMinReset, transform.position.y, 0);
        }
        else if (transform.position.x < xMinReset)
        {
            transform.position = new Vector3(xMaxReset, transform.position.y, 0);
        }

        if (transform.position.y > yMaxReset)
        {
            transform.position = new Vector3(transform.position.x, yMinReset, 0);
        }
        else if (transform.position.y < yMinReset)
        {
            transform.position = new Vector3(transform.position.x, yMaxReset, 0);
        }
    }
}
