using Gameplay;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GDC.Gameplay
{
    public class BGMove : MonoBehaviour
    {
        public float moveSpeedX;
        public float moveSpeedY;

        Vector3 oldPos, oldCamPos;

        // Start is called before the first frame update
        void Start()
        {
            oldPos = transform.position;
            oldCamPos = CameraController.Instance.transform.position;
        }

        // Update is called once per frame
        void Update()
        {

            //Debug.Log(vxCamera);
            Vector3 target = CameraController.Instance.transform.position;
            target.z = 0;

            Vector3 addPos; //do doi cua BG so voi oldPos
            addPos.z = 0;
            addPos.x = ((target.x - oldCamPos.x) * moveSpeedX);
            addPos.y = (target.y - oldCamPos.y) * moveSpeedY;

            transform.position = oldPos /*+ target - oldCamPos*/ + addPos;
        }
    }
}
