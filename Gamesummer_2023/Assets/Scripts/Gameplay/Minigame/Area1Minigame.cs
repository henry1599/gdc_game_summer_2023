using DG.Tweening;
using GDC.Enums;
using GDC.Managers;
using GDC.PlayerManager;
using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace GDC.Minigame
{
    public class Area1Minigame : MonoBehaviour
    {
        #region cloud
        [System.Serializable]
        class Cloud
        {
            public Transform layer;
            public int numberOfSetClouds;
            public int[] numberOfClouds = new int[2];
            public Sprite[] cloudSprites;
            public List<Transform> clouds = new List<Transform>();
            public float duration;
            //[HideInInspector] public Tween cloudTween;
        }

        [System.Serializable]
        enum LayerID
        {
            HIGH,
            MEDIUM,
            LOW,
            ALL,
        }

        [SerializeField, Foldout("Cloud")] Cloud highLayer, mediumLayer, lowLayer;
        [SerializeField, Foldout("Cloud")] LayerID specifiedCloudGenerator;
        [SerializeField, Foldout("Cloud")] float setCloudDistance;
        [Button]
        void GenerateCloud()
        {
            switch (specifiedCloudGenerator)
            {
                case LayerID.HIGH:
                    GenerateSpecifiedCloudLayer(highLayer);
                    break;
                case LayerID.MEDIUM:
                    GenerateSpecifiedCloudLayer(mediumLayer);
                    break;
                case LayerID.LOW:
                    GenerateSpecifiedCloudLayer(lowLayer);
                    break;
                case LayerID.ALL:
                    GenerateSpecifiedCloudLayer(highLayer);
                    GenerateSpecifiedCloudLayer(mediumLayer);
                    GenerateSpecifiedCloudLayer(lowLayer);
                    break;
            }

            void GenerateSpecifiedCloudLayer(Cloud cloud)
            {
                if (cloud.numberOfSetClouds <= 0)
                {
                    print("Check number of set clouds");
                    return;
                }

                Transform template = Instantiate(cloud.layer.GetChild(0));

                cloud.clouds.Clear();
                while (cloud.layer.childCount > 0)
                    DestroyImmediate(cloud.layer.GetChild(0).gameObject);


                for (int set = 0; set < cloud.numberOfSetClouds; set++)
                {
                    Transform cloudSet = Instantiate(template, cloud.layer);
                    cloudSet.localPosition = new Vector2(0, -set * setCloudDistance);
                    cloudSet.name = "Cloud Set";
                    cloud.clouds.Add(cloudSet);

                    SpriteRenderer cloudTemplate = Instantiate(cloudSet.GetChild(0).GetComponent<SpriteRenderer>());

                    while (cloudSet.childCount > 0)
                        DestroyImmediate(cloudSet.GetChild(0).gameObject);

                    int cloudNum = Random.Range(cloud.numberOfClouds[0], cloud.numberOfClouds[1] + 1);
                    for (int i = 0; i < cloudNum; i++)
                    {
                        SpriteRenderer clone = Instantiate(cloudTemplate, cloudSet);
                        clone.transform.localPosition = new Vector2(Random.Range(-7, 7), Random.Range(-3.5f, 3.5f));
                        clone.sprite = cloud.cloudSprites[Random.Range(0, cloud.cloudSprites.Length)];
                        clone.name = "Cloud";
                        if (cloud == highLayer)
                        {
                            clone.color = new Color(1, 1, 1, 100 / 255f);
                            for (int idx = 0; idx < 5; idx++)
                            {
                                SpriteRenderer fuseClone = Instantiate(cloudTemplate, cloudSet);
                                fuseClone.name = "Cloud";
                                fuseClone.sprite = cloud.cloudSprites[Random.Range(0, cloud.cloudSprites.Length)];
                                fuseClone.color = new Color(1, 1, 1, 100 / 255f);
                                float range = Random.Range(1, 1.2f);
                                fuseClone.transform.localPosition = clone.transform.localPosition + new Vector3(range* Mathf.Cos(45 * idx * Mathf.Deg2Rad), range * Mathf.Sin(45 * idx * Mathf.Deg2Rad));
                            }
                        }    
                    }

                    DestroyImmediate(cloudTemplate.gameObject);
                }

                DestroyImmediate(template.gameObject);
            }
        }

        void CloudMove()// call once at start
        {
            CloudMoveSpecified(highLayer);
            CloudMoveSpecified(mediumLayer);
            CloudMoveSpecified(lowLayer);

            void CloudMoveSpecified(Cloud cloud)
            {
                int cloudIdx = Random.Range(0, cloud.clouds.Count);
                Transform currentCloud = cloud.clouds[cloudIdx];
                cloud.clouds.Remove(currentCloud);

                currentCloud.localPosition = new Vector3(0, -setCloudDistance);
                currentCloud.DOMoveY(0, cloud.duration).SetEase(Ease.Linear).OnComplete(() =>
                {
                    CloudMoveSpecified(cloud);
                    currentCloud.DOMoveY(setCloudDistance, cloud.duration).SetEase(Ease.Linear).OnComplete(() =>
                    {
                        cloud.clouds.Add(currentCloud);
                    });
                });

            }
        }

        void SetUpCloud()
        {
            foreach (Transform cloud in highLayer.clouds)
                cloud.localPosition = new Vector3(0, -setCloudDistance);
            foreach (Transform cloud in mediumLayer.clouds)
                cloud.localPosition = new Vector3(0, -setCloudDistance);
            foreach (Transform cloud in lowLayer.clouds)
                cloud.localPosition = new Vector3(0, -setCloudDistance);
            cloudFloor.localPosition = new Vector3(0, -setCloudDistance);
        }

        [SerializeField, Foldout("Cloud")] Transform cloudFloor;
        [SerializeField, Foldout("Cloud")] float cloudDistanceX,cloudDistanceY;
        [SerializeField, Foldout("Cloud")] int numberOfFloor;
        [SerializeField, Foldout("Cloud")] ParticleSystem vfxCloud;
        bool hitCloudFloor = false;

        [Button]
        void GenerateCloudFloor()
        {
            GameObject template = cloudFloor.GetChild(0).gameObject;

            while (cloudFloor.childCount > 1)
                DestroyImmediate(cloudFloor.GetChild(1).gameObject);

            float startPosY = -cloudDistanceY * numberOfFloor / 2 + ((numberOfFloor % 2 == 0) ? cloudDistanceY/2: 0);

            for (int i = 0; i < numberOfFloor; i++)
            {
                float x = (i % 2 == 0) ? -6.5f : -7;
                for (;x<=6.5f;x+=cloudDistanceX)
                {
                    GameObject clone = Instantiate(template, cloudFloor);
                    clone.name = "Cloud";
                    clone.transform.localPosition = new Vector2(x, startPosY + cloudDistanceY * i);
                    clone.GetComponent<SpriteRenderer>().sprite = highLayer.cloudSprites[Random.Range(0, highLayer.cloudSprites.Length)];
                }
            }

            DestroyImmediate(template);
        }
        #endregion

        #region player
        [SerializeField, Foldout("Player")] Player player;
        [SerializeField, Foldout("Player")] Rigidbody2D playerRigid;
        [SerializeField, Foldout("Player")] float fallDuration;
        [SerializeField, Foldout("Player")] float playerRotation;
        [SerializeField, Foldout("Player")] float playerSpeed;
        [SerializeField, Foldout("Player")] float limitRange;

        void PlayerMove()
        {
            var horizontalDirection = Input.GetAxisRaw("Horizontal");
            if (horizontalDirection > 0)
            {
                if (player.transform.position.x > limitRange)
                    return;
                player.graphicContainTransform.rotation = Quaternion.Euler(0, 0, -playerRotation);
                player.graphicOfPlayer.transform.localScale = new Vector3(1, 1, 1);
            }
            if (horizontalDirection < 0)
            {
                if (player.transform.position.x < -limitRange)
                    return;
                player.graphicContainTransform.rotation = Quaternion.Euler(0, 0, playerRotation);
                player.graphicOfPlayer.transform.localScale = new Vector3(-1, 1, 1);
            }
            player.transform.position += new Vector3(horizontalDirection, 0, 0) * playerSpeed * Time.deltaTime;
        }
        #endregion

        #region sky
        [SerializeField, Foldout("Sky")] Transform earth, mars, moon,mountain;
        [SerializeField,Foldout("Sky")] Color spaceColor, skyColor;
        [SerializeField, Foldout("Sky")] float changeColorDuration;
        [SerializeField, Foldout("Sky")] SpriteRenderer skyBackground;
        void SkyAnimation()
        {
            mars.DOMoveY(0, minigameDuration / 6).SetEase(Ease.Linear).OnComplete(() =>
            {
                mars.DOMoveY(setCloudDistance, minigameDuration / 6).SetEase(Ease.Linear);
                moon.DOMoveY(0, minigameDuration / 6).SetEase(Ease.Linear).OnComplete(() =>
                {
                    moon.DOMoveY(setCloudDistance, minigameDuration / 6).SetEase(Ease.Linear);
                    earth.DOMoveY(0, minigameDuration / 6).SetEase(Ease.Linear).OnComplete(() =>
                    {
                        earth.DOMoveY(setCloudDistance, minigameDuration / 6).SetEase(Ease.Linear).OnComplete(() =>
                        {
                            cloudFloor.DOMoveY(0,changeColorDuration/2).SetEase(Ease.Linear).OnComplete(() =>
                            {
                                cloudFloor.DOMoveY(setCloudDistance, changeColorDuration / 2).SetEase(Ease.Linear);
                                vfxCloud.Play();
                                hitCloudFloor = true;
                            });
                            skyBackground.DOColor(skyColor, changeColorDuration).SetEase(Ease.Linear).OnComplete(() =>
                            {
                                
                            });
                        });
                    });
                });
            });
        }

        void SetUpSky()
        {
            skyBackground.color = spaceColor;
            mountain.localPosition = new Vector3(0, -setCloudDistance);
            mars.localPosition = new Vector3(mars.localPosition.x, -setCloudDistance);
            moon.localPosition = new Vector3(moon.localPosition.x, -setCloudDistance);
            earth.localPosition = new Vector3(earth.localPosition.x, -setCloudDistance);
        }
        #endregion

        #region coin 
        [SerializeField, Foldout("Coin")] Transform coinContainer;
        [SerializeField, Foldout("Coin")] float coinDistance;
        [SerializeField, Foldout("Coin")] ParticleSystem lostCoinEffect;
        [SerializeField, Foldout("Coin")] int moneyLost;

        [Button]
        void ExpandCoin()
        {
            for (int i = 1; i < coinContainer.childCount; i++)
            {
                Vector3 lastCoin = coinContainer.GetChild(i - 1).localPosition;
                float distanceX = Random.Range(-1, 2) * coinDistance;
                Vector3 tempPos = lastCoin - new Vector3(distanceX, coinDistance);
                if (tempPos.x < -6.5f || tempPos.x > 6.5f)
                    tempPos = lastCoin - new Vector3(0, coinDistance);
                coinContainer.GetChild(i).localPosition = tempPos;
            }
        }

        public void LostCoin()
        {
            SaveLoadManager.Instance.GameData.AddCoin(-moneyLost);
            ParticleSystem effect = Instantiate(lostCoinEffect, player.transform.position,Quaternion.identity);
            effect.Play();
        }
        
        #endregion

        public static Area1Minigame Instance { get; private set; }
        [SerializeField] Transform objectContainer;
        [SerializeField] float objectMove;
        [SerializeField] float minigameDuration;
        [SerializeField] TMP_Text timeText;
        bool enableGame = false;
        float timeLeft;

        private void Awake()
        {
            Instance = this;
        }
        void ObjectMove()
        {
            objectContainer.DOMoveY(objectMove, minigameDuration - 5).SetEase(Ease.Linear).OnComplete(() =>
            {
                mountain.DOMoveY(-1.5f, 5).SetEase(Ease.Linear);
                player.transform.DOMoveY(-5, fallDuration).SetEase(Ease.Linear).SetDelay(5 - fallDuration).OnStart(() =>
                {
                    playerRigid.constraints = RigidbodyConstraints2D.None;
                }).OnComplete(() =>
                {
                    //GameManager.Instance.LoadSceneManually(Enums.SceneType.BEGIN_AREA_3, Enums.TransitionType.DOWN);
                    GameManager.Instance.LoadSceneAsyncManually(
                        SceneType.BEGIN_AREA_3,
                        TransitionType.DOWN,
                        SoundType.BEGIN_AREA,
                        cb: () =>
                        {
                            GameManager.Instance.UnloadSceneManually(
                                SaveLoadManager.Instance.GameData.CurrentSceneType,
                                cb: () => GameManager.Instance.SetInitData()
                            );
                        });
                });
            });
        }

        void UpdateTime()
        {
            timeLeft -= Time.deltaTime;
            if (timeLeft < 0)
                return;
            int minute = Mathf.FloorToInt(timeLeft / 60);
            int second = Mathf.FloorToInt(timeLeft % 60);
            timeText.text = ((minute >= 10) ? "" : "0") + minute.ToString() + ":" + ((second >= 10) ? "" : "0") + second.ToString();
        }

        IEnumerator SetUpMinigame()
        {
            yield return new WaitUntil(() => GameManager.Instance.isLoadSceneComplete); 
            SetUpCloud();
            SetUpSky();
            timeLeft = minigameDuration;
            player.InMinigame = true;
            player.UseHandMovement = false;
            player.UseWeapon = true;
            player.graphicOfPlayer.Play("surprise");
            player.transform.position = new Vector3(0, 4);//top of the screen
            player.graphicOfPlayer.transform.localScale = new Vector3(-1, 1, 1);
            player.transform.DORotate(new Vector3(0, 0, playerRotation), fallDuration).SetEase(Ease.Linear);
            player.transform.DOMoveY(0, fallDuration).SetEase(Ease.Linear).OnComplete(() =>
            {
                playerRigid.constraints = RigidbodyConstraints2D.FreezePositionY;
                enableGame = true;
                CloudMove();
                SkyAnimation();
                ObjectMove();
            });
        }

        private void Start()
        {
            StartCoroutine(SetUpMinigame());
        }

        private void Update()
        {
            if (enableGame)
            {
                PlayerMove();
                UpdateTime();
            }
            if (hitCloudFloor)
                vfxCloud.transform.position = player.transform.position;
        }
    }
}
