using DG.Tweening;
using GDC.Configuration;
using GDC.Gameplay.UI;
using GDC.Managers;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

namespace GDC.Gameplay
{
    public class ReceiveCoin : MonoBehaviour
    {
        [SerializeField] Dialogue notEnoughMoney;
        [SerializeField] Animator npcAnim;

        [Header("Coin")]
        [SerializeField] int cost;      
        [SerializeField] bool isReceiveRemainMoney;

        [Header("Item (Require use 1 type of item")]
        [SerializeField] List<SO_Item> items;
        [SerializeField] List<int> itemNumbers;

        public void ReceiveMoney() //Ham nay duoc goi bang event trong npc
        {
            if (SaveLoadManager.Instance.GameData.Coin >= cost)
            {
                SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_PURCHASE);
                SaveLoadManager.Instance.GameData.AddCoin(-cost);
            }
            else
            {
                SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_ERROR);
                TriggerDialogue.Instance.EndDialogue();
                TriggerDialogue.Instance.SetDialogue(notEnoughMoney);
                if (npcAnim != null)
                {
                    npcAnim.Play("talk");
                }
                StartCoroutine(Cor_EndTalk());
                if (isReceiveRemainMoney)
                {
                    SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_PURCHASE);
                    SaveLoadManager.Instance.GameData.SetCoin(0);
                }
            }
        }
        public bool CheckEnoughItem()
        {
            for (int i = 0; i < items.Count; i++) 
            {
                if (SaveLoadManager.Instance.GameData.GetAmountOfItem(items[i]) < itemNumbers[i]) return false;
            }
            return true;
        }
        public void ReceiveItem()
        {
            if (CheckEnoughItem())
            {
                for (int i = 0; i < items.Count; i++)
                {
                    SaveLoadManager.Instance.GameData.ReduceItem(items[i], itemNumbers[i]);
                }
            }    
            else
            {
                SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_ERROR);
                TriggerDialogue.Instance.EndDialogue();
                TriggerDialogue.Instance.SetDialogue(notEnoughMoney);
                if (npcAnim != null)
                {
                    npcAnim.Play("talk");
                }
                StartCoroutine(Cor_EndTalk());
            }    
        }
        IEnumerator Cor_EndTalk()
        {
            yield return new WaitUntil(() => TriggerDialogue.Instance.isEndDialogue);
            if (npcAnim != null)
            {
                npcAnim.Play("idle");
            }
        }
    }
}
