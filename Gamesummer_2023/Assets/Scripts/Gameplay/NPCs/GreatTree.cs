using GDC.Managers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GDC.Gameplay
{
    public class GreatTree : MonoBehaviour
    {
        [SerializeField] Animator graphic;
        void Start()
        {
            if (SaveLoadManager.Instance.GameData.CutsceneCureGreatTree)
                graphic.SetBool("isSick", false);
        }
    }
}