using DG.Tweening;
using GDC.Configuration;
using GDC.Gameplay.UI;
using GDC.PlayerManager;
using GDC.PlayerManager.Hand;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Events;

namespace GDC.Gameplay
{
    public class NPC : MonoBehaviour
    {
        [SerializeField] Dialogue dialogue;
        [SerializeField] Animator bubbleChatAnim;
        [SerializeField] List<SO_Item> shop_so_items;
        //[SerializeField] UnityEvent EndTalkEvent;

        [Header("Behavious")]
        public Transform graphicTrans;
        public Animator anim;
        [SerializeField] float speed;
        [SerializeField] bool isNotTurnCharacter, isInitTurnRight, isMoveCharacter, isSpumCharacter, isInteractObject;
        bool isMoving, isPlayerNear = false, playerUseHandMovement;
        Vector3 initPos, target;
        Coroutine moveCor;

        private void Start()
        {
            initPos = transform.position;
            target = initPos;
            if (isMoveCharacter)
                moveCor = StartCoroutine(Cor_Move());
        }
        // Update is called once per frame
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.F))
            {
                Talk();
            }

            if (isMoveCharacter && isMoving)
            {
                Move();
            }
        }

        void Move()
        {
            transform.position = Vector3.MoveTowards(transform.position, target, speed * Time.deltaTime);
        }
        public void SetDirect(Vector3 target)
        {
            if (graphicTrans == null || isNotTurnCharacter == true) return;

            if (target.x > transform.position.x)
            {
                if (isInitTurnRight)
                    graphicTrans.localScale = new Vector3(1, 1, 1);
                else
                    graphicTrans.localScale = new Vector3(-1, 1, 1);
            }
            else
            {
                if (isInitTurnRight)
                    graphicTrans.localScale = new Vector3(-1, 1, 1);
                else
                {
                    graphicTrans.localScale = new Vector3(1, 1, 1);
                }
            }
        }
        public void MoveToPos(Vector3 targetPos, float duration = 2f)
        {
            SetDirect(targetPos);
            if (isSpumCharacter)
            {
                anim.SetFloat("RunState", 0.1f);
            }
            else
            {
                anim.Play("move");
            }
            transform.DOMove(targetPos, duration).SetEase(Ease.Linear).OnComplete(() =>
            {
                if (isSpumCharacter)
                {
                    anim.SetFloat("RunState", 0f);
                }
                else
                {
                    anim.Play("idle");
                }
            });
        }
        public void SetDialogue(Dialogue newDialogue)
        {
            this.dialogue = newDialogue;
        }
        public Dialogue GetDialogue()
        {
            return dialogue;
        }
        void Talk()
        {
            if (TriggerDialogue.Instance.isEndDialogue == false || isPlayerNear == false) return;
            if (Player.Instance.InCutScene) return;

            TriggerDialogue.Instance.SetDialogue(dialogue);
            ShopManager.Instance.LoadShopItem(shop_so_items);
            if (Player.Instance != null)
            {
                SetDirect(Player.Instance.transform.position);
                playerUseHandMovement = Player.Instance.UseHandMovement;
                Player.Instance.UseHandMovement = false;
                Player.Instance.InCutScene = true;
                Player.Instance.ToIdle();
                Player.Instance.coll.enabled = false;
                StartCoroutine(Cor_LoadPlayerDirect());
            }
            if (anim != null)
            {
                if (isSpumCharacter)
                {
                    anim.SetFloat("RunState", 0.1f);
                }
                else
                {
                    anim.Play("talk");
                }
            }

            if (moveCor != null)
                StopCoroutine(moveCor);
            StartCoroutine(Cor_EndTalk());
        }
        IEnumerator Cor_LoadPlayerDirect()
        {
            yield return new WaitUntil(() => !HandMovement.Instance.gameObject.activeInHierarchy);
            if (Player.Instance.transform.position.x > transform.position.x)
            {
                Player.Instance.graphicOfPlayer.transform.localScale = new Vector3(-1, 1, 1);
            }
            else
            {
                Player.Instance.graphicOfPlayer.transform.localScale = new Vector3(1, 1, 1);
            }
        }
        IEnumerator Cor_EndTalk()
        {
            yield return new WaitUntil(() => TriggerDialogue.Instance.isEndDialogue == true);
            Debug.Log("EndDialog");

            Player.Instance.UseHandMovement = playerUseHandMovement;
            Player.Instance.InCutScene = false;
            Player.Instance.SetIsCanGetHit();
            Player.Instance.coll.enabled = true;
            if (isMoveCharacter)
                moveCor = StartCoroutine(Cor_Move());
            if (anim != null)
            {
                if (isSpumCharacter)
                {
                    anim.SetFloat("RunState", 0f);
                }
                else
                {
                    anim.Play("idle");
                }
            }

            //EndTalkEvent?.Invoke();
        }
        IEnumerator Cor_Move()
        {
            isMoving = false;
            if (isSpumCharacter)
            {
                anim.SetFloat("RunState", 0);
            }
            else
            {
                //todo
            }
            yield return new WaitForSeconds(Random.Range(2f, 4f));

            isMoving = true;
            if (isSpumCharacter)
            {
                anim.SetFloat("RunState", 0.1f);
            }
            else
            {
                //todo
            }
            target = initPos + new Vector3(Random.Range(-2f, 2f), Random.Range(-2f, 2f), 0);
            SetDirect(target);
            yield return new WaitUntil(() => Vector3.Magnitude(transform.position - target) < 0.2f);

            moveCor = StartCoroutine(Cor_Move());
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.CompareTag("Player"))
            {
                bubbleChatAnim.transform.gameObject.SetActive(true);
                if (isInteractObject)
                    bubbleChatAnim.Play("PressF");
                bubbleChatAnim.transform.DOScaleX(0.6f, 0.15f);
                bubbleChatAnim.transform.DOScaleY(1.4f, 0.15f).OnComplete(() =>
                {
                    if (isSpumCharacter)
                        bubbleChatAnim.transform.DOScale(0.77f, 0.1f);
                    else
                       bubbleChatAnim.transform.DOScale(1f, 0.1f);
                });
                this.isPlayerNear = true;
            }
        }

        private void OnTriggerExit2D(Collider2D collision)
        {
            if (collision.CompareTag("Player"))
            {               
                bubbleChatAnim.transform.DOScaleX(0.6f, 0.1f);
                bubbleChatAnim.transform.DOScaleY(1.4f, 0.1f).OnComplete(() =>
                {
                    bubbleChatAnim.transform.DOScale(0f, 0.15f).OnComplete(()=>
                    {
                        if (this.isPlayerNear == false)
                        {
                            bubbleChatAnim.transform.gameObject.SetActive(false);
                        }
                    });                
                });
                this.isPlayerNear = false;
            }
        }
        private void OnCollisionStay2D(Collision2D collision)
        {
            if (collision.gameObject.CompareTag("Player"))
            {
                target = transform.position;
            }
            else
            {
                target = initPos + new Vector3(Random.Range(-2f, 2f), Random.Range(-2f, 2f), 0);
                SetDirect(target);
            }
        }
    }
}
