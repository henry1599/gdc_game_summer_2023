using GDC.Managers;
using System.Collections;
using UnityEngine;

namespace GDC.Gameplay
{
    public class Following : MonoBehaviour
    {
        public bool isFollowing, isNotLookAtTarget;
        public Transform target;
        [SerializeField] Transform graphic;
        [SerializeField] Animator anim;
        [SerializeField] float speed, range;
        [SerializeField] bool isInitTurnRight;
        [SerializeField] bool isMysteriousMan;

        private void Start()
        {
            if (isMysteriousMan)
            {
                StartCoroutine(Cor_LoadGameData());
            }
        }
        private void Update()
        {
            if (isNotLookAtTarget == false)
            {
                if (target.position.x > transform.position.x)
                {
                    if (isInitTurnRight)
                    {
                        graphic.localScale = new Vector3(1, 1, 1);
                    }
                    else
                    {
                        graphic.localScale = new Vector3(-1, 1, 1);
                    }
                }
                else
                {
                    if (isInitTurnRight)
                    {
                        graphic.localScale = new Vector3(-1, 1, 1);
                    }
                    else
                    {
                        graphic.localScale = new Vector3(1, 1, 1);
                    }
                }
            }

            if (isFollowing)
            {
                FollowTarget();
            }

        }
        void FollowTarget()
        {
            if (target == null) return;
            if (Vector3.SqrMagnitude(target.position - transform.position) > range)
            {
                anim.Play("move");
                transform.position = Vector3.MoveTowards(transform.position, target.position, speed * Time.deltaTime);
            }
            else
            {
                anim.Play("idle");
            }
        }
        IEnumerator Cor_LoadGameData()
        {
            yield return new WaitUntil(() => SaveLoadManager.Instance != null && target != null && GameManager.Instance.isLoadSceneComplete);
            isFollowing = SaveLoadManager.Instance.GameData.isMysteriousManFollowing;
            if (isFollowing)
            {
                transform.localScale = Vector3.one; //for show charactor
                transform.position = target.position + Vector3.left * 2;
            }
        }    
    }
}