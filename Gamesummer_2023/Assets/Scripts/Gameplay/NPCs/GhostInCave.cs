using GDC.PlayerManager;
using System.Collections;
using UnityEngine;
using DG.Tweening;
using NaughtyAttributes;

public class GhostInCave : MonoBehaviour
{
    [SerializeField] SpriteRenderer graphic;
    [SerializeField] TrailRenderer trail1, trail2;
    Transform player, graphicOfPlayer;
    //Tween fadeTween;
    Coroutine transparentCor, trailCor;
    Color whiteClear, semiWhite;

    [ReadOnly]
    public bool isAppear, isLoadPlayerSuccess;

    [SerializeField] float xLeftAppear, xGhostLeftDisappear, xRightAppear, xGhostRightDisappear;
    private void Start()
    {
        isAppear = false;
        isLoadPlayerSuccess = false;
        whiteClear = Color.white;
        whiteClear.a = 0;
        semiWhite = Color.white;
        semiWhite.a = 0.7f;
        trail1.time = 0;
        trail2.time = 0;
        graphic.color = whiteClear;
        StartCoroutine(Cor_LoadPlayer());
    }
    private void Update()
    {
        if (isLoadPlayerSuccess == false) return;

        if (Mathf.Abs(player.position.x - xLeftAppear) < 0.1f || Mathf.Abs(player.position.x - xRightAppear) < 0.1f)
        {
            Appear();
        }
        else if ((player.position.x < xLeftAppear - 2.5f && graphicOfPlayer.transform.localScale.x > 0)
            || (player.position.x > xRightAppear + 2.5f && graphicOfPlayer.transform.localScale.x < 0)
            || transform.position.x < xGhostLeftDisappear || transform.position.x > xGhostRightDisappear
            || (player.position.x < xLeftAppear - 0.8f && transform.position.x - player.position.x < 0.5f)
            || (player.position.x > xRightAppear + 0.8f && player.position.x - transform.position.x < 0.5f)
            || (player.position.x > xLeftAppear + 3 && player.position.x < xRightAppear - 3))
        {
            Disappear();
        }
    }

    void Appear()
    {
        if (isAppear) return;

        isAppear = true;
        
        //if (fadeTween != null)
        //{
        //    fadeTween.Kill();
        //}
        DOTween.Kill(graphic);
        if (transparentCor != null)
        {
            StopCoroutine(transparentCor);
        }
        if (trailCor!=null)
        {
            StopCoroutine(trailCor);
        }

        trailCor = StartCoroutine(Cor_StartTrail());
        graphic.color = Color.white;
        transform.position = player.position;
        transparentCor = StartCoroutine(Cor_TransparentAnim());
    }
    void Disappear()
    {
        if (isAppear == false) return;

        isAppear = false;
        trail1.time = 0;
        trail2.time = 0;
        DOTween.Kill(graphic);
        if (transparentCor != null)
        {
            StopCoroutine(transparentCor);
        }
        if (trailCor != null)
        {
            StopCoroutine(trailCor);
        }
        /*fadeTween = */
        graphic.DOColor(whiteClear, 0.6f);
    }
    IEnumerator Cor_LoadPlayer()
    {
        yield return new WaitUntil(() => Player.Instance != null);
        player = Player.Instance.transform;
        graphicOfPlayer = Player.Instance.graphicOfPlayer.transform;
        isLoadPlayerSuccess = true;
    }
    IEnumerator Cor_TransparentAnim()
    {
        graphic.DOColor(semiWhite, 1f).SetEase(Ease.Linear).OnComplete(() => graphic.DOColor(Color.white, 1).SetEase(Ease.Linear));
        yield return new WaitForSeconds(2);
        transparentCor = StartCoroutine(Cor_TransparentAnim());
    }
    IEnumerator Cor_StartTrail()
    {
        yield return new WaitForSeconds(0.3f);
        trail1.time = 0.5f;
        trail2.time = 0.5f;
    }
}
