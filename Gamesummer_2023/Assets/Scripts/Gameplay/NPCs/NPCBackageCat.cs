using DG.Tweening;
using GDC.Configuration;
using GDC.Gameplay.UI;
using GDC.PlayerManager.Hand;
using GDC.PlayerManager;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.Managers;

namespace GDC.Gameplay
{
    public class NPCBackageCat : MonoBehaviour
    {
        [SerializeField] Dialogue dialogue1, dialogue2;
        [SerializeField] Animator bubbleChatAnim;
        //[SerializeField] UnityEvent EndTalkEvent;

        [Header("Behavious")]
        public Transform graphicTrans;
        public Animator anim;
        bool isPlayerNear = false, playerUseHandMovement;

        // Update is called once per frame
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.F))
            {
                Talk();
            }
        }

        void Talk()
        {
            if (TriggerDialogue.Instance.isEndDialogue == false || isPlayerNear == false) return;
            if (Player.Instance.InCutScene) return;

            if (SaveLoadManager.Instance.GameData.CutsceneBackageCat)
                TriggerDialogue.Instance.SetDialogue(dialogue2);
            else
                TriggerDialogue.Instance.SetDialogue(dialogue1);

            if (Player.Instance != null)
            {
                playerUseHandMovement = Player.Instance.UseHandMovement;
                Player.Instance.UseHandMovement = false;
                Player.Instance.InCutScene = true;
                Player.Instance.ToIdle();
                Player.Instance.coll.enabled = false;
                StartCoroutine(Cor_LoadPlayerDirect());
            }
            if (anim != null)
            {
                anim.Play("talk");
            }

            StartCoroutine(Cor_EndTalk());
        }
        IEnumerator Cor_LoadPlayerDirect()
        {
            yield return new WaitUntil(() => !HandMovement.Instance.gameObject.activeInHierarchy);
            if (Player.Instance.transform.position.x > transform.position.x)
            {
                Player.Instance.graphicOfPlayer.transform.localScale = new Vector3(-1, 1, 1);
            }
            else
            {
                Player.Instance.graphicOfPlayer.transform.localScale = new Vector3(1, 1, 1);
            }
        }
        IEnumerator Cor_EndTalk()
        {
            yield return new WaitUntil(() => TriggerDialogue.Instance.isEndDialogue == true);

            Player.Instance.UseHandMovement = playerUseHandMovement;
            Player.Instance.InCutScene = false;
            Player.Instance.SetIsCanGetHit();
            Player.Instance.coll.enabled = true;
            if (anim != null)
            {
                anim.Play("idle");
            }

            //EndTalkEvent?.Invoke();
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.CompareTag("Player"))
            {
                bubbleChatAnim.transform.gameObject.SetActive(true);
                bubbleChatAnim.transform.DOScaleX(0.6f, 0.15f);
                bubbleChatAnim.transform.DOScaleY(1.4f, 0.15f).OnComplete(() =>
                {
                    bubbleChatAnim.transform.DOScale(1f, 0.1f);
                });
                this.isPlayerNear = true;
            }
        }

        private void OnTriggerExit2D(Collider2D collision)
        {
            if (collision.CompareTag("Player"))
            {
                bubbleChatAnim.transform.DOScaleX(0.6f, 0.1f);
                bubbleChatAnim.transform.DOScaleY(1.4f, 0.1f).OnComplete(() =>
                {
                    bubbleChatAnim.transform.DOScale(0f, 0.15f).OnComplete(() =>
                    {
                        if (this.isPlayerNear == false)
                        {
                            bubbleChatAnim.transform.gameObject.SetActive(false);
                        }
                    });
                });
                this.isPlayerNear = false;
            }
        }
    }
}
