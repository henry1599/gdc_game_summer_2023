using DG.Tweening;
using GDC.Gameplay.UI;
using GDC.Managers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GDC.Gameplay
{
    public class ResetPosition : MonoBehaviour
    {
        [SerializeField] int cost;
        [SerializeField] Transform[] resetObjects;
        [SerializeField] Dialogue notEnoughMoney, notChangePos;
        [SerializeField] Animator npcAnim;
        [SerializeField] List<Vector3> oldPosObjects;
        [SerializeField] bool isReceiveRemainMoney;

        private void Start()
        {
            oldPosObjects = new List<Vector3>();
            for (int i = 0; i < resetObjects.Length; i++)
            {
                oldPosObjects.Add(resetObjects[i].position);
            }
        }
        bool CheckPosChange()
        {
            for (int i = 0; i < resetObjects.Length; i++)
            {
                if (oldPosObjects[i] != resetObjects[i].position) return true;
            }
            return false;
        }
        public void ResetObject() //Ham nay duoc goi bang event trong npc
        {
            if (CheckPosChange())
            {
                if (SaveLoadManager.Instance.GameData.Coin >= cost)
                {
                    //SaveLoadManager.Instance.GameData.Coin -= cost;
                    //CoinDisplay.Instance.UpdateCoin();
                    SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_SAVE);
                    SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_PURCHASE);
                    SaveLoadManager.Instance.GameData.AddCoin(-cost);
                    StartCoroutine(Cor_ResetPos());
                }
                else
                {
                    SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_ERROR);
                    TriggerDialogue.Instance.SetDialogue(notEnoughMoney);
                    npcAnim.Play("talk");
                    StartCoroutine(Cor_EndTalk());
                    if (isReceiveRemainMoney)
                    {
                        //SaveLoadManager.Instance.GameData.Coin = 0;
                        //CoinDisplay.Instance.UpdateCoin();
                        SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_PURCHASE);
                        SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_SAVE);
                        SaveLoadManager.Instance.GameData.SetCoin(0);
                        StartCoroutine(Cor_ResetPos());
                    }
                }
            }
            else
            {
                TriggerDialogue.Instance.SetDialogue(notChangePos);
                npcAnim.Play("talk");
                StartCoroutine(Cor_EndTalk());
            }
        }
        IEnumerator Cor_ResetPos()
        {
            for (int i = 0; i < resetObjects.Length; i++)
            {
                resetObjects[i].gameObject.SetActive(true);
                resetObjects[i].DOScale(0, 0.2f);
            }
            yield return new WaitForSeconds(0.25f);
            for (int i = 0; i < resetObjects.Length; i++)
            {
                resetObjects[i].position = oldPosObjects[i];
                resetObjects[i].DOScale(1.1f, 0.2f);
            }
            yield return new WaitForSeconds(0.2f);
            foreach(var resetObject in resetObjects)
            {
                resetObject.DOScale(1f, 0.1f);
            }
        }
        IEnumerator Cor_EndTalk()
        {
            yield return new WaitUntil(() => TriggerDialogue.Instance.isEndDialogue);
            npcAnim.Play("idle");
        }
    }
}
