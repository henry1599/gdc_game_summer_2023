using DG.Tweening;
using GDC.Managers;
using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

namespace GDC.Environment
{
    public class Bird : MonoBehaviour
    {
        enum BirdType
        {
            NORMAL_BIRD = 0,
            CHESTNUT = 10,
            FISH = 20,
        }
        [SerializeField] Animator anim;
        [SerializeField] SpriteRenderer graphic;
        [SerializeField] Transform birdTransform;
        [SerializeField] BirdType type;
        [SerializeField,ShowIf("type",BirdType.FISH)] Animator wave;

        [SerializeField,InfoBox("Eat or wander or sth...")] float[] waitDelay;
        //[SerializeField, Foldout("Eat")] int[] eatLoopTime;
        Coroutine corDelay;
        //int currentLoop = -1;

        IEnumerator Cor_Delay()
        {
            yield return new WaitForSeconds(Random.Range(waitDelay[0], waitDelay[1]));
            switch (type)
            {
                case BirdType.NORMAL_BIRD:
                    anim.Play("eat");
                    yield return new WaitForSeconds(2 / 3f);
                    corDelay = StartCoroutine(Cor_Delay());
                    break;
                case BirdType.CHESTNUT:
                    isWandering = true;
                    Move();
                    break;
                case BirdType.FISH:
                    anim.Play("swing");
                    yield return new WaitForSeconds(2 / 3f);
                    corDelay = StartCoroutine(Cor_Delay());
                    break;

            }
            
        }

        [SerializeField, Foldout("Move")] float[] moveRange;
        [SerializeField, Foldout("Move")] float timeMultiplier;
        [SerializeField, Foldout("Move")] AnimationCurve flyingCurve;
        [SerializeField, Foldout("Move")] float flyingHeight;
        [SerializeField, Foldout("Move")] bool flyOutOfCamera;
        [SerializeField, Foldout("Move"),ShowIf("flyOutOfCamera")] float flyAwaySpeed;
        [SerializeField, Foldout("Move"), ShowIf("flyOutOfCamera")] float extraDistanceToOutOfCamera = 3;
        bool flyAway = false;
        float flyAwayAngle = 0;
        Vector2 center;
        bool isMoving = false;
        bool isWandering = false;//only for chestnut
        bool isFloating = false; //only for fish

        void Move()
        {
            float angle = Random.Range(-30, 330f);
            float range = Random.Range(moveRange[0], moveRange[1]);
            Vector2 destination = new Vector2(range * Mathf.Cos(angle * Mathf.Deg2Rad), range * Mathf.Sin(angle * Mathf.Deg2Rad));
            

            if (type == BirdType.NORMAL_BIRD)
            {
                if (angle >= -30 && angle < 30)
                {
                    anim.Play("moveLeft");
                    graphic.flipX = true;
                }
                else if (angle >= 30 && angle < 90)
                {
                    anim.Play("moveUp");
                    graphic.flipX = false;
                }
                else if (angle >= 90 && angle < 150)
                {
                    anim.Play("moveUp");
                    graphic.flipX = true;
                }
                else if (angle >= 150 && angle < 210)
                {
                    anim.Play("moveLeft");
                    graphic.flipX = false;
                }
                else if (angle >= 21 && angle < 270)
                {
                    anim.Play("moveDown");
                    graphic.flipX = false;
                }
                else if (angle >= 270 && angle <= 330)
                {
                    anim.Play("moveDown");
                    graphic.flipX = true;
                }
            }
            else
            {
                float angleToDown = angle - 60;
                if (angleToDown >= -90 && angleToDown < 90)
                    graphic.flipX = true;
                else if (angleToDown>=90 && angleToDown <= 270)
                    graphic.flipX = false;
                anim.Play("move");
            }


            if (flyOutOfCamera && !isWandering)
            {
                graphic.transform.DOLocalMoveY(flyingHeight, 1);
                flyAway = true;
                flyAwayAngle = angle;
                SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_BIRD_SHORT_FLY);
            }
            else
            {
                SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_BIRD_FLY);
                float flyingDuration = timeMultiplier * Vector2.Distance(destination, birdTransform.position) / moveRange[1];
                graphic.transform.DOLocalMoveY(flyingHeight, flyingDuration).SetEase(flyingCurve);
                birdTransform.DOMove(center + destination, flyingDuration).SetEase(Ease.Linear).OnComplete(() =>
                {
                    if (gameObject.activeInHierarchy)
                        corDelay = StartCoroutine(Cor_Delay());
                    anim.Play("idle");
                    isMoving = false;
                    isWandering = false;
                    isFloating = false;
                });
            }
        }

        void FlyAway()
        {
            StopAllCoroutines();
            birdTransform.position += new Vector3(flyAwaySpeed * Mathf.Cos(flyAwayAngle * Mathf.Deg2Rad), flyAwaySpeed * Mathf.Sin(flyAwayAngle * Mathf.Deg2Rad));
            // cong tru 1 de dam bao bay ra khoi man hinh
            if (birdTransform.position.y < Camera.main.ViewportToWorldPoint(Vector2.zero).y - extraDistanceToOutOfCamera ||
                birdTransform.position.y > Camera.main.ViewportToWorldPoint(Vector2.one).y + extraDistanceToOutOfCamera ||
                birdTransform.position.x < Camera.main.ViewportToWorldPoint(Vector2.zero).x - extraDistanceToOutOfCamera ||
                birdTransform.position.x > Camera.main.ViewportToWorldPoint(Vector2.one).x + extraDistanceToOutOfCamera)
            {
                birdTransform.gameObject.SetActive(false);
            }
        }

        [SerializeField, Foldout("Trigger"),Tag] string[] triggerTags; 
        private void OnTriggerStay2D(Collider2D collision)
        {
            if (type == BirdType.FISH && collision.CompareTag("Water") && !isMoving && !isFloating)
            {
                isFloating = true;
                wave.enabled = true;
                wave.Play("wave", 0, 0);
            }

            if (isMoving)
                return;
            foreach(string tag in triggerTags)
            {
                if (collision.CompareTag(tag))
                {
                    StopCoroutine(corDelay);
                    Move();
                    isMoving = true;

                }
            }
        }

        private void Start()
        {
            corDelay = StartCoroutine(Cor_Delay());
            center = birdTransform.position;
            if (birdTransform.GetComponent<CircleCollider2D>() == null)
            {
                birdTransform.gameObject.AddComponent<CircleCollider2D>();
                birdTransform.GetComponent<CircleCollider2D>().radius = 2;
                birdTransform.GetComponent<CircleCollider2D>().isTrigger = true;
            }
        }

        private void Update()
        {
            if (flyAway)
                FlyAway();
        }
    }
}