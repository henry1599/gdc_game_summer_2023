using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using GDC.Configuration;
using GDC.Managers;
using GDC.PlayerManager;
using UnityEngine.Events;
using NaughtyAttributes;

public class ItemDrop : MonoBehaviour
{
    [SerializeField] bool isnotHaveDropRange, isDoNotHaveTimeLife;
    [SerializeField] private float dropRange, timeLife, speedToPlayer = 5;
    [SerializeField] bool isCraft;
    [SerializeField, HideIf("isCraft")] SO_Item so_item;
    [SerializeField, ShowIf("isCraft")] SO_Craft so_craft;
    [SerializeField] SpriteRenderer spriteRenderer;
    
    Vector3 dropTargetPos;

    [ReadOnly, SerializeField] bool isCanGetItem;
    [SerializeField] UnityEvent eventAfterGet;
    //bool isSpawned;
    // Start is called before the first frame update
    void Start()
    {
        //isSpawned = false;
        isCanGetItem = true;
        if (isnotHaveDropRange == false)
        {
            dropTargetPos = transform.position + new Vector3(Random.Range(-dropRange, dropRange), Random.Range(-dropRange, dropRange), 0);
            transform.DOMove(dropTargetPos, 0.5f, false);
        }
        if (isDoNotHaveTimeLife == false)
        {
            StartCoroutine(Cor_EndTimeLife());
        }
        StartCoroutine(Cor_CanGetItem());
    }
    IEnumerator Cor_CanGetItem()
    {
        if (isCraft == false)
        {
            yield return new WaitUntil(() => so_item != null);
            if (so_item.itemType == GDC.Enums.ItemType.KEY_ITEM)
            {
                isCanGetItem = false;
            }
        }
        else
        {
            yield return new WaitUntil(() => so_craft != null);
            isCanGetItem = false;
        }

        yield return new WaitForSeconds(1f);
        isCanGetItem = true;
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            if (isCanGetItem)
            {
                transform.position = Vector3.MoveTowards(transform.position, collision.transform.position, speedToPlayer * Time.deltaTime);
                if (Vector3.Magnitude(transform.position - collision.transform.position) < 0.05f) PlayerGetItem();
            }
        }
    }
    void PlayerGetItem()
    {
        //Player get item
        Debug.Log("Player get " + gameObject.name);
        if (so_item != null)
        {
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_LOOT, 20);
            SaveLoadManager.Instance.GameData.AddItem(so_item);
            GetItemDisplayManager.Instance.ShowItem(so_item.sprite, so_item.itemName);
            if (so_item.itemType == GDC.Enums.ItemType.KEY_ITEM)
            {
                Player.Instance.ObtainItem(so_item);
                eventAfterGet?.Invoke();
            }    
        }
        else if (so_craft != null)
        {
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_LOOT, 20);
            SaveLoadManager.Instance.GameData.AddCraft(so_craft);
            GetItemDisplayManager.Instance.ShowItem(spriteRenderer.sprite, so_craft.SO_item.itemName);
            SO_Item so_fake_item = new SO_Item();
            so_fake_item.sprite = spriteRenderer.sprite;
            Player.Instance.ObtainItem(so_fake_item);
            eventAfterGet?.Invoke();
        }
        else
        {
            if (gameObject.CompareTag("Coin"))
            {
                SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_COLLECT_COIN);
                //SaveLoadManager.Instance.GameData.Coin += 10;
                //CoinDisplay.Instance.UpdateCoin();
                SaveLoadManager.Instance.GameData.AddCoin(5);
            }
            else if (gameObject.CompareTag("Heart"))
            {
                Player.Instance.Heal(5, 0);
            }
            else if (gameObject.CompareTag("Stamina"))
            {
                Player.Instance.Heal(0, 5);
            }
        }
        Destroy(gameObject);
    }

    IEnumerator Cor_EndTimeLife()
    {
        yield return new WaitForSeconds(timeLife);
        Destroy(gameObject);
    }
}
