using GDC.Gameplay.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GDC.Gameplay.Cutscene
{
    public class CutSeeBoss1Gate : MonoBehaviour
    {
        [SerializeField] Dialogue[] dialogues;
        [SerializeField] Animator bubbleChatAnim, book;
        [SerializeField] Transform dot;
        [SerializeField] GameObject vfx;
        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.CompareTag("Player"))
            {
                CutsceneManager.Instance.SeeBoss1Gate(dialogues, bubbleChatAnim, book, dot, vfx);
                gameObject.SetActive(false);
            }
        }
    }
}