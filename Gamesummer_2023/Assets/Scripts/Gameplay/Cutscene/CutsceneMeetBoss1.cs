using GDC.Enemies;
using GDC.Gameplay.UI;
using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GDC.Gameplay.Cutscene
{
    public class CutsceneMeetBoss1 : MonoBehaviour
    {
        [SerializeField] Dialogue[] dialogues;
        [SerializeField] Animator bubbleChatAnim, book, boss1;
        [SerializeField] Transform dot;
        [SerializeField] GameObject vfx;
        [SerializeField] UIBossIntroPanel uiBossIntroPanel;
        [SerializeField] Boss1 bossScr;
        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.CompareTag("Player"))
            {
                CutsceneManager.Instance.MeetBoss1(dialogues, book, boss1, bubbleChatAnim, dot, vfx, uiBossIntroPanel, bossScr);
                gameObject.SetActive(false);
            }
        }
    }
}