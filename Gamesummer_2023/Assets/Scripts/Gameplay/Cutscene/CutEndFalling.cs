using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GDC.Gameplay.Cutscene
{
    public class CutEndFalling : MonoBehaviour
    {
        [SerializeField] Animator book, vodaNPCAnim;
        [SerializeField] SpriteRenderer vodaNPC;
        [SerializeField] GameObject vodaFishing, vfx_waterSplash, fishingRopeIdle;
        [SerializeField] Sprite surpriseVodaSprite;

        private void Start()
        {
            CutsceneEndFalling();
        }
        [Button]
        public void CutsceneEndFalling()
        {
            CutsceneManager.Instance.EndFalling(book, vodaNPCAnim, vodaNPC, vodaFishing, fishingRopeIdle, vfx_waterSplash, surpriseVodaSprite);
            gameObject.SetActive(false);
        }
    }
}
