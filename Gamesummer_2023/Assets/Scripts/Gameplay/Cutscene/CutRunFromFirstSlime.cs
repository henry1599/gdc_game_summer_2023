using GDC.Configuration;
using GDC.Gameplay.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GDC.Gameplay.Cutscene
{
    public class CutRunFromFirstSlime : MonoBehaviour
    {
        [SerializeField] NPC mysteriousMan;
        [SerializeField] Dialogue dialogue;
        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.gameObject.CompareTag("Player"))
                CutsceneManager.Instance.RunFromFirstSlime(mysteriousMan, dialogue);
        }
    }
}
