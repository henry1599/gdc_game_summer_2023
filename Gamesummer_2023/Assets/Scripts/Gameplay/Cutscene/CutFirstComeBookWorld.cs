using GDC.Enums;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GDC.Gameplay.Cutscene
{
    public class CutFirstComeBookWorld : MonoBehaviour
    {
        bool isCall;
        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (isCall) return;
            isCall = true;
            CutsceneManager.Instance.FirstComeBookWorld();
            gameObject.SetActive(false);
        }
    }
}
