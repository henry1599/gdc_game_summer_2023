using GDC.Configuration;
using GDC.Gameplay.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GDC.Gameplay.Cutscene
{
    public class CutFirstBattle : MonoBehaviour
    {
        [SerializeField] EnemyRoom enemyRoom;
        [SerializeField] NPC mysteriousMan;
        [SerializeField] Following mysteriousFollow;
        [SerializeField] Dialogue dialogue;
        [SerializeField] GameObject wallRunFromSlime;
        private void OnTriggerEnter2D(Collider2D collision)
        {
            CutsceneManager.Instance.FirstBattle(enemyRoom, mysteriousMan, mysteriousFollow, dialogue, wallRunFromSlime);
            gameObject.SetActive(false);
        }
    }
}
