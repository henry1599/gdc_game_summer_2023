using GDC.Enemies;
using GDC.Gameplay.UI;
using GDC.Gameplay;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using GDC.Managers;

namespace GDC.Gameplay.Cutscene
{
    public class CutGetDashRing : MonoBehaviour
    {
        [SerializeField] Dialogue[] dialogues;
        [SerializeField] Animator book, acrobanditAnim;
        [SerializeField] Transform dot, acrobandit;
        [SerializeField] GameObject vfx, vfxDirt, tutorialPanel, cutsceneWall, acrobanditPuzzle;

        [SerializeField] GameObject dashRing;

        [Button]
        public void GetDashRing()
        {
            CutsceneGetDashRing();
        }
        public void CutsceneGetDashRing()
        {
            cutsceneWall.SetActive(true);
            acrobanditPuzzle.SetActive(true);
            CutsceneManager.Instance.GetDashRing(dialogues, book, dot, vfx, vfxDirt, acrobanditAnim, acrobandit, tutorialPanel);
        }
        private void Start()
        {
            StartCoroutine(Cor_LoadDashRing());
        }
        IEnumerator Cor_LoadDashRing()
        {
            yield return new WaitUntil(() => GameManager.Instance.isLoadSceneComplete);
            if (SaveLoadManager.Instance.GameData.CutsceneSeeDashRing)
                dashRing.SetActive(false);
            else
                dashRing.SetActive(true);
        }
    }
}
