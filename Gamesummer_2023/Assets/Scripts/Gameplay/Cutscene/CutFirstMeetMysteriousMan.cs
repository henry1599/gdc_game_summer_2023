using GDC.Gameplay.Cutscene;
using GDC.Gameplay.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GDC.Gameplay.Cutscene
{
    public class CutFirstMeetMysteriousMan : MonoBehaviour
    {
        [SerializeField] NPC mysteriousMan;
        [SerializeField] Dialogue dialogue1, dialogue2, dialogue3;
        [SerializeField] Following mysteriousManFollow;
        private void OnTriggerEnter2D(Collider2D collision)
        {
            CutsceneManager.Instance.FirstMeetMysteriousMan(mysteriousMan, dialogue1, dialogue2, dialogue3, mysteriousManFollow);
            gameObject.SetActive(false);
        }
    }
}
