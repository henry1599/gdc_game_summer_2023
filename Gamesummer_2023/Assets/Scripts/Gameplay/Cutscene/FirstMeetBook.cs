using GDC.Enums;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GDC.Gameplay.Cutscene
{
    public class FirstMeetBook : MonoBehaviour
    {
        [SerializeField] GameObject vfx_inhale;
        [SerializeField] SceneType sceneType;
        [SerializeField] Animator bubbleChatAnim, bookAnim;
        private void OnTriggerEnter2D(Collider2D collision)
        {
            CutsceneManager.Instance.FirstMeetBook(vfx_inhale, sceneType, bubbleChatAnim, bookAnim);
            gameObject.SetActive(false);
        }
    }
}
