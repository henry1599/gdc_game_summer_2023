using GDC.Configuration;
using GDC.Gameplay.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GDC.Gameplay.Cutscene
{
    public class CutMeetHouse : MonoBehaviour
    {
        [SerializeField] NPC mysteriousMan;
        [SerializeField] Dialogue dialogue1, dialogue2;
        [SerializeField] GameObject walls;
        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.CompareTag("Player"))
            {
                CutsceneManager.Instance.MeetHouse(mysteriousMan, dialogue1, dialogue2, walls);
                gameObject.SetActive(false);
            }
        }
    }
}
