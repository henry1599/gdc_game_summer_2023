using GDC.Gameplay.Cutscene;
using GDC.Gameplay.UI;
using GDC.Gameplay;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.Configuration;

namespace GDC.Gameplay.Cutscene
{
    public class CutFirstMeetSlime : MonoBehaviour
    {
        [SerializeField] NPC mysteriousMan;
        [SerializeField] Dialogue dialogue1, dialogue2, dialogue3;
        [SerializeField] Following mysteriousManFollow;
        [SerializeField] SO_Item so_item;
        [SerializeField] GameObject tutorialPanel,wall;
        private void OnTriggerEnter2D(Collider2D collision)
        {
            CutsceneManager.Instance.FirstMeetSlime(mysteriousMan, dialogue1, dialogue2, dialogue3, mysteriousManFollow, so_item, tutorialPanel, wall);
            gameObject.SetActive(false);
        }
    }
}
