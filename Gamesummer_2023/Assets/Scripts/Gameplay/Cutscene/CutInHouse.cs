using GDC.Configuration;
using GDC.Gameplay.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GDC.Gameplay.Cutscene
{
    public class CutInHouse : MonoBehaviour
    {
        [SerializeField] NPC mysteriousMan;
        [SerializeField] Dialogue[] dialogues;
        [SerializeField] Following mysteriousManFollow;
        [SerializeField] SpriteRenderer[] storyBGs;
        [SerializeField] SpriteRenderer bossShadow;
        [SerializeField] CircleCollider2D coll;

        private void OnTriggerEnter2D(Collider2D collision)
        {
            CutsceneManager.Instance.InHouse(mysteriousMan, mysteriousManFollow, storyBGs, bossShadow, dialogues, coll);
            gameObject.SetActive(false);
        }
    }
}
