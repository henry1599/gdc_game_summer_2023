using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.Universal;

namespace GDC.Gameplay.Cutscene
{
    public class CutTalkBlackCat : MonoBehaviour
    {
        [SerializeField] Animator blackCatAnim;
        [SerializeField] Transform spiralBall;
        [SerializeField] GameObject vfx, chest;
        [SerializeField] Light2D globalLight, spotLight;
        [SerializeField] SpriteRenderer catBubbleChat;
        public void TalkBlackCat()
        {
            CutsceneTalkBlackCat();
        }
        public void CutsceneTalkBlackCat()
        {
            CutsceneManager.Instance.TalkBlackCat(blackCatAnim, spiralBall, vfx, chest, globalLight, spotLight, catBubbleChat);
        }
    }
}
