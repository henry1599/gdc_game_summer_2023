using GDC.Gameplay.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GDC.Gameplay.Cutscene
{
    public class CutTakeBackBook : MonoBehaviour
    {
        [SerializeField] Dialogue[] dialogues;
        [SerializeField] Animator book;
        [SerializeField] Transform fakeBook;
        [SerializeField] Transform dot;
        [SerializeField] GameObject vfx, walls;

        public void TakeBackBook()
        {
            CutsceneManager.Instance.TakeBackBook(dialogues, book, fakeBook, dot, vfx);
            walls.SetActive(false);
        }  
    }
}
