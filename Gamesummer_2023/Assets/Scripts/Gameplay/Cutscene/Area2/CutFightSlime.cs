﻿using DG.Tweening;
using Gameplay;
using GDC.Enemies;
using GDC.Gameplay.UI;
using GDC.Managers;
using GDC.PlayerManager;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GDC.Gameplay.Cutscene
{
    public class CutFightSlime : MonoBehaviour
    {
        [SerializeField] Animator ducky;
        [SerializeField] Collider2D cutsceneCol;
        [SerializeField] EnemyRoom enemyRoom;
        [SerializeField] Dialogue[] dialogues;
        bool inCutScene = false;
        [SerializeField] CutMeetGreatTree cutMeetGreatTree;
        [SerializeField] Enemy[] enemies; 

        void Start ()
        {
            if (SaveLoadManager.Instance.GameData.CutsceneGetBow)
                cutsceneCol.enabled = false;
        }

        IEnumerator Cor_FightSlime()
        {
            Player.Instance.InCutScene = true;
            Player.Instance.UseHandMovement = false;
            Player.Instance.ToIdle();
            Player.Instance.graphicOfPlayer.transform.localScale = new Vector3(-1, 1, 1);

            CameraController.Instance.isFollowPlayer = false;
            CameraController.Instance.isNotCheckBound = true;
            CameraController.Instance.transform.DOMove(new Vector3(0, 11, -10), 0.5f);
            yield return new WaitForSeconds(1.5f);

            ducky.Play("hurt");
            ducky.transform.DOLocalMoveY(1, 0.5f).SetEase(Ease.InOutSine).SetLoops(2, LoopType.Yoyo);
            yield return new WaitForSeconds(1.5f);
            ducky.Play("idle");
            TriggerDialogue.Instance.SetDialogue(dialogues[0]);
            yield return new WaitUntil(() => TriggerDialogue.Instance.isEndDialogue);

            ducky.Play("Move");
            ducky.transform.parent.DOMove(new Vector2(2, 18), 2).SetEase(Ease.Linear).OnComplete(() => ducky.transform.parent.gameObject.SetActive(false));
            enemyRoom.gameObject.SetActive(true);
            enemyRoom.StartEnemyRoom();
            Player.Instance.InCutScene = false;
            Player.Instance.UseHandMovement = true;
            foreach (Enemy enemy in enemies)
                foreach (MonoBehaviour script in enemy.GetComponents<MonoBehaviour>())
                    script.enabled = true;

            CameraController.Instance.transform.DOMove(Player.Instance.transform.position, 0.5f);
            CameraController.Instance.isFollowPlayer = true;
            CameraController.Instance.isNotCheckBound = false;
            yield return new WaitForSeconds(0.75f);
            CameraController.Instance.coll.enabled = true;
            yield return new WaitUntil(() => !enemyRoom.gameObject.activeInHierarchy);
            Player.Instance.InCutScene = true;
            Player.Instance.UseHandMovement = false;
            Player.Instance.ToIdle();
            Player.Instance.graphicOfPlayer.transform.localScale = new Vector3(-1, 1, 1);

            EchoTextUI.Instance.SpawnText("Tên đó giúp chúng ta sao ?",true);
            yield return new WaitForSeconds(1.5f);
            EchoTextUI.Instance.SpawnText("Liệu hắn có phải người xấu không ?", true);
            yield return new WaitForSeconds(1.5f);
            EchoTextUI.Instance.SpawnText("Có khi nào đó là vị anh hùng đến cứu khu rừng này không ?", true);
            yield return new WaitForSeconds(2.5f);

            cutMeetGreatTree.MeetGreatTree();
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.gameObject.CompareTag("Player") && !inCutScene)
            {
                inCutScene = true;
                StartCoroutine(Cor_FightSlime());
                cutsceneCol.enabled = false;
            }
        }
    }
}