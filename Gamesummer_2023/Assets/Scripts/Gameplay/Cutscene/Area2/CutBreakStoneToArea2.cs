using AudioPlayer;
using DG.Tweening;
using Gameplay;
using GDC.Gameplay.UI;
using GDC.Managers;
using GDC.PlayerManager;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GDC.Gameplay.Cutscene
{
    public class CutBreakStoneToArea2 : MonoBehaviour
    {
        [SerializeField] Transform energyOrb,energyOrbContain;
        [SerializeField] Animator bubbleChatAnim;
        [SerializeField] Transform dot;
        [SerializeField] Animator book;
        [SerializeField] GameObject vfx;
        [SerializeField] Dialogue[] dialogues;
        [SerializeField] ParticleSystem vfxBreakStoneDust;
        [SerializeField] SpriteRenderer[] bigStones, tinyStones;
        [SerializeField] Collider2D cutsceneCol;
        bool inCutScene = false;


        private void Start()
        {
            SetUpWhenLoadScene();
        }
        public void SetUpWhenLoadScene()
        {
            if (SaveLoadManager.Instance.GameData.CutsceneBreakStoneToArea2)
            {
                foreach (SpriteRenderer stone in bigStones)
                    stone.gameObject.SetActive(false);
                foreach (SpriteRenderer stone in tinyStones)
                    stone.color = Color.white;
                cutsceneCol.enabled = false;
            }
        }

        IEnumerator Cor_BreakStone()
        {
            Player.Instance.InCutScene = true;
            Player.Instance.UseHandMovement = false;
            Player.Instance.ToIdle();
            Player.Instance.graphicOfPlayer.transform.localScale = new Vector3(-1, 1, 1);

            CameraController.Instance.isFollowPlayer = false;
            CameraController.Instance.isNotCheckBound = true;
            CameraController.Instance.transform.DOMove(new Vector3(-0.5f, 19.5f, -10), 0.5f);
            yield return new WaitForSeconds(1.5f);
            CameraController.Instance.transform.DOMove(Player.Instance.transform.position, 0.5f);
            CameraController.Instance.isFollowPlayer = true;
            CameraController.Instance.isNotCheckBound = false;
            yield return new WaitForSeconds(0.75f);

            SoundManager.Instance.PlaySound(SoundID.SFX_FAIRY_DUST);
            dot.gameObject.SetActive(true);
            dot.position = Player.Instance.transform.position + new Vector3(0, 0.5f, 0);
            dot.DOMove(Player.Instance.transform.position + new Vector3(-1.5f, 0.5f, 0), 1);
            SoundManager.Instance.PlaySound(SoundID.SFX_EXCLAMATION);
            bubbleChatAnim.transform.position = Player.Instance.transform.position + new Vector3(0, 1.5f, 0);
            bubbleChatAnim.Play("Surprise");
            bubbleChatAnim.transform.DOScaleX(0.6f, 0.15f);
            bubbleChatAnim.transform.DOScaleY(1.4f, 0.15f).OnComplete(() =>
            {
                bubbleChatAnim.transform.DOScale(1f, 0.1f);
            });
            yield return new WaitForSeconds(1);

            bubbleChatAnim.transform.DOScaleX(0.6f, 0.1f);
            bubbleChatAnim.transform.DOScaleY(1.4f, 0.1f).OnComplete(() =>
            {
                bubbleChatAnim.transform.DOScale(0f, 0.15f);
            });

            SoundManager.Instance.PlaySound(SoundID.SFX_APPEAR);
            Instantiate(vfx, dot.transform.position, Quaternion.identity);
            book.gameObject.SetActive(true);
            book.transform.position = dot.transform.position;
            dot.gameObject.SetActive(false);
            SoundManager.Instance.PlaySound(SoundID.SFX_BOOK_PAGE);
            book.Play("Open");
            yield return new WaitForSeconds(0.55f);
            book.Play("Fly");
            yield return new WaitForSeconds(1);
            TriggerDialogue.Instance.SetDialogue(dialogues[0]);
            yield return new WaitUntil(() => TriggerDialogue.Instance.isEndDialogue);

            SoundManager.Instance.PlaySound(SoundID.SFX_APPEAR);
            Instantiate(vfx, book.transform.position, Quaternion.identity);
            energyOrbContain.gameObject.SetActive(true);
            energyOrbContain.transform.position = Player.Instance.transform.position;
            energyOrb.gameObject.SetActive(true);
            energyOrb.localPosition = Vector2.zero;
            SoundManager.Instance.PlaySound(SoundID.SFX_FAIRY_DUST);
            energyOrb.DOLocalMoveY(1.5f, 1);
            SoundManager.Instance.PlaySound(SoundID.SFX_EXCLAMATION);
            bubbleChatAnim.transform.position = Player.Instance.transform.position + new Vector3(0, 1.5f, 0);
            bubbleChatAnim.Play("Surprise");
            bubbleChatAnim.transform.DOScaleX(0.6f, 0.15f);
            bubbleChatAnim.transform.DOScaleY(1.4f, 0.15f).OnComplete(() =>
            {
                bubbleChatAnim.transform.DOScale(1f, 0.1f);
            });
            yield return new WaitForSeconds(1);
            bubbleChatAnim.transform.DOScaleX(0.6f, 0.1f);
            bubbleChatAnim.transform.DOScaleY(1.4f, 0.1f).OnComplete(() =>
            {
                bubbleChatAnim.transform.DOScale(0f, 0.15f);
            });
            yield return new WaitForSeconds(1);

            SoundManager.Instance.PlaySound(SoundID.SFX_FAIRY_DUST);
            energyOrbContain.DORotate(new Vector3(0, 0, 1620), 5, RotateMode.FastBeyond360).SetEase(Ease.InOutSine);
            energyOrb.DOLocalMoveY(2, 5).OnComplete(()=>
            {
                energyOrb.DOMove(new Vector2(-0.5f, 19.5f), 0.25f).SetEase(Ease.OutFlash).OnComplete(() =>
                {
                    CameraEffects.Instance.ShakeOnce();
                    SoundManager.Instance.PlaySound(SoundID.SFX_EXPLOSION);
                    energyOrb.gameObject.SetActive(false);
                    vfxBreakStoneDust.Play();
                });
            });

            yield return new WaitForSeconds(5.5f);
            foreach (SpriteRenderer stone in bigStones)
                stone.DOFade(0, 2).OnComplete(()=>stone.gameObject.SetActive(false));
            foreach (SpriteRenderer stone in tinyStones)
                stone.DOFade(1, 2).SetDelay(1);

            yield return new WaitForSeconds(vfxBreakStoneDust.main.duration + vfxBreakStoneDust.main.startLifetime.constantMax);
            TriggerDialogue.Instance.SetDialogue(dialogues[1]);
            yield return new WaitUntil(() => TriggerDialogue.Instance.isEndDialogue);

            SoundManager.Instance.PlaySound(SoundID.SFX_BOOK_PAGE);
            book.Play("Close");
            yield return new WaitForSeconds(0.55f);
            SoundManager.Instance.PlaySound(SoundID.SFX_APPEAR);
            Instantiate(vfx, book.transform.position, Quaternion.identity);
            dot.gameObject.SetActive(true);
            dot.position = book.transform.position;
            book.gameObject.SetActive(false);
            yield return new WaitForSeconds(1);

            SoundManager.Instance.PlaySound(SoundID.SFX_FAIRY_DUST);
            dot.DOMove(Player.Instance.transform.position + new Vector3(0, 0.5f, 0), 0.6f);
            yield return new WaitForSeconds(0.6f);
            dot.GetComponent<SpriteRenderer>().DOColor(Color.clear, 0.25f).SetEase(Ease.Linear);
            yield return new WaitForSeconds(0.5f);
            dot.gameObject.SetActive(false);

            Player.Instance.InCutScene = false;
            Player.Instance.UseHandMovement = true;
            SaveLoadManager.Instance.GameData.CutsceneBreakStoneToArea2 = true;
            cutsceneCol.enabled = false;
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.gameObject.CompareTag("Player") && !inCutScene)
            {
                inCutScene = true;
                StartCoroutine(Cor_BreakStone());
            }
        }
    }
}