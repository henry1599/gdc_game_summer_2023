﻿using AudioPlayer;
using DG.Tweening;
using Gameplay;
using GDC.Configuration;
using GDC.Gameplay.UI;
using GDC.Managers;
using GDC.PlayerManager;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GDC.Gameplay.Cutscene
{

    public class CutMeetGreatTree : MonoBehaviour
    {
        [SerializeField] Animator bubbleChatAnim;
        [SerializeField] Transform dot;
        [SerializeField] Animator book;
        [SerializeField] GameObject vfx;
        [SerializeField] Dialogue[] dialogues;
        [SerializeField] Animator greatTree;
        [SerializeField] SO_Item bow;
        [SerializeField] Transform obtainIcon;

        public void MeetGreatTree()
        {
            StartCoroutine(Cor_MeetGreatTree());
        }

        IEnumerator Cor_MeetGreatTree()
        {
            CameraController.Instance.isFollowPlayer = false;
            CameraController.Instance.isNotCheckBound = true;
            CameraController.Instance.transform.DOMove(new Vector3(0, 22, -10), 2);
            yield return new WaitForSeconds(1.5f);

            Player.Instance.MoveTo(new Vector2(0, 18.5f), 3);
            yield return new WaitForSeconds(3);
            Player.Instance.graphicOfPlayer.transform.localScale = new Vector3(1, 1, 1);
            yield return new WaitForSeconds(1);

            greatTree.SetFloat("state", 1);
            TriggerDialogue.Instance.SetDialogue(dialogues[0]);
            yield return new WaitUntil(() => TriggerDialogue.Instance.isEndDialogue);

            Player.Instance.graphicOfPlayer.Play("obtain2");
            yield return new WaitForSeconds(0.5f);
            Player.Instance.graphicOfPlayer.Play("idle(noWeapon)");
            yield return new WaitForSeconds(1);

            TriggerDialogue.Instance.SetDialogue(dialogues[1]);
            yield return new WaitUntil(() => TriggerDialogue.Instance.isEndDialogue);

            Player.Instance.ObtainItem(bow);
            obtainIcon.localPosition = new Vector2(0.15f, -0.2f);
            SaveLoadManager.Instance.GameData.AddItem(bow);
            GetItemDisplayManager.Instance.ShowItem(bow.sprite, bow.itemName);
            yield return new WaitForSeconds(3.5f);

            EchoTextUI.Instance.SpawnText("Người có chắc không vậy thưa THỤ THẦN ?", true);
            yield return new WaitForSeconds(1.5f);
            EchoTextUI.Instance.SpawnText("Hắn có thể là kẻ xấu...", true);
            yield return new WaitForSeconds(1.5f);
            EchoTextUI.Instance.SpawnText("Thần nghĩ Người không nên đưa hắn cây cung đấy", true);
            yield return new WaitForSeconds(2.5f);

            TriggerDialogue.Instance.SetDialogue(dialogues[2]);
            yield return new WaitUntil(() => TriggerDialogue.Instance.isEndDialogue);

            CameraController.Instance.transform.DOMove(Player.Instance.transform.position, 0.5f);
            CameraController.Instance.isFollowPlayer = true;
            CameraController.Instance.isNotCheckBound = false;
            yield return new WaitForSeconds(0.75f);

            SoundManager.Instance.PlaySound(SoundID.SFX_FAIRY_DUST);
            dot.gameObject.SetActive(true);
            dot.position = Player.Instance.transform.position + new Vector3(0, 0.5f, 0);
            dot.DOMove(Player.Instance.transform.position + new Vector3(-1.5f, 0.5f, 0), 1);
            yield return new WaitForSeconds(1);
            SoundManager.Instance.PlaySound(SoundID.SFX_APPEAR);
            Instantiate(vfx, dot.transform.position, Quaternion.identity);
            book.gameObject.SetActive(true);
            book.transform.position = dot.transform.position;
            dot.gameObject.SetActive(false);
            SoundManager.Instance.PlaySound(SoundID.SFX_BOOK_PAGE);
            book.Play("Open");
            yield return new WaitForSeconds(0.55f);
            book.Play("Fly");
            yield return new WaitForSeconds(1);
            TriggerDialogue.Instance.SetDialogue(dialogues[3]);
            yield return new WaitUntil(() => TriggerDialogue.Instance.isEndDialogue);

            SoundManager.Instance.PlaySound(SoundID.SFX_BOOK_PAGE);
            book.Play("Close");
            yield return new WaitForSeconds(0.55f);
            SoundManager.Instance.PlaySound(SoundID.SFX_APPEAR);
            Instantiate(vfx, book.transform.position, Quaternion.identity);
            dot.gameObject.SetActive(true);
            dot.position = book.transform.position;
            book.gameObject.SetActive(false);
            yield return new WaitForSeconds(1);

            SoundManager.Instance.PlaySound(SoundID.SFX_FAIRY_DUST);
            dot.DOMove(Player.Instance.transform.position + new Vector3(0, 0.5f, 0), 0.6f);
            yield return new WaitForSeconds(0.6f);
            dot.GetComponent<SpriteRenderer>().DOColor(Color.clear, 0.25f).SetEase(Ease.Linear);
            yield return new WaitForSeconds(0.5f);
            dot.gameObject.SetActive(false);

            Player.Instance.InCutScene = false;
            Player.Instance.UseHandMovement = true;
            SaveLoadManager.Instance.GameData.CutsceneGetBow = true;
        }
    }
}