﻿using AudioPlayer;
using DG.Tweening;
using GDC.Gameplay.UI;
using GDC.Managers;
using GDC.PlayerManager;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace GDC.Gameplay.Cutscene
{
    public class CutSecondHearForestEcho : MonoBehaviour
    {
        [SerializeField] Animator bubbleChatAnim;
        [SerializeField] Transform dot;
        [SerializeField] Animator book;
        [SerializeField] Collider2D cutsceneCol;
        [SerializeField] GameObject vfx;
        [SerializeField] Dialogue[] dialogues;
        bool inCutScene = false;

        private void Start()
        {
            if (SaveLoadManager.Instance.GameData.CutsceneSecondHearForestEcho)
                cutsceneCol.enabled = false;
        }

        IEnumerator Cor_HearForestEcho()
        {
            Player.Instance.InCutScene = true;
            Player.Instance.UseHandMovement = false;
            Player.Instance.ToIdle();
            Player.Instance.graphicOfPlayer.transform.localScale = new Vector3(-1, 1, 1);

            EchoTextUI.Instance.SpawnText("Có lẽ chúng ta nên tin tưởng hắn một lần",true);
            yield return new WaitForSeconds(Random.Range(0.25f, 0.5f));
            EchoTextUI.Instance.SpawnText("Chắc hắn sẽ không làm chúng ta thất vọng", true);
            yield return new WaitForSeconds(Random.Range(0.25f, 0.5f));
            EchoTextUI.Instance.SpawnText("Mong rằng hắn có thể cứu rỗi khu rừng này", true);
            yield return new WaitForSeconds(Random.Range(0.25f, 0.5f));
            EchoTextUI.Instance.SpawnText("Mong THỤ THẦN được khoẻ mạnh", true);
            yield return new WaitForSeconds(Random.Range(0.25f, 0.5f));

            SoundManager.Instance.PlaySound(SoundID.SFX_EXCLAMATION);
            bubbleChatAnim.transform.position = Player.Instance.transform.position + new Vector3(0, 1.5f, 0);
            bubbleChatAnim.Play("Surprise");
            bubbleChatAnim.transform.DOScaleX(0.6f, 0.15f);
            bubbleChatAnim.transform.DOScaleY(1.4f, 0.15f).OnComplete(() =>
            {
                bubbleChatAnim.transform.DOScale(1f, 0.1f);
            });
            yield return new WaitForSeconds(1);

            bubbleChatAnim.transform.DOScaleX(0.6f, 0.1f);
            bubbleChatAnim.transform.DOScaleY(1.4f, 0.1f).OnComplete(() =>
            {
                bubbleChatAnim.transform.DOScale(0f, 0.15f);
            });

            yield return new WaitForSeconds(0.5f);
            Player.Instance.graphicOfPlayer.transform.localScale = new Vector3(-1, 1, 1);
            yield return new WaitForSeconds(0.2f);
            Player.Instance.graphicOfPlayer.transform.localScale = new Vector3(1, 1, 1);
            yield return new WaitForSeconds(0.2f);
            Player.Instance.graphicOfPlayer.transform.localScale = new Vector3(-1, 1, 1);
            yield return new WaitForSeconds(0.2f);
            Player.Instance.graphicOfPlayer.transform.localScale = new Vector3(1, 1, 1);
            yield return new WaitForSeconds(1f);

            SoundManager.Instance.PlaySound(SoundID.SFX_FAIRY_DUST);
            dot.gameObject.SetActive(true);
            dot.position = Player.Instance.transform.position + new Vector3(0, 0.5f, 0);
            dot.DOMove(Player.Instance.transform.position + new Vector3(-1.5f, 0.5f, 0), 1);
            yield return new WaitForSeconds(1);
            SoundManager.Instance.PlaySound(SoundID.SFX_APPEAR);
            Instantiate(vfx, dot.transform.position, Quaternion.identity);
            book.gameObject.SetActive(true);
            book.transform.position = dot.transform.position;
            dot.gameObject.SetActive(false);
            SoundManager.Instance.PlaySound(SoundID.SFX_BOOK_PAGE);
            book.Play("Open");
            yield return new WaitForSeconds(0.55f);
            book.Play("Fly");
            yield return new WaitForSeconds(1);
            TriggerDialogue.Instance.SetDialogue(dialogues[0]);
            yield return new WaitUntil(() => TriggerDialogue.Instance.isEndDialogue);

            SoundManager.Instance.PlaySound(SoundID.SFX_BOOK_PAGE);
            book.Play("Close");
            yield return new WaitForSeconds(0.55f);
            SoundManager.Instance.PlaySound(SoundID.SFX_APPEAR);
            Instantiate(vfx, book.transform.position, Quaternion.identity);
            dot.gameObject.SetActive(true);
            dot.position = book.transform.position;
            book.gameObject.SetActive(false);
            yield return new WaitForSeconds(1);

            SoundManager.Instance.PlaySound(SoundID.SFX_FAIRY_DUST);
            dot.DOMove(Player.Instance.transform.position + new Vector3(0, 0.5f, 0), 0.6f);
            yield return new WaitForSeconds(0.6f);
            dot.GetComponent<SpriteRenderer>().DOColor(Color.clear, 0.25f).SetEase(Ease.Linear);
            yield return new WaitForSeconds(0.5f);
            dot.gameObject.SetActive(false);

            Player.Instance.InCutScene = false;
            Player.Instance.UseHandMovement = true;
            SaveLoadManager.Instance.GameData.CutsceneSecondHearForestEcho = true;
            cutsceneCol.enabled = false;
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.gameObject.CompareTag("Player") && !inCutScene)
            {
                inCutScene = true;
                StartCoroutine(Cor_HearForestEcho());
            }
        }
    }
}