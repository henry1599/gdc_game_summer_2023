using AudioPlayer;
using DG.Tweening;
using Gameplay;
using GDC.Enemies;
using GDC.Gameplay.UI;
using GDC.Managers;
using GDC.PlayerManager;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GDC.Gameplay.Cutscene
{
    public class CutMeetDucky : MonoBehaviour
    {
        [SerializeField] Animator bubbleChatAnim;
        [SerializeField] Transform dot;
        [SerializeField] Animator book;
        [SerializeField] Collider2D cutsceneCol;
        [SerializeField] GameObject vfx;
        [SerializeField] Dialogue[] dialogues;
        [SerializeField] Transform beanbonGraphic;
        [SerializeField] GameObject wall;
        [SerializeField] Animator ducky;
        bool inCutScene = false;
        [SerializeField] Transform slime;
        [SerializeField] Enemy[] enemies;

        private void Start()
        {
            if (SaveLoadManager.Instance.GameData.CutsceneGetBow)
            {
                cutsceneCol.enabled = false;
                beanbonGraphic.parent.gameObject.SetActive(false);
                ducky.gameObject.SetActive(false);
                foreach (Enemy enemy in enemies) 
                    enemy.gameObject.SetActive(false);
            }
        }

        IEnumerator Cor_MeetDucky()
        {
            foreach (Enemy enemy in enemies)
                foreach (MonoBehaviour script in enemy.GetComponents<MonoBehaviour>())
                    script.enabled = false;

            Player.Instance.InCutScene = true;
            Player.Instance.UseHandMovement = false;
            Player.Instance.ToIdle();
            Player.Instance.graphicOfPlayer.transform.localScale = new Vector3(-1, 1, 1);
            ducky.Play("scary");

            CameraController.Instance.isFollowPlayer = false;
            CameraController.Instance.isNotCheckBound = true;
            CameraController.Instance.transform.DOMove(new Vector3(0, 11, -10), 0.5f);
            yield return new WaitForSeconds(1.5f);

            Vector2 slimeOriginPos = slime.position;
            slime.DOMoveX(beanbonGraphic.position.x + 0.3f, 0.5f).SetEase(Ease.OutFlash).OnComplete(() =>
            {
                beanbonGraphic.GetComponent<Animator>().Play("hurt");
                beanbonGraphic.parent.DOMoveX(-0.15f, 0.25f).OnComplete(()=> beanbonGraphic.GetComponent<Animator>().Play("idle"));
                slime.DOMove(slimeOriginPos, 0.5f);
            });
            yield return new WaitForSeconds(1.5f);


            TriggerDialogue.Instance.SetDialogue(dialogues[0]);
            yield return new WaitUntil(() => TriggerDialogue.Instance.isEndDialogue);
            beanbonGraphic.DOLocalMoveY(1, 0.5f).SetEase(Ease.InOutSine).SetLoops(2, LoopType.Yoyo);
            yield return new WaitForSeconds(1);
            TriggerDialogue.Instance.SetDialogue(dialogues[1]);
            yield return new WaitUntil(() => TriggerDialogue.Instance.isEndDialogue);

            CameraController.Instance.transform.DOMove(Player.Instance.transform.position, 0.5f);
            CameraController.Instance.isFollowPlayer = true;
            CameraController.Instance.isNotCheckBound = false;
            yield return new WaitForSeconds(0.75f);

            SoundManager.Instance.PlaySound(SoundID.SFX_FAIRY_DUST);
            dot.gameObject.SetActive(true);
            dot.position = Player.Instance.transform.position + new Vector3(0, 0.5f, 0);
            dot.DOMove(Player.Instance.transform.position + new Vector3(-1.5f, 0.5f, 0), 1);
            yield return new WaitForSeconds(1);
            SoundManager.Instance.PlaySound(SoundID.SFX_APPEAR);
            Instantiate(vfx, dot.transform.position, Quaternion.identity);
            book.gameObject.SetActive(true);
            book.transform.position = dot.transform.position;
            dot.gameObject.SetActive(false);
            SoundManager.Instance.PlaySound(SoundID.SFX_BOOK_PAGE);
            book.Play("Open");
            yield return new WaitForSeconds(0.55f);
            book.Play("Fly");
            yield return new WaitForSeconds(1);
            TriggerDialogue.Instance.SetDialogue(dialogues[2]);
            yield return new WaitUntil(() => TriggerDialogue.Instance.isEndDialogue);

            SoundManager.Instance.PlaySound(SoundID.SFX_BOOK_PAGE);
            book.Play("Close");
            yield return new WaitForSeconds(0.55f);
            SoundManager.Instance.PlaySound(SoundID.SFX_APPEAR);
            Instantiate(vfx, book.transform.position, Quaternion.identity);
            dot.gameObject.SetActive(true);
            dot.position = book.transform.position;
            book.gameObject.SetActive(false);
            yield return new WaitForSeconds(1);

            SoundManager.Instance.PlaySound(SoundID.SFX_FAIRY_DUST);
            dot.DOMove(Player.Instance.transform.position + new Vector3(0, 0.5f, 0), 0.6f);
            yield return new WaitForSeconds(0.6f);
            dot.GetComponent<SpriteRenderer>().DOColor(Color.clear, 0.25f).SetEase(Ease.Linear);
            yield return new WaitForSeconds(0.5f);
            dot.gameObject.SetActive(false);

            Player.Instance.InCutScene = false;
            Player.Instance.UseHandMovement = true;
            wall.SetActive(true);
            cutsceneCol.enabled = false;
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.gameObject.CompareTag("Player") && !inCutScene)
            {
                inCutScene = true;
                StartCoroutine(Cor_MeetDucky());
            }
        }
    }
}