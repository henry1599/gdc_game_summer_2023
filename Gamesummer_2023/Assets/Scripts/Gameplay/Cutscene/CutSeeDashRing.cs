using GDC.Enemies;
using GDC.Gameplay.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GDC.Gameplay.Cutscene
{
    public class CutSeeDashRing : MonoBehaviour
    {
        [SerializeField] Dialogue[] dialogues;
        [SerializeField] Animator bubbleChatAnim, book;
        [SerializeField] Transform dot;
        [SerializeField] GameObject vfx;
        [SerializeField] Boxy boxy;
        [SerializeField] EnemyRoom enemyRoom;
        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.CompareTag("Player"))
            {
                CutsceneManager.Instance.SeeDashRing(dialogues, bubbleChatAnim, book, dot, vfx, boxy, enemyRoom);
                gameObject.SetActive(false);
            }
        }
    }
}
