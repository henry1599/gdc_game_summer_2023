using GDC.Configuration;
using GDC.Gameplay.UI;
using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GDC.Gameplay.Cutscene
{
    public class CutWakeUpEndChapter1 : MonoBehaviour
    {
        [SerializeField] Dialogue[] dialoges;
        [SerializeField] NPC voda;
        [SerializeField] Following vodaFollowing;
        [SerializeField] Animator book;
        [SerializeField] Transform dot;
        [SerializeField] GameObject vfx;
        [SerializeField] SO_Item wishyPiece1;
        [SerializeField] Beta betaPanel;

        private void Start()
        {
            CutsceneWakeUpEndChapter1();
        }

        [Button]
        void CutsceneWakeUpEndChapter1()
        {
            CutsceneManager.Instance.WakeUpEndChapter1(dialoges, voda, vodaFollowing, book, dot, vfx, wishyPiece1, betaPanel);
            gameObject.SetActive(false);
        }
    }
}
