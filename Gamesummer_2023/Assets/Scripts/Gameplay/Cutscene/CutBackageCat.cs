using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.Gameplay.UI;
using GDC.Configuration;

namespace GDC.Gameplay.Cutscene
{
    public class CutBackageCat : MonoBehaviour
    {
        [SerializeField] Dialogue[] dialogues;
        [SerializeField] Animator backageCatAnim; 
        [SerializeField] SO_Item rewardItem;
        [SerializeField] ParticleSystem[] vfx;
        [SerializeField] SpriteRenderer[] backageCatGraphic;
        [SerializeField] SpriteRenderer pinkBG, bubbleChat;

        public void CutsceneBackageCat()
        {
            CutsceneManager.Instance.BackageCat(dialogues, backageCatAnim, rewardItem, vfx, backageCatGraphic, pinkBG, bubbleChat);
        }
    }
}
