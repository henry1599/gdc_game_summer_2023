using GDC.Gameplay.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GDC.Gameplay.Cutscene
{
    public class CutLearnSpell : MonoBehaviour
    {
        [SerializeField] Dialogue[] dialogues;
        [SerializeField] Animator bubbleChatAnim, book;
        [SerializeField] Transform dot;
        [SerializeField] GameObject vfx, tutorialPanel;
        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.CompareTag("Player"))
            {
                CutsceneManager.Instance.LearnSpell(dialogues, bubbleChatAnim, book, dot, vfx, tutorialPanel);
                gameObject.SetActive(false);
            }
        }
    }
}
