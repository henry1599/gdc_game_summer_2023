using GDC.Configuration;
using GDC.Gameplay.UI;
using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GDC.Gameplay.Cutscene
{
    public class CutDefeatBoss1 : MonoBehaviour
    {
        [SerializeField] Dialogue[] dialogues;
        [SerializeField] Animator book, bubbleChatAnim;
        [SerializeField] Transform dot, boss1, energyOrb, energyOrbContain;
        [SerializeField] GameObject vfx, vfx_inhale, vfx_fly, bgSky;
        [SerializeField] SO_Item energyOrbItem;
        [Button]
        public void CutsceneDefeatBoss1()
        {
            CutsceneManager.Instance.DefeatBoss1(dialogues, book, bubbleChatAnim, boss1, energyOrb, energyOrbContain, dot, vfx, vfx_inhale, vfx_fly, bgSky, energyOrbItem);
        }
    }
}
