using GDC.Gameplay.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GDC.Gameplay.Cutscene
{
    public class ComeInBookStore : MonoBehaviour
    {
        [SerializeField] Dialogue dialogue;
        [SerializeField] GameObject[] tutorialPanels;

        private void OnTriggerEnter2D(Collider2D collision)
        {
            CutsceneManager.Instance.ComeInBookStore(dialogue, tutorialPanels);
            gameObject.SetActive(false);
        }
    }
}
