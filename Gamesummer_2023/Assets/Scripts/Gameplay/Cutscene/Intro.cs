using DG.Tweening;
using GDC.Enums;
using GDC.Managers;
using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace GDC.Gameplay.Cutscene
{
    public class Intro : MonoBehaviour
    {
        [SerializeField] string[] introText;
        [SerializeField] TMP_Text text;
        [SerializeField] TMP_InputField inputField;
        [SerializeField] TMP_Text buttonText;
        [SerializeField] Image button;
        [SerializeField] float timeBetweenLetter;
        [SerializeField] Image fButton;
        [SerializeField] Sprite[] fButtonSprites;
        [SerializeField] GameObject fButtonText;
        int currentTextIndex = 0;
        [SerializeField] TMP_Text errorText;
        Tween fadeError;
        bool isLoadScene = false;
        IEnumerator Cor_ButtonAnimation()
        {
            fButton.sprite = fButtonSprites[1];
            yield return new WaitForSeconds(0.05f);
            fButton.sprite = fButtonSprites[2];
            yield return new WaitForSeconds(0.05f);
            fButton.sprite = fButtonSprites[1];
            yield return new WaitForSeconds(0.05f);
            fButton.sprite = fButtonSprites[0];
            yield return new WaitForSeconds(0.05f);
            StartCoroutine(Cor_ButtonAnimation());
        }

        void RunText()
        {
            if (currentTextIndex == introText.Length)
            {
                //GameManager.Instance.LoadSceneManually(SceneType.REAL_WORLD_AREA_01, TransitionType.FADE, SoundType.REAL_WORLD_AREA);
                if (isLoadScene)
                    return;
                isLoadScene = true;
                SaveLoadManager.Instance.CacheData.isChangeCameraBound = true;
                SaveLoadManager.Instance.CacheData.camMaxX = 100;
                SaveLoadManager.Instance.CacheData.camMinX = -100;
                SaveLoadManager.Instance.CacheData.camMaxY = 100;
                SaveLoadManager.Instance.CacheData.camMinY = -100;
                GameManager.Instance.LoadSceneAsyncManually(
                SceneType.REAL_WORLD_AREA_01,
                TransitionType.FADE,
                SoundType.REAL_WORLD_AREA,
                cb: () =>
                {
                    GameManager.Instance.UnloadSceneManually(
                        SceneType.INTRO,
                        cb: () => GameManager.Instance.SetInitData()
                    ) ;

                });
                return;
            }    
            StartCoroutine(Cor_RunText());
        }

        IEnumerator Cor_RunText()
        {
            fButtonText.SetActive(false);
            foreach (char letter in introText[currentTextIndex++])
            {
                yield return new WaitForSeconds(timeBetweenLetter);
                text.text += letter;
            }
            text.text += "\n";
            if (currentTextIndex == 3)
            {
                inputField.gameObject.SetActive(true);
                button.gameObject.SetActive(true);
            }
            else
                fButtonText.SetActive(true);
        }

        public void EnterButton()
        {
            button.transform.DOScale(1.2f, 0.5f).SetEase(Ease.OutBack);
            buttonText.DOFade(1, 0.5f);
            button.DOFade(1, 0.5f);
        }

        public void ExitButton()
        {
            button.transform.DOScale(1, 0.5f);
            buttonText.DOFade(0.8f, 0.5f);
            button.DOFade(0.8f, 0.5f);
        }

        public void ButtonDown()
        {
            button.color = new Color(0.5f, 0.5f, 0.5f);
            buttonText.color = new Color(0.5f, 0.5f, 0.5f);
        }

        bool DetectOnlySpace(string playerName)
        {
            foreach (char ch in playerName)
            {
                if (ch != ' ')
                    return false;
            }
            return true;
        }    

        public void TypingSound()
        {
            if (inputField.text.Length == 0 && Input.GetKey(KeyCode.Backspace))
                return;
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_TYPING);
        }    

        public void ButtonUp()
        {
            string playerName = inputField.text;
            if (playerName.Length > 12 || playerName == string.Empty || DetectOnlySpace(playerName))
            {
                SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_ERROR);
                fadeError.Kill();
                errorText.color = Color.red;
                fadeError = errorText.DOFade(0, 2).SetDelay(2);
                fadeError.Play();
                return;
            }
            SaveLoadManager.Instance.GameData.PlayerName = playerName;
            inputField.gameObject.SetActive(false);
            text.text = string.Empty;
            button.gameObject.SetActive(false);
            RunText();
        }

        private void Start()
        {
            RunText();
            button.color = new Color(1, 1, 1, 0.5f);
            buttonText.color = new Color(1, 1, 1, 0.5f);
            button.gameObject.SetActive(false);
            inputField.gameObject.SetActive(false);
            fButtonText.gameObject.SetActive(false);
            text.text = string.Empty;
            errorText.color = new Color(errorText.color.r, errorText.color.g, errorText.color.b, 0);

            SoundManager.Instance.PlayMusic(AudioPlayer.SoundID.MUSIC_STORY);
        }

        private void Update()
        {
            if (fButtonText.activeInHierarchy&&Input.GetKeyDown(KeyCode.F))
            {
                SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_INTERACT);
                RunText();
            }

            if (button.gameObject.activeInHierarchy && Input.GetKeyDown(KeyCode.KeypadEnter))
            {
                ButtonUp();
            }

            //text.ForceMeshUpdate();
            //var textInfo = text.textInfo;
            //for (int i=0;i < textInfo.characterCount;++i)
            //{
            //    var charInfo = textInfo.characterInfo[i];
            //    if (!charInfo.isVisible)
            //        continue;

            //    var verts = textInfo.meshInfo[charInfo.materialReferenceIndex].vertices;

            //    for (int j = 0;j<4;++j)
            //    {
            //        var orig = verts[charInfo.vertexIndex + j];
            //        verts[charInfo.vertexIndex + j] = orig + new Vector3(0, Mathf.Sin(Time.time * 2 + orig.x * 0.01f) * 10, 0);
            //    }
            //}

            //for (int i=0;i<textInfo.meshInfo.Length;++i)
            //{
            //    var meshInfo = textInfo.meshInfo[i];
            //    meshInfo.mesh.vertices = meshInfo.vertices;
            //    text.UpdateGeometry(meshInfo.mesh,i);
            //}
        }
    }
}
