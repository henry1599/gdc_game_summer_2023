using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.PlayerManager;
using GDC.Managers;

namespace GDC.Enemies
{
    public class ElectricJelly : Enemy
    {
        [SerializeField] float timer;
        [SerializeField] float minDistanceDetectPlayer, maxDistanceDetectPlayer;
        [SerializeField] GameObject VFX_shoot;
        float timerHelper;
        int speedHelper;
        Vector2 dir;
        void Start()
        {
            timerHelper = timer;
            speedHelper = Spe;
        }
        void FixedUpdate()
        {
            if (!isCanMove || !isVisible) return;
            dir = Player.Instance.transform.position - transform.position;
            float dist = dir.magnitude;
            if (dist <= maxDistanceDetectPlayer && dist >= minDistanceDetectPlayer && !DetectWall(dir, dist))
            {
                Chase();
            }

            timerHelper -= Time.fixedDeltaTime;
            if (timerHelper <= 0f)
            {
                Spe = 0;
                anim.Play("shoot");
                StartCoroutine(Cor_ElectricDelay());
                VFX_shoot.SetActive(true);
                timerHelper = timer;
            }
        }
        IEnumerator Cor_ElectricDelay()
        {
            yield return new WaitForSeconds(1f/3);
            SoundManager.Instance.PlaySoundRelatedToCamera(transform.position, AudioPlayer.SoundID.SFX_ENEMY_ELECTRIC);
        }
        void Chase()
        {
            var desiredVelocity = dir.normalized * Spe;
            var deltaVelocity = desiredVelocity - rb.velocity;
            rb.AddForce(Spe * Time.fixedDeltaTime * deltaVelocity, ForceMode2D.Impulse);
        }
        public void DoneShootElec()
        {
            Spe = speedHelper;
            SoundManager.Instance.StopSFX(AudioPlayer.SoundID.SFX_ENEMY_ELECTRIC);
        }
        public override void Sleep(float duration)
        {
            base.Sleep(duration);
            rb.velocity = Vector2.zero;
            anim.Play("sleep", -1, 0);
            anim.SetFloat("sleepSpeed", 1f/duration);
        }
        public override void Die()
        {
            base.Die();
            SoundManager.Instance.StopSFX(AudioPlayer.SoundID.SFX_ENEMY_ELECTRIC);
        }
    }
}