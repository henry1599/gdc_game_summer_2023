using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.PlayerManager;

namespace GDC.Enemies
{
    public class Glurp : Enemy
    {
        [SerializeField] GameObject glurpBullet, glurpPoisonBullet;
        [SerializeField] Transform bulletTip;
        [SerializeField] float minDistanceDetectPlayer, maxDistanceDetectPlayer;
        float timerToShoot = 2f;
        bool isShooting = false, isMoving = false, isHiding = false, isAttacking = false;
        Vector3 dir;
        void FixedUpdate()
        {
            if (!isCanMove || isAttacking || !isVisible) return;
            dir = Player.Instance.transform.position - transform.position;
            if (dir.x >= 0) transform.localScale = new Vector3(-1,1,1);
            else transform.localScale = new Vector3(1,1,1);

            float dist = dir.magnitude;
            bool detectWall = DetectWall(dir, dist);
            if (!detectWall) timerToShoot -= Time.fixedDeltaTime;
            else timerToShoot = 2f;

            if (timerToShoot <= 0f)
            {
                isShooting = true;
            }

            if (dist <= minDistanceDetectPlayer && !isAttacking && !isHiding && !detectWall)
            {
                string atk = RandomBiteOrPoison();
                if (atk == "Bite")
                {
                    Bite();
                }
                else if (atk == "Poison")
                {
                    Poison();
                }
                isAttacking = true;
                isMoving = false;
                isHiding = false;
            }
            else if (!isShooting && dist > minDistanceDetectPlayer && dist <= maxDistanceDetectPlayer && !detectWall)
            {
                if (!isMoving)
                {
                    isMoving = true;
                    anim.Play("move");
                    isHiding = true;
                }
            }
            else if (isShooting && !isHiding && !detectWall)
            {
                anim.Play("shoot");
                isAttacking = true;
                StartCoroutine(Cor_ResetAttacking(1f));
            }
            if (isHiding && !detectWall)
            {
                transform.position += dir.normalized * Time.fixedDeltaTime * Spe;
            }
            else 
            {
                isHiding = false;
            }
        }
        void Bite()
        {
            anim.Play("bite");
            StartCoroutine(Cor_ResetAttacking(2f));
        }
        void Poison()
        {
            anim.Play("poison");
            StartCoroutine(Cor_ResetAttacking(2f));
        }
        public void DoneHiding()
        {
            isHiding = false;
        }
        public void ShootGlurpPoisonBullet()
        {
            isShooting = false;
            isMoving = false;
            var go = Instantiate(glurpPoisonBullet, bulletTip.position + dir.normalized * 0.75f, Quaternion.identity);
            go.GetComponent<EnemyBulletMove>().SetDirection(dir);
            timerToShoot = 2f;
        }
        public void ShootGlurpBullet()
        {
            isShooting = false;
            isMoving = false;
            var go = Instantiate(glurpBullet, bulletTip.position + dir.normalized * 0.75f, Quaternion.identity);
            go.GetComponent<EnemyBulletMove>().SetDirection(dir);
            timerToShoot = 2f;
        }
        public override void Sleep(float duration)
        {
            base.Sleep(duration);
            anim.Play("sleep", -1, 0);
            anim.SetFloat("sleepSpeed", 1f/duration);
        }
        public override void Die()
        {
            base.Die();
            anim.Play("die");
        }
        IEnumerator Cor_ResetAttacking(float seconds)
        {
            yield return new WaitForSeconds(seconds);
            isAttacking = false;
        }
        string RandomBiteOrPoison()
        {
            string[] strings = { "Bite", "Poison" };
            float[] weights = { 3f, 1f };

            float totalWeight = 0f;
            foreach (float weight in weights)
            {
                totalWeight += weight;
            }

            float randomValue = Random.value * totalWeight;
            float weightSum = 0f;

            for (int i = 0; i < strings.Length; i++)
            {
                weightSum += weights[i];
                if (randomValue <= weightSum)
                {
                    return strings[i];
                }
            }
            return strings[0];
        }
    }
}