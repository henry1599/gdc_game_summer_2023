using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.PlayerManager;

namespace GDC.Enemies
{
    public class Crow : Enemy
    {
        [SerializeField] float minDistanceDetectPlayer, maxDistanceDetectPlayer;
        [SerializeField] List<Transform> Trees;
        [SerializeField] float timeToWait;
        [SerializeField] GameObject thisShadow;
        [SerializeField] RuntimeAnimatorController[] anims;
        float timeToWaitHelper;
        Vector3 destination, direction;
        float timeToFlyUp = 1f, timeToFlyUpHelper;
        bool isFlyingUp, isFlyingToDestination, isWaiting = true, isFlyingDown, isFlyingAway;
        float timeToAppear, dist;
        float timeToRun = 0.4f, timeToRunHelper;
        bool detectWall, detectWallWithOffset;
        Vector3 firstShadowPos = new(-0.05f, -2f, 0f);
        Vector3 lastShadowPos = new(-0.05f, -0.15f, 0f);
        Vector3 tempShadowPos;
        void Start()
        {
            timeToFlyUpHelper = timeToFlyUp;
            timeToWaitHelper = timeToWait;
            timeToRunHelper = timeToRun;

            int randNum = Random.Range(0, anims.Length);
            anim.runtimeAnimatorController = anims[randNum];
        }

        void FixedUpdate()
        {
            direction = Player.Instance.transform.position - transform.position;
            dist = direction.magnitude;
            FlyUp();
            FlyDown();
            FlyToDestination();
            detectWall = DetectWall(direction, dist);
            detectWallWithOffset = DetectWall(direction, dist + 1.5f);
            if (isWaiting && !isFlyingUp && !isFlyingToDestination && dist >= minDistanceDetectPlayer && dist <= maxDistanceDetectPlayer)
            {
                if (!isFlyingAway) anim.Play("idle");
                if (!detectWall) timeToWaitHelper -= Time.fixedDeltaTime;
                if (timeToWaitHelper <= 0f)
                {
                    timeToWaitHelper = timeToWait;
                    isFlyingUp = true;
                    tempShadowPos = thisShadow.transform.position;
                    anim.Play("fly");
                }
            }
            else if (dist < minDistanceDetectPlayer)
            {
                if (!isFlyingAway && !isFlyingToDestination)
                {
                    isFlyingAway = true;
                }
            }
            if (isFlyingAway && !isFlyingToDestination && !isFlyingDown) SetUpFlyAway();
        }
        void SetUpFlyAway()
        {
            timeToRunHelper -= Time.fixedDeltaTime;
            if (timeToRunHelper <= 0f)
            {
                timeToRunHelper = timeToRun;
                timeToWaitHelper = timeToWait;
                FlyingAway();
            }
        }
        void FlyingAway()
        {
            GetComponent<BoxCollider2D>().enabled = false;
            isFlyingToDestination = true;
            int idx = Random.Range(0, Trees.Count);
            destination = Trees[idx].position + Vector3.up * timeToFlyUp;
            timeToAppear = (destination - transform.position).magnitude / Spe;
            if (destination.x >= transform.position.x) transform.localScale = new Vector3(-1,1,1);
            else transform.localScale = Vector3.one;
        }
        void FlyUp()
        {
            if (isFlyingUp)
            {
                this.transform.position += Vector3.up * Time.fixedDeltaTime;
                thisShadow.transform.position = tempShadowPos;
                timeToFlyUpHelper -= Time.fixedDeltaTime;
                if (timeToFlyUpHelper <= 0f)
                {
                    isFlyingUp = false;
                    isFlyingToDestination = true;
                    timeToFlyUpHelper = timeToFlyUp;
                }
                else if (timeToFlyUpHelper <= 0.5f)
                {
                    GetComponent<BoxCollider2D>().enabled = false;
                }
            }
        }
        void FlyDown()
        {
            if (isFlyingDown)
            {
                this.transform.position += Vector3.down * Time.fixedDeltaTime;
                thisShadow.transform.position = tempShadowPos;
                timeToFlyUpHelper -= Time.fixedDeltaTime;
                if (timeToFlyUpHelper <= 0f)
                {
                    isFlyingDown = false;
                    isFlyingAway = false;
                    timeToFlyUpHelper = timeToFlyUp;
                }
            }
        }
        void FlyToDestination()
        {
            if (isFlyingToDestination)
            {
                this.transform.position = Vector3.MoveTowards(this.transform.position, destination, Time.fixedDeltaTime * Spe);
                if (!isFlyingAway)
                {
                    thisShadow.transform.localPosition = Vector3.MoveTowards(thisShadow.transform.localPosition, lastShadowPos, Time.fixedDeltaTime / timeToAppear);
                }
                else
                {
                    thisShadow.transform.localPosition = Vector3.MoveTowards(thisShadow.transform.localPosition, firstShadowPos, Time.fixedDeltaTime / timeToAppear);
                }
                timeToAppear -= Time.fixedDeltaTime;
                if (timeToAppear <= 0f && !isFlyingAway)
                {
                    GetComponent<BoxCollider2D>().enabled = true;
                }
                if (Vector2.Distance(transform.position, destination) <= 0.25f)
                {
                    isFlyingToDestination = false;
                    if (isFlyingAway)
                    {
                        isFlyingDown = true;
                        tempShadowPos = thisShadow.transform.position;
                    }
                    else isFlyingAway = true;
                    isWaiting = true;
                }
            }
            else if (!isFlyingDown)
            {
                timeToAppear = dist / Spe - 0.25f;
                if (direction.x >= 0) transform.localScale = new Vector3(-1,1,1);
                else transform.localScale = Vector3.one;
                if (detectWallWithOffset) destination = Player.Instance.transform.position;
                else destination = Player.Instance.transform.position + direction.normalized * 1.5f;
            }
        }
        public override void Sleep(float duration)
        {
            base.Sleep(duration);
            anim.Play("sleep", -1, 0);
            anim.SetFloat("sleepSpeed", 1f/duration);
        }
    }
}
