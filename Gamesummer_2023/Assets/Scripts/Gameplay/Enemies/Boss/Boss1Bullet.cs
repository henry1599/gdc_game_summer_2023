using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.PlayerManager;
using DG.Tweening;
using GDC.Managers;
using System.Linq;

namespace GDC.Enemies
{
    public class Boss1Bullet : MonoBehaviour
    {
        [SerializeField] SpriteRenderer thisSprite, thisShadow;
        [SerializeField] Sprite[] bulletSprites;
        [SerializeField] List<Enemy> enemies;
        [SerializeField] GameObject VFX_Explode, VFX_TrailAfterHit, VFX_Trail;
        EnemyBulletMove enemyBulletMove;
        int wallCollisionCount = 0;
        Collider2D col;
        bool stopRotate, shadowMove, canHitBoss;
        Vector3 currentRotation;
        float shadowY = -0.7f;
        Boss1 boss;
        void Start()
        {
            col = GetComponent<Collider2D>();
            enemyBulletMove = GetComponent<EnemyBulletMove>();
            
            boss = FindObjectOfType<Boss1>();
            Physics2D.IgnoreCollision(col, boss.GetComponent<BoxCollider2D>());

            StartCoroutine(Cor_BulletRotate(Vector3.forward * 90, 0.5f));
            thisShadow.transform.SetParent(null);
        }
        void FixedUpdate()
        {
            if (shadowMove) shadowY += Time.fixedDeltaTime * 0.2f / 1.25f;
            thisShadow.transform.position = transform.position + new Vector3(0,shadowY);
        }
        void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.gameObject.CompareTag("Enemy"))
            {
                if (!canHitBoss) Physics2D.IgnoreCollision(col, collision.collider, true);
                else 
                {
                    collision.gameObject.GetComponent<Boss>().GetHit(transform.position, 1, 0, gameObject.layer);
                    canHitBoss = false;
                    VFX_Explode.SetActive(true);
                    VFX_Explode.transform.SetParent(null);
                    Destroy(thisShadow);
                    Destroy(gameObject);
                }
            }
            if (collision.gameObject.CompareTag("Wall"))
            {
                SoundManager.Instance.PlaySoundRelatedToCamera(transform.position, AudioPlayer.SoundID.SFX_EGG_REFLECT, 1.5f);
                wallCollisionCount++;
                Vector2 reflectDirection = Vector2.Reflect(enemyBulletMove.GetDirection().normalized, collision.GetContact(0).normal);
                enemyBulletMove.SetDirection(reflectDirection, false);
                CheckBullet();
            }
        }
        void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.CompareTag("PlayerAttack"))
            {
                SoundManager.Instance.PlaySoundRelatedToCamera(transform.position, AudioPlayer.SoundID.SFX_ENEMY_SHOOT);
                canHitBoss = true;
                Physics2D.IgnoreCollision(col, boss.GetComponent<Collider2D>(), false);
                VFX_TrailAfterHit.SetActive(true);
                VFX_Trail.SetActive(false);
                var dir = (Vector2)transform.position - (Vector2)Player.Instance.transform.position;
                enemyBulletMove.SetDirection(dir.normalized, false);
            }
        }
        void CheckBullet()
        {
            if (wallCollisionCount == 3 && !canHitBoss)
            {
                StartCoroutine(Cor_DestroyBullet());
                return;
            }
            else if (canHitBoss)
            {
                VFX_Explode.SetActive(true);
                VFX_Explode.transform.SetParent(null);
                Destroy(thisShadow);
                Destroy(gameObject);
            }
            if (bulletSprites.Length > wallCollisionCount) 
                thisSprite.sprite = bulletSprites[wallCollisionCount];
        }
        IEnumerator Cor_DestroyBullet()
        {
            shadowMove = true;
            float step = Time.fixedDeltaTime * enemyBulletMove.speed;
            while (enemyBulletMove.speed > 0)
            {
                enemyBulletMove.speed -= step;
                yield return new WaitForSeconds(Time.fixedDeltaTime);
            }
            transform.DOPause();
            stopRotate = true;
            enemyBulletMove.speed = 0;
            shadowMove = false;
            int random = Random.Range(0, 10);
            if (random <= 3 && boss.Hp > 0)
            {
                currentRotation = transform.rotation.eulerAngles;
                StartCoroutine(Cor_FinalRotate(30, 0.15f));
                yield return new WaitForSeconds(0.75f);
                thisSprite.DOFade(0, 0.25f);
                thisShadow.DOFade(0, 0.25f);

                int rand = Random.Range(0, 2);
                if (rand == 0)
                    SoundManager.Instance.PlaySoundRelatedToCamera(transform.position, AudioPlayer.SoundID.SFX_EGG_HATCH, 0.75f);
                else SoundManager.Instance.PlaySoundRelatedToCamera(transform.position, AudioPlayer.SoundID.SFX_EGG_HATCH_1, 0.75f);

                VFX_Explode.SetActive(true);
                VFX_Explode.transform.SetParent(null);
                yield return new WaitForSeconds(0.5f);
                int randomIdx = Random.Range(0, enemies.Count);
                if (enemies[randomIdx] != null)
                {
                    var enemy = Instantiate(enemies[randomIdx], transform.position + Vector3.down * 0.3f, Quaternion.identity);
                    enemy.StartCoroutine(Cor_EnemyDelay(enemy));
                    boss.enemies.Add(enemy);

                    if (boss.Hp <= 0) enemy.Die();
                }
            }
            else 
            {
                thisSprite.DOFade(0, 0.5f);
                thisShadow.DOFade(0, 0.5f);
                yield return new WaitForSeconds(0.5f);
            }
            Destroy(thisShadow);
            Destroy(gameObject);
        }
        IEnumerator Cor_BulletRotate(Vector3 angleValue, float duration)
        {
            transform.DORotate(angleValue, duration);
            Vector3 newAngleValue = Vector3.forward * (angleValue.z + 90);
            yield return new WaitForSeconds(duration - 0.25f);
            if (!stopRotate) StartCoroutine(Cor_BulletRotate(newAngleValue, duration));
        }
        IEnumerator Cor_FinalRotate(int angleValue, float duration)
        {
            transform.DORotate(currentRotation + Vector3.forward * angleValue, duration);
            yield return new WaitForSeconds(duration);
            StartCoroutine(Cor_FinalRotate(- angleValue, duration));
        }
        IEnumerator Cor_EnemyDelay(Enemy enemy)
        {
            enemy.isCanMove = false;
            yield return new WaitForSeconds(2f);
            enemy.isCanMove = true;
        }
    }
}