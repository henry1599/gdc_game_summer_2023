using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GDC.PlayerManager;
using GDC.Gameplay;
using DG.Tweening;
using NaughtyAttributes;
using GDC.Managers;
using AudioPlayer;

namespace GDC.Enemies
{
    public class Boss1 : Boss
    {
        [Header("BossGameObject")]
        [SerializeField] GameObject quake;
        [SerializeField] GameObject vfx_quake;
        [SerializeField] GameObject bossBullet;
        [SerializeField] Transform bossBulletTip;
        [SerializeField] SpriteRenderer spriteRenderer;
        Vector3 dir, destination;
        [SerializeField] Collider2D thisCol, graphicCol;
        [SerializeField] List<GameObject> specialBullet;
        [Header("Statistic")]
        [SerializeField] float minTimer;
        [SerializeField] float maxTimer;
        bool isMoving, isShooting, isJumping, isFirstTime50Health, isSpecialSkill, firstMove;
        float timerHelper;
        bool warning, isChanging;
        [Header("Health")]
        [SerializeField] Slider boss1HealthSlider;
        [SerializeField] Image fill, icon;
        [HideInInspector] public List<Enemy> enemies;
        [SerializeField] GameObject VFX_RoarIfLowHP;
        private Coroutine skillCoroutine, jumpCoroutine, shootCoroutine;
        void Start()
        {
            timerHelper = maxTimer;
            enemies = new();
            firstMove = true;
        }
        void Update()
        {
            if (isSpecialSkill) isCanGetHit = false;
            if (Hp > 0)
                boss1HealthSlider.transform.localScale = new Vector3 (1.75f / transform.localScale.x, 1.75f / transform.localScale.y, 1);
            if (warning && isChanging)
            {
                isChanging = false;
                icon.DOColor(Color.red, 0.5f);
                icon.DOColor(new Color(0.5137255f, 0.8352941f, 0f), 0.5f).SetDelay(0.5f).OnComplete(() 
                    => isChanging = true
                );
            }
        }
        void FixedUpdate()
        {
            if (!isVisible || !isCanMove || isSpecialSkill) return;
            dir = Player.Instance.transform.position - transform.position;
            if (!isMoving) TrueFlip(dir);
            Move();
            timerHelper -= Time.fixedDeltaTime;
            if (!isMoving && !isShooting && !isJumping && timerHelper <= 0f)
            {
                int ranIdx = Random.Range(0,2);
                if (ranIdx == 1)
                {
                    shootCoroutine = StartCoroutine(Boss1Shoot());
                }
                else
                {
                    jumpCoroutine = StartCoroutine(Boss1Jump());
                }
            }
        }
        void Move()
        {
            if (!isMoving) return;
            anim.Play("move");
            var direction = destination - transform.position;
            TrueFlip(direction);
            transform.position += Spe * Time.fixedDeltaTime * direction.normalized;
            if (firstMove) 
            {
                firstMove = false;
                SoundManager.Instance.PlaySoundRelatedToCamera(transform.position, SoundID.SFX_BOSS_WALK, 0.5f);
            }
            if (direction.magnitude <= 0.5f)
            {
                isMoving = false;
                Idle();
                timerHelper = Random.Range(minTimer, maxTimer);
                SoundManager.Instance.StopSFX(SoundID.SFX_BOSS_WALK);
                firstMove = true;
            }
        }
        void TrueFlip(Vector3 direction)
        {
            if (direction.x >= 0) transform.localScale = new Vector3(-1,1,1);
            else transform.localScale = Vector3.one;
        }
        IEnumerator Boss1Shoot()
        {
            isShooting = true;
            anim.Play("prepare_attack");
            yield return new WaitForSeconds(0.5f);
            anim.Play("attack");
            yield return new WaitForSeconds(0.5f);
            dir = (Vector2)Player.Instance.transform.position - (Vector2)bossBulletTip.position;
            TrueFlip(dir);
            GameObject go1 = Instantiate(bossBullet, bossBulletTip.position + dir.normalized * 0.2f, Quaternion.identity);
            GameObject go2 = Instantiate(bossBullet, bossBulletTip.position + dir.normalized * 0.2f, Quaternion.identity);
            GameObject go3 = Instantiate(bossBullet, bossBulletTip.position + dir.normalized * 0.2f, Quaternion.identity);

            go1.GetComponent<EnemyBulletMove>().SetDirection(dir.normalized, false);
            go2.GetComponent<EnemyBulletMove>().SetDirection(Quaternion.Euler(30f, 0f, 30f) * dir.normalized, false);
            go3.GetComponent<EnemyBulletMove>().SetDirection(Quaternion.Euler(-30f, 0f, -30f) * dir.normalized, false);
            
            yield return new WaitForSeconds(0.25f);
            Idle();

            yield return new WaitForSeconds(1f);
            isShooting = false;
            SetUpMove();
        }
        IEnumerator Boss1Jump()
        {
            isJumping = true;

            destination = Player.Instance.transform.position;
            destination.x = Mathf.Clamp(destination.x, minAreaX, maxAreaX);
            destination.y = Mathf.Clamp(destination.y, minAreaY, maxAreaY);
            var dir = destination - transform.position;

            anim.Play("jump");

            yield return new WaitForSeconds(0.5f);
            thisCol.enabled = false;
            graphicCol.enabled = false;

            float height = Mathf.Clamp(dir.magnitude * 0.5f, 1.5f, 3);
            float timeToDropDown;
            if (height > 2.5f) timeToDropDown = 1f;
            else if (height > 2) timeToDropDown = 0.75f;
            else timeToDropDown = 0.5f;
            float timerForDropdown = timeToDropDown;

            while (timerForDropdown > 0f)
            {
                timerForDropdown -= Time.fixedDeltaTime;
                transform.position += dir / timeToDropDown * Time.fixedDeltaTime;
                float tempY = Mathf.Sin(timerForDropdown * Mathf.PI / timeToDropDown) * height;
                spriteRenderer.transform.localPosition = new Vector3(0, tempY, 0);
                yield return new WaitForSeconds(Time.fixedDeltaTime);
                if (timerForDropdown <= Time.fixedDeltaTime) 
                {
                    graphicCol.enabled = true;
                }
            }
            spriteRenderer.transform.localPosition = Vector3.zero;
            CameraEffects.Instance.ShakeOnce(0.5f, 7);
            quake.SetActive(true);
            vfx_quake.SetActive(true);
            quake.GetComponent<Animator>().Play("quake");

            yield return new WaitForSeconds(Time.fixedDeltaTime);
            graphicCol.enabled = false;
            thisCol.enabled = true;
            Idle();

            float resetTime = Random.Range(1, 1.75f);
            yield return new WaitForSeconds(resetTime);

            quake.SetActive(false);
            isJumping = false;
            SetUpMove();
        }
        IEnumerator Boss1SpecialSkill()
        {
            isMoving = false;
            SoundManager.Instance.StopSFX(SoundID.SFX_BOSS_WALK);
            yield return new WaitUntil(CheckSpecialSkill);
            fill.DOColor(new Color(0.3882353f, 0.6f, 0), 1f);
            spriteRenderer.DOColor(new Color(0.95f, 1, 0), 0.5f);
            yield return new WaitForSeconds(0.5f);
            spriteRenderer.DOColor(Color.white, 0.5f);
            yield return new WaitForSeconds(0.5f);
            anim.Play("roar");
            SoundManager.Instance.PlaySoundRelatedToCamera(transform.position, SoundID.SFX_BOSS_ROAR, 0.75f);
            yield return new WaitForSeconds(0.5f);
            VFX_RoarIfLowHP.SetActive(true);
            yield return new WaitForSeconds(0.25f);
            CameraEffects.Instance.ShakeOnce(1.25f);
            yield return new WaitForSeconds(1.5f);

            anim.Play("stomp");
            isJumping = true;
            yield return new WaitForSeconds(0.5f);
            thisCol.enabled = false;
            graphicCol.enabled = false;

            float height = 1.5f, timeToDropDown = 0.5f;
            float timerForDropdown = timeToDropDown;

            while (timerForDropdown > 0f)
            {
                timerForDropdown -= Time.fixedDeltaTime;
                float tempY = Mathf.Sin(timerForDropdown * Mathf.PI / timeToDropDown) * height;
                spriteRenderer.transform.localPosition = new Vector3(0, tempY, 0);
                yield return new WaitForSeconds(Time.fixedDeltaTime);
                if (timerForDropdown <= Time.fixedDeltaTime) 
                {
                    graphicCol.enabled = true;
                }
            }
            spriteRenderer.transform.localPosition = Vector3.zero;
            quake.SetActive(true);
            quake.GetComponent<Animator>().Play("quake");
            SoundManager.Instance.PlaySoundRelatedToCamera(transform.position, SoundID.SFX_GROUND_POUND, 0.75f);

            yield return new WaitForSeconds(Time.fixedDeltaTime);
            graphicCol.enabled = false;
            thisCol.enabled = true;
            isJumping = false;

            ShootSpecialBullets();
            CameraEffects.Instance.ShakeOnce();
            yield return new WaitForSeconds(0.5f);
            quake.SetActive(false);
            Idle();
            yield return new WaitForSeconds(1f);
            isCanGetHit = true;
            isSpecialSkill = false;
        }
        bool CheckSpecialSkill()
        {
            return !isShooting && !isJumping;
        }
        void ShootSpecialBullets()
        {
            int n = specialBullet.Count;
            for (int i = 0; i < n; i++)
            {
                EnemyBulletMove go = specialBullet[i].GetComponent<EnemyBulletMove>();
                go.transform.localPosition = new Vector3(go.transform.localPosition.x * transform.localScale.x, go.transform.localPosition.y, 0);
                go.gameObject.SetActive(true);
                go.transform.SetParent(null);
            }
        }
        void SetUpMove()
        {
            var previousDestination = destination;
            timerHelper = Random.Range(minTimer, maxTimer);
            var pos = Player.Instance.transform.position;
            var direction = pos - transform.position;
            if (direction.magnitude <= 2f)
            {
                destination = pos + direction.normalized * 1.5f;
                destination.x = Mathf.Clamp(destination.x, minAreaX, maxAreaX);
                destination.y = Mathf.Clamp(destination.y, minAreaY, maxAreaY);
                if (Vector2.Distance(previousDestination, destination) <= 0.5f)
                {
                    isMoving = false;
                }
                else isMoving = true;
            }
            else 
            {
                destination = pos + (Vector3)Random.insideUnitCircle * 2f;
                destination.x = Mathf.Clamp(destination.x, minAreaX, maxAreaX);
                destination.y = Mathf.Clamp(destination.y, minAreaY, maxAreaY);

                isMoving = true;
            }
        }
        public override void UpdateHealth(float damage)
        {
            base.UpdateHealth(damage);
            if (Hp <= MaxHp * 0.1f)
            {
                warning = true;
                isChanging = true;
            }
            if (!isFirstTime50Health && Hp <= MaxHp * 0.5f)
            {
                Hp = MaxHp * 0.5f;
                skillCoroutine = StartCoroutine(Boss1SpecialSkill());
                isFirstTime50Health = true;
                isSpecialSkill = true;
            }
            boss1HealthSlider.DOValue(Hp / MaxHp, 0.5f);
        }
        public override void Die()
        {
            if (skillCoroutine != null) StopCoroutine(skillCoroutine);
            if (jumpCoroutine != null) StopCoroutine(jumpCoroutine);
            if (shootCoroutine != null) StopCoroutine(shootCoroutine);

            base.Die();
            SoundManager.Instance.StopSFX(SoundID.SFX_BOSS_WALK);

            isMoving = isJumping = isShooting = false;
            isCanMove = false;
            boss1HealthSlider.GetComponent<RectTransform>().DOSizeDelta(Vector3.zero, 1).SetEase(Ease.InBounce);
            icon.GetComponent<RectTransform>().DOSizeDelta(Vector3.zero, 1).SetEase(Ease.InBounce);

            foreach(var enemy in enemies)
            {
                if (enemy != null && enemy.gameObject.activeSelf)
                {
                    if (enemy.Hp > 0)
                    {
                        enemy.Hp = 0;
                        enemy.Die();
                    }
                }
            }
        }
        void Idle()
        {
            int ranIdx = Random.Range(0,2);
            if (ranIdx == 0) anim.Play("idle");
            else anim.Play("idle2");
        }
        [Button]
        public override void StartBoss()
        {
            enabled = true;
            boss1HealthSlider.gameObject.SetActive(true);
            boss1HealthSlider.value = 0;
            boss1HealthSlider.DOValue(1,1);
        }
    }
}