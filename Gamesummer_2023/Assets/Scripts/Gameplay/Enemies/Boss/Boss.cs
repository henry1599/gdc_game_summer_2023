using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.Enums;
using GDC.PlayerManager;
using DG.Tweening;
using Unity.VisualScripting;
using UnityEngine.Events;
using GDC.Managers;

namespace GDC.Enemies
{
    public class Boss : MonoBehaviour
    {
        public Animator anim;
        public Rigidbody2D rb;

        public string ID;
        public SpellType spellType;
        float timeLife = 5;

        [SerializeField] private List<SpriteRenderer> spriteRendererList;

        #region status
        [Header("Stat")]
        [SerializeField] int spe;
        public int Spe
        {
            get => spe;
            set => spe = value;
        }
        [SerializeField] int atk;
        public int Atk
        {
            get => atk;
            set => atk = value;
        }
        [SerializeField] int def;
        public int Def
        {
            get => def;
            set => def = value;
        }
        [SerializeField] int maxHp;
        public int MaxHp
        {
            get => maxHp;
            set => maxHp = value;
        }
        [SerializeField] float hp;
        public float Hp
        {
            get => hp;
            set => hp = value;
        }
        [SerializeField] float maxStamina;
        public float MaxStamina
        {
            get => MaxStamina;
            set => MaxStamina = value;
        }
        [SerializeField] float stamina;
        public float Stamina
        {
            get => stamina;
            set => stamina = value;
        }
        #endregion

        public float staminaGain;
        public float expGain;
        [SerializeField] private int initSpe;
        [SerializeField] GameObject dizzyCircle, disappearEffect;
        [SerializeField] protected float minAreaX, maxAreaX, minAreaY, maxAreaY;

        [Header("ItemDrop")]
        [SerializeField] private int coinDrop;
        [SerializeField] private GameObject coinPreFab;
        [SerializeField] private List<GameObject> itemDrops;

        [Header("LayerDetect")]
        [SerializeField] LayerMask noEffectLayer; //Layer ma quai khong nhan damage
        [SerializeField] LayerMask superEffectLayer; //Layer ma quai bi nhan damame gap doi

        protected bool isCanGetHit;
        public bool isCanMove, isCanAttack, isVisible = false, isDisappearWhenDie;
        bool died;

        [Header("DamageTextPopup")]
        [SerializeField] Transform damageTextPopup;
        [SerializeField] GameObject VFX_Die;

        [SerializeField] UnityEvent eventAfterDie;
        private void Awake()
        {
            initSpe = spe;
            isCanMove = true;
            isCanGetHit = true;
        }

        public void GetHit(Vector3 attackPos, float spellAtk, float spellEffectDuration, LayerMask playerAttackLayer)
        {
            if (isCanGetHit == false) return;

            isCanGetHit = false;
            StartCoroutine(Cor_RefreshGetHit(0.5f));

            #region damage calculate
            Transform damagePopup = Instantiate(damageTextPopup, transform.position, Quaternion.identity);
            DamageTextPopup damagePopupScr = damagePopup.GetComponent<DamageTextPopup>();

            int layerMatchup, criticalRate;
            if (((1 << playerAttackLayer) & noEffectLayer) != 0) //so sanh LayerMark playerAttackLayer == noEffectLayer
            {
                Debug.Log(gameObject.name + " had no effect with " + playerAttackLayer);
                damagePopupScr.NoEffect();
                layerMatchup = 0;
            }
            else
            {
                if (((1 << playerAttackLayer) & superEffectLayer) != 0)
                {
                    Debug.Log(gameObject.name + " had SUPER effect with " + playerAttackLayer);
                    damagePopupScr.SuperEffect();
                    layerMatchup = 2;
                }
                else
                {
                    damagePopupScr.NormalEffect();
                    layerMatchup = 1;
                }

                StartCoroutine(Cor_ToColor(Color.red, 0f));
                StartCoroutine(Cor_ToColor(Color.white, 0.2f));

            }

            criticalRate = Random.Range(0, 100) / 89 + 1; //co ti le 90% la critical Rate = 2. Con lai = 1
            if (criticalRate == 2)
            {
                Debug.Log(gameObject.name + " GET CRITICAL HIT!");
                damagePopupScr.CriticalEffect();
            }

            float damage = 3 * criticalRate * layerMatchup * spellAtk * Random.Range(0.75f, 1.25f) * Player.Instance.Attack / def;
            damagePopupScr.SetText((int)damage);

            SoundManager.Instance.PlaySoundRelatedToCamera(transform.position, AudioPlayer.SoundID.SFX_ENEMY_HURT);

            // if (isNotMoveAway == false && damage > 0.5f)
            // {
            //    isCanMove = false;
            //    Vector3 direct = (this.transform.position - attackPos).normalized;
            //    rb.velocity = direct * 7; //Enemy bi vang ra
            //    StartCoroutine(Cor_CanMove(1));
            // }

            if (hp <= 0) //Neu da ngat thi se bi vang ra mot doan roi bien mat
            {
                StartCoroutine(Cor_Disappear(0.4f));
                return;
            }
            UpdateHealth(damage);
            // hp -= (int)damage;
            // if (hp <= 0)
            // {
            //     hp = 0;
            //     Die();
            // }
            #endregion 

            //Debug.Log(gameObject.name + " is gotten hit with damage = " + damage);
        }
        IEnumerator Cor_RefreshGetHit(float sec)
        {
            yield return new WaitForSeconds(sec);
            isCanGetHit = true;
        }
        
        IEnumerator Cor_ToColor(Color color, float sec)
        {
            yield return new WaitForSeconds(sec);
            foreach (var spriteRenderer in spriteRendererList)
            {
                spriteRenderer.material.DOColor(color, 0.2f);
            }
        }
        public virtual void StartBoss()
        {
            enabled = true;
        }
        public virtual void Die()
        {
            anim.Play("die");
            VFX_Die.SetActive(true);
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_BOSS_DIE);
            StartCoroutine(Cor_SlowTime());

            if (isDisappearWhenDie)
            {
                isCanMove = false;
                StartCoroutine(Cor_Disappear(5));
                StartCoroutine(Cor_AnimRotateDie(new Vector3(0, 0, 5f))); //Xoay qua xoay lai tu -5 -> 5 do
                dizzyCircle.SetActive(true);
            }

            //update exp for player here, and make sure eventAfterDie below will be worked after level up panel finish! (maybe use Ienumrator)

            eventAfterDie?.Invoke();
        }
        IEnumerator Cor_AnimRotateDie(Vector3 angleValue)
        {
            transform.DORotate(angleValue, 0.2f);
            yield return new WaitForSeconds(0.2f);
            StartCoroutine(Cor_AnimRotateDie(-angleValue));
        }
        IEnumerator Cor_Disappear(float sec)
        {
            yield return new WaitForSeconds(sec);
            Disappear();
        }
        public virtual void Disappear()
        {
            Debug.Log("Enemy Disappear!");
            DropItem();
            if (disappearEffect == null)
            {
                Debug.Log("Keo tha prefab DisappearEffect vao " + gameObject.name + "! Tuy kich co quai thi DisappearEffect nay co nhung loai prefab to nho khac nhau.");
            }
            else Instantiate(disappearEffect, transform.position, Quaternion.identity);
            gameObject.SetActive(false);
        }
        public void DropItem()
        {
            for (int i = 1; i <= coinDrop; i += 5)
            {
                Instantiate(coinPreFab, transform.position, Quaternion.identity);
            }
            foreach (var itemDrop in itemDrops)
            {
                Instantiate(itemDrop, transform.position, Quaternion.identity);
            }
        }
        IEnumerator Cor_SlowTime()
        {
            Camera.main.DOOrthoSize(3f, 0.5f).SetUpdate(true);
            if (Player.Instance.transform.position.x < transform.position.x)
            {
                float newX = Mathf.Clamp(transform.position.x + 2, minAreaX, maxAreaX);
                transform.DOJump(new Vector3(newX, transform.position.y, 0), 0.2f, 1, 1);
            }
            else 
            {
                float newX = Mathf.Clamp(transform.position.x - 2, minAreaX, maxAreaX);
                transform.DOJump(new Vector3(newX, transform.position.y, 0), 0.2f, 1, 1);
            }
            Time.timeScale = 0.2f;
            yield return new WaitForSecondsRealtime(2f);
            Time.timeScale = 1;
            Camera.main.DOOrthoSize(4.5f, 0.5f).SetUpdate(true);
        }
       
        public bool DetectWall(Vector3 direction, float distance)
        {
            // int layerMask = ~(1 << gameObject.layer);
            int layerMask = 1 << 3;
            RaycastHit2D[] hits = new RaycastHit2D[1];
            int numHits = Physics2D.RaycastNonAlloc(transform.position, direction, hits, distance, layerMask);
            for (int i = 0; i < numHits; i++)
            {
                if (hits[i].collider.CompareTag("Wall"))
                    return true;
            }
            return false;
        }
        public bool DetectWater(Vector3 direction, float distance = 1.5f)
        {
            int layerMask = 1 << 4;
            RaycastHit2D[] hits = new RaycastHit2D[1];
            int numHits = Physics2D.RaycastNonAlloc(transform.position, direction, hits, distance, layerMask);
            for (int i = 0; i < numHits; i++)
            {
                if (hits[i].collider.CompareTag("Water"))
                    return true;
            }
            return false;
        }

        public virtual void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.CompareTag("MainCamera"))
            {
                isVisible = true;
            }
        }
        public virtual void OnTriggerExit2D(Collider2D collision)
        {
            if (collision.CompareTag("MainCamera"))
            {
                isVisible = false;
            }
        }
        public virtual void OnTriggerStay2D(Collider2D collision)
        {
            if (collision.CompareTag("MainCamera"))
            {
                isVisible = true;
            }
        }
        public virtual void UpdateHealth(float damage)
        {
            hp -= (int)damage;
            if (hp <= 0)
            {
                hp = 0;
                if (!died) Die();
                died = true;
            }
        }
    }
}
