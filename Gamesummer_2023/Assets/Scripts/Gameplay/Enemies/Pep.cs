using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.PlayerManager;

namespace GDC.Enemies
{
    public enum PepState 
    {
        SpeedMode,
        AttackMode,
        DefenseMode
    }
    public class Pep : Enemy
    {
        [SerializeField] float minDistanceDetectPlayer, maxDistanceDetectPlayer, distanceToAttack;
        private PepState pepState = PepState.SpeedMode;
        private bool canDash = true;
        private bool isDashing = false;
        [SerializeField] private float dashingPower = 5f;
        private float dashingTime = 0.25f;
        private float dashingCooldown = 1f;
        Vector3 direction;
        float hitTimer = 0.5f;
        float dist;
        [SerializeField] private float timeToChange = 3f;
        [SerializeField] float timer;
        bool isHitting = false;
        bool isChanging = false;
        void Start()
        {
            timer = timeToChange;
        }
        void Update()
        {
            
        }

        void FixedUpdate()
        {
            if (isDashing || !isCanMove || isChanging || !isVisible) 
            {
                return;
            }
            direction = Player.Instance.transform.position - transform.position;
            dist = direction.magnitude;
            
            if (direction.x >= 0) transform.localScale = new Vector3(-1,1,1);
            else transform.localScale = new Vector3(1,1,1);

            if (dist >= minDistanceDetectPlayer && dist <= maxDistanceDetectPlayer)
            {
                switch (pepState)
                {
                    case PepState.SpeedMode:
                        SpeedMode();
                        break;
                    case PepState.AttackMode:
                        AttackMode();
                        break;
                    case PepState.DefenseMode:
                        DefenseMode();
                        break;
                }
                timer -= Time.fixedDeltaTime;
                if (timer <= 0f)
                {
                    PepChangeState();
                    timer = timeToChange;
                }
            }
            else 
            {
                timer = timeToChange/2;
            }
        }
        void PepChangeState()
        {
            int randNum = Random.Range(0,3);
            switch (randNum)
            {
                case 0:
                    StartCoroutine(PepChangeState(pepState, PepState.SpeedMode));
                    spellType = Enums.SpellType.SPE_BUFF;
                    break;
                case 1:
                    StartCoroutine(PepChangeState(pepState, PepState.AttackMode));
                    spellType = Enums.SpellType.ATK_BUFF;
                    break;
                case 2:
                    StartCoroutine(PepChangeState(pepState, PepState.DefenseMode));
                    spellType = Enums.SpellType.DEF_BUFF;
                    break;
            }
        }
        void SpeedMode()
        {
            if (canDash) StartCoroutine(Dash());
            anim.Play("idle_speed");
        }
        private IEnumerator Dash()
        {
            anim.Play("dash_speed");
            canDash = false;
            isDashing = true;
            rb.velocity = direction.normalized * dashingPower;
            yield return new WaitForSeconds(dashingTime);
            isDashing = false;
            yield return new WaitForSeconds(dashingCooldown);
            canDash = true;
        }
        void AttackMode()
        {
            if (isHitting)
            {
                hitTimer -= Time.fixedDeltaTime;
                if (hitTimer <= 0f)
                {
                    hitTimer = 0.5f;
                    isHitting = false;
                }
                return;
            }
            if (dist <= distanceToAttack && isHitting == false)
            {
                isHitting = true;
                anim.Play("hit_attack");
            }
            else
            {
                anim.Play("idle_attack");
                transform.position += direction.normalized * Time.fixedDeltaTime * Spe;
            }
        }
        void DefenseMode()
        {
            anim.Play("idle_defense");
            var defenseDirection = -direction.normalized;
            transform.position += defenseDirection * Time.fixedDeltaTime * Spe;
        }
        public void PepHit()
        {
            
        }
        public override void Sleep(float duration)
        {
            base.Sleep(duration);
            switch (pepState)
            {
                case PepState.SpeedMode:
                    anim.Play("sleep_speed", -1, 0);
                    break;
                case PepState.AttackMode:
                    anim.Play("sleep_attack", -1, 0);
                    break;
                case PepState.DefenseMode:
                    anim.Play("sleep_defense", -1, 0);
                    break;
            }
            // anim.Play("sleep");
            anim.SetFloat("sleepSpeed", 1f/duration);
        }
        IEnumerator PepChangeState(PepState before, PepState after)
        {
            isChanging = true;
            if (before == after) 
            {
                yield return null;
                isChanging = false;
            }
            else
            {
                string stateBefore = "disappear_" + StateToString(before);
                string stateAfter = "appear_" + StateToString(after);

                anim.Play(stateBefore);
                yield return new WaitForSeconds(0.25f);
                anim.Play(stateAfter);
                yield return new WaitForSeconds(0.25f);
                this.pepState = after;

                isChanging = false;
            }
        }
        string StateToString(PepState pepState)
        {
            return pepState switch
            {
                PepState.SpeedMode => "speed",
                PepState.AttackMode => "attack",
                PepState.DefenseMode => "defense",
                _ => "",
            };
        }
    }
}