using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.PlayerManager;
using GDC.Managers;

namespace GDC.Enemies
{
    public class SlimeMove : MonoBehaviour
    {
        [SerializeField] Enemy enemy;
        [SerializeField] float moveTime, waitTime;
        float waitTimeHelper, moveTimeHelper, resetTime = 0f;
        Vector3 direction;
        float dist;
        [SerializeField] float minDistanceDetectPlayer, maxDistanceDetectPlayer;
        [HideInInspector] public bool canFlip = true;
        [HideInInspector] public bool slimeCanMove = true;
        bool canMoveNext, isMoving;
        [HideInInspector] public float delayTime;
        bool isSpikeSlime;
        void Start()
        {
            waitTimeHelper = waitTime;
            moveTimeHelper = 0f;
            canMoveNext = false;
            isSpikeSlime = GetComponent<SpikeSlime>() != null;
        }
        void FixedUpdate()
        {
            if (!enemy.isCanMove || !enemy.isVisible)
            {
                ResetTime(0f, 0f, 0.2f);
                return;
            }
            if (delayTime > 0f)
            {
                delayTime -= Time.fixedDeltaTime;
                return;
            }
            direction = Player.Instance.transform.position - transform.position;
            dist = direction.magnitude;
            
            if (canFlip && !isSpikeSlime)
            {
                if (direction.x >= 0) transform.localScale = new Vector3(-1,1,1);
                else transform.localScale = Vector3.one;
            }

            if (slimeCanMove && canMoveNext)
            {
                if (!isMoving)
                {
                    enemy.anim.speed = 1f;
                    enemy.anim.Play("move");
                    SoundManager.Instance.PlaySoundRelatedToCamera(transform.position, AudioPlayer.SoundID.SFX_SLIME_WALK, 0.5f);
                    isMoving = true;
                }
                transform.position += enemy.Spe * Time.fixedDeltaTime * direction.normalized;
                moveTimeHelper -= Time.fixedDeltaTime;
                waitTimeHelper = waitTime;
            }
            else if (dist > maxDistanceDetectPlayer)
            {
                enemy.anim.Play("idle");
            }
            if (resetTime >= 0f)
            {
                resetTime -= Time.fixedDeltaTime;
                return;
            }
            if (moveTimeHelper <= 0f)
            {
                slimeCanMove = false;
                isMoving = false;
                waitTimeHelper -= Time.fixedDeltaTime;

                if (dist <= maxDistanceDetectPlayer && dist >= minDistanceDetectPlayer) canMoveNext = true;
                else canMoveNext = false;
            }
            if (waitTimeHelper <= 0f)
            {
                slimeCanMove = true;
                moveTimeHelper = moveTime;
            }
            bool detectObstacle = enemy.DetectWall(direction, dist) || enemy.DetectWater(direction);
            if (detectObstacle)
            {
                canMoveNext = false;
                waitTimeHelper = waitTime;
                moveTimeHelper = 0f;
            }
            else if (!canMoveNext && dist <= maxDistanceDetectPlayer && dist >= minDistanceDetectPlayer)
            {
                canMoveNext = true;
            } 
        }
        public void ResetTime(float resetTime, float moveTimeHelper, float waitTimeHelper)
        {
            this.resetTime = resetTime;
            this.moveTimeHelper = moveTimeHelper;
            this.waitTimeHelper = waitTimeHelper;
        }
    }
}