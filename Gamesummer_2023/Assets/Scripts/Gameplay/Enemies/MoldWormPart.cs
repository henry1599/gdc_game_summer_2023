using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.PlayerManager;

namespace GDC.Enemies
{
    public class MoldWormPart : MonoBehaviour
    {
        [SerializeField] bool isHead;
        MoldWorm enemy;
        void Start()
        {
            enemy = GetComponentInParent<MoldWorm>();
        }
        void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.gameObject.CompareTag("PlayerAttack"))
            {
                if (isHead) 
                    enemy.GetHit(Player.Instance.transform.position, 1, 0, collision.gameObject.layer);
                else 
                    enemy.GetHit(Player.Instance.transform.position, 0, 0, collision.gameObject.layer);
                enemy.resetTime = 0f;
                enemy.MoldWormStopSFXMove();
            }
        }
        void OnCollisionStay2D(Collision2D collision)
        {
            if (collision.gameObject.CompareTag("PlayerAttack"))
            {
                if (isHead) 
                    enemy.GetHit(Player.Instance.transform.position, 1, 0, collision.gameObject.layer);
                else 
                    enemy.GetHit(Player.Instance.transform.position, 0, 0, collision.gameObject.layer);
                enemy.resetTime = 0f;
                enemy.MoldWormStopSFXMove();
            }
        }
        void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.CompareTag("PlayerAttack"))
            {
                if (isHead) 
                    enemy.GetHit(Player.Instance.transform.position, 1, 0, collision.gameObject.layer);
                else 
                    enemy.GetHit(Player.Instance.transform.position, 0, 0, collision.gameObject.layer);
                enemy.resetTime = 0f;
                enemy.MoldWormStopSFXMove();
            }
        }
        void OnTriggerStay2D(Collider2D collision)
        {
            if (collision.gameObject.CompareTag("PlayerAttack"))
            {
                if (isHead) 
                    enemy.GetHit(Player.Instance.transform.position, 1, 0, collision.gameObject.layer);
                else 
                    enemy.GetHit(Player.Instance.transform.position, 0, 0, collision.gameObject.layer);
                enemy.resetTime = 0f;
                enemy.MoldWormStopSFXMove();
            }
        }
    }
}