using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.Managers;

namespace GDC.Enemies
{
    public class Slime : Enemy
    {
        public override void Die()
        {
            anim.Play("die");
            base.Die();
            GetComponent<SlimeMove>().enabled = false;
        }
        public override void Sleep(float duration)
        {
            base.Sleep(duration);
            anim.Play("sleep", -1, 0);
            anim.speed = 1f/duration;
        }
        // void Update()
        // {
        //     if (Input.GetKeyDown(KeyCode.V))
        //     {
        //         SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_DIG);
        //     }
        //     if (Input.GetKeyDown(KeyCode.B))
        //     {
        //         SoundManager.Instance.StopSFX(AudioPlayer.SoundID.SFX_DIG);
        //     }
        // }
    }
}
