using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.PlayerManager;
using GDC.Managers;

namespace GDC.Enemies
{
    public class PoisonSlime : Enemy
    {
        [SerializeField] float timer;
        float timerHelper;
        [SerializeField] GameObject poisonBullet, poisonArea;
        [SerializeField] float minDistanceDetectPlayer, maxDistanceDetectPlayer;
        bool canShoot;
        void Start()
        {
            timerHelper = timer;
        }
        void FixedUpdate()
        {
            if (!isCanMove) return;
            ShootPoison();
        }
        void ShootPoison()
        {
            if (!isCanMove) return;
            var direction = Player.Instance.transform.position - this.transform.position;
            var dist = direction.magnitude;
            if (!canShoot)
            {
                if (!DetectWall(direction, dist)) timerHelper -= Time.fixedDeltaTime;
                else timerHelper = timer;
                if (timerHelper <= 0f)
                {
                    canShoot = true;
                    timerHelper = timer;
                }
            }
            if (canShoot && dist <= maxDistanceDetectPlayer && dist >= minDistanceDetectPlayer)
            {
                GetComponent<SlimeMove>().slimeCanMove = false;
                anim.Play("shoot");
            }
            else if (dist > maxDistanceDetectPlayer || dist < minDistanceDetectPlayer)
            {
                timerHelper = timer;
            }
        }
        public void ShootEvent()
        {
            Vector3 pos;
            if (transform.localScale.x == -1) pos = transform.position + new Vector3 (0.5f, 0.5f, 0);
            else pos = transform.position + new Vector3 (-0.5f, 0.5f, 0);
            var go = Instantiate(poisonBullet, pos, Quaternion.identity);
            go.GetComponent<EnemyBulletMove>().SetDirection(Player.Instance.transform.position - pos, false);
            // Instantiate(vfx_poison_shoot, pos, Quaternion.identity);
            var temp = GetComponent<SlimeMove>();
            temp.ResetTime(0f, 0f, 0.5f);
            canShoot = false;

            StartCoroutine(Cor_PoisonArea(go));
        }
        IEnumerator Cor_PoisonArea(GameObject go)
        {
            yield return new WaitForSeconds(0.94f);
            GameObject area = null;
            if (go != null) 
            {
                area = Instantiate(poisonArea, go.transform.position, Quaternion.identity);
                SoundManager.Instance.PlaySoundRelatedToCamera(transform.position, AudioPlayer.SoundID.SFX_SLIME_POISON_BOMB);
            }
            if (area != null)
            {
                float scale = Random.Range(0.7f, 1);
                var sprite = area.GetComponentInChildren<SpriteRenderer>();
                sprite.transform.localScale = new(scale, scale, 1);

                float randomAngle = Random.Range(0, 360);
                Quaternion randomRotation = Quaternion.Euler(0, 0, randomAngle);
                sprite.transform.rotation = randomRotation;
                Destroy(area, 3);
            }
        }
        public override void Die()
        {
            anim.Play("die");
            base.Die();
            GetComponent<SlimeMove>().enabled = false;
        }
        public override void Sleep(float duration)
        {
            base.Sleep(duration);
            anim.Play("sleep", -1, 0);
            anim.speed = 1f/duration;
        }
    }
}