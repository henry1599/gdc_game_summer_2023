using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.PlayerManager;

namespace GDC.Enemies
{
    public class Puffstool : Enemy
    {
        [SerializeField] float minDistanceDetectPlayer, maxDistanceDetectPlayer, distanceToAttack;
        [SerializeField] float maxTurningAngle;
        [SerializeField] float timeToTurn;
        [SerializeField] float timeToShoot = 3f;
        [SerializeField] List<EnemyBulletMove> puffstoolBullet;
        [SerializeField] GameObject thisShadow;
        float timeSinceLastTurn = 0f, currentHeadingY = 0f, currentHeadingX = 0f, timeToShootHelper;
        Vector3 direction, leftShadow = new Vector3(-0.03f, 0, 0);
        SpriteRenderer sprite;
        bool isShooting = false;
        void Start()
        {
            timeToShootHelper = timeToShoot;
            sprite = GetComponentInChildren<SpriteRenderer>();
        }
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.V))
            {
                Sleep(2f);
            }
        }

        void FixedUpdate()
        {
            if (!isCanMove || !isVisible) return;
            direction = Player.Instance.transform.position - this.transform.position;
            var dist = direction.magnitude;

            if (DetectWall(direction, dist))
            {
                anim.Play("stop");
                return;
            }

            FlipForPuffstool();

            if (dist <= maxDistanceDetectPlayer && dist >= minDistanceDetectPlayer && !isShooting)
            {
                if (!DetectWater(direction))
                {
                    anim.Play("move");
                    direction = direction.normalized;
                    direction += new Vector3(currentHeadingX, currentHeadingY, 0).normalized * 0.5f;

                    timeSinceLastTurn += Time.fixedDeltaTime;
                    if (timeSinceLastTurn > timeToTurn)
                    {
                        currentHeadingX = Random.Range(-maxTurningAngle, maxTurningAngle);
                        currentHeadingY = Random.Range(-maxTurningAngle, maxTurningAngle);
                        timeSinceLastTurn = 0f;
                    }
                    transform.position += direction.normalized * Time.fixedDeltaTime * Spe;
                }
                else anim.Play("stop");

                timeToShootHelper -= Time.fixedDeltaTime;
                if (timeToShootHelper <= 0f && dist <= distanceToAttack)
                {
                    timeToShootHelper = timeToShoot;
                    anim.Play("shoot");
                    isShooting = true;
                }
            }
            else if (!isShooting)
            {
                anim.Play("stop");
            }
        }

        void FlipForPuffstool()
        {
            if (direction.x >= 0)
            {
                sprite.flipX = true;
                thisShadow.transform.localPosition = -leftShadow;
            }
            else
            {
                sprite.flipX = false;
                thisShadow.transform.localPosition = leftShadow;
            }
        }

        public void PuffstoolShoot()
        {
            for (int i=0; i<puffstoolBullet.Count; i++)
            {
                puffstoolBullet[i].gameObject.SetActive(true);
                puffstoolBullet[i].transform.SetParent(null);
            }
            StartCoroutine(Cor_ResetPuffstoolBullet());
        }
        IEnumerator Cor_ResetPuffstoolBullet()
        {
            yield return new WaitForSeconds(1f);
            isShooting = false;
            yield return new WaitForSeconds(1.5f);
            for (int i=0; i<puffstoolBullet.Count; i++)
            {
                puffstoolBullet[i].transform.SetParent(this.transform);
                puffstoolBullet[i].transform.localPosition = puffstoolBullet[i].firstPos;
                puffstoolBullet[i].thisVFX.transform.SetParent(puffstoolBullet[i].transform);
                puffstoolBullet[i].thisVFX.transform.localPosition = Vector3.zero;
            }
        }
        public override void Sleep(float duration)
        {
            base.Sleep(duration);
            anim.Play("sleep", -1, 0);
            anim.SetFloat("sleepSpeed", 1f/duration);
        }
    }
}
