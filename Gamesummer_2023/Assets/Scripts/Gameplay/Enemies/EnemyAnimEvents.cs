using System.Collections;
using System.Collections.Generic;
using GDC.Managers;
using UnityEngine;

namespace GDC.Enemies
{
    public class EnemyAnimEvents : MonoBehaviour
    {
        [SerializeField] private GameObject objectToCastEvent;
        void Spike() {
            var obj = objectToCastEvent.GetComponent<SpikeSlime>();
            StartCoroutine(obj.Cor_ShootSpikes());
        }
        void FuzzBush() {
            objectToCastEvent.GetComponent<FuzzBush>().SpawnSlime();
        }
        void Poison() {
            objectToCastEvent.GetComponent<PoisonSlime>().ShootEvent();
        }
        void Anuboo() {
            objectToCastEvent.GetComponent<Anuboo>().ShootLaser();
        }
        void CrazeeDayzee() {
            objectToCastEvent.GetComponent<CrazeeDayzee>().Sing();
        }
        void FireMon() {
            objectToCastEvent.GetComponent<FireMon>().FireShoot();
        }
        void CrazyElec() {
            objectToCastEvent.GetComponent<CrazyElec>().ShootElecBullet();
        }
        void PepHit() {
            objectToCastEvent.GetComponent<Pep>().PepHit();
        }
        void SnowMan() {
            objectToCastEvent.GetComponent<SnowMan>().ShootSnow();
        }
        void ElectricJelly() {
            objectToCastEvent.GetComponent<ElectricJelly>().DoneShootElec();
        }
        void HotLips() {
            objectToCastEvent.GetComponent<HotLips>().ShootHotBullet();
        }
        void Zoras() {
            objectToCastEvent.GetComponent<Zoras>().ShootZoras();
        }
        void ZorasCollider() {
            objectToCastEvent.GetComponent<Zoras>().SetCollider();
        }
        void GlurpBullet() {
            objectToCastEvent.GetComponent<Glurp>().ShootGlurpBullet();
        }
        void GlurpPoison() {
            objectToCastEvent.GetComponent<Glurp>().ShootGlurpPoisonBullet();
        }
        void GlurpDoneHiding() {
            objectToCastEvent.GetComponent<Glurp>().DoneHiding();
        }
        void Puffstool() {
            objectToCastEvent.GetComponent<Puffstool>().PuffstoolShoot();
        }
        void Froggy() {
            objectToCastEvent.GetComponent<Froggy>().Tounge();
        }
        void CactusMan() {
            objectToCastEvent.GetComponent<CactusMan>().ShootCactusBullet();
        }
        void EyeMon() {
            objectToCastEvent.GetComponent<EyeMon>().ShootLaser();
        }
        void BoxyBite() {
            objectToCastEvent.GetComponent<Boxy>().BiteAnimEvent();
        }
    }
}
