using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace GDC.Enemies
{
    public class Wander : MonoBehaviour
    {
        float moveSpeed;
        bool isWandering = false;
        [HideInInspector] public bool isWalking = false;
        [HideInInspector] public Vector3 vectorHelper = Vector3.right;
        Vector3[] corner = new Vector3[2];
        [SerializeField] float distanceToWander, minWalkWait, maxWalkWait, minVelocity, maxVelocity;
        float timer = 0.5f, timerHelper;
        Enemy enemy;
        Coroutine cor;

        void Start()
        {
            ResetCorner();
            enemy = GetComponent<Enemy>();
            moveSpeed = enemy.Spe;
            timerHelper = timer;
        }        
        void FixedUpdate()
        {
            if (!enemy.isCanMove)
            {
                ResetCorner();
                return;
            }
            if (timerHelper >= 0f)
            {
                timerHelper -= Time.fixedDeltaTime;
                return;
            }
            if (!isWandering) cor = StartCoroutine(Wandering());
            if (isWalking) transform.position += moveSpeed * Time.fixedDeltaTime * vectorHelper.normalized;
        }

        IEnumerator Wandering()
        {
            bool detectWall = true;
            bool detectWater = true;
            while (detectWall || detectWater)
            {
                vectorHelper = new Vector3(Random.Range(corner[0].x, corner[1].x), Random.Range(corner[0].y, corner[1].y), 0) - transform.position;
                if (vectorHelper.magnitude < 1f) vectorHelper = vectorHelper.normalized;
                detectWall = enemy.DetectWall(vectorHelper, vectorHelper.magnitude + 1.5f);
                detectWater = enemy.DetectWater(vectorHelper, vectorHelper.magnitude + 1.5f);
            }
            
            float walkWait = Random.Range(minWalkWait, maxWalkWait);
            moveSpeed = Random.Range(minVelocity, maxVelocity) * enemy.Spe;
            float walkTime = vectorHelper.magnitude / moveSpeed;

            isWandering = true;

            yield return new WaitForSeconds(walkWait);

            if (vectorHelper.x >= 0) transform.localScale = new Vector3(-1,1,1);
            else transform.localScale = Vector3.one;

            isWalking = true;
            yield return new WaitForSeconds(walkTime);
            isWalking = false;
            isWandering = false;
        }
        public void ResetCorner()
        {
            if (cor != null) StopCoroutine(cor);
            isWalking = false;
            isWandering = false;
            corner[0] = transform.position + new Vector3(distanceToWander, distanceToWander, 0f);
            corner[1] = transform.position + new Vector3(-distanceToWander, -distanceToWander, 0f);
        }
        public void ResetTimer()
        {
            timerHelper = timer;
        }
        private void OnDrawGizmos()
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawLine(transform.position, transform.position + vectorHelper);
        }
    }
}