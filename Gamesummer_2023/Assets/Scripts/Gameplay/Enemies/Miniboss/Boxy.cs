using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.PlayerManager;
using UnityEngine.UI;
using DG.Tweening;
using GDC.Managers;

namespace GDC.Enemies
{
    public class Boxy : Boss
    {
        [Header("GameObjects")]
        [SerializeField] GameObject boxyBullet;
        [SerializeField] Transform bulletTip;
        [SerializeField] SpriteRenderer spriteRenderer;
        [SerializeField] GameObject shadow;
        [SerializeField] Collider2D thisCol;
        [Header("Statistics")]
        [SerializeField] float minTimer;
        [SerializeField] float maxTimer;
        [SerializeField] float baseDistance;
        [Header("Health")]
        [SerializeField] Slider boxyHealthSlider;
        [SerializeField] Image icon;
        [HideInInspector] public List<Enemy> enemies;
        [Header("FirstJump")]
        [SerializeField] GameObject VFX_Water;
        [SerializeField] GameObject VFX_Dirt, ring;
        bool isJumping, isSpitting, canDash = true, isDashing, isBitting;
        Vector3 direction, pos;
        Vector3 normalShadow = new(0.14f, 0.06f, 0), dashShadow = new(0.5f, 0.06f, 0);
        float timerHelper;
        bool firstJump;
        Coroutine jumpCoroutine, dashCoroutine, spitCoroutine;
        void Start()
        {
            firstJump = true;
            boxyHealthSlider.value = 0;
            timerHelper = 0;

            isCanMove = isCanGetHit = false;
            anim.gameObject.SetActive(false);
            shadow.SetActive(false);

            var sliderCanvas = boxyHealthSlider.GetComponentInParent<Canvas>();
            sliderCanvas.sortingLayerName = "UI_World";
            boxyHealthSlider.gameObject.SetActive(false);
        }
        void Update()
        {
            if (!isCanMove) return;
            direction = Player.Instance.transform.position - transform.position;
            if (!isJumping && !isSpitting && !isDashing && !isBitting)
            {
                anim.Play("idle");
                if (direction.x >= 0) transform.localScale = new Vector3(-1,1,1);
                else transform.localScale = Vector3.one;
            }
            boxyHealthSlider.transform.localScale = new Vector3 (1.75f / transform.localScale.x, 1.75f, 1);
            if (direction.magnitude <= 1.5f && !isDashing && !isJumping && !isSpitting)
            {
                if (!isBitting) BoxyBite();
            }
        }
        void FixedUpdate()
        {
            if (!isCanMove) return;
            if (direction.magnitude > baseDistance && canDash)
            {
                dashCoroutine = StartCoroutine(Dash());
                return;
            }
            timerHelper -= Time.fixedDeltaTime;
            if (timerHelper <= 0 && !isJumping && !isSpitting && !isDashing && !isBitting)
            {
                int idx = Random.Range(0,2);
                if (idx == 0) spitCoroutine = StartCoroutine(BoxySpit());
                else jumpCoroutine = StartCoroutine(BoxyJump(Player.Instance.transform.position));
            }
        }
        void BoxyBite()
        {
            int rand;
            if (Player.Instance.isCanGetHit == false) rand = 2;
            else rand = Random.Range(0,3);
            
            isBitting = true;
            if (rand < 2)
            {
                anim.Play("bite");
            }
            else 
            {
                var playerPos = Player.Instance.transform.position;
                var dest = playerPos + (transform.position - playerPos).normalized * 4;
                if (dest != FixPos(dest))
                {
                    dest = playerPos + (playerPos - transform.position).normalized * 4;
                }
                jumpCoroutine = StartCoroutine(BoxyJump(dest));
                isBitting = false;
            }
        }
        IEnumerator Dash()
        {
            isDashing = true;
            shadow.transform.localPosition = dashShadow;
            anim.Play("dash");
            SoundManager.Instance.PlaySoundRelatedToCamera(transform.position, AudioPlayer.SoundID.SFX_ACRO_JUMP);
            canDash = false;
            rb.velocity = direction.normalized * 15f;
            yield return new WaitForSeconds(0.75f);
            anim.Play("idle");
            shadow.transform.localPosition = normalShadow;
            yield return new WaitForSeconds(0.5f);

            canDash = true;
            isDashing = false;
            timerHelper = minTimer;
        }
        IEnumerator BoxySpit()
        {
            isSpitting = true;
            canDash = false;
            anim.Play("spit");
            yield return new WaitForSeconds(0.4f);

            for (int i=0; i<3; i++)
            {
                yield return new WaitForSeconds(0.45f);

                pos = Player.Instance.transform.position + (Vector3)Random.insideUnitCircle * 2f;
                pos = FixPos(pos);
                if (transform.localScale.x == 1 && pos.x > bulletTip.position.x) pos.x = bulletTip.position.x;
                else if (transform.localScale.x == -1 && pos.x < bulletTip.position.x) pos.x = bulletTip.position.x;
                var dir = pos - bulletTip.position;
                
                var go = Instantiate(boxyBullet, bulletTip.position, Quaternion.identity);
                SoundManager.Instance.PlaySoundRelatedToCamera(transform.position, AudioPlayer.SoundID.SFX_ENEMY_SHOOT);
                go.GetComponent<BoxyBullet>().destination = pos;
                go.GetComponent<EnemyBulletMove>().SetDirection(dir, false);
            }

            yield return new WaitForSeconds(0.5f);
            anim.Play("idle");

            isSpitting = false;
            canDash = true;
            timerHelper = Random.Range(minTimer, maxTimer);
        }
        Vector3 FixPos(Vector3 pos)
        {
            pos.x = Mathf.Clamp(pos.x, minAreaX, maxAreaX);
            pos.y = Mathf.Clamp(pos.y, minAreaY, maxAreaY);
            return pos;
        }
        IEnumerator BoxyJump(Vector3 destination)
        {
            isJumping = true;
            canDash = false;
            // var destination = Player.Instance.transform.position;
            Vector3 dir;

            if (!firstJump) dir = destination - transform.position;
            else dir = ring.transform.position + new Vector3(-0.5f,-0.3f,0) - transform.position;
            
            if (!firstJump) 
            {
                anim.Play("jump");
                yield return new WaitForSeconds(0.5f);
            }
            else anim.Play("down");

            float height = 1.5f;
            float timeToDropDown = 0.5f;
            float timerForDropdown = timeToDropDown;

            SoundManager.Instance.PlaySoundRelatedToCamera(transform.position, AudioPlayer.SoundID.SFX_ACRO_JUMP);
            while (timerForDropdown > 0f)
            {
                timerForDropdown -= Time.fixedDeltaTime;
                if (timerForDropdown <= timeToDropDown / 2) anim.Play("down");
                transform.position += dir / timeToDropDown * Time.fixedDeltaTime;
                float tempY = Mathf.Sin(timerForDropdown * Mathf.PI / timeToDropDown) * height;
                spriteRenderer.transform.localPosition = new Vector3(0, tempY, 0);
                yield return new WaitForSeconds(Time.fixedDeltaTime);

                if (timerForDropdown <= Time.fixedDeltaTime)
                {
                    thisCol.enabled = true;
                }
                else if (timerForDropdown <= 0.4f)
                {
                    thisCol.enabled = false;
                }
            }
            spriteRenderer.transform.localPosition = Vector3.zero;
            yield return new WaitForSeconds(0.5f);
            anim.Play("idle");

            isJumping = false;
            canDash = true;
            timerHelper = Random.Range(minTimer, maxTimer);
        }
        public override void UpdateHealth(float damage)
        {
            base.UpdateHealth(damage);
            boxyHealthSlider.DOValue(Hp / MaxHp, 0.5f);
        }
        public override void Die()
        {
            base.Die();
            foreach(var enemy in enemies)
            {
                if (enemy != null && enemy.gameObject.activeSelf)
                {
                    if (enemy.Hp > 0)
                    {
                        enemy.Hp = 0;
                        enemy.Die();
                    }
                }
            }
            isDashing = isJumping = isSpitting = isBitting = isCanMove = false;
            if (dashCoroutine != null) StopCoroutine(dashCoroutine);
            if (jumpCoroutine != null) StopCoroutine(jumpCoroutine);
            if (spitCoroutine != null) StopCoroutine(spitCoroutine);

            boxyHealthSlider.GetComponent<RectTransform>().DOSizeDelta(Vector3.zero, 1).SetEase(Ease.InBounce);
            icon.GetComponent<RectTransform>().DOSizeDelta(Vector3.zero, 1).SetEase(Ease.InBounce);
        }
        public override void Disappear()
        {
            ring.SetActive(true);
            ring.transform.position = transform.position;
            SoundManager.Instance.PlayMusicWithIntro(AudioPlayer.SoundID.MUSIC_LAKE_BEGIN, AudioPlayer.SoundID.MUSIC_LAKE, 1, 1);
            base.Disappear();        
        }
        [NaughtyAttributes.Button]
        public void JumpToArea()
        {
            StartCoroutine(JumpFromWater());
        }
        IEnumerator JumpFromWater()
        {
            transform.localScale = new Vector3(-1,1,1);
            anim.gameObject.SetActive(true);
            shadow.SetActive(true);
            anim.Play("water");
            thisCol.isTrigger = true;
            VFX_Water.SetActive(true);
            yield return new WaitForSeconds(1f/3);

            SoundManager.Instance.PlaySoundRelatedToCamera(transform.position, AudioPlayer.SoundID.SFX_SPLASH);
            StartCoroutine(BoxyJump(Vector3.zero));
            yield return new WaitForSeconds(0.4f);

            VFX_Dirt.SetActive(true);
            VFX_Dirt.transform.position = transform.position - new Vector3(0, 0.5f, 0);
            yield return new WaitForSeconds(0.6f);

            thisCol.isTrigger = false;
            VFX_Water.SetActive(false);

            yield return new WaitForSeconds(0.5f);
            anim.Play("bite");
            yield return new WaitForSeconds(0.3f);
            transform.DOMove(ring.transform.position + new Vector3(0, -0.3f, 0), 0.2f);
            ring.SetActive(false);
            yield return new WaitForSeconds(0.5f);


            boxyHealthSlider.gameObject.SetActive(true);
            boxyHealthSlider.transform.localScale = Vector3.zero;
            boxyHealthSlider.GetComponent<RectTransform>().DOScale(new Vector3(-1.75f, 1.75f, 1), 0.5f)
                .SetEase(Ease.OutBounce)
                .OnComplete(
                    () => boxyHealthSlider.DOValue(1,0.5f)
                        .OnComplete(
                        () => {
                            //isCanMove = true; 
                            //firstJump = false;
                        }
                    )
            );
            yield return new WaitForSeconds(0.5f);
            anim.Play("spit");
            yield return new WaitForSeconds(0.8f);
            for (int i = 0; i < 3; i++)
            {
                Camera.main.DOOrthoSize(3.6f, 0.25f).OnComplete(() =>
                {
                    SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_BITE, 2);
                    Camera.main.DOOrthoSize(3.9f, 0.25f);
                });
                yield return new WaitForSeconds(0.5f);
            }
            Camera.main.DOOrthoSize(4.5f, 0.25f);
            // Wait for the animation to end
            //yield return new WaitForSeconds(1.25f);
            isCanMove = true;
            firstJump = false;
            isCanGetHit = true;
        }
    
        public void BiteAnimEvent()
        {
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_BITE, 2);
            if (firstJump) return;

            if (transform.localScale.x == 1) transform.DOMoveX(transform.position.x - 1, 0.5f);
            else transform.DOMoveX(transform.position.x + 1, 0.5f);
            StartCoroutine(Cor_ResetBite());
        }
        IEnumerator Cor_ResetBite()
        {
            yield return new WaitForSeconds(0.5f);
            isBitting = false;
            timerHelper = minTimer;
        }
    }
}
