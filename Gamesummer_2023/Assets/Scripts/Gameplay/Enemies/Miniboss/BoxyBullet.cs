using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.Managers;
using DG.Tweening;

namespace GDC.Enemies
{
    public class BoxyBullet : MonoBehaviour
    {
        [SerializeField] List<Enemy> enemies;
        [SerializeField] List<GameObject> items;
        [SerializeField] GameObject shadow;
        [SerializeField] BoxCollider2D col;
        [HideInInspector] public Vector3 destination;
        Boxy boxy;
        void Start()
        {
            boxy = FindObjectOfType<Boxy>();
            StartCoroutine(BoxExplode());
        }
        IEnumerator BoxExplode()
        {
            yield return new WaitForSeconds(1);
            GetComponentInChildren<Animator>().Play("boxy_bullet");
            yield return new WaitForSeconds(0.5f);
            SoundManager.Instance.PlaySoundRelatedToCamera(transform.position, AudioPlayer.SoundID.SFX_SMALL_EXPLOSION);
            int rand = Random.Range(1, 101);
            if (rand <= 25)
            {
                int randomIdx = Random.Range(0, items.Count);
                Instantiate(items[randomIdx], transform.position + Vector3.down * 0.3f, Quaternion.identity);
            }
            else if (rand <= 50)
            {
                int randomIdx = Random.Range(0, enemies.Count);
                if (boxy.Hp > 0)
                {
                    var enemy = Instantiate(enemies[randomIdx], transform.position + Vector3.down * 0.3f, Quaternion.identity);
                    boxy.enemies.Add(enemy);
                    enemy.StartCoroutine(Cor_EnemyDelay(enemy));
                }
            }
            yield return new WaitForSeconds(1);
            shadow.GetComponent<SpriteRenderer>().DOColor(Color.clear, 1).OnComplete(() => Destroy(shadow));
        }
        IEnumerator Cor_EnemyDelay(Enemy enemy)
        {
            enemy.isCanMove = false;
            yield return new WaitForSeconds(2f);
            enemy.isCanMove = true;
        }
    }
}
