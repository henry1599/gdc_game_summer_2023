using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.PlayerManager;
using GDC.Managers;

namespace GDC.Enemies
{
    public class FireSlime : Enemy
    {
        [SerializeField] Transform fireTip;
        [SerializeField] GameObject fireBullet;
        [SerializeField] float minDistanceDetectPlayer, maxDistanceDetectPlayer, distanceToFire;
        bool canShoot, isSleeping;
        SlimeMove slimeMove;
        float waitTime = 1;
        GameObject fire = null;
        Coroutine shooting;
        void Start()
        {
            slimeMove = GetComponent<SlimeMove>();
        }

        void FixedUpdate()
        {
            if (isSleeping)
            {
                if (fire != null) 
                {
                    fire.GetComponent<Animator>().Play("disappear");
                    Destroy(fire, 0.5f);
                    fire = null;
                }
                isSleeping = false;
            }
            if (!isCanMove || !isVisible) return;
            var direction = Player.Instance.transform.position - transform.position;
            var dist = direction.magnitude;
            if (!canShoot)
            {
                if (!DetectWall(direction, dist)) waitTime -= Time.fixedDeltaTime;
                else waitTime = 1;
                if (waitTime <= 0f)
                {
                    slimeMove.slimeCanMove = true;
                    slimeMove.canFlip = true;
                    canShoot = true;
                    waitTime = 4f;
                    return;
                }
                else 
                {
                    slimeMove.slimeCanMove = false;
                }
            }
            if (canShoot && dist <= distanceToFire && slimeMove.slimeCanMove == false)
            {
                slimeMove.canFlip = false;
                anim.Play("shoot");
                canShoot = false;
                SoundManager.Instance.PlaySoundRelatedToCamera(transform.position, AudioPlayer.SoundID.SFX_SLIME_FIRE);
                shooting = StartCoroutine(FireShoot());
            }
        }
        IEnumerator FireShoot()
        {
            slimeMove.ResetTime(3f, 0.5f, 0f);
            var dir = Player.Instance.transform.position - transform.position;
            int idx = 1;
            for (int i=0; i<10; i++)
            {
                yield return new WaitForSeconds(0.15f);
                var angle = Random.Range(0f, 15f);
                fire = Instantiate(fireBullet, fireTip.position, Quaternion.identity);
                var newDirection = Quaternion.Euler(idx * angle, 0f, idx * angle) * dir.normalized;
                fire.GetComponent<EnemyBulletMove>().SetDirection(newDirection);
                fire.transform.position += dir.normalized * 0.1f;
                idx *= -1;
            }
        }
        public override void Die()
        {
            anim.Play("die");
            StopCoroutine(shooting);
            base.Die();
            GetComponent<SlimeMove>().enabled = false;
        }
        public override void Sleep(float duration)
        {
            base.Sleep(duration);
            isSleeping = true;
            anim.Play("sleep", -1, 0);
            anim.speed = 1f/duration;
        }
    }
}