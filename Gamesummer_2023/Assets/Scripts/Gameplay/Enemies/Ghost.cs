using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.PlayerManager;
using DG.Tweening;

namespace GDC.Enemies
{
    public class Ghost : Enemy
    {
        [SerializeField] float frequency = 1, magnitude = 0.5f;
        float timeRunning = 0;
        Vector3 pos, axis;
        [SerializeField] float minDistanceDetectPlayer, maxDistanceDetectPlayer;
        [SerializeField] GameObject[] trailRendeder;
        float resetTime = 2.2f, resetTimeHelper;
        SpriteRenderer sprite;
        bool isClear, isChanging;
        Tween tween;
    
        void Start()
        {
            sprite = GetComponentInChildren<SpriteRenderer>();
            pos = transform.position;
            resetTimeHelper = 0f;
        }
        void FixedUpdate()
        {
            if (!isCanMove || !isVisible) return;
            if (resetTimeHelper >= 0f)
            {
                resetTimeHelper -= Time.fixedDeltaTime;
                return;
            }
            var perpendicular = new Vector3(axis.y, -axis.x);

            axis = Player.Instance.transform.position - transform.position;
            if (axis.x >= 0) transform.localScale = new Vector3(-1,1,1);
            else transform.localScale = Vector3.one;

            if (!isChanging && !isClear)
            {
                isChanging = true;
                tween = sprite.DOFade(0.7f, 0.5f).OnComplete(() => 
                    sprite.DOFade(1, 0.5f).OnComplete(() => isChanging = false)
                );
            }

            float dist = Vector2.Distance(transform.position, Player.Instance.transform.position);
            if (dist >= minDistanceDetectPlayer && dist <= maxDistanceDetectPlayer)
            {
                pos += Spe * Time.fixedDeltaTime * axis.normalized;
                timeRunning += Time.fixedDeltaTime;
                transform.position = pos + magnitude * Mathf.Sin(timeRunning * frequency) * perpendicular;
            }
            else 
            {
                transform.position = transform.position;
            }
        }
        public override void OnTriggerEnter2D(Collider2D collision)
        {
            base.OnTriggerEnter2D(collision);
            if (collision.CompareTag("Player"))
            {
                resetTimeHelper = resetTime;
                timeRunning = 0f;
            }
        }
        public override void OnTriggerStay2D(Collider2D collision)
        {
            base.OnTriggerStay2D(collision);
            tween.Kill();
            if (collision.CompareTag("Wall"))
            {
                if (!isClear) 
                {
                    sprite.DOFade(0, 0.4f);
                    isClear = true;
                    foreach (var i in trailRendeder)
                        i.SetActive(false);
                }
            }
        }
        public override void OnTriggerExit2D(Collider2D collision)
        {
            base.OnTriggerExit2D(collision);
            if (collision.CompareTag("Wall"))
            {   
                if (isClear)
                {
                    sprite.DOFade(1, 0.4f);
                    isClear = false;
                    foreach (var i in trailRendeder)
                        i.SetActive(true);
                }
            }
        }
        public override void Sleep(float duration)
        {
            base.Sleep(duration);
            anim.Play("sleep", -1, 0);
            anim.SetFloat("sleepSpeed", 1f/duration);
        }
        public override void Die()
        {
            base.Die();
            anim.Play("die");
        }
    }
}