using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.PlayerManager;
using GDC.Managers;
using AudioPlayer;

namespace GDC.Enemies
{
    public class MoldWorm : Enemy
    {
        [SerializeField] float circleDiameter;
        [SerializeField] List<Transform> wormTails = new();
        List<Vector2> positions = new();
        [SerializeField] Animator wormHead;
        [SerializeField] SpriteRenderer wormHeadFlip;
        [SerializeField] float minDistanceDetectPlayer, maxDistanceDetectPlayer, distanceToTurn;
        [SerializeField] GameObject hole;
        [SerializeField] float timerDig, timerAppear;
        Vector3 previousDirection, direction, constDirection, newPos;
        int removeIdx = -1, i = 20;
        float timerDigHelper, hideTime = 3, delayTime = 1;
        [HideInInspector] public float resetTime;
        bool isHiding = false, isAppearing = false, isDigging = false, haveToTurnCircle, isFinding, appeared, firstSound;
        Vector3 previousPos;
        void Start()
        {
            firstSound = true;
            previousPos = transform.position;
            timerDigHelper = timerDig;
            haveToTurnCircle = false;
            previousDirection = Vector3.left;
            positions.Add(this.transform.position);
            foreach (var i in wormTails)
                positions.Add(i.position);

            wormHead.gameObject.SetActive(false);
            foreach (var tail in wormTails)
                tail.gameObject.SetActive(false);
        }
        void FixedUpdate()
        {
            if (!isCanMove || !isVisible) return;
            if (delayTime >= 0)
            {
                delayTime -= Time.fixedDeltaTime;
                return;
            }
            else if (!appeared)
            {
                appeared = true;
                isAppearing = true;
            }
            if (resetTime >= 0)
            {
                positions.Clear();
                positions.Add(this.transform.position);
                foreach (var i in wormTails)
                    positions.Add(i.position);
                resetTime -= Time.fixedDeltaTime;
                return;
            }
            anim.speed = 1f;
            rb.constraints = RigidbodyConstraints2D.FreezeRotation;

            direction = Player.Instance.transform.position - this.transform.position;
            var distance = direction.magnitude;
            bool detectWallOrWater = DetectWall(direction, 1f) || DetectWater(direction, 1f);
            if (detectWallOrWater && !isDigging) 
            {
                isDigging = true;
                constDirection = direction;
            }
            Dig();
            ChooseAnimForHead();
            
            haveToTurnCircle = true;
            direction = new Vector3(previousDirection.x + (direction.x - previousDirection.x) / 3, previousDirection.y + (direction.y - previousDirection.y) / 3, 0f);

            if (distance <= maxDistanceDetectPlayer && distance >= minDistanceDetectPlayer)
            {
                transform.position += Spe * Time.fixedDeltaTime * direction.normalized;
                if (firstSound)
                {
                    SoundManager.Instance.PlaySoundRelatedToCamera(transform.position, SoundID.SFX_MOLDWORM);
                    firstSound = false;
                }
                if (!haveToTurnCircle) previousDirection = Player.Instance.transform.position - this.transform.position;
                else 
                {
                    i--;
                    if (i == 0)
                    {
                        haveToTurnCircle = false;
                        previousDirection = direction;
                        i = 20;
                    }
                }
                float dist = ((Vector2)this.transform.position - positions[0]).magnitude;
                if (dist > circleDiameter)
                {
                    Vector2 dir = ((Vector2)this.transform.position - positions[0]).normalized;
                    positions.Insert(0, positions[0] + dir * circleDiameter);
                    positions.RemoveAt(positions.Count - 1);
                    dist -= circleDiameter;
                }
                for (int i=0; i<wormTails.Count; i++)
                    wormTails[i].position = Vector2.Lerp(positions[i+1], positions[i], dist / circleDiameter);
            }
            else if (distance < minDistanceDetectPlayer)
            {
                positions.Clear();
                positions.Add(this.transform.position);
                foreach (var i in wormTails)
                    positions.Add(i.position);
            }
        }
        void Dig()
        {
            if (isDigging)
            {
                direction = constDirection;
                if (!isFinding)
                {
                    newPos = NewPositionAfterDigging(7);
                    isFinding = true;
                }
                timerDigHelper -= Time.fixedDeltaTime;
                if (timerDigHelper <= 0f)
                {
                    timerDigHelper = timerDig;
                    SetWormPart(false);
                    if (removeIdx == wormTails.Count) 
                    {
                        removeIdx = -1;
                        isDigging = false;
                        isHiding = true;
                    }
                }
            }
            if (isHiding)
            {
                if (isFinding)
                {
                    if (newPos != new Vector3(-1000,-1000,0)) 
                    {
                        transform.position = newPos;
                        previousPos = newPos;
                    }
                    else 
                    {
                        transform.position = previousPos;
                    }
                    isFinding = false;
                }
                hideTime -= Time.fixedDeltaTime;
                if (hideTime <= 0f)
                {
                    isAppearing = true;
                    hideTime = 3f;
                    isHiding = false;
                }
            }
            if (isAppearing)
            {
                isNotMoveAway = true;
                timerDigHelper -= Time.fixedDeltaTime;
                if (timerDigHelper <= 0f)
                {
                    timerDigHelper = timerAppear;
                    SetWormPart(true);
                    if (removeIdx == wormTails.Count) 
                    {
                        removeIdx = -1;
                        isDigging = false;
                        isHiding = false;
                        isAppearing = false;
                        timerDigHelper = timerDig;
                        isNotMoveAway = false;
                    }
                }
            }
        }
        void SetWormPart(bool setActive)
        {
            if (removeIdx != -1)
            {
                if (!setActive) wormHeadFlip.flipX = false;
                wormTails[removeIdx].gameObject.SetActive(setActive);
            }
            else 
            {
                HoleAppear();
                if (!setActive) wormHeadFlip.flipX = false;
                wormHead.gameObject.SetActive(setActive);
            }
            removeIdx++;
        }
        void HoleAppear()
        {
            var go = Instantiate(hole, wormHead.gameObject.transform.position + direction.normalized * 0.2f + Vector3.down * 0.35f, Quaternion.identity);
            Destroy(go, 5f);
            SoundManager.Instance.PlaySoundRelatedToCamera(transform.position, SoundID.SFX_DIG);
        }
        Vector3 NewPositionAfterDigging(float radius)
        {
            int num = 0;
            int layerMask = (1 << 3) | (1 << 4);
            Vector3 randomPosition = Player.Instance.transform.position;
            bool isEmpty = false;
            while (!isEmpty)
            {
                if (++num > 10000) 
                {
                    return new(-1000,-1000,0);
                }
                randomPosition = 2 * radius * Random.insideUnitCircle;
                Collider2D[] colliders = Physics2D.OverlapCircleAll(randomPosition, radius, layerMask);
                isEmpty = colliders.Length == 0;
            }
            return randomPosition;
        }
        void ChooseAnimForHead()
        {
            if (isDigging || isHiding || (isAppearing && removeIdx == -1)) {
                return;
            }
            float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
            if (angle >= -22.5f && angle < 22.5f) {
                wormHead.Play("right");
            }
            else if (angle >= 22.5f && angle < 67.5f) {
                wormHead.Play("upright");
            }
            else if (angle >= 67.5f && angle < 112.5f) {
                wormHead.Play("up");
            }
            else if (angle >= 112.5f && angle < 157.5f) {
                wormHead.Play("upleft");
            }
            else if (angle >= -67.5f && angle < -22.5f) {
                wormHead.Play("downright");
            }
            else if (angle >= -112.5f && angle < -67.5f) {
                wormHead.Play("down");
            }
            else if (angle >= -157.5f && angle < -112.5f) {
                wormHead.Play("downleft");
            }
            else {
                wormHead.Play("left");
            }
        }
        void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.gameObject.CompareTag("Player"))
            {
                resetTime = 0f;
                rb.constraints = RigidbodyConstraints2D.FreezeAll;
            }
        }
        void OnCollisionStay2D(Collision2D collision)
        {
            if (collision.gameObject.CompareTag("Player"))
            {
                resetTime = 0f;
                rb.constraints = RigidbodyConstraints2D.FreezeAll;
            }
        }
        public override void Sleep(float duration)
        {
            base.Sleep(duration);
            anim.speed = 0f;
        }
        public override void Die()
        {
            base.Die();
            SoundManager.Instance.StopSFX(SoundID.SFX_MOLDWORM);
        }
        public void MoldWormStopSFXMove()
        {
            SoundManager.Instance.StopSFX(SoundID.SFX_MOLDWORM);
            firstSound = true;
        }
    }
}