using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.PlayerManager;
using GDC.Managers;

namespace GDC.Enemies
{
    public class Zoras : Enemy
    {
        [SerializeField] GameObject zorasBullet;
        [SerializeField] float timer = 5f;
        [SerializeField] float maxDistanceDetectPlayer = 5f;
        [SerializeField] Transform zorasTip;
        [SerializeField] GameObject thisShadow;
        float timerHelper;
        Vector3 dir;
        bool isUp;
        void Start()
        {
            isUp = false;
            timerHelper = timer;
        }

        void FixedUpdate()
        {
            if (!isCanMove || !isVisible) return;
            isVisible = true;
            dir = Player.Instance.transform.position - transform.position;
            var dist = dir.magnitude;

            if (dist <= maxDistanceDetectPlayer && !DetectWall(dir, dist))
            {
                timerHelper -= Time.fixedDeltaTime;
                if (timerHelper <= 0f)
                {
                    anim.Play("shoot");
                    timerHelper = timer;
                }
            }
            else timerHelper = timer;
        }
        public void ShootZoras()
        {
            GameObject go = Instantiate(zorasBullet, zorasTip.position + dir.normalized * 0.1f, Quaternion.identity);
            go.GetComponent<EnemyBulletMove>().SetDirection(dir, false);
        }
        public void SetCollider()
        {
            isUp = !isUp;
            if (isUp)
                SoundManager.Instance.PlaySoundRelatedToCamera(transform.position, AudioPlayer.SoundID.SFX_SPLASH);
            else SoundManager.Instance.PlaySoundRelatedToCamera(transform.position, AudioPlayer.SoundID.SFX_SPLASH_1);
        }
        public override void Sleep(float duration)
        {
            base.Sleep(duration);
            anim.Play("sleep", -1, 0);
            anim.SetFloat("sleepSpeed", 1f/duration);
        }
    }
}