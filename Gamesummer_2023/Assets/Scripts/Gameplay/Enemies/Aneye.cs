using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.PlayerManager;
using GDC.Managers;

namespace GDC.Enemies
{
    public class Aneye : Enemy
    {
        [SerializeField] float baseDistance, timer, eyeDistance;
        [SerializeField] Animator aneyeAnim;
        [SerializeField] List<GameObject> bullets, bullets1;
        [SerializeField] GameObject eye;
        [SerializeField] List<GameObject> spritesHelper;
        float timerHelper;
        bool degree90, canShoot, firstAppear;
        void Start()
        {
            canShoot = true;
            timerHelper = timer;
        }
        void Update()
        {
            UpdateEyeLocalPosition();
        }
        void FixedUpdate()
        {
            float distance = Vector3.Distance(Player.Instance.transform.position, transform.position);
            if (distance < baseDistance)
            {
                timerHelper -= Time.fixedDeltaTime;
                if (timerHelper < 0 && canShoot)
                {
                    StartCoroutine(AneyeShoot());
                }
                if (!firstAppear)
                {
                    aneyeAnim.Play("aneye_appear");
                    anim.Play("appear");
                    firstAppear = true;
                    SoundManager.Instance.PlaySoundRelatedToCamera(transform.position, AudioPlayer.SoundID.SFX_POP);
                }
            }
            else
            {
                if (firstAppear)
                {
                    timerHelper = timer;
                    firstAppear = false;
                    aneyeAnim.Play("aneye_disappear");
                    anim.Play("disappear");
                    SoundManager.Instance.PlaySoundRelatedToCamera(transform.position, AudioPlayer.SoundID.SFX_POP);
                }
            }
        }
        void UpdateEyeLocalPosition()
        {
            var direction = Player.Instance.transform.position - transform.position;
            eye.transform.localPosition = Vector3.zero + direction.normalized * eyeDistance;
        }
        IEnumerator AneyeShoot()
        {
            canShoot = false;
            anim.Play("shoot");
            aneyeAnim.Play("aneye_shoot");
            yield return new WaitForSeconds(0.42f);
            SoundManager.Instance.PlaySoundRelatedToCamera(transform.position, AudioPlayer.SoundID.SFX_ENEMY_SHOOT);
            yield return new WaitForSeconds(0.08f);
            List<GameObject> temp;
            if (degree90) temp = bullets;
            else temp = bullets1;
            foreach (var go in temp)
            {
                go.SetActive(true);
                go.transform.SetParent(null);
            }
            yield return new WaitForSeconds(1.6f);
            foreach (var go in temp)
            {
                go.transform.SetParent(transform);
                go.transform.localPosition = go.GetComponent<EnemyBulletMove>().firstPos;
            }
            degree90 = !degree90;
            timerHelper = timer;
            canShoot = true;
        }
        public override void Die()
        {
            base.Die();
            foreach(var go in spritesHelper)
            {
                go.SetActive(false);
            }
        }
        public override void Sleep(float duration)
        {
            base.Sleep(duration);
            anim.Play("sleep", -1, 0);
            anim.SetFloat("sleepSpeed", 1f/duration);
        }
    }
}
