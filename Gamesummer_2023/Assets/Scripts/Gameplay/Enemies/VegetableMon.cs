using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.PlayerManager;

namespace GDC.Enemies
{
    public class VegetableMon : Enemy
    {
        private void OnCollisionStay2D(Collision2D collision)
        {
            GetComponent<SlimeMove>().ResetTime(0f, 0.2f, 0f);
        }
        public override void Sleep(float duration)
        {
            base.Sleep(duration);
            anim.Play("sleep", -1, 0);
            anim.SetFloat("sleepSpeed", 1f/duration);
        }
        public override void Die()
        {
            base.Die();
            GetComponent<SlimeMove>().enabled = false;
        }
    }
}