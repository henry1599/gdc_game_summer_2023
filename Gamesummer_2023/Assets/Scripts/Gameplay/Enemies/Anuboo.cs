using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.PlayerManager;
using GDC.Managers;

namespace GDC.Enemies
{
    public class Anuboo : Enemy
    {
        [SerializeField] GameObject laserBeam, vfx_tele;
        Vector3 posToTeleport;
        [SerializeField] float timer = 3f, offsetToTele;
        bool canShoot = true;
        [SerializeField] Transform laserTip;
        void Start()
        {
            laserBeam.SetActive(false);
        }

        void FixedUpdate()
        {
            if (!isCanMove || !isVisible) return;
            else anim.speed = 1f;
            timer -= Time.fixedDeltaTime;
            if (timer <= 0 && canShoot)
            {
                StartCoroutine(Teleport());
                canShoot = false;
            }
        }
        IEnumerator Teleport()
        {
            vfx_tele.SetActive(true);
            yield return new WaitForSeconds(1.5f);
            bool isFacingRight = Player.Instance.GetComponentInChildren<Animator>().transform.localScale.x == 1;
            Vector3 newScale;
            if (isFacingRight)
            {
                posToTeleport = Player.Instance.transform.position + new Vector3(-offsetToTele, 0, 0);
                newScale = new Vector3(-1,1,1);
            }
            else
            {
                posToTeleport = Player.Instance.transform.position + new Vector3(offsetToTele, 0, 0);
                newScale = Vector3.one;
            }
            laserBeam.transform.position = laserTip.position;
            anim.Play("tele");
            SoundManager.Instance.PlaySoundRelatedToCamera(transform.position, AudioPlayer.SoundID.SFX_ENEMY_TELEPORT, 1.2f);
            yield return new WaitForSeconds(0.25f);
            transform.localScale = newScale;
            this.transform.position = posToTeleport;
            anim.Play("appear");
            // SoundManager.Instance.PlaySoundRelatedToCamera(transform.position, AudioPlayer.SoundID.SFX_ENEMY_TELEPORT);
            yield return new WaitForSeconds(0.25f);
            anim.Play("shoot");
            yield return new WaitForSeconds(1f);
            
            laserBeam.transform.SetParent(transform);
            laserBeam.transform.position = laserTip.position;
            canShoot = true;
            timer = 3f;
        }
        public void ShootLaser()
        {
            laserBeam.transform.SetParent(null);
            laserBeam.transform.localScale = Vector3.one;
            laserBeam.GetComponent<EnemyBulletMove>().SetDirection(Player.Instance.transform.position - laserTip.position);
            laserBeam.SetActive(true);
            SoundManager.Instance.PlaySoundRelatedToCamera(transform.position, AudioPlayer.SoundID.SFX_LASER_BEAM);
        }
        public override void Sleep(float duration)
        {
            base.Sleep(duration);
            anim.Play("sleep", -1, 0);
            anim.SetFloat("sleepSpeed", 1f/duration);
        }
    }
}