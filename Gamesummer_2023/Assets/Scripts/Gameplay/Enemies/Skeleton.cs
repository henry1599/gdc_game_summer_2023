using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.Managers;
using GDC.PlayerManager;
using NaughtyAttributes;

namespace GDC.Enemies
{
    public class Skeleton : Enemy
    {
        [SerializeField] float circleRadius, timerAttack, maxDistanceDetectPlayer, distanceToAttack;
        [SerializeField, Foldout("Bow Skeleton")] GameObject bowArrow;
        [SerializeField, Foldout("Bow Skeleton")] Transform bowArrowTip;
        float timerAttackHelper;
        float timerTurn = 10f, timerTurnHelper;
        [HideInInspector] public bool isAttacking;
        Vector3 dir;
        int turn = 1;
        Coroutine afterSleepCoroutine;
        void Start()
        {
            timerAttackHelper = timerAttack;
            timerTurnHelper = timerTurn;
        }
        void FixedUpdate()
        {
            if (!isCanMove || !isVisible) return;

            dir = Player.Instance.transform.position - transform.position;
            float dist = dir.magnitude;
            if (dir.x >= 0f) transform.localScale = Vector3.one;
            else transform.localScale = new Vector3(-1,1,1);

            if (dist <= maxDistanceDetectPlayer && !isAttacking && !DetectWall(dir, dist))
            {
                anim.SetBool("isMoving", true);
                if (dist >= circleRadius)
                {
                    Chase(dir.normalized * Spe);
                }
                else
                {
                    timerAttackHelper -= Time.fixedDeltaTime;
                    if (timerAttackHelper <= 0f)
                    {
                        if (!isAttacking && dist > distanceToAttack)
                        {
                            Chase(dir.normalized * Spe);
                            return;
                        }
                        StartAttack();
                        timerAttackHelper = timerAttack;
                    }
                    if (DetectWater(dir)) 
                    {
                        anim.SetBool("isMoving", false);
                        return;
                    }
                    dir = new Vector2(-dir.y, dir.x);
                    timerTurnHelper -= Time.fixedDeltaTime;
                    if (timerTurnHelper <= 0f)
                    {
                        turn *= -1;
                        timerTurnHelper = timerTurn;
                    }
                    Chase(Spe * turn * dir.normalized);
                }
            }
            else 
            {
                anim.SetBool("isMoving", false);
                rb.velocity = Vector2.zero;
            }
        }
        void Chase(Vector2 desiredVelocity)
        {
            if (DetectWater(dir)) 
            {
                anim.SetBool("isMoving", false);
                return;
            }
            Vector2 deltaVelocity;
            deltaVelocity = (Vector2) desiredVelocity - rb.velocity;
            rb.AddForce(Spe * Time.fixedDeltaTime * deltaVelocity, ForceMode2D.Impulse);
        }
        public void BowAttack()
        {
            dir = Player.Instance.transform.position - bowArrowTip.position;
            var rot = Quaternion.Euler(0f, 0f, Mathf.Atan2(dir.x, -dir.y) * Mathf.Rad2Deg);
            var go = Instantiate(bowArrow, bowArrowTip.position + dir.normalized * 0.5f, rot);
            SoundManager.Instance.PlaySoundRelatedToCamera(transform.position, AudioPlayer.SoundID.SFX_ENEMY_SHOOT);
            go.GetComponent<EnemyBulletMove>().SetDirection(dir.normalized, false);
        }

        void StartAttack()
        {
            isAttacking = true;
        }
        public override void Die()
        {
            base.Die();
            anim.Play("sleep");
            rb.velocity = Vector2.zero;
            StopCoroutine(afterSleepCoroutine);
        }
        public override void Sleep(float duration)
        {
            base.Sleep(duration);
            anim.Play("sleep", -1, 0);
            rb.velocity = Vector2.zero;
            afterSleepCoroutine = StartCoroutine(Cor_AfterSleep(duration));
        }
        IEnumerator Cor_AfterSleep(float duration)
        {
            yield return new WaitForSeconds(duration);
            anim.Play("idle");
            timerAttackHelper = timerAttack;
        }
    }
}
