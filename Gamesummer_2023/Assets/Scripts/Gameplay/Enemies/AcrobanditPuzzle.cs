using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.PlayerManager;
using DG.Tweening;
using GDC.Managers;
using UnityEngine.Events;

namespace GDC.Enemies
{
    public class GraphNode
    {
        public Vector2 position;
        public List<GraphNode> neighbors;
        public bool isDisabled;

        public GraphNode(Vector2 pos)
        {
            position = pos;
            neighbors = new List<GraphNode>();
            isDisabled = false;
        }
    }
    public class AcrobanditPuzzle : MonoBehaviour
    {
        [SerializeField] float baseDistance = 2;
        [SerializeField] GameObject book;
        [SerializeField] Animator anim;
        [SerializeField] float speed;
        bool isRunning, damaged, doneHelper, done;
        Vector3 direction;
        LayerMask posLayer = 1 << 3 | 1 << 6;
        [SerializeField] List<GameObject> points, rocks, turnPoints;
        GraphNode thisNode;
        List<GraphNode> graphNodes, shortestPath;
        [SerializeField] UnityEvent eventAfterDone;

        void Awake()
        {
            graphNodes = new List<GraphNode>();
            GraphBuilder();
        }
        void Start()
        {
            shortestPath = new();
        }
        void Update()
        {
            CheckDisableNodes();

            if (Vector3.Distance(Player.Instance.transform.position, transform.position) <= baseDistance)
            {
                if (!doneHelper && !isRunning)
                {
                    isRunning = true;
                    StartCoroutine(Run());
                }
                else if (doneHelper && !isRunning && !done)
                {
                    DonePuzzle();
                    done = true;
                }
            }
            if (direction.x >= 0) transform.localScale = new(-1,1,1);
            else transform.localScale = Vector3.one;
        }
        GraphNode FindNodeFromPos(Vector2 pos)
        {
            foreach (var node in graphNodes)
                if (node.position == pos) return node;
            return null;
        }
        void GraphBuilder()
        {
            foreach (GameObject i in points)
            {
                var point = i.transform.position;
                GraphNode node = new(point);
                graphNodes.Add(node);
                foreach (GraphNode otherNode in graphNodes)
                {
                    if (otherNode == node) continue;
                    float distance = Vector2.Distance(point, otherNode.position);
                    if (distance <= 1)
                    {
                        node.neighbors.Add(otherNode);
                        otherNode.neighbors.Add(node);
                    }
                }
            }
        }
        void CheckDisableNodes()
        {
            foreach (var node in graphNodes)
                node.isDisabled = false;

            foreach (var rock in rocks)
            {
                Vector2 rockPos = rock.transform.position;
                GraphNode node = FindNodeFromPos(rockPos + Vector2.up * 0.3f);
                if (node != null) node.isDisabled = true;
            }
            var playerNode = FindClosestNode(Player.Instance.transform.position);
            if (playerNode != null) playerNode.isDisabled = true;
        }
        GraphNode FindClosestNode(Vector2 rockPos)
        {
            int fromX = Mathf.FloorToInt(rockPos.x);
            int toX = Mathf.CeilToInt(rockPos.x);
            float y;
            if (rockPos.y - Mathf.FloorToInt(rockPos.y) <= 0.8f) 
                y = Mathf.FloorToInt(rockPos.y) + 0.3f;
            else y = Mathf.CeilToInt(rockPos.y) + 0.3f;
            return FindNodeFromPos(new((fromX + toX) * 0.5f, y));
        }
        List<GraphNode> FindShortestPathAStar(GraphNode startNode, GraphNode targetNode)
        {
            HashSet<GraphNode> openSet = new(), closedSet = new();
            Dictionary<GraphNode, GraphNode> cameFrom = new();
            Dictionary<GraphNode, float> gScore = new(), fScore = new();

            openSet.Add(startNode);
            gScore[startNode] = 0;
            fScore[startNode] = Heuristic(startNode, targetNode);

            while (openSet.Count > 0)
            {
                GraphNode current = GetLowestFScore(openSet, fScore);
                
                if (current == targetNode)
                    return ReconstructPath(cameFrom, current);

                openSet.Remove(current);
                closedSet.Add(current);

                foreach (GraphNode neighbor in current.neighbors)
                {
                    if (closedSet.Contains(neighbor) || neighbor.isDisabled) continue;

                    float tentativeGScore = gScore[current] + Vector2.Distance(current.position, neighbor.position);

                    if (!openSet.Contains(neighbor) || tentativeGScore < gScore[neighbor])
                    {
                        cameFrom[neighbor] = current;
                        gScore[neighbor] = tentativeGScore;
                        fScore[neighbor] = gScore[neighbor] + Heuristic(neighbor, targetNode);

                        if (!openSet.Contains(neighbor))
                            openSet.Add(neighbor);
                    }
                }
            }
            return null;
        }
        float Heuristic(GraphNode nodeA, GraphNode nodeB)
        {
            return Vector2.Distance(nodeA.position, nodeB.position);
        }
        GraphNode GetLowestFScore(HashSet<GraphNode> set, Dictionary<GraphNode, float> fScore)
        {
            GraphNode lowestNode = null;
            float lowestScore = float.MaxValue;
            foreach (GraphNode node in set)
            {
                if (fScore.TryGetValue(node, out float score) && score < lowestScore && !node.isDisabled)
                {
                    lowestNode = node;
                    lowestScore = score;
                }
            }
            return lowestNode;
        }
        List<GraphNode> ReconstructPath(Dictionary<GraphNode, GraphNode> cameFrom, GraphNode current)
        {
            List<GraphNode> path = new();
            while (cameFrom.ContainsKey(current))
            {
                path.Insert(0, current);
                current = cameFrom[current];
            }
            path.Insert(0, current);
            return path;
        }
        bool HasPathBetweenNodes(GraphNode startNode, GraphNode targetNode)
        {
            List <GraphNode> path = FindShortestPathAStar(startNode, targetNode);
            if (path != null)
            {
                shortestPath = path;
                return true;
            }
            return false;
        }
        IEnumerator Run()
        {
            bool check = DetectPos();
            if (!check) 
            {
                DoneHelper();
            }
            SoundManager.Instance.PlaySoundRelatedToCamera(transform.position, AudioPlayer.SoundID.SFX_DASH);
            foreach (var point in shortestPath)
            {
                direction = (point.position - (Vector2)transform.position).normalized;
                while (Vector3.Distance(point.position, transform.position) > 0.2f)
                {
                    transform.position += speed * Time.fixedDeltaTime * direction;
                    yield return new WaitForSeconds(Time.fixedDeltaTime);
                }
                transform.position = point.position;
            }
            isRunning = false;
            shortestPath.Clear();
        }
        bool DetectPos()
        {
            int length = 100;
            List<GraphNode> res = new();
            if (transform.position.x == 31.5f)
            {
                if (SpecialPos()) return true;
                else return false;
            }
            for (int i=0; i<turnPoints.Count; i++)
            {
                var nextNode = FindNodeFromPos(turnPoints[i].transform.position);
                thisNode = FindNodeFromPos(transform.position);

                if (thisNode != nextNode)
                {
                    bool find = HasPathBetweenNodes(thisNode, nextNode);
                    if (find) 
                    {
                        if (shortestPath.Count < length)
                        {
                            res = shortestPath;
                            length = shortestPath.Count;
                        }
                    }
                }
            }
            if (length != 100)
            {
                shortestPath = res;
                return true;
            }
            shortestPath = null;
            return false;
        }
        bool SpecialPos()
        {
            bool hit1 = Raycast(Vector3.up, Vector3.up, 3.5f);
            bool hit2 = Raycast(Vector3.down, Vector3.down, 3.5f);
            bool hit3 = Raycast(Vector3.right, Vector3.right, 10);
            bool hit4 = Raycast(Vector3.right, Vector3.right, 7.5f);
            if (!hit1 && !hit2 && !hit3 && !hit4) return false;

            List<Vector3> pos = new();
            if (transform.position.y == 22.3f)
            {
                if (hit1) pos.Add(new(31.5f, 26.3f));
                if (hit2) pos.Add(new(31.5f, 18.3f));
                if (hit3) pos.Add(new(42.5f, 22.3f));
            }
            else if (transform.position.y == 18.3f)
            {
                if (hit1) pos.Add(new(31.5f, 22.3f));
                if (hit3) pos.Add(new(42.5f, 18.3f));
                if (hit4) pos.Add(new(39.5f, 18.3f));
            }
            else if (transform.position.y == 26.3f)
            {
                if (hit2) pos.Add(new(31.5f, 22.3f));
                if (hit3) pos.Add(new(42.5f, 26.3f));
            }

            int idx = Random.Range(0, pos.Count);
            shortestPath.Clear();
            shortestPath.Add(FindNodeFromPos(transform.position));
            shortestPath.Add(FindNodeFromPos(pos[idx]));

            return true;
        }
        bool Raycast(Vector3 offset, Vector3 direction, float distance)
        {
            RaycastHit2D hit1, hit2, hit3;
            var curr = transform.position + Vector3.up * 0.3f;
            if (direction == Vector3.up || direction == Vector3.down)
            {
                hit1 = Physics2D.Raycast(curr + offset * 0.5f, direction, distance, posLayer);
                hit2 = Physics2D.Raycast(curr + offset * 0.5f + Vector3.left * 0.4f, direction, distance, posLayer);
                hit3 = Physics2D.Raycast(curr + offset * 0.5f + Vector3.right * 0.4f, direction, distance, posLayer);
            }
            else 
            {
                hit1 = Physics2D.Raycast(curr + offset * 0.5f, direction, distance, posLayer);
                hit2 = Physics2D.Raycast(curr + offset * 0.5f + Vector3.down * 0.25f, direction, distance, posLayer);
                hit3 = Physics2D.Raycast(curr + offset * 0.5f + Vector3.down * 0.5f, direction, distance, posLayer);
            }
            if (hit1.collider == null && hit2.collider == null && hit3.collider == null)
                return true;
            return false;
        }
        void DoneHelper()
        {
            var curr = transform.position + Vector3.up * 0.3f;
            Vector3 pos = Vector3.zero;

            RaycastHit2D hit1 = Physics2D.Raycast(curr, Vector3.up, 10, posLayer);
            if (hit1.collider != null && hit1.collider.CompareTag("Boulder"))
                pos = hit1.collider.transform.position + Vector3.down + Vector3.up * 0.3f;

            RaycastHit2D hit2 = Physics2D.Raycast(curr, Vector3.down, 10, posLayer);
            if (hit2.collider != null && hit2.collider.CompareTag("Boulder"))
                pos = hit2.collider.transform.position + Vector3.up + Vector3.up * 0.3f;

            RaycastHit2D hit3 = Physics2D.Raycast(curr, Vector3.left, 10, posLayer);
            if (hit3.collider != null && hit3.collider.CompareTag("Boulder"))
                pos = hit3.collider.transform.position + Vector3.right + Vector3.up * 0.3f;

            RaycastHit2D hit4 = Physics2D.Raycast(curr, Vector3.right, 10, posLayer);
            if (hit4.collider != null && hit4.collider.CompareTag("Boulder"))
                pos = hit4.collider.transform.position + Vector3.left + Vector3.up * 0.3f;

            if (shortestPath != null) shortestPath.Clear();
            else shortestPath = new();
            shortestPath.Add(FindNodeFromPos(transform.position));
            shortestPath.Add(FindNodeFromPos(pos));

            doneHelper = true;
        }
        void DonePuzzle()
        {
            anim.Play("flip_puzzle");
            StartCoroutine(Cor_WaitUntilDamaged());
        }
        IEnumerator Cor_WaitUntilDamaged()
        {
            yield return new WaitUntil(() => damaged == true);
            SoundManager.Instance.PlaySoundRelatedToCamera(transform.position, AudioPlayer.SoundID.SFX_ENEMY_HURT);
            anim.Play("hide_puzzle");
            book.transform.DOLocalMoveY(0, 0.25f)
                .SetEase(Ease.OutBounce)
                .OnComplete(() => book.transform.SetParent(null));
            yield return new WaitForSeconds(1);
            eventAfterDone?.Invoke();
            gameObject.SetActive(false);
        }
        void OnTriggerStay2D(Collider2D collision)
        {
            if (collision.gameObject.CompareTag("PlayerAttack"))
                damaged = true;
        }
        void OnCollisionStay2D(Collision2D collision)
        {
            if (collision.gameObject.CompareTag("PlayerAttack"))
                damaged = true;
        }
    }
}
