using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.PlayerManager;
using GDC.Managers;

namespace GDC.Enemies
{
    public class EyePillar : Enemy
    {
        [SerializeField] float minDistanceDetectPlayer, maxDistanceDetectPlayer;
        [SerializeField] float timer = 3f;
        float timerHelper;
        [SerializeField] GameObject laserBeam;
        [SerializeField] GameObject[] VFX_laser;
        float angle = 180;
        bool isShooting = false, canShoot = true;
        Vector3 laserPos = new(0, 0.5f, 0);
        List<Vector3> vfx_pos = new();
        Vector3 direction;
        void Start()
        {
            timerHelper = timer;
            for (int i=0; i<VFX_laser.Length; i++)
                vfx_pos.Add(VFX_laser[i].transform.localPosition);
        }

        void FixedUpdate()
        {
            if (!isCanMove || !isVisible) return;

            direction = Player.Instance.transform.position - this.transform.position;
            var dist = direction.magnitude;

            if (!isShooting)
            {
                if (canShoot && !DetectWall(direction, dist) && dist <= maxDistanceDetectPlayer && dist >= minDistanceDetectPlayer) 
                    ShootLaser();
                angle -= 1.5f;
                if (angle <= 0) angle += 360;
            }

            if (!canShoot) 
            {
                timerHelper -= Time.fixedDeltaTime;
                if (timerHelper <= 1.75f)
                {
                    anim.speed = 1f;
                    isShooting = false;
                    if (timerHelper <= 0f)
                    {
                        timerHelper = timer;
                        canShoot = true;
                        for (int i=0; i<VFX_laser.Length; i++)
                        {
                            VFX_laser[i].transform.SetParent(laserBeam.transform);
                            VFX_laser[i].transform.localPosition = vfx_pos[i];
                        }
                    }
                }
            }
        }
        void ShootLaser()
        {
            float angleInRadians = angle * Mathf.Deg2Rad;
            Vector2 aimingDirection = new Vector2(Mathf.Cos(angleInRadians), Mathf.Sin(angleInRadians));

            if (Vector2.Angle(direction, aimingDirection) <= 5f)
            {
                anim.speed = 0f;
                isShooting = true;
                canShoot = false;

                laserBeam.transform.localPosition = laserPos + direction.normalized * 0.4f;
                var laser = laserBeam.GetComponent<EnemyBulletMove>();
                laser.SetDirection(direction);
                laserBeam.SetActive(true);
                SoundManager.Instance.PlaySoundRelatedToCamera(transform.position, AudioPlayer.SoundID.SFX_KAMEHAMEHA);
                for (int i=0; i<VFX_laser.Length; i++)
                {
                    VFX_laser[i].SetActive(true);
                    VFX_laser[i].transform.SetParent(null);
                }
            }
        }
        public override void Sleep(float duration)
        {
            base.Sleep(duration);
            anim.Play("sleep", -1, 0);
            anim.SetFloat("sleepSpeed", 1f/duration);
        }
    }
}
