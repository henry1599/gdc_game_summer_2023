using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.PlayerManager;
using GDC.Managers;

namespace GDC.Enemies
{
    public class Acrobandit : Enemy
    {
        [SerializeField] float minDistanceDetectPlayer, maxDistanceDetectPlayer, distanceToHide;
        [SerializeField] float timerHide;
        [SerializeField] GameObject thisShadow;
        float timerHelper, dist, timerRun = 3f;
        Vector3 direction;
        bool runAway, idling = true, isAppearing, doneAppearing, isHiding, mustHide;
        bool isUp, isDown, canAttack, getHit;
        SpriteRenderer graphic;
        Vector3 shadowPos = new(-0.04f, 0.09f, 0f);
        Vector3 firstShadowPos = new(0.0028f, 0.1076f, 0f);
        [SerializeField] BoxCollider2D childrenCollider, thisCollider;
        void Start()
        {
            graphic = GetComponentInChildren<SpriteRenderer>();
            timerHelper = timerRun;
        }
        void FixedUpdate()
        {
            UpdateCollider();
            if (isUp) {
                graphic.transform.localPosition += 5f * Time.fixedDeltaTime * Vector3.up;
                return;
            }
            if (isDown) {
                graphic.transform.localPosition += 2.5f * Time.fixedDeltaTime * Vector3.down;
                return;
            }
            if (isHiding || getHit) isVisible = true;
            if (!isCanMove || !isVisible) return;

            direction = Player.Instance.transform.position - transform.position;
            dist = direction.magnitude;

            bool detectWallOrWater = DetectWall(direction, dist) || DetectWater(direction, dist);

            if (isHiding)
            {
                thisShadow.SetActive(false);
                timerHelper -= Time.fixedDeltaTime;
                if (timerHelper <= -0.5f)
                {
                    timerHelper = timerRun;
                    idling = true;
                    isHiding = false;
                    thisShadow.SetActive(true);
                }
                if (timerHelper <= 0f)
                {
                    getHit = false;
                    anim.Play("appear");
                }
                return;
            }
            if (idling)
            {
                anim.Play("idle");
                thisShadow.transform.localPosition = firstShadowPos;
                rb.constraints = RigidbodyConstraints2D.FreezeAll;
                if (!detectWallOrWater) timerHelper -= Time.fixedDeltaTime;
                if (timerHelper <= 0f && dist <= maxDistanceDetectPlayer)
                {
                    canAttack = true;
                    timerHelper = timerRun;
                    idling = false;
                    runAway = false;
                    doneAppearing = false;
                }
                return;
            }
            
            if (detectWallOrWater || (dist <= distanceToHide && !canAttack && !runAway))
            {
                mustHide = true;
            }
            if (mustHide)
            {
                anim.Play("hide");
                SoundManager.Instance.PlaySoundRelatedToCamera(transform.position, AudioPlayer.SoundID.SFX_DIG);
                SoundManager.Instance.StopSFXAfterDelay(AudioPlayer.SoundID.SFX_DIG, 0.9f);
                timerHelper = timerHide;
                isHiding = true;
                mustHide = false;
                return;
            }
            if (runAway)
            {
                rb.constraints = RigidbodyConstraints2D.FreezeRotation;
                timerHelper -= Time.fixedDeltaTime;
                direction = transform.position - Player.Instance.transform.position;
                transform.position += direction.normalized * Time.fixedDeltaTime * Spe;
                TrueFlip();
                if (timerHelper <= 0f)
                {
                    runAway = false;
                    doneAppearing = true;
                    idling = false;
                }
                return;
            }

            if (dist <= maxDistanceDetectPlayer && dist >= minDistanceDetectPlayer)
            {
                if (doneAppearing)
                {
                    rb.constraints = RigidbodyConstraints2D.FreezeRotation;
                    anim.Play("move");
                    direction = Player.Instance.transform.position - transform.position;
                    if (canAttack) transform.position += direction.normalized * Time.fixedDeltaTime * Spe;
                    TrueFlip();
                    return;
                }
                if (!isAppearing)
                {
                    StartCoroutine(Cor_Appear());
                }
            }
            else 
            {
                mustHide = true;
                canAttack = false;
            }
        }
        IEnumerator Cor_Appear()
        {
            rb.constraints = RigidbodyConstraints2D.FreezeAll;
            direction = Player.Instance.transform.position - transform.position;
            TrueFlip();
            anim.Play("jumpout");
            isAppearing = true;
            yield return new WaitForSeconds(0.75f);
            SoundManager.Instance.PlaySoundRelatedToCamera(transform.position, AudioPlayer.SoundID.SFX_ACRO_JUMP);
            isUp = true;
            yield return new WaitForSeconds(0.25f);
            isUp = false;
            isDown = true;
            yield return new WaitForSeconds(0.5f);
            isDown = false;
            graphic.transform.localPosition = Vector3.zero;
            TrueFlip();
            yield return new WaitForSeconds(0.5f);
            isAppearing = false;
            doneAppearing = true;
            canAttack = true;
            thisShadow.transform.localPosition = shadowPos;
            rb.constraints = RigidbodyConstraints2D.FreezeRotation;
        }
        void TrueFlip()
        {
            if (direction.x >= 0) transform.localScale = new Vector3(-1,1,1);
            else transform.localScale = Vector3.one;
        }
        void OnCollisionEnter2D(Collision2D collision) 
        {
            if (collision.gameObject.CompareTag("Player") && !isUp && !isDown && !idling && !isHiding && !isAppearing)
            {
                runAway = true;
                canAttack = false;
                timerHelper = timerRun;
                Player.Instance.GetHit(transform.position, Atk, 0f, gameObject.layer);
            }
        }
        void UpdateCollider()
        {
            bool isEnabled = childrenCollider.enabled;
            thisCollider.enabled = isEnabled;
            thisCollider.size = childrenCollider.size;
            thisCollider.offset = childrenCollider.offset;
        }
        public override void Die()
        {
            anim.Play("die");
            base.Die();
        }
        public override void Sleep(float duration)
        {
            base.Sleep(duration);
            anim.Play("sleep", -1, 0);
            anim.SetFloat("sleepSpeed", 1f/duration);
            doneAppearing = true;
            runAway = mustHide = isHiding = idling = canAttack = false;
        }
        public override void GetHit(Vector3 attackPos, float spellAtk, float spellEffectDuration, LayerMask playerAttackLayer, int damageID = 0)
        {
            getHit = true;
            base.GetHit(attackPos, spellAtk, spellEffectDuration, playerAttackLayer);
            if (Hp > 0)
            {
                StartCoroutine(Cor_WaitNewPos());
            }
        }
        IEnumerator Cor_WaitNewPos()
        {
            var currentState = anim.GetCurrentAnimatorStateInfo(0);
            if (currentState.IsName("jumpout"))
            // if (isUp)
            {
                yield return new WaitUntil(() => isDown);
                yield return new WaitUntil(() => !isDown);
                idling = isUp = isDown = false;
            }
            else 
            {
                idling = isUp = isDown = false;
                yield return new WaitForSeconds(0.5f);
            }
            mustHide = true;
            anim.Play("hide");
            thisShadow.SetActive(false);
            timerHelper = timerHide;

            Vector3 newPos = NewPos(2);
            yield return new WaitForSeconds(5f/6);
            if (newPos != new Vector3(-1000, -1000, 0)) transform.position = newPos;
        }
        Vector3 NewPos(float radius)
        {
            int num = 0;
            int layerMask = (1 << 3) | (1 << 4);
            Vector3 randomPosition = Player.Instance.transform.position;
            bool isEmpty = false;
            while (!isEmpty)
            {
                if (++num > 10000) 
                {
                    return new(-1000, -1000, 0);
                }
                randomPosition = Random.insideUnitCircle * (radius * 2f);
                Collider2D[] colliders = Physics2D.OverlapCircleAll(randomPosition, radius, layerMask);
                isEmpty = colliders.Length == 0;
            }
            return randomPosition;
        }
    }
}