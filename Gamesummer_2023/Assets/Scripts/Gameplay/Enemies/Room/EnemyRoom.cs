using DG.Tweening;
using Gameplay;
using GDC.PlayerManager;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GDC.Gameplay
{
    public class EnemyRoom : MonoBehaviour
    {
        [SerializeField] List<GameObject> enemies;
        public List<GameObject> Enemies => enemies;
        [SerializeField] float minX, maxX, minY, maxY;
        float oldMinX, oldMaxX, oldMinY, oldMaxY;
        public bool isEnemyRoomStart = false;
        public float defaultOrthoSize = 4.5f;
        Transform player;
        public void StartEnemyRoom()
        {
            isEnemyRoomStart = true;
            oldMinX = CameraController.Instance.minX;
            oldMaxX = CameraController.Instance.maxX;
            oldMinY = CameraController.Instance.minY;
            oldMaxY = CameraController.Instance.maxY;
            player = Player.Instance.transform;
            CameraController.Instance.MoveToNewBound(minX, maxX, minY, maxY, player.position.x, player.position.y);
        }
        private void Update()
        {
            if (isEnemyRoomStart)
            {
                if (CheckEnemiesDead())
                {
                    EndEnemyRoom();
                }
            }
        }
        bool CheckEnemiesDead()
        {
            foreach(var enemy in enemies)
            {
                if (enemy.activeSelf == true) return false;
            }
            return true;
        }    
        void EndEnemyRoom()
        {
            isEnemyRoomStart = false;
            CameraController.Instance.MoveToNewBound(oldMinX, oldMaxX, oldMinY, oldMaxY, player.position.x, player.position.y);
            Camera.main.DOOrthoSize(defaultOrthoSize, 0.25f);
            gameObject.SetActive(false);
        }
    }
}
