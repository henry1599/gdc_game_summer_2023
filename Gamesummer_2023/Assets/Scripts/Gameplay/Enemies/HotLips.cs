using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.PlayerManager;

namespace GDC.Enemies
{
    public class HotLips : Enemy
    {
        [SerializeField] private float timer;
        float timerHelper;
        Vector3 dir;
        [SerializeField] private GameObject hotBullet;
        // [SerializeField] private float minDistanceDetectPlayer;
        [SerializeField] private float maxDistanceDetectPlayer = 10;
        void Start()
        {
            timerHelper = timer;
        }
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.X))
            {
                Sleep(2f);
            }
        }
        void FixedUpdate()
        {
            if (!isCanMove || !isVisible) return;
            dir = Player.Instance.transform.position - transform.position;
            var dist = dir.magnitude;
            if (dir.x >= 0) transform.localScale = new Vector3(-1,1,1);
            else transform.localScale = Vector3.one;

            if (dist <= maxDistanceDetectPlayer && !DetectWall(dir, dist))
            {
                timerHelper -= Time.fixedDeltaTime;
                if (timerHelper <= 0f)
                {
                    timerHelper = timer;
                    anim.Play("shoot");
                }
            }
            else timerHelper = timer;
        }
        public void ShootHotBullet()
        {
            var go = Instantiate(hotBullet, transform.position, Quaternion.identity);
            go.GetComponent<EnemyBulletMove>().SetDirection(dir);
            go.GetComponentInChildren<ParticleSystem>().gameObject.transform.SetParent(null);
        }
        public override void Sleep(float duration)
        {
            base.Sleep(duration);
            anim.Play("sleep", -1, 0);
            anim.SetFloat("sleepSpeed", 1f/duration);
        }
    }
}