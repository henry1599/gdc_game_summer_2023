using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.PlayerManager;
using GDC.Managers;

namespace GDC.Enemies
{
    public class CrazeeDayzee : Enemy
    {
        [SerializeField] GameObject musicBullet;
        [SerializeField] Transform musicTip;
        [SerializeField] float minDistanceDetectPlayer, maxDistanceDetectPlayer, timer = 5f;
        float timerHelper;
        Vector3 dir;
        Wander thisWander;
        void Start()
        {
            timerHelper = timer;
            thisWander = GetComponent<Wander>();
        }

        void FixedUpdate()
        {
            if (!isCanMove || !isVisible) 
            {
                thisWander.vectorHelper = Vector3.zero;
                return;
            }
            dir = Player.Instance.transform.position - transform.position;
            var dist = dir.magnitude;
            if (dist >= minDistanceDetectPlayer && dist <= maxDistanceDetectPlayer && !DetectWall(dir, dist))
            {
                timerHelper -= Time.fixedDeltaTime;
                
                if (timerHelper <= 0f && !thisWander.isWalking)
                {
                    if (dir.x >= 0) transform.localScale = new Vector3(-1,1,1);
                    else transform.localScale = Vector3.one;
                    thisWander.ResetTimer();
                    anim.Play("sing");
                    thisWander.ResetTimer();
                }
                else if (!thisWander.isWalking) anim.Play("stop");
                else anim.Play("move");
            }
            else 
            {
                timerHelper = timer;
                if (!thisWander.isWalking) anim.Play("stop");
                else anim.Play("move");
            }
        }
        public void Sing()
        {
            GameObject go = Instantiate(musicBullet, musicTip.position + dir.normalized * 0.5f, Quaternion.identity);
            go.GetComponent<EnemyBulletMove>().SetDirection(dir.normalized, false);
            SoundManager.Instance.PlaySoundRelatedToCamera(transform.position, AudioPlayer.SoundID.SFX_SING);
            timerHelper = timer;
        }
        public override void Die()
        {
            thisWander.ResetTimer();
            anim.Play("die");
            base.Die();
        }
        public override void Sleep(float duration)
        {
            base.Sleep(duration);
            anim.Play("sleep", -1, 0);
            anim.SetFloat("sleepSpeed", 1f/duration);
            GetComponent<Wander>().vectorHelper = Vector3.zero;
        }
    }
}