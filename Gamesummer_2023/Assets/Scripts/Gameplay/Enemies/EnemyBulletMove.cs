using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

namespace GDC.Enemies
{
    public enum BulletMoveType 
    {
        STRAIGHT,
        SINE,
        DROPDOWN,
    }
    public class EnemyBulletMove : MonoBehaviour
    {
        [SerializeField] BulletMoveType bulletMoveType = BulletMoveType.STRAIGHT;
        [SerializeField] bool willDestroy;
        [SerializeField, HideIf("willDestroy")] bool willContinueUntilDone;
        public float speed;
        [HideInInspector] public Vector3 firstPos;
        Vector3 startPosition;
        public float timer = 1f;
        float timerHelper;
        [Header("VFX")]
        [SerializeField] GameObject VFX_ExplodeWhenHitWall;
        [Header("STRAIGHT")]
        [SerializeField] Vector3 direction;
        [Header("SINE")]
        public float sineTime = 0f;
        [Header("DROPDOWN")]
        [SerializeField] float heightOfParabol = 0.1f;
        [SerializeField] float timeToDropDown = 1f;
        bool isChosen = false;
        float timerForDropdown = 0f;
        GameObject spriteForDropDown;
        [HideInInspector] public ParticleSystem thisVFX;
        Collider2D col;
        
        void Awake()
        {
            firstPos = transform.localPosition;
            startPosition = transform.position;
            thisVFX = GetComponentInChildren<ParticleSystem>();
        }
        void Start()
        {
            sineTime = 0f;
            timerHelper = timer;
        }

        void FixedUpdate()
        {
            Move();
            timerHelper -= Time.fixedDeltaTime;
            if (timerHelper <= 0) 
            {
                timerHelper = timer;
                if (!willDestroy) gameObject.SetActive(false);
                else Destroy(gameObject);
            }
        }
        void Move()
        {
            switch (bulletMoveType)
            {
                case BulletMoveType.STRAIGHT:
                    transform.position += speed * Time.fixedDeltaTime * direction.normalized;
                    return;

                case BulletMoveType.SINE:
                    startPosition += speed * Time.fixedDeltaTime * direction;
                    sineTime += Time.fixedDeltaTime;
                    var perpendicular = new Vector3(direction.y, -direction.x);
                    transform.position = startPosition + 0.5f * Mathf.Sin(sineTime * 3f) * perpendicular;
                    return;

                case BulletMoveType.DROPDOWN:
                    if (!isChosen)
                    {
                        isChosen = true;
                        spriteForDropDown = GetComponentInChildren<SpriteRenderer>().gameObject;
                        if (TryGetComponent(out col)) col.isTrigger = true;
                        else 
                        {
                            col = GetComponentInChildren<Collider2D>();
                            if (col != null) col.isTrigger = true;
                        }
                    }
                    if (timerForDropdown > timeToDropDown) return;

                    timerForDropdown += Time.fixedDeltaTime;
                    transform.position += direction / timeToDropDown * Time.fixedDeltaTime;
                    float tempY = Mathf.Sin(timerForDropdown * Mathf.PI / timeToDropDown) * heightOfParabol;
                    spriteForDropDown.transform.localPosition = new Vector3(0, tempY, 0);
                    
                    if (timerForDropdown >= timeToDropDown - 0.2f)
                        if (col != null) col.isTrigger = false;
                    return;
            }
        }
        public void SetDirection(Vector3 dir, bool setRotation = true)
        {
            direction = dir;
            if (setRotation)
            {
                transform.rotation = Quaternion.Euler(0f, 0f, Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg);
            }
        }
        public Vector3 GetDirection()
        {
            return direction;
        }
        void OnTriggerStay2D(Collider2D collision)
        {
            if (collision.CompareTag("Player"))
            {
                if (thisVFX != null) thisVFX.transform.SetParent(null);
                if (!willDestroy) 
                {
                    if (!willContinueUntilDone) 
                    {
                        gameObject.SetActive(false);
                        timerHelper = timer;
                    }
                }
            }
        }
        void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.gameObject.CompareTag("Wall"))
            {
                if (VFX_ExplodeWhenHitWall != null)
                {
                    VFX_ExplodeWhenHitWall.transform.SetParent(null);
                    VFX_ExplodeWhenHitWall.SetActive(true);
                    if (!willDestroy) 
                    {
                        if (!willContinueUntilDone) 
                        {
                            gameObject.SetActive(false);
                            timerHelper = timer;
                        }
                    }
                    else Destroy(gameObject);
                }
            }
        }
        void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.CompareTag("Wall"))
            {
                if (VFX_ExplodeWhenHitWall != null)
                {
                    VFX_ExplodeWhenHitWall.transform.SetParent(null);
                    VFX_ExplodeWhenHitWall.SetActive(true);
                    if (!willDestroy) 
                    {
                        if (!willContinueUntilDone) 
                        {
                            gameObject.SetActive(false);
                            timerHelper = timer;
                        }
                    }
                    else Destroy(gameObject);
                }
            }
        }
        public void ResetVFXExplode()
        {
            VFX_ExplodeWhenHitWall.SetActive(false);
            VFX_ExplodeWhenHitWall.transform.SetParent(transform);
            VFX_ExplodeWhenHitWall.transform.localPosition = Vector3.zero;
        }
    }
}