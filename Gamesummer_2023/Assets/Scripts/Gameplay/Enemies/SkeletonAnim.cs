﻿using GDC.Enemies;
using GDC.PlayerManager;
using NaughtyAttributes;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

enum SkeletonType
{
    SWORD,
    BOW,
}

public class SkeletonAnim : MonoBehaviour
{
    [SerializeField] Skeleton skeleton;
    [SerializeField] Animator anim;
    
    [SerializeField, Foldout("Info")] SkeletonType skeletonType;

    #region attack animation
    [SerializeField, Foldout("Attack Animation")] float radius;
    [SerializeField, Foldout("Attack Animation"),ReadOnly]float radiusReduction;
    [SerializeField, Foldout("Attack Animation")] Vector2 center;
    [SerializeField, Foldout("Attack Animation"),ReadOnly] float currentAngle;
    [SerializeField, Foldout("Attack Animation"),ReadOnly] float angleSlash;
    [SerializeField, Foldout("Attack Animation")] Transform handController;
    //[SerializeField, Foldout("Attack Animation")] GameObject slashCollider, slashTrail;

    bool DetectAttack()
    {
        var currentState = anim.GetCurrentAnimatorStateInfo(0);
        if (currentState.IsName("slash") || currentState.IsName("shoot"))
            return true;

        return false;
    }

    void AttackAnimation()
    {
        Vector2 playerPos = Player.Instance.transform.position;
        Vector2 centerPos = (Vector2)transform.position + center;

        if (!DetectAttack())
        {
            handController.localPosition = Vector2.zero;
            handController.localRotation = Quaternion.Euler(Vector2.zero);
            if (skeleton.isAttacking)
            {
                

                float angle = Vector2.Angle(playerPos - centerPos, (transform.parent.localScale.x < 0)?Vector2.left :Vector2.right);
                if (playerPos.y < centerPos.y)
                    angle = 0 - angle;
                currentAngle = angle;
                if (skeletonType == SkeletonType.SWORD)
                {
                    
                    anim.Play("slash");
                }    
                if (skeletonType == SkeletonType.BOW)
                {
                    anim.Play("shoot");
                }
            }
        }
        if (DetectAttack())
        {
            float totalAngle = currentAngle + angleSlash;
            
            handController.localRotation = Quaternion.Euler(0, 0, totalAngle);

            float totalRadius = radius - radiusReduction;
            handController.localPosition = new Vector2(
                center.x + totalRadius * Mathf.Cos(totalAngle * Mathf.Deg2Rad),   
                center.y + totalRadius * Mathf.Sin(totalAngle * Mathf.Deg2Rad)
            );
        }
    }

    void StopAttack()
    {
        //if (skeletonType == SkeletonType.SWORD)
        //{
        //    slashCollider.SetActive(false);
        //    slashTrail.SetActive(false);
        //}
        // anim.speed = 1;
        anim.Play("idle");
        skeleton.isAttacking = false;
    }

    void BowAttack() // animation event để bow attack tấn công
    {
        skeleton.BowAttack();
    }


    #endregion
    void Start()
    {
        
    }

    void Update()
    {
        AttackAnimation();
    }

    // có biến bool isMoving khi transition giữa idle và move
}
