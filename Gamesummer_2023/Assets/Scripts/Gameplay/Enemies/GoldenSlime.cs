using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.PlayerManager;

namespace GDC.Enemies
{
    public class GoldenSlime : Enemy
    {
        [SerializeField] float minDistanceDetectPlayer, maxDistanceDetectPlayer;
        float disappearSpeed = 0.01f;
        SpriteRenderer spriteRenderer;
        bool doneRunAway, isRunningAway;
        Vector3 direction;
        float timer = 4f, timerFlip = 2;

        void Start()
        {
            spriteRenderer = GetComponentInChildren<SpriteRenderer>();
            if (Random.Range(0, 100) > 20) gameObject.SetActive(false); //Rate appear of golden slime
        }

        void FixedUpdate()
        {
            if (!isCanMove || !isVisible) return;
            timerFlip -= Time.fixedDeltaTime;
            if (timerFlip <= 0 && !isRunningAway)
            {
                timerFlip = 2;
                transform.localScale = new Vector3(-transform.localScale.x,1,1);
            }

            var dist = Vector2.Distance(transform.position, Player.Instance.transform.position);
            direction = this.transform.position - Player.Instance.transform.position;
            
            if (!isRunningAway && dist <= maxDistanceDetectPlayer && dist >= minDistanceDetectPlayer)
            {
                anim.Play("run");
                isRunningAway = true;
            }
            else if (isRunningAway) 
            {
                if (direction.x >= 0) transform.localScale = new Vector3(-1,1,1);
                else transform.localScale = Vector3.one;
                transform.position += direction.normalized * Time.fixedDeltaTime * Spe;
                timer -= Time.fixedDeltaTime;
                if (timer <= 0f) doneRunAway = true;
            }

            if (doneRunAway)
            {
                spriteRenderer.color = new Color(1.0f, 1.0f, 1.0f, spriteRenderer.color.a - disappearSpeed);
                if (spriteRenderer.color.a <= 0f) Destroy(this.gameObject);
            }
        }
        public override void Die()
        {
            anim.Play("die");
            base.Die();
        }
        public override void Sleep(float duration)
        {
            base.Sleep(duration);
            anim.Play("sleep", -1, 0);
            anim.speed = 1f/duration;
        }
    }
}
