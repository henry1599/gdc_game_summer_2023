using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.PlayerManager;

namespace GDC.Enemies
{
    public class Bat : Enemy
    {
        [SerializeField] float minOffsetToMove, maxOffsetToMove, maxDistanceDetectPlayer;
        bool isFlying = false;
        Vector3 randomPos;

        void FixedUpdate()
        {
            if (!isCanMove || !isVisible) return;
            var direction = Player.Instance.transform.position - (transform.position + Vector3.up);
            var dist = direction.magnitude;
            if (DetectWall(direction, dist)) return;

            if (dist >= maxDistanceDetectPlayer) return;

            if (!isFlying)
            {
                float randomX = Random.Range(minOffsetToMove, maxOffsetToMove) * (Random.Range(0,2) == 0 ? -1 : 1);
                float randomy = Random.Range(minOffsetToMove, maxOffsetToMove) * (Random.Range(0,2) == 0 ? -1 : 1);
                randomPos = Player.Instance.transform.position + new Vector3 (randomX, randomy, 0);
                isFlying = true;
            }
            else 
            {
                this.transform.position = Vector2.MoveTowards(transform.position, randomPos, Spe * Time.fixedDeltaTime);
                if (transform.position == randomPos) 
                {
                    isFlying = false;
                }
            }
        }
        public override void Die()
        {
            base.Die();
            anim.Play("die");
        }
        public override void Sleep(float duration)
        {
            base.Sleep(duration);
            anim.Play("sleep", -1, 0);
            anim.SetFloat("sleepSpeed", 1f/duration);
        }
        void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.gameObject.CompareTag("Wall"))
                isFlying = false;
        }
    }
}