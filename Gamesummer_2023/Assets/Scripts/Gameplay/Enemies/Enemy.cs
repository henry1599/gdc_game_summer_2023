using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.Enums;
using GDC.PlayerManager;
using DG.Tweening;
using GDC.Gameplay;
using GDC.Gameplay.UI;
using GDC.Managers;

namespace GDC.Enemies
{
    public class Enemy : MonoBehaviour
    {
        public Animator anim;
        public Rigidbody2D rb;

        public string ID;
        public SpellType spellType;
        float timeLife = 5;

        [HideInInspector] public EnemyRoom EnemyRoom;

        [SerializeField] private List<SpriteRenderer> spriteRendererList;
        [SerializeField] private GameObject sleepEffect, dizzyCircle, slowEffect, freezeEffect, poisonEffect, disappearEffect;

        #region status
        [Header("Stat")]
        [SerializeField] int spe;
        public int Spe
        {
            get => spe;
            set => spe = value;
        }
        [SerializeField] int atk;
        public int Atk
        {
            get => atk;
            set => atk = value;
        }
        [SerializeField] int def;
        public int Def
        {
            get => def;
            set => def = value;
        }
        [SerializeField] int maxHp;
        public int MaxHp
        {
            get => maxHp;
            set => maxHp = value;
        }
        [SerializeField] float hp;
        public float Hp
        {
            get => hp;
            set => hp = value;
        }
        [SerializeField] float maxStamina;
        public float MaxStamina
        {
            get => MaxStamina;
            set => MaxStamina = value;
        }
        [SerializeField] float stamina;
        public float Stamina
        {
            get => stamina;
            set => stamina = value;
        }
        #endregion

        [SerializeField] private int initSpe;
        public float staminaGain;
        public float expGain;

        [Header("ItemDrop")]
        [SerializeField] private int coinDrop;
        [SerializeField] private GameObject coinPreFab;
        [SerializeField] private List<GameObject> commonItemDrop, rareItemDrop, epicItemDrop;

        [Header("LayerDetect")]
        [SerializeField] LayerMask noEffectLayer; //Layer ma quai khong nhan damage
        [SerializeField] LayerMask superEffectLayer; //Layer ma quai bi nhan damame gap doi

        [SerializeField] protected bool isNotMoveAway; //Quai co khong bi vang ra xa khi bi player tan cong khong
        private bool isSleep, isSlow, isFreeze, isPoison;
        protected bool isCanGetHit;
        public bool isCanMove, isCanAttack, isVisible = false;

        Coroutine freezeCor, sleepCor, slowCor, poisonCor;

        [Header("DamageTextPopup")]
        [SerializeField] Transform damageTextPopup;
        private int currentDamageID;
        private void Awake()
        {
            initSpe = spe;
            isCanMove = true;
            isCanGetHit = true;

            currentDamageID = 0;
        }

        public virtual void GetHit(Vector3 attackPos, float spellAtk, float spellEffectDuration, LayerMask playerAttackLayer, int damageID = 0)
        {
            if (!isCanGetHit) return;

            if (currentDamageID == damageID && damageID != 0) return; 
            currentDamageID = damageID;

            if (playerAttackLayer == LayerMask.NameToLayer("Sleep")) Sleep(spellEffectDuration);
            else if (playerAttackLayer == LayerMask.NameToLayer("Poison")) Poison(spellEffectDuration);
            else if (playerAttackLayer == LayerMask.NameToLayer("Ice")) Freeze(spellEffectDuration);
            else if (playerAttackLayer == LayerMask.NameToLayer("Electric")) Slow(spellEffectDuration);

            if (isSleep || isPoison || isFreeze || isSlow) isCanGetHit = true;
            else isCanGetHit = false;
            StartCoroutine(Cor_RefreshGetHit(0.5f));

            if (spellAtk == 0) return;
            SoundManager.Instance.PlaySoundRelatedToCamera(transform.position, AudioPlayer.SoundID.SFX_ENEMY_HURT);

            #region damage calculate
            Transform damagePopup = Instantiate(damageTextPopup, transform.position, Quaternion.identity);
            DamageTextPopup damagePopupScr = damagePopup.GetComponent<DamageTextPopup>();

            int layerMatchup, criticalRate;
            if (((1 << playerAttackLayer) & noEffectLayer) != 0) //so sanh LayerMark playerAttackLayer == noEffectLayer
            {
                Debug.Log(gameObject.name + " had no effect with " + playerAttackLayer);
                damagePopupScr.NoEffect();
                layerMatchup = 0;
            }
            else
            {
                if (((1 << playerAttackLayer) & superEffectLayer) != 0)
                {
                    Debug.Log(gameObject.name + " had SUPER effect with " + playerAttackLayer);
                    damagePopupScr.SuperEffect();
                    layerMatchup = 2;
                }
                else
                {
                    damagePopupScr.NormalEffect();
                    layerMatchup = 1;
                }

                StartCoroutine(Cor_ToColor(Color.red, 0f));
                StartCoroutine(Cor_ToColor(Color.white, 0.2f));
            }

            criticalRate = Random.Range(0, 100) / 89 + 1; //co ti le 90% la critical Rate = 2. Con lai = 1
            if (criticalRate == 2)
            {
                Debug.Log(gameObject.name + " GET CRITICAL HIT!");
                damagePopupScr.CriticalEffect();
            }

            float damage = 3 * criticalRate * layerMatchup * spellAtk * Random.Range(0.75f, 1.25f) * Player.Instance.Attack / def;
            damagePopupScr.SetText((int)damage);

            if ((int)damage > 0)
            {
                CameraEffects.Instance.ShakeOnce(0.5f, 2, camera: Camera.main);
            }

            if (isNotMoveAway == false && damage > 0.5f)
            {
                isCanMove = false;
                Vector3 direct = (this.transform.position - attackPos).normalized;
                rb.velocity = direct * 7; //Enemy bi vang ra
                StartCoroutine(Cor_CanMove(1));
            }

            if (hp <= 0) //Neu da ngat thi se bi vang ra mot doan roi bien mat
            {
                StartCoroutine(Cor_Disappear(0.4f));
                return;
            }

            hp -= (int)damage;
            if (hp <= 0)
            {
                hp = 0;
                Die();
            }
            #endregion 

            //Debug.Log(gameObject.name + " is gotten hit with damage = " + damage);
        }
        IEnumerator Cor_RefreshGetHit(float sec)
        {
            yield return new WaitForSeconds(sec);
            isCanGetHit = true;
            
            currentDamageID = 0;
        }
        #region Status Effect
        public void Freeze(float duration)
        {
            if (isFreeze)
            {
                StopCoroutine(freezeCor);
                Debug.Log(gameObject.name + " is freezed again");
            }
            Debug.Log(gameObject.name + " is freezed");
            anim.speed = 0;
            isFreeze = true;
            isCanMove = false;
            isCanAttack = false;
            freezeEffect.SetActive(true);
            freezeCor = StartCoroutine(Cor_EndStatus(freezeEffect, duration));
        }
        public virtual void Sleep(float duration)
        {
            if (isFreeze)
            {
                Debug.Log(gameObject.name + " can't sleep because it is freezing!");
                return;
            }
            if (isSleep)
            {
                StopCoroutine(sleepCor);
                Debug.Log(gameObject.name + " is slept again");
            }
            Debug.Log(gameObject.name + " is slept");
            isSleep = true;
            isCanMove = false;
            isCanAttack = false;
            sleepEffect.SetActive(true);
            sleepCor = StartCoroutine(Cor_EndStatus(sleepEffect, duration));
        }
        public void Slow(float duration)
        {
            if (isSlow)
            {
                StopCoroutine(slowCor);
                Debug.Log(gameObject.name + " is slowed again");
            }
            Debug.Log(gameObject.name + " is slowed");
            isSlow = true;
            slowEffect.SetActive(true);
            spe = initSpe / 2;
            slowCor = StartCoroutine(Cor_EndStatus(slowEffect, duration));
        }
        public void Poison(float duration)
        {
            if (isPoison)
            {
                StopCoroutine(poisonCor);
                Debug.Log(gameObject.name + " is poisoned again");
            }
            Debug.Log(gameObject.name + " is poisoned");
            isPoison = true;
            poisonEffect.SetActive(true);
            poisonCor = StartCoroutine(Cor_EndStatus(poisonEffect, duration));
        }
        #endregion
        IEnumerator Cor_EndStatus(GameObject effectObj, float statusDuration)
        {
            yield return new WaitForSeconds(statusDuration);
            if (hp > 0)
            {
                effectObj.SetActive(false);

                if (effectObj == sleepEffect) { isSleep = false; isCanMove = true; isCanAttack = true; }
                else if (effectObj == slowEffect) { isSlow = false; spe = initSpe; }
                else if (effectObj == freezeEffect) { isFreeze = false; anim.speed = 1; isCanAttack = true; isCanMove = true; }
                else if (effectObj == poisonEffect) isPoison = false;
            }
        }
        IEnumerator Cor_ToColor(Color color, float sec)
        {
            yield return new WaitForSeconds(sec);
            foreach (var spriteRenderer in spriteRendererList)
            {
                spriteRenderer.material.DOColor(color, 0.2f);
            }
        }
        public virtual void Die()
        {
            Debug.Log(gameObject.name + " is died");
            isCanMove = false;
            StartCoroutine(Cor_Disappear(5));
            StartCoroutine(Cor_AnimRotateDie(new Vector3(0, 0, 5f))); //Xoay qua xoay lai tu -5 -> 5 do
            dizzyCircle.SetActive(true);
            sleepEffect.SetActive(false);
            slowEffect.SetActive(false);
            freezeEffect.SetActive(false);
            poisonEffect.SetActive(false);
        }
        IEnumerator Cor_AnimRotateDie(Vector3 angleValue)
        {
            transform.DORotate(angleValue, 0.2f);
            yield return new WaitForSeconds(0.2f);
            StartCoroutine(Cor_AnimRotateDie(-angleValue));
        }

        IEnumerator Cor_CanMove(float sec)
        {
            yield return new WaitForSeconds(sec);
            if (isSleep == false && isFreeze == false && Hp > 0)
                isCanMove = true;
        }
        public void DropItem()
        {
            for (int i = 1; i <= coinDrop; i += 5)
            {
                Instantiate(coinPreFab, transform.position, Quaternion.identity);
            }
            foreach (var itemDrop in commonItemDrop)
            {
                Instantiate(itemDrop, transform.position, Quaternion.identity);
            }
            foreach (var itemDrop in rareItemDrop)
            {
                if (Random.Range(0, 100) < 40)
                    Instantiate(itemDrop, transform.position, Quaternion.identity);
            }
            foreach (var itemDrop in epicItemDrop)
            {
                if (Random.Range(0, 100) < 10)
                    Instantiate(itemDrop, transform.position, Quaternion.identity);
            }
        }
        public void PullToPos(Vector2 pos, float speed)
        {
            if (isNotMoveAway) return;
            transform.position = Vector2.MoveTowards(transform.position, pos, speed * Time.deltaTime);
        }
        IEnumerator Cor_Disappear(float sec)
        {
            yield return new WaitForSeconds(sec);
            Disappear();
        }
        public virtual void Disappear()
        {
            Debug.Log("Enemy Disappear!");
            DropItem();
            if (disappearEffect == null)
            {
                Debug.Log("Keo tha prefab DisappearEffect vao " + gameObject.name + "! Tuy kich co quai thi DisappearEffect nay co nhung loai prefab to nho khac nhau.");
            }
            else Instantiate(disappearEffect, transform.position, Quaternion.identity);

            //Update exp for player here
            Player.Instance.Exp += (int)expGain;
            Exp.Instance.SpawnExp(transform.position);

            gameObject.SetActive(false);
        }
        public bool DetectWall(Vector3 direction, float distance)
        {
            int layerMask = 1 << 3;
            RaycastHit2D[] hits = new RaycastHit2D[1];
            int numHits = Physics2D.RaycastNonAlloc(transform.position, direction, hits, distance, layerMask);
            for (int i = 0; i < numHits; i++)
            {
                if (hits[i].collider.CompareTag("Wall"))
                    return true;
            }
            return false;
        }
        public bool DetectWater(Vector3 direction, float distance = 1.5f)
        {
            int layerMask = 1 << 4;
            RaycastHit2D[] hits = new RaycastHit2D[1];
            int numHits = Physics2D.RaycastNonAlloc(transform.position, direction, hits, distance, layerMask);
            for (int i = 0; i < numHits; i++)
            {
                if (hits[i].collider.CompareTag("Water"))
                    return true;
            }
            return false;
        }

        public virtual void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.CompareTag("MainCamera"))
            {
                isVisible = true;
            }
        }
        public virtual void OnTriggerExit2D(Collider2D collision)
        {
            if (collision.CompareTag("MainCamera"))
            {
                isVisible = false;
            }
        }
        public virtual void OnTriggerStay2D(Collider2D collision)
        {
            if (collision.CompareTag("MainCamera"))
            {
                isVisible = true;
            }
        }
    }
}
