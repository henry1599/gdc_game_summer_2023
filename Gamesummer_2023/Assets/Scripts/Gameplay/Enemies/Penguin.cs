using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.PlayerManager;

namespace GDC.Enemies
{
    public class Penguin : Enemy
    {
        [SerializeField] float minDistanceDetectPlayer, maxDistanceDetectPlayer;
        [SerializeField] float bounceForce, bounceParam;
        [SerializeField] float timeWait = 1;
        float timeWaitHelper, bounceForceHelper;
        Vector3 lastMoveDirection;
        Wander thisWander;
        bool isBouncing = false, isWaiting = false;
        void Start()
        {
            bounceForceHelper = bounceForce;
            thisWander = GetComponent<Wander>();
            timeWaitHelper = timeWait;   
        }

        void FixedUpdate()
        {
            if (!isCanMove || !isVisible) 
            {
                GetComponent<Wander>().vectorHelper = Vector3.zero;
                return;
            }
            var direction = Player.Instance.transform.position - this.transform.position;
            var dist = direction.magnitude;
            if (isBouncing)
            {
                if (rb.velocity.magnitude <= 0.1f)
                {
                    isBouncing = false;
                    bounceForceHelper = bounceForce;
                    anim.Play("stop");
                    isWaiting = true;
                    thisWander.ResetCorner();
                }
                return;
            }
            if (!thisWander.enabled)
            {
                if (direction.x >= 0) transform.localScale = new Vector3(-1,1,1);
                else transform.localScale = Vector3.one;
            }
            bool detectWall = DetectWall(direction, dist);
            if (detectWall) 
            {
                timeWaitHelper = timeWait;
                isWaiting = true;
                thisWander.enabled = true;
            }
            if (isWaiting)
            {
                timeWaitHelper -= Time.fixedDeltaTime;
                if (timeWaitHelper <= 0f)
                {
                    timeWaitHelper = timeWait;
                    isWaiting = false;
                    if (!thisWander.enabled) thisWander.enabled = true;
                }
            }
            if (!isWaiting && dist >= minDistanceDetectPlayer && dist <= maxDistanceDetectPlayer && !isBouncing)
            {
                thisWander.enabled = false;
                rb.AddForce(direction.normalized * bounceForceHelper, ForceMode2D.Impulse);
                bounceForceHelper *= bounceParam;
                isBouncing = true;
                lastMoveDirection = direction;
                anim.Play("spin");
            }
            else 
            {
                if (!thisWander.isWalking) anim.Play("stop");
                else anim.Play("move");
            }
        }
        void OnCollisionEnter2D(Collision2D collision)
        {
            if (!thisWander.isWalking) NormalBounce(collision);
        }
        void NormalBounce(Collision2D collision)
        {
            isBouncing = true;
            Vector2 reflectDirection = Vector2.Reflect(lastMoveDirection.normalized, collision.GetContact(0).normal);
            rb.AddForce(reflectDirection.normalized * bounceForceHelper, ForceMode2D.Impulse);
            bounceForceHelper *= bounceParam;
        }
        public override void Sleep(float duration)
        {
            base.Sleep(duration);
            anim.Play("sleep", -1, 0);
            anim.SetFloat("sleepSpeed", 1f/duration);
            GetComponent<Wander>().vectorHelper = Vector3.zero;
        }
    }
}