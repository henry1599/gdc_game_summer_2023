using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.PlayerManager;

namespace GDC.Enemies
{
    public class FireOhoJee : Enemy
    {
        [SerializeField] float frequency = 10.0f;
        [SerializeField] float magnitude = 0.5f;
        Vector3 pos, axis, direction, touchPoint;
        float dist;
        [SerializeField] float minDistanceDetectPlayer, maxDistanceDetectPlayer, distanceToAttack;
        float resetTimeAfterTouchWall = 1f, currentTime;
        bool FireOhoJeeCanMove, canAttack;
    
        void Start()
        {
            pos = transform.position;
            currentTime = 0f;
            FireOhoJeeCanMove = true;
        }
        void FixedUpdate()
        {
            if (!isCanMove || !isVisible) return;
            direction = Player.Instance.transform.position - this.transform.position;
            dist = direction.magnitude;
            var perpendicular = new Vector3(axis.y, -axis.x);
            if (direction.x >= 0) transform.localScale = new Vector3(-1,1,1);
            else transform.localScale = Vector3.one;

            if (canAttack)
            {
                if (!FireOhoJeeCanMove)
                {
                    direction = touchPoint - transform.position;
                    resetTimeAfterTouchWall -= Time.fixedDeltaTime;
                    Spe = -Mathf.Abs(Spe);
                    if (resetTimeAfterTouchWall <= 0f)
                    {
                        Spe = Mathf.Abs(Spe);
                        resetTimeAfterTouchWall = 1f;
                        FireOhoJeeCanMove = true;
                        currentTime = 0f;
                        pos = this.transform.position;
                        canAttack = !DetectWall(direction, dist);
                    }
                    transform.position += direction.normalized * Spe * Time.fixedDeltaTime;
                }
                else if (dist >= distanceToAttack && dist <= maxDistanceDetectPlayer)
                {
                    pos += Spe * Time.fixedDeltaTime * axis;
                    axis = direction.normalized;

                    transform.position = pos + magnitude * Mathf.Sin(currentTime * frequency) * perpendicular;
                    currentTime += Time.fixedDeltaTime;
                }
                else if (dist >= minDistanceDetectPlayer && dist <= distanceToAttack)
                {
                    transform.position += Spe * Time.fixedDeltaTime * direction.normalized;
                }
                else 
                {
                    transform.position = this.transform.position;
                }
            }
            else 
            {
                canAttack = !DetectWall(direction, dist);
            }
        }
        void OnCollisionStay2D(Collision2D collision)
        {
            if (collision.gameObject.CompareTag("Wall"))
            {
                touchPoint = collision.GetContact(0).normal;
                FireOhoJeeCanMove = false;
            }
            else if (collision.gameObject.CompareTag("Player"))
            {
                touchPoint = Player.Instance.transform.position;
                FireOhoJeeCanMove = false;
            }
        }
        public override void Sleep(float duration)
        {
            base.Sleep(duration);
            anim.Play("sleep", -1, 0);
            anim.SetFloat("sleepSpeed", 1f/duration);
        }
    }
}
