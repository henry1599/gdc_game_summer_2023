using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.PlayerManager;

namespace GDC.Enemies
{
    public class FireMon : Enemy
    {
        [SerializeField] bool isBigFireMon;
        [SerializeField] GameObject[] fireMons;
        [SerializeField] GameObject fireBullet;
        [SerializeField] Transform fireBulletTip;
        [SerializeField] RuntimeAnimatorController[] anims;
        [SerializeField] float maxDistanceDetectPlayer = 10f;
        Wander thisWander;
        bool isShooting;
        Vector3 dir;
        List<Vector2> fireMonVec;
        float delayTime;
        void Start()
        {
            if (isBigFireMon) 
            {
                anim.runtimeAnimatorController = anims[0];
                fireMonVec = new List<Vector2>
                {
                    new Vector2(1, -1) * Random.Range(4f, 5f),
                    new Vector2(-1, 1) * Random.Range(4f, 5f)
                };
            }
            else
            {
                int randNum = Random.Range(0, anims.Length);
                anim.runtimeAnimatorController = anims[randNum];
            }
            thisWander = GetComponent<Wander>();
        }
        void FixedUpdate()
        {
            if (!isCanMove || !isVisible) 
            {
                this.GetComponent<Wander>().vectorHelper = Vector3.zero;
                return;
            }
            if (delayTime >= 0f)
            {
                delayTime -= Time.fixedDeltaTime;
                return;
            }
            dir = Player.Instance.transform.position - this.transform.position;

            if (!thisWander.isWalking && dir.magnitude <= maxDistanceDetectPlayer)
            {
                if (!isShooting && !DetectWall(dir, dir.magnitude))
                    anim.Play("shoot");
            }
            else 
            {
                anim.Play("idle");
                isShooting = false;
            }
        }
        public void FireShoot()
        {
            if (!isShooting)
            {
                if (dir.x >= 0) transform.localScale = new Vector3(-1,1,1);
                else transform.localScale = Vector3.one;

                GameObject go1 = Instantiate(fireBullet, fireBulletTip.position + dir.normalized * 0.2f, Quaternion.identity);
                GameObject go2 = Instantiate(fireBullet, fireBulletTip.position + dir.normalized * 0.2f, Quaternion.identity);
                GameObject go3 = Instantiate(fireBullet, fireBulletTip.position + dir.normalized * 0.2f, Quaternion.identity);

                go1.GetComponent<EnemyBulletMove>().SetDirection(dir.normalized, false);
                go2.GetComponent<EnemyBulletMove>().SetDirection(Quaternion.Euler(45f, 0f, 45f) * dir.normalized, false);
                go3.GetComponent<EnemyBulletMove>().SetDirection(Quaternion.Euler(-45f, 0f, -45f) * dir.normalized, false);

                isShooting = true;
            }
        }
        IEnumerator Cor_InstantiateTwoFireMons()
        {
            yield return new WaitForSeconds(4.95f);
            for (int i=0; i<2; i++)
            {
                fireMons[i].SetActive(true);
                fireMons[i].GetComponent<Rigidbody2D>().AddForce(fireMonVec[i], ForceMode2D.Impulse);
                fireMons[i].GetComponent<FireMon>().delayTime = 2f;
                fireMons[i].transform.parent = null;
            }
        }
        public override void Die()
        {
            if (isBigFireMon) StartCoroutine(Cor_InstantiateTwoFireMons());
            base.Die();
        }
        public override void Sleep(float duration)
        {
            base.Sleep(duration);
            anim.Play("sleep", -1, 0);
            anim.SetFloat("sleepSpeed", 1f/duration);
            GetComponent<Wander>().vectorHelper = Vector3.zero;
        }
    }
}