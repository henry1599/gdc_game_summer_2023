using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.PlayerManager;
using GDC.Managers;

namespace GDC.Enemies
{
    public class SpikeSlime : Enemy
    {
        [SerializeField] List<GameObject> spikes_toPool;
        [SerializeField] GameObject thisShadow;
        [SerializeField] float distanceToAttack;
        [SerializeField] float spikeTimer = 5f, timerHelper;
        SlimeMove slimeMove;
        bool isShooting = false;
        Vector3 leftShadow = new(-0.05f, 0f, 0f), direction;
        Coroutine cor_shoot;
        void Start()
        {
            slimeMove = GetComponent<SlimeMove>();
            timerHelper = spikeTimer;
        }
        void FixedUpdate()
        {
            if (!isCanMove || !isVisible) return;
            direction = Player.Instance.transform.position - transform.position;
            var dist = direction.magnitude;
            FlipForSpikeSlime();
            if (DetectWall(direction, dist))
            {
                anim.Play("idle");
                return;
            }
            if (isShooting)
            {
                timerHelper -= Time.fixedDeltaTime;
                if (timerHelper <= 0f)
                {
                    isShooting = false;
                    timerHelper = spikeTimer;
                }
                return;
            }
            if (dist <= distanceToAttack)
            {
                slimeMove.slimeCanMove = false;
                if (cor_shoot == null)
                {
                    anim.Play("idle");
                    cor_shoot = StartCoroutine(Cor_Delay());
                }
            }
        }
        void FlipForSpikeSlime()
        {
            if (direction.x >= 0) {
                GetComponentInChildren<SpriteRenderer>().flipX = true;
                thisShadow.transform.localPosition = -leftShadow;
            }
            else {
                GetComponentInChildren<SpriteRenderer>().flipX = false;
                thisShadow.transform.localPosition = leftShadow;
            }
        }
        IEnumerator Cor_Delay()
        {
            yield return new WaitForSeconds(0.5f);
            anim.Play("spike");
        }
        public IEnumerator Cor_ShootSpikes()
        {
            isShooting = true;
            slimeMove.canFlip = false;
            SoundManager.Instance.PlaySoundRelatedToCamera(transform.position, AudioPlayer.SoundID.SFX_ENEMY_SHOOT);
            for (int i = 0; i < 8; i++)
            {
                EnemyBulletMove go = spikes_toPool[i].GetComponent<EnemyBulletMove>();
                go.timer = 1f;
                go.gameObject.SetActive(true);
                go.ResetVFXExplode();
                go.transform.SetParent(null);
            }
            yield return new WaitForSeconds(1.5f);
            for (int i = 0; i < 8; i++)
            {
                EnemyBulletMove go = spikes_toPool[i].GetComponent<EnemyBulletMove>();
                go.transform.SetParent(transform);
                go.transform.localPosition = go.firstPos;
                go.thisVFX.transform.SetParent(go.transform);
                go.thisVFX.transform.localPosition = Vector3.zero;
            }
            slimeMove.canFlip = true;
            slimeMove.ResetTime(0f, 0f, 0.5f);

            cor_shoot = null;
        }
        public override void Die()
        {
            anim.Play("die");
            base.Die();
            GetComponent<SlimeMove>().enabled = false;
        }
        public override void Sleep(float duration)
        {
            base.Sleep(duration);
            anim.Play("sleep", -1, 0);
            anim.speed = 1f/duration;
        }
    }
}