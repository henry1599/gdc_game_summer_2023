using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.PlayerManager;
using GDC.Managers;

namespace GDC.Enemies
{
    public class CactusMan : Enemy
    {
        [SerializeField] GameObject cactusBullet;
        [SerializeField] Transform cactusTip;
        [SerializeField] float minDistanceDetectPlayer, maxDistanceDetectPlayer, timer = 5f;
        float timerHelper;
        Vector3 dir;
        Wander thisWander;
        void Start()
        {
            timerHelper = timer;
            thisWander = GetComponent<Wander>();
        }

        void FixedUpdate()
        {
            if (!isCanMove || !isVisible) 
            {
                thisWander.vectorHelper = Vector3.zero;
                return;
            }
            dir = Player.Instance.transform.position - transform.position;
            var dist = dir.magnitude;
            if (dist >= minDistanceDetectPlayer && dist <= maxDistanceDetectPlayer && !DetectWall(dir, dist))
            {
                timerHelper -= Time.fixedDeltaTime;
                if (timerHelper <= 0f && !thisWander.isWalking)
                {
                    if (dir.x >= 0) transform.localScale = new Vector3(-1,1,1);
                    else transform.localScale = Vector3.one;
                    thisWander.ResetTimer();
                    anim.Play("shoot");
                }
                else if (!thisWander.isWalking)
                {
                    anim.Play("idle");
                }
            }
            else 
            {
                timerHelper = timer;
                if (!thisWander.isWalking) anim.Play("idle");
                else anim.Play("move");
            }
        }
        public void ShootCactusBullet()
        {
            GameObject go = Instantiate(cactusBullet, cactusTip.position + dir.normalized * 0.5f, Quaternion.identity);
            SoundManager.Instance.PlaySoundRelatedToCamera(transform.position, AudioPlayer.SoundID.SFX_ENEMY_SHOOT);
            go.GetComponent<EnemyBulletMove>().SetDirection(dir);
            timerHelper = timer;
        }
        public override void Sleep(float duration)
        {
            base.Sleep(duration);
            anim.Play("sleep", -1, 0);
            anim.SetFloat("sleepSpeed", 1f/duration);
            GetComponent<Wander>().vectorHelper = Vector3.zero;
        }
    }
}