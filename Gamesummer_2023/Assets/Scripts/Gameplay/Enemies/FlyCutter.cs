using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.PlayerManager;

namespace GDC.Enemies
{
    public class FlyCutter : Enemy
    {
        [SerializeField] float minDistanceDetectPlayer, maxDistanceDetectPlayer;
        [SerializeField] GameObject vfx_fly;
        [SerializeField] float timerFly = 1f;
        bool isMoving, isPreparing, isOnWater;
        float timer = 2f, timerPrepare = 1f, timerFlyHelper;
        Vector3 direction;
        void Start()
        {
            timerFlyHelper = timerFly;
        }
        void FixedUpdate()
        {
            if (!isCanMove || !isVisible) return;
            if (!isPreparing && !isMoving && !isOnWater)
            {
                anim.Play("idle");
                timer -= Time.fixedDeltaTime;
                if (timer <= 0f) timer = 2f;
                else return;
            }

            var tempDirection = Player.Instance.transform.position - this.transform.position;
            var dist = tempDirection.magnitude;
            bool detectWall = DetectWall(tempDirection, tempDirection.magnitude);
            if (!isMoving && dist >= minDistanceDetectPlayer && dist <= maxDistanceDetectPlayer && !detectWall && !isOnWater)
            {
                if (!isPreparing)
                {
                    anim.Play("prepare");
                    isPreparing = true;
                }
                timerPrepare -= Time.fixedDeltaTime;
                if (timerPrepare <= 0f)
                {
                    isMoving = true;
                    anim.Play("move");
                    vfx_fly.transform.localPosition = Vector3.zero;
                    vfx_fly.SetActive(true);
                    // vfxFlyInstantiate = Instantiate(vfx_fly, transform.position, Quaternion.identity);
                    isPreparing = false;
                    timerPrepare = 1f;
                    direction = tempDirection;
                }
            }
            else if (detectWall)
            {
                isPreparing = false;
                timerPrepare = 1f;
            }
            if (isMoving)
            {
                CheckWater();
                timerFlyHelper -= Time.fixedDeltaTime;
                if (timerFlyHelper >= timerFly / 4f)
                    rb.AddForce(direction.normalized * Spe * 0.5f, ForceMode2D.Impulse);
                else if (timerFlyHelper >= 0f)
                    rb.AddForce(direction.normalized * Spe, ForceMode2D.Impulse);
                else if (timerFlyHelper >= -0.5f)
                    rb.AddForce(direction.normalized * Spe * 0.5f, ForceMode2D.Impulse);
                else
                {
                    timerFlyHelper = timerFly;
                    isMoving = false;
                }
            }
        }
        void CheckWater()
        {
            bool detectWater = DetectWater(direction);
            if (detectWater) GetComponent<Collider2D>().isTrigger = true;
        }
        public override void OnTriggerStay2D(Collider2D collision)
        {
            base.OnTriggerStay2D(collision);
            if (collision.CompareTag("Water") && !isMoving)
            {
                anim.Play("move");
                isOnWater = true;
                rb.AddForce(direction.normalized * Spe * 3);
            }
        }
        public override void OnTriggerExit2D(Collider2D collision)
        {
            base.OnTriggerExit2D(collision);
            if (collision.gameObject.CompareTag("Water"))
            {
                GetComponent<Collider2D>().isTrigger = false;
                isOnWater = false;
            }
        }
        public override void Sleep(float duration)
        {
            base.Sleep(duration);
            anim.Play("sleep", -1, 0);
            anim.SetFloat("sleepSpeed", 1f/duration);
        }
    }
}