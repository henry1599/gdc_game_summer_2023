using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.PlayerManager;

namespace GDC.Enemies
{
    public class Fleeper : Enemy
    {
        float moveTime, waitTime;
        Vector3 direction;
        bool isJumping = false, canJumpNext = false, isCanMoveFleeper = true;
        float dist;
        SpriteRenderer graphic;
        float t = 0f;
        [SerializeField] float minDistanceDetectPlayer, maxDistanceDetectPlayer;
        [SerializeField] GameObject vfx_jump;
        void Start()
        {
            isCanMoveFleeper = true;
            graphic = GetComponentInChildren<SpriteRenderer>();
            waitTime = 0.5f;
            moveTime = 0f;
        }
        void FixedUpdate()
        {
            if (!isCanMove || !isVisible) return;

            direction = Player.Instance.transform.position - transform.position;
            dist = direction.magnitude;

            if (direction.x >= 0) transform.localScale = new Vector3(-1,1,1);
            else transform.localScale = Vector3.one;
 
            if (isCanMoveFleeper && canJumpNext)
            {
                if (!isJumping)
                {
                    anim.Play("jump");
                    isJumping = true;
                    t = 0f;
                }
                GetComponent<BoxCollider2D>().enabled = false;
                t += Time.fixedDeltaTime;
                transform.position += direction.normalized * Time.fixedDeltaTime;
                float tempY = Mathf.Sin(t * Mathf.PI / 0.5f) * 0.5f;
                graphic.transform.localPosition = new Vector3(0, tempY, 0);
                moveTime -= Time.fixedDeltaTime;
                waitTime = 0.5f;
            }
            
            if (moveTime <= 0f)
            {
                isCanMoveFleeper = false;
                waitTime -= Time.fixedDeltaTime;
                GetComponent<BoxCollider2D>().enabled = true;
                // if (canJumpNext) vfx_jump.SetActive(true);
                isJumping = false;
            }
            if (waitTime <= 0f)
            {
                isCanMoveFleeper = true;
                moveTime = 0.5f;
            }
            if (DetectWall(direction, dist) && !isJumping)
            {
                canJumpNext = false;
                waitTime = 0.5f;
                moveTime = 0f;
            }
            else if (!canJumpNext && dist <= maxDistanceDetectPlayer && dist >= minDistanceDetectPlayer)
            {
                canJumpNext = true;
            }
        }
        public override void Sleep(float duration)
        {
            base.Sleep(duration);
            anim.Play("sleep", -1, 0);
            anim.SetFloat("sleepSpeed", 1f/duration);
        }
    }
}