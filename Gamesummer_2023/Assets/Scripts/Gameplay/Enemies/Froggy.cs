using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.PlayerManager;
using GDC.Managers;

namespace GDC.Enemies
{
    public class Froggy : Enemy
    {
        float moveTime = 0.5f, waitTime = 0.5f;
        Vector3 dir;
        bool isJumping = false, isCanMoveFroggy = true, isCanShoot = true, canJumpNext = false;
        float dist;
        bool isShooting;
        [SerializeField] float minDistanceDetectPlayer, maxDistanceDetectPlayer;
        [SerializeField] float timerShoot;
        float timerShootHelper;
        [SerializeField] GameObject tounge;
        [SerializeField] GameObject vfx_jump;
        void Start()
        {
            timerShootHelper = timerShoot;
        }
        void FixedUpdate()
        {
            if (!isCanMove || isShooting || !isVisible) return;

            dir = Player.Instance.transform.position - transform.position;
            dist = dir.magnitude;
            
            timerShootHelper -= Time.fixedDeltaTime;
            if (timerShootHelper <= 0f && isCanShoot)
            {
                isShooting = true;
                anim.Play("shoot");
                StartCoroutine(Cor_SFXFrogEat());
                float angle;
                if (dir.x <= 0) 
                {
                    angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
                    tounge.transform.localScale = new Vector3(-1,1,1);
                    if (angle >= 0) angle = Mathf.Clamp(angle, 150f, 180f);
                    else angle = Mathf.Clamp(angle, -180f, -120f);
                }
                else
                {
                    angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
                    tounge.transform.localScale = Vector3.one;
                    if (angle >= 0) angle = Mathf.Clamp(angle, 0f, 60f);
                    else angle = Mathf.Clamp(angle, -30f, 0f);
                }
                tounge.transform.rotation = Quaternion.Euler(0f, 0f, angle);
                return;
            }

            if (dir.x >= 0) transform.localScale = new Vector3(-1,1,1);
            else transform.localScale = Vector3.one;
 
            if (isCanMoveFroggy && canJumpNext)
            {
                if (!isJumping)
                {
                    anim.Play("jump");
                    isJumping = true;
                }
                isCanShoot = false;
                transform.position += dir.normalized * Time.fixedDeltaTime * Spe;
                moveTime -= Time.fixedDeltaTime;
                waitTime = 0.5f;
            }
            if (moveTime <= 0f)
            {
                isCanMoveFroggy = false;
                waitTime -= Time.fixedDeltaTime;
                isJumping = false;
                isCanShoot = true;
                // vfx_jump.SetActive(true);

                if (dist <= maxDistanceDetectPlayer && dist >= minDistanceDetectPlayer) canJumpNext = true;
                else canJumpNext = false;
            }
            if (waitTime <= 0f)
            {
                isCanMoveFroggy = true;
                moveTime = 0.5f;
            }
            bool temp = DetectWall(dir, dist) || DetectWater(dir);
            if (temp && !isJumping)
            {
                canJumpNext = false;
                waitTime = 0.5f;
                moveTime = 0f;
            }
            if (!canJumpNext && dist <= maxDistanceDetectPlayer && dist >= minDistanceDetectPlayer)
                canJumpNext = true;
        }
        IEnumerator Cor_SFXFrogEat()
        {
            yield return new WaitForSeconds(0.5f);
            SoundManager.Instance.PlaySoundRelatedToCamera(transform.position, AudioPlayer.SoundID.SFX_FROG_EAT);
        }
        public void Tounge()
        {
            timerShootHelper = timerShoot;
            moveTime = 0f;
            waitTime = 0.5f;
            canJumpNext = false;
            isShooting = false;
        }
        public override void Die()
        {
            anim.Play("die");
            base.Die();
        }
        public override void Sleep(float duration)
        {
            base.Sleep(duration);
            anim.Play("sleep", -1, 0);
            anim.SetFloat("sleepSpeed", 1f/duration);
        }
    }
}