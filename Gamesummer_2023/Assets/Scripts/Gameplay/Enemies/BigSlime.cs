using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.Managers;

namespace GDC.Enemies
{
    public class BigSlime : Enemy
    {
        [SerializeField] SlimeMove[] slimes;
        List<Vector2> slimeVec;
        void Start()
        {
            slimeVec = new List<Vector2>
            {
                new(Random.Range(-5f, -4f), Random.Range(-5f, -4f)),
                new(Random.Range(4f, 5f), Random.Range(4f, 5f)),
                new(Random.Range(-5f, -4f), Random.Range(4f, 5f))
            };
        }
        void InstantiateThreeSlimes()
        {
            SoundManager.Instance.PlaySoundRelatedToCamera(transform.position, AudioPlayer.SoundID.SFX_ENEMY_SHOOT);
            for (int i = 0; i < 3; i++)
            {
                slimes[i].gameObject.SetActive(true);
                slimes[i].transform.parent = null;
                slimes[i].transform.rotation = Quaternion.identity;
                slimes[i].GetComponent<Rigidbody2D>().AddForce(slimeVec[i], ForceMode2D.Impulse);
                slimes[i].delayTime = 1f;
            }
        }
        public override void Disappear()
        {
            InstantiateThreeSlimes();
            base.Disappear();
        }
        public override void Sleep(float duration)
        {
            base.Sleep(duration);
            anim.Play("sleep", -1, 0);
            anim.speed = 1f/duration;
        }
        public override void Die()
        {
            base.Die();
            GetComponent<SlimeMove>().enabled = false;
        }
    }
}