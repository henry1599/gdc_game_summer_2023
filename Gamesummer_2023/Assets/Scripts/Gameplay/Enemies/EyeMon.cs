using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.PlayerManager;

namespace GDC.Enemies
{
    public class EyeMon : Enemy
    {
        [SerializeField] float maxDistanceDetectPlayer;
        [SerializeField] GameObject laserBeam, VFX_laser, smallEye;
        [SerializeField] float timer;
        float timerHelper;
        Vector3 direction, firstDirection;
        float circleRadius = 5f, circleDuration = 7f, circleAngle = 0f, startTime;
        float delayTime = 0.1f;
        bool isShooting;
        int loopCount = 0;
        Vector3 laserPos = new(0.0f, 0.25f, 0f);
        Vector3 vfx_pos;
        void Start()
        {
            startTime = Time.time;
            vfx_pos = VFX_laser.transform.localPosition;
            laserBeam.SetActive(false);
            timerHelper = timer;
        }

        void FixedUpdate()
        {
            if (!isCanMove || isShooting || !isVisible) return;
            direction = Player.Instance.transform.position - transform.position;
            if (direction.magnitude >= maxDistanceDetectPlayer) return;
            bool detectWall = DetectWall(direction, direction.magnitude);

            if (detectWall) smallEye.transform.localPosition = Vector3.zero;
            else smallEye.transform.localPosition = direction.normalized * 0.05f;

            if (detectWall) timerHelper = 0f;
            else if (Player.Instance.transform.hasChanged)
            {
                Player.Instance.transform.hasChanged = false;
                timerHelper = 0f;
            }
            else timerHelper += Time.fixedDeltaTime;

            if (timerHelper >= timer)
            {
                VFX_laser.transform.SetParent(laserBeam.transform);
                VFX_laser.transform.localPosition = vfx_pos;
                isShooting = true;
                anim.Play("change");
            }
            if (!detectWall) CheckCircleLoop();
        }
        public void ShootLaser()
        {
            laserBeam.transform.localPosition = laserPos + direction.normalized * 0.4f;
            var laser = laserBeam.GetComponent<EnemyBulletMove>();
            laser.SetDirection(direction);
            timerHelper = 0f;
            StartCoroutine(Cor_ResetLaser(laser.timer));
            laserBeam.SetActive(true);
            VFX_laser.SetActive(true);
            VFX_laser.transform.SetParent(null);
        }
        IEnumerator Cor_ResetLaser(float time)
        {
            yield return new WaitForSeconds(time);
            isShooting = false;
        }
        void CheckCircleLoop()
        {
            if (direction.magnitude <= circleRadius)
            {
                float currentAngle = Vector3.Angle(firstDirection, direction);
                if (currentAngle > 179f) 
                {
                    firstDirection = direction;
                    circleAngle = 0f;
                    currentAngle = 0.1f;
                    loopCount++;
                    delayTime = 0.1f;
                    CheckCircleCondition();
                }
                else if (currentAngle >= circleAngle - 0.05f)
                {
                    delayTime = 0.1f;
                    circleAngle = currentAngle;
                    CheckCircleCondition();
                }
                else 
                {
                    delayTime -= Time.fixedDeltaTime;
                    if (delayTime <= 0f) ResetCounting();
                }
            }
            else ResetCounting();
        }
        void CheckCircleCondition()
        {
            if (loopCount == 4)
            {
                if (Time.time - startTime <= circleDuration)
                    base.Die();
                else 
                {
                    loopCount = 0;
                    startTime = Time.time;
                    firstDirection = direction;
                    circleAngle = 0f;
                }
            }
        }
        void ResetCounting()
        {
            loopCount = 0;
            startTime = Time.time;
            firstDirection = direction;
            circleAngle = 0f;
        }
        void OnDrawGizmos()
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawRay(transform.position, firstDirection.normalized * 3);
            Gizmos.color = Color.red;
            Gizmos.DrawRay(transform.position, direction.normalized * 2);
        }
        public override void Sleep(float duration)
        {
            base.Sleep(duration);
            // anim.Play("sleep");
            // anim.speed = 1f/duration;
        }
    }
}
