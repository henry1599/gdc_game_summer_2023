using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.PlayerManager;

namespace GDC.Enemies
{
    public class CrazyElec : Enemy
    {
        [SerializeField] float minDistanceDetectPlayer, maxDistanceDetectPlayer;
        [SerializeField] GameObject elecBullet;
        [SerializeField] Transform elecTip;
        [SerializeField] GameObject vfx_elecBullet;
        [SerializeField] float shootTimer;
        float shootTimerHelper;
        Wander thisWander;
        Vector3 dir;
        void Start()
        {
            thisWander = GetComponent<Wander>();
            shootTimerHelper = shootTimer;
        }
        void FixedUpdate()
        {
            if (!isCanMove || !isVisible) 
            {
                GetComponent<Wander>().vectorHelper = Vector3.zero;
                return;
            }
            dir = Player.Instance.transform.position - transform.position;
            if (!DetectWall(dir, dir.magnitude))
            {
                shootTimerHelper -= Time.fixedDeltaTime;
            }
            else 
            {
                shootTimerHelper = shootTimer;
            }
            if (dir.magnitude >= minDistanceDetectPlayer && dir.magnitude <= maxDistanceDetectPlayer)
            {
                if (thisWander.isWalking == false && shootTimerHelper <= 0f)
                {
                    thisWander.ResetTimer();
                    StartCoroutine(Teleport());
                    shootTimerHelper = shootTimer;
                }
            }
        }
        IEnumerator Teleport()
        {
            var posToTeleport = Player.Instance.transform.position;
            posToTeleport += new Vector3(Random.Range(1.5f, 3f), Random.Range(1.5f, 3f), 0);

            anim.Play("shoot");
            vfx_elecBullet.SetActive(true);
            yield return new WaitForSeconds(0.5f);
            anim.Play("tele");
            yield return new WaitForSeconds(0.25f);
            this.transform.position = posToTeleport;
            anim.Play("appear");
            yield return new WaitForSeconds(0.25f);
            
            thisWander.ResetCorner();
        }
        public void ShootElecBullet()
        {
            var go = Instantiate(elecBullet, elecTip.position + dir.normalized * 0.4f, Quaternion.identity);
            go.GetComponent<EnemyBulletMove>().SetDirection(dir);
        }
        public override void Sleep(float duration)
        {
            base.Sleep(duration);
            anim.Play("sleep", -1, 0);
            anim.SetFloat("sleepSpeed", 1f/duration);
            GetComponent<Wander>().vectorHelper = Vector3.zero;
        }
    }
}