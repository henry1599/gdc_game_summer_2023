using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GDC.Enemies
{
    public class FuzzBush : Enemy
    {
        [SerializeField] float spawnTimer = 5f;
        float spawnTimerHelper;
        [SerializeField] SlimeMove slime, metalSlime;
        [SerializeField] GameObject goldenSlime;
        [SerializeField] GameObject VFX_Appear;
        void Start()
        {
            spawnTimerHelper = spawnTimer;
        }
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.V))
            {
                Sleep(10f);
            }
        }

        void FixedUpdate()
        {
            if (!isCanMove || !isVisible) 
            {
                this.GetComponent<Wander>().vectorHelper = Vector3.zero;
                return;
            }

            spawnTimerHelper -= Time.fixedDeltaTime;
            if (spawnTimerHelper <= 0f)
            {
                spawnTimerHelper = spawnTimer;
                this.GetComponent<Wander>().vectorHelper = Vector3.zero;
                anim.Play("born");
            }
        }
        public void SpawnSlime()
        {
            var pos = transform.position + Vector3.down * 0.5f;

            int randomValue = Random.Range(1, 101);
            if (randomValue <= 2f)
            {
                var go = Instantiate(goldenSlime, pos, Quaternion.identity);
                var vec = (new Vector2(Random.Range(-1, 1), -1)).normalized;
                go.GetComponent<Rigidbody2D>().AddForce(vec, ForceMode2D.Impulse);
                if (EnemyRoom != null)
                    EnemyRoom.Enemies.Add(go);
            }
            else 
            {
                SlimeMove go;
                if (randomValue <= 12f) go = Instantiate(metalSlime, pos, Quaternion.identity);
                else go = Instantiate(slime, pos, Quaternion.identity);
                var vec = (new Vector2(Random.Range(-1, 1), -1)).normalized;
                go.GetComponent<Rigidbody2D>().AddForce(vec, ForceMode2D.Impulse);
                go.delayTime = 1;
                if (EnemyRoom != null)
                    EnemyRoom.Enemies.Add(go.gameObject);
            }
            VFX_Appear.transform.position = pos;
            VFX_Appear.SetActive(true);
        }
        public override void Sleep(float duration)
        {
            base.Sleep(duration);
            anim.SetFloat("sleepSpeed", 1f/duration);
            anim.Play("sleep", -1, 0);
            GetComponent<Wander>().vectorHelper = Vector3.zero;
        }
    }
}