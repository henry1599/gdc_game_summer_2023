using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.PlayerManager;

namespace GDC.Enemies
{
    public class SnowMan : Enemy
    {
        [SerializeField] GameObject snowBullet;
        [SerializeField] float timer = 5f;
        [SerializeField] float maxDistanceDetectPlayer = 5f;
        [SerializeField] Transform snowTip;
        float timerHelper;
        Vector3 dir;
        void Start()
        {
            timerHelper = timer;
        }
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.V))
            {
                Sleep(2f);
            }
        }

        void FixedUpdate()
        {
            if (!isCanMove || !isVisible) return;
            dir = Player.Instance.transform.position - transform.position;
            var dist = dir.magnitude;
            if (dir.x >= 0) transform.localScale = new Vector3(-1,1,1);
            else transform.localScale = Vector3.one;

            if (dist <= maxDistanceDetectPlayer && !DetectWall(dir, dist))
            {
                timerHelper -= Time.fixedDeltaTime;
                if (timerHelper <= 0f)
                {
                    anim.Play("shoot");
                    timerHelper = timer;
                }
            }
            else timerHelper = timer;
        }
        public void ShootSnow()
        {
            GameObject go = Instantiate(snowBullet, snowTip.position + dir.normalized * 0.3f, Quaternion.identity);
            go.GetComponent<EnemyBulletMove>().SetDirection(dir, false);
        }
        public override void Sleep(float duration)
        {
            base.Sleep(duration);
            anim.Play("sleep", -1, 0);
            anim.SetFloat("sleepSpeed", 1f/duration);
        }
    }
}