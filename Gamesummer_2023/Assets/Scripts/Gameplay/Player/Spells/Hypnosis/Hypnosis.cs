using DG.Tweening;
using GDC.Managers;
using GDC.PlayerManager.Spell;
using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hypnosis : Spell
{
    #region eye
    [SerializeField, Foldout("Eye")] SpriteRenderer eye;
    [SerializeField, Foldout("Eye")] Sprite[] eyeSprites;
    [SerializeField, Foldout("Eye")] float timeStep; 
    #endregion

    #region ring
    [SerializeField, Foldout("Ring")] int numberOfRing;
    [SerializeField, Foldout("Ring")] List<SpriteRenderer> rings = new List<SpriteRenderer>();
    [SerializeField, Foldout("Ring")] Transform ringsContainer;
    [SerializeField, Foldout("Ring")] float hypnosisRadius, initialScale;
    [SerializeField, Foldout("Ring")] float eyeCenterPos; // y pos
    [SerializeField, Foldout("Ring")] float timeBetweenRings, timeMoving;
    [SerializeField, Foldout("Ring")] int loopTime;
    //  SerializeField, Foldout("Ring")] AnimationCurve ringFade;

    [SerializeField, Foldout("Range")] SpriteRenderer range;
    [SerializeField, Foldout("Range")] float fadeDuration;
    [SerializeField, Foldout("Range")] AnimationCurve fadeCurve;

    [Button]
    void GenerateRings()
    {
        if (numberOfRing == 0)
            return;

        GameObject template = Instantiate(ringsContainer.GetChild(0).gameObject);

        rings.Clear();
        while (ringsContainer.childCount > 0)
            DestroyImmediate(ringsContainer.GetChild(0).gameObject);    

        for (int i = 0; i < numberOfRing;i++)
        {
            GameObject clone = Instantiate(template, ringsContainer);
            clone.name = "Ring (" + i.ToString() + ")";
            rings.Add(clone.GetComponent<SpriteRenderer>());
        }

        DestroyImmediate(template);
    }

    #endregion

    [SerializeField, Foldout("Collider")] GameObject colliderHandle;

    IEnumerator Cor_HypnosisAnimation()
    {
        
        for (int i = 0; i < eyeSprites.Length; i++)
        {
            eye.sprite = eyeSprites[i];
            yield return new WaitForSeconds(timeStep);
        }

        SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_HYPNOSIS);
        for (int loop = 0; loop < loopTime; loop++)
        {
            for (int i=0;i<numberOfRing;i++)
            {
                SpriteRenderer ring = rings[0];
                rings.RemoveAt(0);

                Transform ringTransform = ring.transform;
                ringTransform.localPosition = new Vector2(0, eyeCenterPos);
                ringTransform.localScale = Vector2.one*initialScale;
                ringTransform.DOScale(hypnosisRadius, timeMoving);
                ringTransform.DOLocalMoveY(0, timeMoving);

                ring.color = Color.white;
                ring.DOFade(0, timeMoving*0.2f).SetDelay(timeMoving*0.8f).OnComplete(()=> 
                {
                    rings.Add(ring); 
                    colliderHandle.SetActive(true);
                });

                yield return new WaitForSeconds(timeBetweenRings);
            }
                
        }

        for (int i = eyeSprites.Length-1; i >= 0; i--)
        {
            eye.sprite = eyeSprites[i];
            yield return new WaitForSeconds(timeStep);
        }

        SoundManager.Instance.StopSFX(AudioPlayer.SoundID.SFX_HYPNOSIS);

        Destroy(gameObject);
    }

    public override void Cast()
    {
        eye.gameObject.SetActive(true);
        ringsContainer.gameObject.SetActive(true);
        StartCoroutine(Cor_HypnosisAnimation());
    }

    public override IEnumerator Cor_EndSpell()
    {
        return null;
    }

    public override void HandleRange()
    {
        range.DOFade(1, fadeDuration).SetEase(fadeCurve).SetLoops(-1);
    }
}
