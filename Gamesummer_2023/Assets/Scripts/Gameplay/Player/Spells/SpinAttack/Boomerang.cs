using DG.Tweening;
using GDC.Objects;
using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GDC.PlayerManager.Spell
{
    public class Boomerang : PlayerAttackObject
    {
        [SerializeField,ReadOnly] float radius;
        public float Radius { set => radius = value; }

        [SerializeField, ReadOnly] float startAngle;
        public float StartAngle { set => startAngle = value; }

        [SerializeField, ReadOnly] float duration;
        public float Duration { set => duration = value; }
        float maxAngle;
        float time;

        bool doneSpell = false;

        [SerializeField] Transform boomerang;
        [SerializeField, ReadOnly] SpinAttack spinAttack;
        public SpinAttack SpinAttack
        {
            set => spinAttack = value;
        }

        private void Start()
        {
            maxAngle = startAngle + 720;
            time = 0;
        }

        void Update()
        {
            if (time<duration)
            {
                time+= Time.deltaTime;
                float currentAngle = Mathf.Lerp(startAngle, maxAngle, time/duration);
                float currentRadius = Mathf.Lerp(0, radius, time/duration);
                boomerang.localPosition = new Vector2(
                    currentRadius * Mathf.Cos(Mathf.Deg2Rad * currentAngle),
                    currentRadius * Mathf.Sin(Mathf.Deg2Rad * currentAngle) * Mathf.Cos(30 * Mathf.Deg2Rad));
            }
            else if (!doneSpell)
            {
                doneSpell = true;
                transform.DOMove(Player.Instance.transform.position, 0.5f).OnComplete(() =>
                {
                    spinAttack.EndSpell = true;
                });
            }
        }
    }
}