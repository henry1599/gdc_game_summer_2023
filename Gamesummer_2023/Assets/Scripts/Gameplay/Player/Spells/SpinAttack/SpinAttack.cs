using DG.Tweening;
using GDC.Managers;
using GDC.PlayerManager.Spell;
using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GDC.PlayerManager.Spell
{
    public class SpinAttack : Spell
    {

        bool endSpell = false;
        public bool EndSpell
        {
            set => endSpell = value;
        }

        [SerializeField] List<Boomerang> boomerangs = new List<Boomerang>();
        [SerializeField] int boomerangNumber;
        [SerializeField] Transform boomerangContainer;
        [SerializeField] float boomerangRadius;
        [SerializeField] float duration;

        [SerializeField, Foldout("Range")] SpriteRenderer range;
        [SerializeField, Foldout("Range")] float fadeDuration;
        [SerializeField, Foldout("Range")] AnimationCurve fadeCurve;

        [Button]
        void GenerateBoomerang()
        {
            if (boomerangNumber == 0)
                return;
            GameObject template = Instantiate(boomerangContainer.GetChild(0).gameObject);

            while (boomerangContainer.childCount > 0)
            {
                DestroyImmediate(boomerangContainer.GetChild(0).gameObject);
            }
            boomerangs.Clear();

            for (int i = 0; i < boomerangNumber; i++)
            {
                GameObject clone = Instantiate(template, boomerangContainer);
                clone.name = "Boomerang (" + i.ToString() + ")";
                Boomerang boomerang = clone.GetComponent<Boomerang>();
                boomerang.StartAngle = 360 / boomerangNumber * i;
                boomerang.Radius = boomerangRadius;
                boomerang.SpinAttack = this;
                boomerang.Duration = duration;

                boomerangs.Add(boomerang);
            }
            DestroyImmediate(template);
        }

        public override void Cast()
        {
            foreach (Boomerang boomerang in boomerangs)
            {
                boomerang.gameObject.SetActive(true);
            }
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_BOOMERANG);
            StartCoroutine(Cor_EndSpell());
        }

        [Button]
        void Test()
        {
            Cast();
        }

        public override IEnumerator Cor_EndSpell()
        {
            yield return new WaitUntil(() => endSpell);
            SoundManager.Instance.StopSFX(AudioPlayer.SoundID.SFX_BOOMERANG);
            Destroy(gameObject);
        }

        public override void HandleRange()
        {
            range.DOFade(1, fadeDuration).SetEase(fadeCurve).SetLoops(-1);
        }
    }
}