using DG.Tweening;
using GDC.Gameplay;
using GDC.Managers;
using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GDC.PlayerManager.Spell
{
    public class EnergyBall : Spell
    {
        [SerializeField,Foldout("BlackHole")] Transform blackHoleContainer;
        [SerializeField, Foldout("BlackHole")] Transform blackHole;
        [SerializeField, Foldout("BlackHole")] float timeToTarget,radius;

        [SerializeField,Foldout("Star")] Transform starBeforeExplosion;
        [SerializeField, Foldout("Star")] float rotationEuler,starScale;
        [SerializeField, Foldout("Star")] AnimationCurve starAnimation;

        [SerializeField, Foldout("Explosion")] GameObject explosionContainer;
        [SerializeField,Foldout("Explosion")] Transform explosion,smallExplosion;
        [SerializeField, Foldout("Explosion")] float explosionRotation;
        [SerializeField, Foldout("Explosion")] ParticleSystem dustPS;
        [SerializeField, Foldout("Explosion")] float explosionScale;
        [SerializeField, Foldout("Explosion")] AnimationCurve explosionAnimation;
        [SerializeField, Foldout("Explosion")] float explosionDuration;

        [SerializeField, Foldout("Range")] SpriteRenderer range;
        [SerializeField, Foldout("Range")] AnimationCurve fadeCurve;
        [SerializeField, Foldout("Range")] float fadeDuration;
        [SerializeField, Foldout("Range")] Transform target;
        [SerializeField, Foldout("Range")] Transform targetContainer;
        [SerializeField, Foldout("Range")] float targetDuration;
        [SerializeField, Foldout("Range")] LineRenderer line;
        [SerializeField, Foldout("Range")] AnimationCurve targetCurve;
        public override void Cast()
        {
            //float angle = Vector2.Angle(targetContainer.position - transform.position, Vector2.right); ;
            //if (targetContainer.position.y < transform.position.y)
            //    angle = 0 - angle;
            blackHoleContainer.gameObject.SetActive(true);
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_INHALE);
            blackHoleContainer.DOLocalMove(targetContainer.localPosition, timeToTarget)
                .SetEase(Ease.Linear)
                .OnComplete(() =>
                {
                    SoundManager.Instance.StopSFX(AudioPlayer.SoundID.SFX_INHALE);
                    blackHole.DOScale(Vector3.zero, 0.5f).OnComplete(() =>
                    {
                        blackHole.gameObject.SetActive(false);
                        starBeforeExplosion.gameObject.SetActive(true);
                        starBeforeExplosion.DOScale(starScale, 0.5f).SetEase(starAnimation);
                        starBeforeExplosion.DORotate(new Vector3(0, 0, rotationEuler), 0.5f).OnComplete(() =>
                        {
                            explosionContainer.SetActive(true);
                            dustPS.Play();
                            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_EXPLOSION);
                            CameraEffects.Instance.ShakeOnce(explosionDuration,5, camera: Camera.main);
                            smallExplosion.DORotate(new Vector3(0, 0, -explosionRotation), explosionDuration,RotateMode.FastBeyond360).SetEase(explosionAnimation);
                            explosion.DORotate(new Vector3(0,0,explosionRotation),explosionDuration, RotateMode.FastBeyond360).SetEase(explosionAnimation);
                            smallExplosion.DOScale(explosionScale*2/3,explosionDuration).SetEase(explosionAnimation);
                            explosion.DOScale(explosionScale, explosionDuration).SetEase(explosionAnimation);
                            smallExplosion.GetComponent<SpriteRenderer>().DOFade(0, explosionDuration);
                            explosion.GetComponent<SpriteRenderer>().DOFade(0, explosionDuration).OnComplete(()=>
                            {
                                Destroy(gameObject);
                            });
                        });
                    });
                });

            
        }

        public override IEnumerator Cor_EndSpell()
        {
            return null;
        }

        public override void HandleRange()
        {
            range.DOFade(1, fadeDuration).SetEase(fadeCurve).SetLoops(-1);
            target.DOScale(1, targetDuration).SetEase(targetCurve).SetLoops(-1);
        }

        public override void HandleUpdateRange()
        {
            Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            //if (Vector2.Distance(mousePos, rangeField.position) > radius)
            //{
                float angle = Vector2.Angle(mousePos - (Vector2)rangeField.position, Vector2.right); ;
                if (mousePos.y < rangeField.position.y)
                    angle = 0 - angle;
                targetContainer.position = rangeField.position + new Vector3(radius * Mathf.Cos(angle * Mathf.Deg2Rad), radius * Mathf.Sin(angle * Mathf.Deg2Rad));
                line.SetPosition(0, line.transform.position);
                line.SetPosition(1, targetContainer.position);
            //    return;
            //}
            //targetContainer.position = mousePos;
            line.SetPosition(0, line.transform.position);
            line.SetPosition(1, targetContainer.position);
        }
    }
}