using GDC.Enemies;
using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlackHole : MonoBehaviour
{
    #region gravity gizmos
    [SerializeField, Foldout("Gizmos")] Transform gizmosCenter;
    [SerializeField, Foldout("Gizmos")] float radius;
    [SerializeField, Foldout("Gizmos")] LayerMask enemyMask;

    [SerializeField] float gravityStrength;

    void DetectEnemy()
    {
        Collider2D[] enemies = Physics2D.OverlapCircleAll(gizmosCenter.position, radius, enemyMask);

        if (enemies.Length == 0)
            return;

        foreach (Collider2D enemy in enemies)
        {
            enemy.GetComponent<Enemy>().PullToPos(transform.position, gravityStrength);
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(gizmosCenter.position, radius);
    }
    #endregion

    private void Update()
    {
        DetectEnemy();
    }
}
