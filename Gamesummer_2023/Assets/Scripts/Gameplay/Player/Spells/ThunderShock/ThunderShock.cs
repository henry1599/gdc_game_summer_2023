using DG.Tweening;
using GDC.Gameplay;
using GDC.PlayerManager.Spell;
using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThunderShock : Spell
{
    [SerializeField, Foldout("Gizmos")] Transform gizmosMark;
    [SerializeField, Foldout("Gizmos")] float radius;
    [SerializeField, Foldout("Gizmos")] LayerMask enemy;

    [SerializeField, Foldout("Info")] int numberOfThunder;
    [SerializeField, Foldout("Info")] List<Thunder> thunders = new List<Thunder>();
    [SerializeField, Foldout("Info")] Transform thunderContainer;
    [SerializeField, Foldout("Info")] float timeBetweenThunders;

    [SerializeField, Foldout("Range")] SpriteRenderer range;
    [SerializeField, Foldout("Range")] AnimationCurve fadeCurve;
    [SerializeField, Foldout("Range")] float fadeDuration;
    [SerializeField, Foldout("Range")] Transform target;
    [SerializeField, Foldout("Range")] Transform targetContainer;
    [SerializeField, Foldout("Range")] float targetDuration;
    [SerializeField, Foldout("Range")] AnimationCurve targetCurve;

    [Button]
    void GenerateThunder()
    {
        if (numberOfThunder == 0) 
            return;

        GameObject template = Instantiate(thunderContainer.GetChild(0).gameObject);
        thunders.Clear();

        while (thunderContainer.childCount > 0)
            DestroyImmediate(thunderContainer.GetChild(0).gameObject);

        for (int i = 0; i < numberOfThunder; i++)
        {
            GameObject clone = Instantiate(template, thunderContainer);
            clone.name = "Thunder (" + i.ToString() + ")";
            thunders.Add(clone.GetComponent<Thunder>());    
        }

        DestroyImmediate(template);
    }

    public override void Cast()
    {
        transform.position = Vector3.zero;
        thunderContainer.position = targetContainer.transform.position;
        StartCoroutine(Cor_CastThunder());
    }

    IEnumerator Cor_CastThunder()
    {
        foreach (Thunder thunder in thunders)
        {
            Transform target = DetectEnemy();
            thunder.transform.position = (target != null) ? target.position : ((Vector2)thunderContainer.position + new Vector2(
                Random.Range(0, radius) * Mathf.Cos(Random.Range(0, 360) * Mathf.Deg2Rad),
                Random.Range(0, radius) * Mathf.Sin(Random.Range(0, 360) * Mathf.Deg2Rad)));
            thunder.gameObject.SetActive(true);
            yield return new WaitForSeconds(timeBetweenThunders);
        }

        StartCoroutine(Cor_EndSpell());
    }

    Transform DetectEnemy()
    {
        Collider2D[] enemies = Physics2D.OverlapCircleAll(thunderContainer.position, radius, enemy);
        if (enemies.Length == 0)
            return null;
        float minDistance = radius;
        Transform target = null;
        foreach (Collider2D enemy in  enemies)
        {
            float distanceFromCenter = Vector2.Distance(enemy.transform.position, thunderContainer.position);
            if (distanceFromCenter < minDistance)
            {
                minDistance = distanceFromCenter;
                target = enemy.transform;
            }
        }    
        return target;
    }

    public override IEnumerator Cor_EndSpell()
    {
        foreach (Thunder thunder in thunders)
        {
            if (thunder.gameObject.activeInHierarchy)
            {
                yield return new WaitUntil(() => !thunder.gameObject.activeInHierarchy);
            }
        }

        Destroy(gameObject);
    }
    public override void HandleRange()
    {
        range.DOFade(1, fadeDuration).SetEase(fadeCurve).SetLoops(-1);
        target.DOScale(2, targetDuration).SetEase(targetCurve).SetLoops(-1);
    }

    public override void HandleUpdateRange()
    {
        Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        //if (Vector2.Distance(mousePos, rangeField.position) > radius)
        //{
        //    float angle = Vector2.Angle(mousePos - (Vector2)rangeField.position, Vector2.right); ;
        //    if (mousePos.y < rangeField.position.y)
        //        angle = 0 - angle;
        //    targetContainer.position = rangeField.position + new Vector3(radius * Mathf.Cos(angle * Mathf.Deg2Rad), radius * Mathf.Sin(angle * Mathf.Deg2Rad));
        //    return;
        //}
        targetContainer.position = mousePos;
    }
}
