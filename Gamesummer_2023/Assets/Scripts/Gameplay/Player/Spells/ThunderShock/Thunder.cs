using DG.Tweening;
using GDC.Gameplay;
using GDC.Managers;
using GDC.Objects;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Thunder : PlayerAttackObject
{
    [SerializeField] SpriteRenderer burn;
    [SerializeField] ParticleSystem dust;
    //public void EndThunder()
    //{
    //    gameObject.SetActive(false);
    //}

    public void ThunderHit()
    {
        CameraEffects.Instance.ShakeOnce(0.5f, 5, camera: Camera.main);
        SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_THUNDER);
        burn.color = Color.black;
        burn.DOFade(0, 1);
        dust.Play();
        StartCoroutine(Cor_EndThunder());
    }    

    IEnumerator Cor_EndThunder()
    {
        yield return new WaitForSeconds(dust.main.startLifetime.constantMax);
        gameObject.SetActive(false);
    }
}
