using DG.Tweening;
using GDC.Gameplay;
using GDC.Managers;
using GDC.PlayerManager.Spell;
using Helpers;
using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
namespace GDC.PlayerManager.Spell
{
    public class FireBreath : Spell
    {
        [SerializeField] float fireBreathDuration;


        [Foldout("Breath"), SerializeField]
        float time;
        [Foldout("Breath"), SerializeField]
        float radius;
        [SerializeField] Breath breath;
        [SerializeField] GameObject fireInit;

        [SerializeField] List<GameObject> setLavaGround;
        [SerializeField] float lavaAnimationDuration;
        [ShowNonSerializedField] float distanceBetweenGrounds = 0.7f;

        [SerializeField] Transform fireEarths;

        [SerializeField,Foldout("Range")] Transform range;
        [SerializeField, Foldout("Range")] AnimationCurve fadeCurve;
        [SerializeField, Foldout("Range")] float fadeDuration;
        private void Awake()
        {
            //breath = transform.Find("Breath").GetComponent<Breath>();
            breath.Time = time;
            breath.Range = radius;

            setLavaGround = new List<GameObject>();


            for (int i = 0; i < fireEarths.childCount; i++)
            {
                setLavaGround.Add(fireEarths.GetChild(i).gameObject);
                setLavaGround[i].SetActive(false);
                foreach (LavaGround lavaGround in setLavaGround[i].GetComponentsInChildren(typeof(LavaGround), true))
                {
                    lavaGround.LavaAnimationDuration = lavaAnimationDuration;
                }
            }
        }


        public override void Cast()
        {
            float angle = Vector2.Angle(range.localPosition, Vector2.right);
            if (range.localPosition.y < 0)
                angle = 0 - angle;
            breath.Angle = angle;
            breath.transform.localRotation = Quaternion.Euler(0, 0, angle);
            breath.gameObject.SetActive(true);
            breath.Cast();
            fireInit.SetActive(true);
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_FIRE);
            StartCoroutine(Cor_SpawnLava(angle));
            CameraEffects.Instance.ShakeOnce(0.5f, 3, camera: Camera.main);
            StartCoroutine(Cor_EndSpell());

        }


        IEnumerator Cor_SpawnLava(float angle)
        {

            float timeToFadeLava = fireBreathDuration - time * 2; // time to spawn lava and time to end lava => time * 2
            for (int i = 0; i < setLavaGround.Count; i++)
            {
                if (breath.HitObstacle)
                {
                    print("Hit");
                    yield break;
                }
                setLavaGround[i].transform.localPosition = new Vector2(distanceBetweenGrounds * i * Mathf.Cos(angle * Mathf.Deg2Rad), distanceBetweenGrounds * i * Mathf.Sin(angle * Mathf.Deg2Rad));
                setLavaGround[i].transform.localRotation = Quaternion.Euler(0, 0, angle);
                setLavaGround[i].SetActive(true);
                foreach (LavaGround lavaGround in setLavaGround[i].GetComponentsInChildren(typeof(LavaGround), true))
                {
                    lavaGround.HandleEndLava(time, timeToFadeLava);
                }
                yield return new WaitForSeconds(time / setLavaGround.Count);
            }
        }

        [Button("Generate Lava Ground")]
        public void GenerateLavaGround()
        {
            GameObject templateGround = Instantiate(fireEarths.GetChild(0).gameObject);

            while (fireEarths.childCount > 0)
            {

                DestroyImmediate(fireEarths.GetChild(0).gameObject);
            }

            int idx = 0;
            for (float i = 0; i < radius; i += distanceBetweenGrounds)
            {
                GameObject lavaGround = Instantiate(templateGround, fireEarths);
                lavaGround.name = "SetFireEarth (" + idx++ + ")";
            }

            DestroyImmediate(templateGround);
        }

        public override IEnumerator Cor_EndSpell()
        {
            yield return new WaitForSeconds(fireBreathDuration);
            Destroy(gameObject);
        }
        public override void HandleRange()
        {
            range.GetComponent<SpriteRenderer>().DOFade(1, fadeDuration).SetEase(fadeCurve).SetLoops(-1);
        }
        public override void HandleUpdateRange()
        {
            Vector2 playerPos = Player.Instance.transform.position;
            Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            float angle = Vector2.Angle(mousePos - playerPos, Vector2.right);
            if (mousePos.y < playerPos.y)
                angle = 0 - angle;

            range.localRotation = Quaternion.Euler(new Vector3(0, 0, angle));
            range.localPosition = new Vector3(
                radius/2 * Mathf.Cos(angle * Mathf.Deg2Rad),
                radius/2 * Mathf.Sin(angle * Mathf.Deg2Rad));
        }
    }
}