using DG.Tweening;
using GDC.Objects;
using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GDC.PlayerManager.Spell
{
    public class LavaGround : MonoBehaviour
    {

        SpriteRenderer lava;
        SpriteRenderer ground;
        float lavaAnimationDuration;
        public float LavaAnimationDuration { set => lavaAnimationDuration = value; }

        float colorValue;
        private void Awake()
        {
            lava = transform.Find("Lava").GetComponent<SpriteRenderer>();
            ground = transform.Find("Ground").GetComponent<SpriteRenderer>();
            
        }
        // Start is called before the first frame update
        void Start()
        {
            StartCoroutine(LavaAnimation());
        }

        private void Update()
        {
            lava.color = new Color(colorValue, colorValue,colorValue,lava.color.a);
        }

        IEnumerator LavaAnimation()
        {
            
            while (true)
            {
                DOTween.To(() => colorValue, x => colorValue = x, 0.5f, lavaAnimationDuration);
                yield return new WaitForSeconds(lavaAnimationDuration);
                DOTween.To(() => colorValue, x => colorValue = x, 1, lavaAnimationDuration);
                yield return new WaitForSeconds(lavaAnimationDuration);
            }
        }
        public void HandleEndLava(float time,float fadingTime)
        {
            StartCoroutine(EndLavaAnimation(time,fadingTime));
        }
        IEnumerator EndLavaAnimation(float time, float fadingTime)
        {
            yield return new WaitForSeconds(fadingTime);
            
            lava.DOColor(Color.clear, time);
            ground.DOColor(Color.clear, time);
        }    
    }
}