using DG.Tweening;
using GDC.Objects;
using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GDC.PlayerManager.Spell
{
    public class Breath : PlayerAttackObject
    {
        
        SpriteRenderer breathSprite;
        [ShowNonSerializedField] float angle;
        public float Angle { set => angle = value; }
        [ShowNonSerializedField] float time;
        public float Time { set => time = value; }
        [ShowNonSerializedField] float range;
        public float Range { set => range = value; }
        
        [InfoBox("Range from 0.8 to 0.9")]
        [SerializeField] float fadingTime;
        
        [SerializeField,ShowAssetPreview] Sprite[] spritesForAnimation = new Sprite[4];

        bool hitObstacle = false;
        public bool HitObstacle
        {
            get => hitObstacle;
        }
        [SerializeField] LayerMask obstacleMask;

        private void Awake()
        {
            breathSprite = transform.GetChild(0).GetComponent<SpriteRenderer>();
            base.Awake();
        }
        // Start is called before the first frame update
        public void Cast()
        {
            
            StartCoroutine(EndBreath());
            StartCoroutine(BreathAnimation());
            transform.DOLocalMove(new Vector2(range * Mathf.Cos(angle * Mathf.Deg2Rad), range * Mathf.Sin(angle * Mathf.Deg2Rad)), time).SetEase(Ease.Linear);
        }

        IEnumerator EndBreath()
        {
            yield return new WaitForSeconds(time);
            breathSprite.DOFade(0, fadingTime);
            GetComponent<Collider2D>().enabled = false;
            //yield return new WaitForSeconds(fadingTime);
            //gameObject.SetActive(false);
        }

        IEnumerator BreathAnimation()
        {
            foreach (Sprite sprite in spritesForAnimation)
            {
                
                breathSprite.sprite= sprite;
                yield return new WaitForSeconds(time/(spritesForAnimation.Length));
            }
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.CompareTag("Ledge")||collision.CompareTag("Water")||collision.CompareTag("Wall") || collision.CompareTag("Boulder"))
            {
                hitObstacle = true;
            }
        }
    }
}