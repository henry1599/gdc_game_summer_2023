using DG.Tweening;
using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GDC.PlayerManager.Spell
{
    public class IceBlast : MonoBehaviour
    {
        [SerializeField] int spikeInIceBlast;
        [SerializeField] float endDuration;
        [SerializeField] float iceRange;
        [SerializeField] Transform spikeContainer;

        [SerializeField, Foldout("Ice Dust")]
        Sprite[] dust = new Sprite[8];
        [SerializeField, Foldout("Ice Dust")] List<SpriteRenderer> dustSprite;
        [SerializeField, Foldout("Ice Dust")] float dustDuration;
        [SerializeField, Foldout("Ice Dust")] Transform iceDustContainer;
        [SerializeField, Foldout("Ice Dust")] float iceDustCircleRotationY,iceDustCircleRange;
        [SerializeField, Foldout("Ice Dust")] int numberOfIceDust;
        public float EndDuration { get { return endDuration; } }

        private void Start()
        {
            foreach (SpriteRenderer sprite in dustSprite)
            {

                sprite.DOFade(0, dustDuration);
            }

            StartCoroutine(DustAnimation());
        }

        IEnumerator DustAnimation()
        {
            for (int i = 0; i < dust.Length; i++)
            {
                foreach (SpriteRenderer sprite in dustSprite)
                {

                    sprite.sprite = dust[i];
                }
                yield return new WaitForSeconds(dustDuration / dust.Length);
            }
        }

        [Button]
        void GenerateIceBlast()
        {
            if (spikeInIceBlast <= 0) 
                return;
            GameObject templateSpike = Instantiate(spikeContainer.GetChild(0).gameObject);

            while (spikeContainer.childCount > 0)
            {

                DestroyImmediate(spikeContainer.GetChild(0).gameObject);
            }

            for (float i = 0; i < spikeInIceBlast; i++)
            {
                GameObject spike = Instantiate(templateSpike, spikeContainer);
                spike.name = "IceSpike (" + i + ")";
                spike.transform.localPosition = Vector2.right * Random.Range(-iceRange, iceRange);
                spike.GetComponent<IceSpike>().SpikeForIceBlast = true;
                spike.GetComponent<IceSpike>().TimeEndSpike= endDuration;
            }

            DestroyImmediate(templateSpike);
        }

        [Button]
        void GenerateIceDust()
        {
            if (numberOfIceDust <= 0)
                return;
            dustSprite = new List<SpriteRenderer>();
            GameObject templateDust = Instantiate(iceDustContainer.GetChild(0).gameObject);

            while (iceDustContainer.childCount > 0)
            {

                DestroyImmediate(iceDustContainer.GetChild(0).gameObject);
            }

            for (float i = 0; i < numberOfIceDust; i++)
            {
                GameObject dust = Instantiate(templateDust, iceDustContainer);
                dust.name = "Dust (" + i + ")";
                dust.transform.localPosition = new Vector2(iceDustCircleRange*Mathf.Cos(360 / numberOfIceDust * i * Mathf.Deg2Rad),iceDustCircleRange*Mathf.Sin(iceDustCircleRotationY*Mathf.Deg2Rad)*Mathf.Sin(360 / numberOfIceDust * i * Mathf.Deg2Rad));
                if (dust.transform.localPosition.y <= 0)
                    dust.GetComponent<Depth>().baseDepth = 1;
                else
                    dust.GetComponent<Depth>().baseDepth = -1;

                dustSprite.Add(dust.GetComponent<SpriteRenderer>());
                
            }

            DestroyImmediate(templateDust);
        }
        
    }
}