using DG.Tweening;
using GDC.Objects;
using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

namespace GDC.PlayerManager.Spell
{
    public class IceSpike : MonoBehaviour
    {
        [SerializeField] float bottom;

        [SerializeField] float timeSpawnSpike;
        //public float TimeSpawnSpike { set => timeSpawnSpike = value; }
        [SerializeField] Transform iceSpikeSprite;
        [SerializeField] Transform iceSpikeAngle;

        [SerializeField] SpriteMask spriteMask;

        bool spikeForIceBlast = false;
        public bool SpikeForIceBlast { set { spikeForIceBlast = value;} }
         public float TimeEndSpike;
        // Start is called before the first frame update
        void Start()
        {
            iceSpikeSprite.DOLocalMoveY(0, timeSpawnSpike);

            //if (spikeForIceBlast)
            //    dustSprite.DOFade(0, dustDuration);
            //else
                
        }

        private void Update()
        {
            HandleSpriteMask();
        }

        async void HandleSpriteMask()
        {
            int depth = iceSpikeSprite.GetComponent<SpriteRenderer>().sortingOrder;

            Task front = ChangeFrontOrder(depth);
            Task back = ChangeBackOrder(depth);
            await Task.WhenAll(front, back);
        }

        async Task ChangeFrontOrder(int order)
        {
            spriteMask.frontSortingOrder = order;
            await Task.Delay(10);
        }

        async Task ChangeBackOrder(int order)
        {
            spriteMask.backSortingOrder = order - 1;
            await Task.Delay(10);
        }

        public void ChangeAngle(float angle)
        {
            iceSpikeAngle.localRotation = Quaternion.Euler(0, 0, angle);
        }

        public void HandleEndIceSpike(/*float timeToEnd*/)
        {
            if ( spikeForIceBlast )
            {
                iceSpikeSprite.DOLocalMoveY(bottom, TimeEndSpike);
                return;
            }
            iceSpikeSprite.DOLocalMoveY(bottom, timeSpawnSpike);
        }

        
        
    }
}