using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using GDC.PlayerManager.Spell;
using DG.Tweening;

namespace GDC.PlayerManager.Spell
{
    public class IceSpit : Spell
    {
        [Foldout("Spike Infomation"), SerializeField] float distanceBetweenSpikes, timeBetweenSpikes;

        [Foldout("Spike Infomation"), SerializeField] List<IceSpike> iceSpikes = new List<IceSpike>();

        [SerializeField, Foldout("Ice Spit")] float iceSpitDuration;
        [SerializeField, Foldout("Ice Spit")] float radius;
        [SerializeField, Foldout("Spike Infomation")] Transform icePath;

        [SerializeField] Transform iceBlast;

        [SerializeField] Transform dustVFX;

        [SerializeField, Foldout("Range")] SpriteRenderer range;
        [SerializeField, Foldout("Range")] AnimationCurve fadeCurve;
        [SerializeField, Foldout("Range")] float fadeDuration;
        [SerializeField, Foldout("Range")] Transform target;
        [SerializeField, Foldout("Range")] Transform targetContainer;
        [SerializeField, Foldout("Range")] float targetDuration;
        [SerializeField, Foldout("Range")] LineRenderer line;
        [SerializeField, Foldout("Range")] AnimationCurve targetCurve;
        
        public override void Cast()
        {
            Vector2 path = targetContainer.position - rangeField.position;

            int spikeNumberNeeded = (int)(Vector2.Distance(targetContainer.position, rangeField.position) / distanceBetweenSpikes);

            for (int i = 0; i < spikeNumberNeeded; i++)
            {
                iceSpikes[i].transform.localPosition = path.normalized * (i + 1) * distanceBetweenSpikes;
                iceSpikes[i].ChangeAngle(Random.Range(-30f, 30f));
            }

            iceBlast.localPosition = targetContainer.localPosition;
            foreach (IceSpike iceSpike in iceBlast.GetComponentsInChildren(typeof(IceSpike)))
            {
                iceSpike.ChangeAngle(Random.Range(-60f, 60f));
            }

            StartCoroutine(SpawnSpike(spikeNumberNeeded));
        }

        [Button]
        public void GenerateIceSpikes()
        {
            GameObject template = Instantiate(icePath.GetChild(0).gameObject);
            iceSpikes.Clear();

            while (icePath.childCount > 0)
            {

                DestroyImmediate(icePath.GetChild(0).gameObject);
            }

            for (float i = 0; i <= (int)(radius / distanceBetweenSpikes); i++)
            {
                GameObject clone = Instantiate(template, icePath);
                clone.name = "IceSpike (" + i + ")";
                iceSpikes.Add(clone.GetComponent<IceSpike>());
            }

            DestroyImmediate(template);
        }

        IEnumerator SpawnSpike(int spikeNumberNeeded)
        {
            for (int i = 0; i < spikeNumberNeeded; i++)
            {
                iceSpikes[i].gameObject.SetActive(true);
                //if (iceSpikes[i].)
                dustVFX.position = iceSpikes[i].transform.position;
                yield return new WaitForSeconds(timeBetweenSpikes);
            }
            iceBlast.gameObject.SetActive(true);

            yield return new WaitForSeconds(iceSpitDuration - timeBetweenSpikes);
            for (int i = 0; i < spikeNumberNeeded; i++)
            {
                iceSpikes[i].HandleEndIceSpike();
                yield return new WaitForSeconds(timeBetweenSpikes);
            }

            foreach (IceSpike iceSpike in iceBlast.GetComponentsInChildren(typeof(IceSpike),true))
            {
                iceSpike.HandleEndIceSpike();
            }

            yield return new WaitForSeconds(iceBlast.GetComponent<IceBlast>().EndDuration);

            Destroy(gameObject);
        }

        public override IEnumerator Cor_EndSpell()
        {
            yield return null;
        }

        public override void HandleRange()
        {
            range.DOFade(1, fadeDuration).SetEase(fadeCurve).SetLoops(-1);
            target.DOScale(1, targetDuration).SetEase(targetCurve).SetLoops(-1);
        }

        public override void HandleUpdateRange()
        {
            Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            if (Vector2.Distance(mousePos, rangeField.position) > radius)
            {
                float angle = Vector2.Angle(mousePos - (Vector2)rangeField.position, Vector2.right); ;
                if (mousePos.y < rangeField.position.y)
                    angle = 0 - angle;
                targetContainer.position = rangeField.position + new Vector3(radius * Mathf.Cos(angle * Mathf.Deg2Rad), radius * Mathf.Sin(angle * Mathf.Deg2Rad));
                line.SetPosition(0, line.transform.position);
                line.SetPosition(1, targetContainer.position);
                return;
            }
            targetContainer.position = mousePos;
            line.SetPosition(0, line.transform.position);
            line.SetPosition(1, targetContainer.position);
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.cyan;
            Gizmos.DrawWireSphere(transform.position, radius);
        }
    }
}