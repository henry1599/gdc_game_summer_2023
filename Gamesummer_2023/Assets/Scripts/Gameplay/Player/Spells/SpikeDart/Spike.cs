using DG.Tweening;
using GDC.Enemies;
using GDC.Objects;
using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.Gameplay.UI;


namespace GDC.PlayerManager.Spell
{
    public class Spike : MonoBehaviour
    {
        [SerializeField] int numberOfSpike;
        [SerializeField] List<Transform> spikes = new List<Transform>();
        [SerializeField] Transform spikeContainer;
        bool endSpike = false;
        public bool EndSpike
        { get => endSpike; }

        [Button]
        void GenerateSpike()
        {
            if (numberOfSpike == 0)
                return;

            spikes.Clear();

            GameObject template = Instantiate(spikeContainer.GetChild(0).gameObject);
            while (spikeContainer.childCount > 0)
                DestroyImmediate(spikeContainer.GetChild(0).gameObject);

            for (int i = 0; i < numberOfSpike; i++)
            {
                GameObject clone = Instantiate(template, spikeContainer);
                clone.name = "Spike (" + i.ToString() + ")";
                clone.transform.localRotation = Quaternion.Euler(new Vector3(0, 0, 360 / numberOfSpike * i));
                spikes.Add(clone.transform);
            }

            DestroyImmediate(template);
        }

        public void Cast(float range, float duration)
        {
            if (UIGameplay.Instance.isPause)
            {
                for (int i = 0; i < numberOfSpike; i++)
                {
                    Transform spike = spikes[i];
                    spike.DOLocalMove(new Vector3(
                        Mathf.Cos(360 / numberOfSpike * i * Mathf.Deg2Rad) * range,
                        Mathf.Sin(360 / numberOfSpike * i * Mathf.Deg2Rad) * range), duration).SetUpdate(true).OnComplete(() =>
                        {
                            Destroy(spike.gameObject);
                            endSpike = true;
                        });
                }
            }
            else 
            {
                for (int i = 0; i < numberOfSpike; i++)
                {
                    Transform spike = spikes[i];
                    spike.DOLocalMove(new Vector3(
                        Mathf.Cos(360 / numberOfSpike * i * Mathf.Deg2Rad) * range,
                        Mathf.Sin(360 / numberOfSpike * i * Mathf.Deg2Rad) * range), duration).OnComplete(() =>
                        {
                            Destroy(spike.gameObject);
                            endSpike = true;
                        });
                }
            }
        }
        public void EndSpell()
        {
            for (int i=0; i<numberOfSpike; i++)
            {
                Destroy(spikes[i].gameObject);
            }
        }
    }
}
