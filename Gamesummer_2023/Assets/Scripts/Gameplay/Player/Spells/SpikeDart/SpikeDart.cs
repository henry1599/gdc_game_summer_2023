using DG.Tweening;
using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.Gameplay.UI;
using GDC.Managers;

namespace GDC.PlayerManager.Spell
{
    public class SpikeDart : Spell
    {
        
        [SerializeField] float spikeRange, spikeDuration,timeBetweenSpike,angleBetweenSpike;
        [SerializeField] int numberOfSpike;

        [SerializeField] Transform spikeContainer;
        [SerializeField] List<Spike> spikes = new List<Spike>();

        [SerializeField,Foldout("Range")] SpriteRenderer range;
        [SerializeField, Foldout("Range")] float fadeDuration;
        [SerializeField, Foldout("Range")] AnimationCurve fadeCurve;

        [Button]
        void GenerateSpike()
        {
            if (numberOfSpike == 0)
                return;

            spikes.Clear();

            GameObject template = Instantiate(spikeContainer.GetChild(0).gameObject);
            while (spikeContainer.childCount > 0)
                DestroyImmediate(spikeContainer.GetChild(0).gameObject);

            for (int i = 0; i < numberOfSpike; i++)
            {
                GameObject clone = Instantiate(template, spikeContainer);
                clone.name = "Spike (" + i.ToString() + ")";
                clone.transform.localRotation = Quaternion.Euler(new Vector3(0, 0, angleBetweenSpike*i));
                spikes.Add(clone.GetComponent<Spike>());
            }

            DestroyImmediate(template);
        }

        IEnumerator Cor_Cast()
        {
            foreach (Spike spike in spikes)
            {
                spike.gameObject.SetActive(true);
                spike.Cast(spikeRange, spikeDuration);
                SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_SPIKE);
                if (UIGameplay.Instance.isPause)
                    yield return new WaitForSecondsRealtime(timeBetweenSpike);
                else yield return new WaitForSeconds(timeBetweenSpike);
            }
        }

        public override void HandleRange()
        {
            if (UIGameplay.Instance.isPause)
                range.DOFade(1, fadeDuration).SetEase(fadeCurve).SetLoops(-1).SetUpdate(true);
            else range.DOFade(1, fadeDuration).SetEase(fadeCurve).SetLoops(-1);
        }

        public override void Cast()
        {
            range.gameObject.SetActive(false);
            StartCoroutine(Cor_Cast());
            StartCoroutine(Cor_EndSpell());
        }

        public override IEnumerator Cor_EndSpell()
        {
            foreach (Spike spike in spikes)
            {
                yield return new WaitUntil(() => spike.EndSpike);
            }
            Destroy(gameObject);
        }
        public void EndSpell()
        {
            range.gameObject.SetActive(false);
            Destroy(gameObject);
        }
    }
}