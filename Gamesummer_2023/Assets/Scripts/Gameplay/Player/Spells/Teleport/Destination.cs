using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destination : MonoBehaviour
{
    [SerializeField] Transform gizmosMark;
    [SerializeField] Vector2 size;
    [SerializeField] LayerMask ledge;
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.black;
        Gizmos.DrawWireCube(gizmosMark.position, size);
    }

    public Vector3 Detect()
    {
        ChangeDestination();
        return transform.position;
    }

    void ChangeDestination()
    {
        Collider2D[] ledges = Physics2D.OverlapBoxAll(gizmosMark.position, size, 0,ledge);

        if (ledges.Length == 0)
            return;

        if (ledges.Length > 0)
        {
            Bounds bound = ledges[0].bounds;
            if (bound.extents.x < bound.extents.y)
            {
                transform.position = new Vector3(
                    (transform.position.x > bound.center.x) ?
                    (bound.center.x + bound.extents.x + size.x / 2 - gizmosMark.localPosition.x) :
                    (bound.center.x - bound.extents.x - size.x / 2 - gizmosMark.localPosition.x),
                    transform.position.y);
            }
            else
            {
                transform.position = new Vector3(transform.position.x,
                    (transform.position.y > bound.center.y) ?
                    (bound.center.y + bound.extents.y + size.y / 2 - gizmosMark.localPosition.y) :
                    (bound.center.y - bound.extents.y - size.y / 2 - gizmosMark.localPosition.y));
                print(transform.position);
            }
        }
    }
}
