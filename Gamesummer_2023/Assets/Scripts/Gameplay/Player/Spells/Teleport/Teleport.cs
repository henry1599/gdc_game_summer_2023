using DG.Tweening;
using GDC.Managers;
using GDC.PlayerManager;
using GDC.PlayerManager.Hand;
using GDC.PlayerManager.Spell;
using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleport : Spell
{
    [SerializeField] float radius;
    [SerializeField] Transform destinationDetector;
    [SerializeField] float teleportDuration;

    [SerializeField, Foldout("Range")] SpriteRenderer range;
    [SerializeField, Foldout("Range")] Transform target;
    [SerializeField, Foldout("Range")] AnimationCurve fadeCurve, targetCurve;
    [SerializeField, Foldout("Range")] float fadeDuration, targetDuration;

    public override void Cast()
    {
        destinationDetector.localPosition = target.localPosition;
        Transform player = Player.Instance.transform;
        Transform handUsingWeapon = HandMovement.Instance.transform;
        Player.Instance.IsTeleporting = true;
        SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_TELEPORT);
        handUsingWeapon.DOScale(new Vector3(0, 2, 1), teleportDuration / 2);
        player.DOScale(new Vector3(0, 2, 1), teleportDuration / 2).OnComplete(()=>
        {
            player.position = destinationDetector.GetComponent<Destination>().Detect();
            HandMovement.Instance.HandleMoveHand();
            handUsingWeapon.DOScale(Vector3.one, teleportDuration / 2);
            player.DOScale(Vector3.one, teleportDuration / 2).OnComplete(() => 
            {
                Player.Instance.IsTeleporting = false;
                Destroy(gameObject);
            });
        });

        
        
    }

    public override IEnumerator Cor_EndSpell()
    {
        return null;
    }

    public override void HandleRange()
    {
        range.DOFade(1, fadeDuration).SetEase(fadeCurve).SetLoops(-1);
        target.DOScale(1, targetDuration).SetEase(targetCurve).SetLoops(-1);
    }
    public override void HandleUpdateRange()
    {
        Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        if (Vector2.Distance(mousePos, rangeField.position) > radius)
        {
            float angle = Vector2.Angle(mousePos - (Vector2)rangeField.position, Vector2.right); ;
            if (mousePos.y < rangeField.position.y)
                angle = 0 - angle;
            target.position = rangeField.position + new Vector3(radius * Mathf.Cos(angle * Mathf.Deg2Rad), radius * Mathf.Sin(angle * Mathf.Deg2Rad));
            return;
        }
        target.position = mousePos;
    }
}
