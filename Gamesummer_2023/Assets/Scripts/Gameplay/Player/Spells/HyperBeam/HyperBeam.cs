using DG.Tweening;
using GDC.Gameplay;
using GDC.Managers;
using GDC.PlayerManager;
using GDC.PlayerManager.Spell;
using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HyperBeam : Spell
{
    #region beam
    [SerializeField, Foldout("Beam")] Transform[] beams;
    [SerializeField, Foldout("Beam")] int numberOfBeamTrail;
    [SerializeField, Foldout("Beam")] float beamLength, beamTrailLength;

    [Button]
    void GenerateBeamTrail()
    {
        if (numberOfBeamTrail == 0)
            return;

        beamTrailSpriteRenderers.Clear();

        foreach (Transform beam in beams)
        {
            GameObject template = Instantiate(beam.GetChild(0).gameObject);

            while (beam.childCount > 0)
                DestroyImmediate(beam.GetChild(0).gameObject);

            for (int i = 0;i< numberOfBeamTrail;i++)
            {
                GameObject clone = Instantiate(template, beam);
                clone.name = "BeamTrail (" + i.ToString() + ")";
                clone.transform.localPosition = new Vector2(beamLength + beamTrailLength * i, 0);
                beamTrailSpriteRenderers.Add(clone.GetComponent<SpriteRenderer>());
            }

            DestroyImmediate(template);
        }
    }
    #endregion

    #region cast
    [SerializeField, Foldout("Hyper Beam Info")] float beamDuration;
    [SerializeField, Foldout("Hyper Beam Info")] Transform beamContainer;
    [SerializeField, Foldout("Hyper Beam Info")] GameObject colliderContainer;

    [SerializeField, Foldout("Range")] Transform range;
    [SerializeField, Foldout("Range")] AnimationCurve fadeCurve;
    [SerializeField, Foldout("Range")] float fadeDuration;
    [SerializeField, Foldout("Range")] float rangePos;

    public override void Cast()
    {
        float angle = Vector2.Angle(range.position - transform.position, Vector2.right); ;
        if (range.position.y < transform.position.y)
            angle = 0 - angle;
        chargingAnimation.gameObject.SetActive(true);
        transform.localRotation = Quaternion.Euler(new Vector3(0, 0, angle));
        chargingAnimation.DOScale(0, 0.5f).OnComplete(() =>
        {
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_HYPER_BEAM);
            CameraEffects.Instance.ShakeOnce(beamDuration, 10, camera: Camera.main);
            colliderContainer.SetActive(true);
            beamContainer.gameObject.SetActive(true);
            StartCoroutine(Cor_BeamAnimation());
            StartCoroutine(Cor_BeamScale());
        });
    }

    public override IEnumerator Cor_EndSpell()
    {
        return null;
    }

    public override void HandleRange()
    {
        range.GetComponent<SpriteRenderer>().DOFade(1, fadeDuration).SetEase(fadeCurve).SetLoops(-1);
    }

    public override void HandleUpdateRange()
    {
        Vector2 playerPos = Player.Instance.transform.position;
        Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        float angle = Vector2.Angle(mousePos - playerPos, Vector2.right);
        if (mousePos.y < playerPos.y)
            angle = 0 - angle;

        range.localRotation = Quaternion.Euler(new Vector3(0, 0, angle));
        range.localPosition = new Vector3(
            rangePos * Mathf.Cos(angle * Mathf.Deg2Rad),
            rangePos * Mathf.Sin(angle * Mathf.Deg2Rad));
    }
    #endregion

    #region beam animation

    [SerializeField, Foldout("Beam Animation")] Sprite[] beamSprites;
    [SerializeField, Foldout("Beam Animation")] Sprite[] beamTrailSprites;
    [SerializeField, Foldout("Beam Animation")] List<SpriteRenderer> beamSpriteRenderers = new List<SpriteRenderer>();
    [SerializeField, Foldout("Beam Animation")] List<SpriteRenderer> beamTrailSpriteRenderers = new List<SpriteRenderer>();
    [SerializeField, Foldout("Beam Animation")] int loopTime;

    IEnumerator Cor_BeamScale()
    {
        yield return new WaitForSeconds(beamDuration*0.8f);
        foreach (Transform beam in beams)
        {
            beam.DOScaleY(0, beamDuration*0.2f);
        }
    }

    IEnumerator Cor_BeamAnimation()
    {
        int numberOfSprite = beamSprites.Length;

        for (int loop = 0; loop < loopTime; loop++)
        {
            for (int i = 0; i < numberOfSprite; i++)
            {
                foreach (SpriteRenderer beamSpriteRenderer in beamSpriteRenderers)
                    beamSpriteRenderer.sprite = beamSprites[i];
                foreach (SpriteRenderer beamTrailSpriteRenderer in beamTrailSpriteRenderers)
                    beamTrailSpriteRenderer.sprite = beamTrailSprites[i];
                yield return new WaitForSeconds(beamDuration / (numberOfSprite * loopTime));
            }
        }

        Destroy(gameObject);
    }
    #endregion

    #region charge animation
    [SerializeField, Foldout("Charge Animation")] Transform chargingAnimation;


    #endregion
}
