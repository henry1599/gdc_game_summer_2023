using DG.Tweening;
using GDC.Gameplay.UI;
using GDC.Managers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GDC.PlayerManager.Spell
{
    public abstract class Spell : MonoBehaviour
    {
        [SerializeField] protected Transform rangeField;
        float originOrtho, scaleOrtho = 5.5f;
        public Color introColor;
        public Sprite introSprite;
        [HideInInspector] public bool FixedCamera = false;

        [HideInInspector] public bool IsCasting = false;
        public virtual void HandleRange() { }
        public virtual void HandleUpdateRange() { }

        public void HandleCast()
        {
            if (!FixedCamera)
            {
                if (UIGameplay.Instance.isPause) Camera.main.DOOrthoSize(originOrtho, 0.25f).SetUpdate(true);
                else Camera.main.DOOrthoSize(originOrtho, 0.25f);
            }
            if (rangeField != null) rangeField.gameObject.SetActive(false);
            gameObject.SetActive(true);
            Cast();
        }
        public abstract void Cast();
        public abstract IEnumerator Cor_EndSpell();

        private void Update()
        {
            if (!IsCasting)
            {
                rangeField.position = Player.Instance.transform.position;
                HandleUpdateRange();
            }
        }
        private void Start()
        {
            StartCoroutine(Cor_SetOriginOrthoSize());
            HandleRange();
            if (!FixedCamera)
            {
                if (UIGameplay.Instance.isPause) Camera.main.DOOrthoSize(scaleOrtho, 0.25f).SetUpdate(true);
                else Camera.main.DOOrthoSize(scaleOrtho, 0.25f);
            }
        }
        IEnumerator Cor_SetOriginOrthoSize()
        {
            yield return new WaitUntil(() => GameManager.Instance.isLoadSceneComplete);
            originOrtho = Camera.main.orthographicSize;
        }
    }
}
