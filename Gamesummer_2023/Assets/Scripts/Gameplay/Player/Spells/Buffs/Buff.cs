using GDC.PlayerManager.Spell;
using GDC.Enums;
using System.Collections;
using UnityEngine;
using GDC.PlayerManager;
using NaughtyAttributes;
using DG.Tweening;
using GDC.Managers;

public class Buff : Spell
{
    [SerializeField, Foldout("Info")] float buffValue,duration;
    [SerializeField, Foldout("Info")] Sprite atkIntro, defIntro, speIntro;

    [SerializeField, Foldout("Range")] SpriteRenderer range;
    [SerializeField, Foldout("Range")] float fadeDuration;
    [SerializeField, Foldout("Range")] AnimationCurve fadeCurve;

    public override void Cast()
    {
        switch(PlayerSpell.Instance.CurrentSpellType)
        {
            case SpellType.ATK_BUFF:
                Player.Instance.BuffAtk(buffValue,duration);
                break;
            case SpellType.DEF_BUFF:
                Player.Instance.BuffDef(buffValue, duration);
                break;
            case SpellType.SPE_BUFF:
                Player.Instance.BuffSpe(buffValue, duration);
                break;
        }
        
        Destroy(gameObject);
        SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_BUFF);
    }

    public override IEnumerator Cor_EndSpell()
    {
        yield return new WaitForSeconds(duration);
        Destroy(gameObject);
    }

    public override void HandleRange()
    {
        switch (PlayerSpell.Instance.CurrentSpellType)
        {
            case SpellType.ATK_BUFF:
                introColor = Color.red;
                introSprite = atkIntro;
                range.color = Color.red;
                break;
            case SpellType.DEF_BUFF:
                introColor = Color.green;
                introSprite = defIntro;
                range.color = Color.green;
                break;
            case SpellType.SPE_BUFF:
                introColor = Color.blue;
                introSprite = speIntro;
                range.color = Color.blue;
                break;
        }
        range.color = new Color(range.color.r, range.color.g, range.color.b, 0);
        range.DOFade(1, fadeDuration).SetEase(fadeCurve).SetLoops(-1);
    }
}
