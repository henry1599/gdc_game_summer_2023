using DG.Tweening;
using GDC.Gameplay;
using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.GraphicsBuffer;

namespace GDC.PlayerManager.Spell
{
    public class PoisonBombs : Spell
    {
        [SerializeField] PoisonBomb firstBomb;
        [SerializeField] List<PoisonBomb> extraBombs = new List<PoisonBomb>();
        public List<PoisonBomb> ExtraBombs
        { get => extraBombs; }

        [SerializeField, Foldout("Bombs info")] float bombHeight,bombRotation,poisonScale,poisonDuration;
        [SerializeField, Foldout("Bombs info")] float[] bombDuration;
        [SerializeField, Foldout("Bombs info")] int numberOfExtraBombs;
        [SerializeField, Foldout("Bombs info")] float[] extraBombRange;
        
        [SerializeField, Foldout("Bombs info")] Transform extraBombsContainer;

        [SerializeField, Foldout("Range")] SpriteRenderer range;
        [SerializeField, Foldout("Range")] Transform target;
        [SerializeField, Foldout("Range")] AnimationCurve fadeCurve,targetCurve;
        [SerializeField, Foldout("Range")] float fadeDuration,targetDuration;
        [SerializeField, Foldout("Range")] float radius;

        [Button]
        void GenerateExtraBombs()
        {
            if (numberOfExtraBombs == 0)
                return;

            extraBombs.Clear();

            GameObject template = Instantiate(extraBombsContainer.GetChild(0).gameObject);
            while (extraBombsContainer.childCount > 0)
                DestroyImmediate(extraBombsContainer.GetChild(0).gameObject);

            for (int i=0;i<numberOfExtraBombs;i++)
            {
                GameObject clone = Instantiate(template, extraBombsContainer);
                clone.name = "Bomb (" + i.ToString() + ")";
                clone.GetComponent<PoisonBomb>().Spell = this;
                extraBombs.Add(clone.GetComponent<PoisonBomb>());   
            }

            DestroyImmediate(template);
        }
        public override void Cast()
        {
            firstBomb.gameObject.SetActive(true);
            float bombMovingTime = Random.Range(bombDuration[0], bombDuration[1]);
            firstBomb.BombAnimation(bombRotation,bombHeight, bombMovingTime, poisonScale*1.5f, poisonDuration);
            firstBomb.transform.DOMove(target.position - transform.position, bombMovingTime).SetEase(Ease.Linear).OnComplete(() =>
            {
                CameraEffects.Instance.ShakeOnce(0.5f, 2, camera: Camera.main);
                foreach (PoisonBomb extraBomb in extraBombs)
                {
                    float range = Random.Range(extraBombRange[0], extraBombRange[1]);
                    float angle = Random.Range(0, 360f);
                    Vector2 target = new Vector2(range*Mathf.Cos(angle*Mathf.Deg2Rad),range*Mathf.Sin(angle*Mathf.Deg2Rad));
                    bombMovingTime = Random.Range(bombDuration[0], bombDuration[1]);
                    extraBomb.gameObject.SetActive(true);
                    extraBomb.BombAnimation(bombRotation,bombHeight, bombMovingTime, poisonScale, poisonDuration);
                    extraBomb.transform.DOLocalMove(target, bombMovingTime).SetEase(Ease.Linear);
                }
            });
            StartCoroutine(Cor_EndSpell());
        }

        public override IEnumerator Cor_EndSpell()
        {
            yield return new WaitUntil(() => extraBombs.Count == 0);
            Destroy(gameObject);
        }

        public override void HandleRange()
        {
            range.DOFade(1, fadeDuration).SetEase(fadeCurve).SetLoops(-1);
            target.DOScale(1,targetDuration).SetEase(targetCurve).SetLoops(-1);
        }
        public override void HandleUpdateRange()
        {
            Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            if (Vector2.Distance(mousePos, rangeField.position) > radius)
            {
                float angle = Vector2.Angle(mousePos - (Vector2)rangeField.position, Vector2.right); ;
                if (mousePos.y < rangeField.position.y)
                    angle = 0 - angle;
                target.localPosition = new Vector3(radius * Mathf.Cos(angle * Mathf.Deg2Rad), radius * Mathf.Sin(angle * Mathf.Deg2Rad));
                return;
            }
            target.position = mousePos;
        }
    }
}