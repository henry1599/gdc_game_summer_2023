using DG.Tweening;
using GDC.Managers;
using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

namespace GDC.PlayerManager.Spell
{
    
    public class PoisonBomb : MonoBehaviour
    {
        [SerializeField] Transform poison;
        [SerializeField] SpriteRenderer bombSpriteContainer;
        [SerializeField] SpriteRenderer shadow;
        [SerializeField] AnimationCurve bombOrbit;
        [SerializeField,HideInInspector]PoisonBombs spell;
        public PoisonBombs Spell
        {
            get => spell; 
            set => spell = value;
        }

        public void BombAnimation(float bombRotation,float bombHeight,float bombDuration, float poisonScale, float poisonDuration)
        {
            bombSpriteContainer.transform.DOLocalMoveY(bombHeight, bombDuration).SetEase(bombOrbit);
            shadow.transform.DOLocalRotate(new Vector3(0, 0, bombRotation), bombDuration, RotateMode.FastBeyond360).SetEase(Ease.Linear);
            bombSpriteContainer.transform.DOLocalRotate(new Vector3(0, 0, bombRotation), bombDuration,RotateMode.FastBeyond360).SetEase(Ease.Linear).OnComplete(() =>
            {
                SpawnPoison(poisonScale, poisonDuration);
            });
        }
        void SpawnPoison(float poisonScale,float poisonDuration)
        {
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_BOMB);
            shadow.color = Color.clear;
            bombSpriteContainer.color = Color.clear;
            poison.gameObject.SetActive(true);
            poison.DOScale(poisonScale, poisonDuration * 0.8f).OnComplete(() =>
            {
                poison.GetComponent<SpriteRenderer>().DOFade(0, poisonDuration * 0.2f).OnComplete(()=>
                {
                    poison.gameObject.SetActive(false);
                    spell.ExtraBombs.Remove(this);
                });
            });
        }
    }
}
