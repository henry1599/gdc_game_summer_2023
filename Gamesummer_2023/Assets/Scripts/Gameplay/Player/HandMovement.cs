using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.PlayerManager;
using GDC.PlayerManager.Attack;
using GDC.Enums;
using UnityEngine.UI;
using static UnityEngine.Rendering.DebugUI;
using GDC.Objects;
using Unity.VisualScripting.Dependencies.NCalc;
using GDC.Managers;

namespace GDC.PlayerManager.Hand
{

    
    public class HandMovement : MonoBehaviour
    {
        public static HandMovement Instance { get; private set; }
        [SerializeField] Animator anim;
        [Header("Weapon")]
        [SerializeField] Transform weapon;
        public Sprite Weapon
        {
            get => weapon.GetComponent<SpriteRenderer>().sprite;
        }
        [SerializeField] WeaponType typeOfWeapon;
        [SerializeField] Sprite treeBranch;
        public void ChangeWeapon(WeaponType type = WeaponType.SWORD, Sprite weaponSprite = null)
        {
            if (weaponSprite == null)
            {
                typeOfWeapon = WeaponType.SWORD;
                radiusOfHand = 0.3125f;
                weapon.GetComponent<SpriteRenderer>().sprite = treeBranch;
                return;
            }

            weapon.GetComponent<SpriteRenderer>().sprite = weaponSprite;
            if (type == WeaponType.BOW)
            {
                radiusOfHand = 0.46875f;
            }
            else
            {
                radiusOfHand = 0.3125f;
            }

            typeOfWeapon = type;
        }    
        [Header("Hand")]
        [SerializeField] float centerOfHand;
        public float CenterOfHand { get => centerOfHand; }
        [SerializeField] float radiusOfHand;
        [Header("Attack")]
        [SerializeField] float currentAngleOfHand;
        [SerializeField] float angleSlash; //used for sword anim
        [SerializeField] float radiusReduction; //used for bow anim
        [SerializeField] float timeToStartCharging;
        [SerializeField] float timeCharging;
        float timeStartAttacking;
        bool isCharging = false;
        [SerializeField] Slider chargingSlider;

        [SerializeField] GameObject swordCollider, swordTrail;
        [Header("Arrow")]
        [SerializeField] Transform normalArrows;
        [SerializeField] Transform strongArrows;
        public List<GameObject> normalArrow;
        public List<GameObject> strongArrow;

        [SerializeField] float normalArrowSpeed, normalArrowRange,normalArrowDamage;
        [SerializeField] float strongArrowSpeed, strongArrowRange, strongArrowDamage;

        bool startAttack = false;
        bool enableWhenStopTime = false;
        public bool EnableWhenStopTime
        {
            get => enableWhenStopTime;
            set 
            {
                if (value)
                    anim.updateMode = AnimatorUpdateMode.UnscaledTime;
                else
                    anim.updateMode = AnimatorUpdateMode.Normal;
                enableWhenStopTime = value;
            }
        }


        private void Awake()
        {
            //if (Instance != null)
            //{
            //    Destroy(gameObject);
            //}
            //else
                Instance = this;

            normalArrow= new List<GameObject>();
            strongArrow= new List<GameObject>();

            for (int i = 0; i < normalArrows.childCount; i++)
            {
                GameObject arrow = normalArrows.GetChild(i).gameObject;
                Arrow arrowScript = arrow.GetComponent<Arrow>();
                arrowScript.ArrowSpeed = normalArrowSpeed;
                arrowScript.Range = normalArrowRange;
                //arrowScript.Damage = normalArrowDamage;
                arrowScript.Type = ArrowType.NORMAL;
                normalArrow.Add(arrow);
            }

            for (int i = 0; i < strongArrows.childCount; i++)
            {
                GameObject arrow = strongArrows.GetChild(i).gameObject;
                Arrow arrowScript = arrow.GetComponent<Arrow>();
                arrowScript.ArrowSpeed = strongArrowSpeed;
                arrowScript.Range = strongArrowRange;
                //arrowScript.Damage = strongArrowDamage;
                arrowScript.Type = ArrowType.STRONG;
                strongArrow.Add(arrow);
            }

        }
        // Start is called before the first frame update
        void Start()
        {
            chargingSlider?.gameObject.SetActive(false);
        }

        // Update is called once per frame
        void Update()
        {
            if (!gameObject.activeInHierarchy)
                return;
            HandleMoveHand();
            Attack();
        }

        public void StopAttack()
        {
            if (!gameObject.activeInHierarchy)
                return;
            anim.Play("normalHand");
            // sword
            swordCollider.SetActive(false);
            swordTrail.SetActive(false);
            //bow
        }    

        bool DetectAttack()
        {
            var currentState = anim.GetCurrentAnimatorStateInfo(0);
            if (currentState.IsName("slash") || currentState.IsName("shoot") || currentState.IsName("spinSlash") || currentState.IsName("strongShoot"))
                return true;

            return false;
        }
        public void HandleMoveHand()
        {
            if (!enableWhenStopTime && Time.timeScale == 0)
                return;
            Player player = Player.Instance;
            Vector2 playerPos = player.gameObject.transform.position;
            Vector2 centerPos = new Vector2(playerPos.x, playerPos.y + centerOfHand);
            Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            if (!DetectAttack())
            {
                currentAngleOfHand = Vector2.Angle(mousePos - centerPos, Vector2.down);
                if (mousePos.x > centerPos.x)
                    player.graphicOfPlayer.transform.localScale = new Vector3(1, 1, 1);
                else
                    player.graphicOfPlayer.transform.localScale = new Vector3(-1, 1, 1);

            }
            float totalAngle = currentAngleOfHand + angleSlash;
            transform.localRotation = Quaternion.Euler(0, 0, totalAngle);

            float totalRadius = radiusOfHand - radiusReduction;
            transform.localPosition = new Vector2(
                totalRadius * Mathf.Sin(totalAngle * Mathf.Deg2Rad),
                centerOfHand - totalRadius * Mathf.Cos(totalAngle * Mathf.Deg2Rad));

        }

        void Attack()
        {
            if (enableWhenStopTime && Time.timeScale == 0)
                return;
            if (Input.GetMouseButtonDown(0))
            {
                timeStartAttacking = Time.time;
                startAttack = true;
                
            }

            if (Input.GetMouseButton(0)&&startAttack)
            {
                if (Time.time - timeStartAttacking >= timeToStartCharging)
                {
                    if (!isCharging)
                    {
                        chargingSlider.gameObject.SetActive(true);
                        isCharging = true;
                        SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_CHARGE);
                    }    
                    chargingSlider.value = (Time.time - timeStartAttacking - timeToStartCharging) / timeCharging;
                }
            }

            if (Input.GetMouseButtonUp(0)&&startAttack)
            {
                startAttack = false;
                if (Time.time - timeStartAttacking < timeToStartCharging + timeCharging)
                {
                    if (!anim.GetCurrentAnimatorStateInfo(0).IsName("slash") && typeOfWeapon == WeaponType.SWORD)
                    {
                        anim.Play("slash");
                        SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_SWORD_ATTACK);
                        
                    }
                    if (!anim.GetCurrentAnimatorStateInfo(0).IsName("shoot") && typeOfWeapon == WeaponType.BOW)
                    {
                        anim.Play("shoot");
                        SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_BOW_ATTACK);

                        GameObject arrow = normalArrow[0];
                        normalArrow.RemoveAt(0);

                        arrow.transform.position = transform.position;
                        arrow.SetActive(true);

                        float angle = currentAngleOfHand;
                        if (Player.Instance.graphicOfPlayer.transform.localScale.x < 0)
                            angle = 0 - angle;

                        
                        arrow.GetComponent<Arrow>().Angle = angle;
                        arrow.GetComponent<Arrow>().StartPoint = transform.position;
                        arrow.transform.localRotation = Quaternion.Euler(0, 0, angle);
                    }
                    SoundManager.Instance.StopSFX(AudioPlayer.SoundID.SFX_CHARGE);
                }
                else
                {

                    if (!anim.GetCurrentAnimatorStateInfo(0).IsName("spinSlash") && typeOfWeapon == WeaponType.SWORD)
                    {
                        anim.Play("spinSlash");
                        swordTrail.GetComponent<TrailRenderer>().widthMultiplier = 1.5f;
                        swordCollider.GetComponent<PlayerAttackObject>().MultiValue = 2;
                        SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_CHARGE_SWORD_ATTACK);
                    }
                    if (!anim.GetCurrentAnimatorStateInfo(0).IsName("shoot") && typeOfWeapon == WeaponType.BOW)
                    {
                        anim.Play("shoot");
                        SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_BOW_ATTACK);
                        GameObject arrow = strongArrow[0];
                        strongArrow.RemoveAt(0);

                        arrow.transform.position = transform.position;
                        arrow.SetActive(true);

                        float angle = currentAngleOfHand;
                        if (Player.Instance.graphicOfPlayer.transform.localScale.x < 0)
                            angle = 0 - angle;


                        arrow.GetComponent<Arrow>().Angle = angle;
                        arrow.transform.localRotation = Quaternion.Euler(0, 0, angle);
                    }
                    SoundManager.Instance.StopSFX(AudioPlayer.SoundID.SFX_FULL_CHARGE);
                }
                chargingSlider.gameObject.SetActive(false);
                isCharging = false;
            }
        }

        void EndSpinSlash()
        {
            swordTrail.GetComponent<TrailRenderer>().widthMultiplier = 1;
            swordCollider.GetComponent<PlayerAttackObject>().MultiValue = 1;
        }
    }
}