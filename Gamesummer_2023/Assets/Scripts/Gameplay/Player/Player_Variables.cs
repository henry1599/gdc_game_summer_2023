﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.Enums;
using DG.Tweening;
using GDC.Enemies;
using GDC.Gameplay.UI;
using GDC.PlayerManager.Hand;
using GDC.Configuration;
using NaughtyAttributes;
using GDC.Gameplay;
using GDC.PlayerManager.Spell;
using GDC.Managers;
using GDC.Pets;
using GDC.Gameplay.Effect;
using UnityEngine.SceneManagement;
using TMPro;
using AudioPlayer;

namespace GDC.PlayerManager
{
    public partial class Player
    {
        public static Player Instance { get; private set; }

        [SerializeField] GameObject backHand;
        [Header("Animations")]
        public Animator graphicOfPlayer;
        public Transform graphicContainTransform;
        [SerializeField] Rigidbody2D rb;
        public Collider2D coll;

        [InfoBox("Weapon luôn nằm ở cuối cùng",EInfoBoxType.Warning)]
        public List<SpriteRenderer> spriteRendererList;
        [SerializeField] Sprite nullArmor, nullShoe;

        [ReadOnly,SerializeField] public bool isOnTallGrass = false;
        bool isTeleporting = false;
        public bool IsTeleporting
        {
            set => isTeleporting = value;
        }

        int speed;
        [ShowNativeProperty]
        public int Speed
        {
            get => speed;
            set
            {
                speed = value;
            }
        }
        float speedScale = 0;
        [ShowNativeProperty]
        public int Attack
        {
            get => attack;
            set => attack = value;
        }
        int attack;
        [ShowNativeProperty]
        public int Defense
        {
            get => defense;
            set => defense = value;
        }
        int defense;
        [ShowNativeProperty]
        public int MaxHealth
        {
            get => maxHealth;
            set => maxHealth = value;
        }
        int maxHealth;
        [ShowNativeProperty]
        public float CurrentHealth
        {
            get => currentHealth;
            set
            {
                currentHealth = value;
                SaveLoadManager.Instance.GameData.PlayerHp = currentHealth;
            }
        }
        [SerializeField] float currentHealth; //Chi dung de hien thi de theo doi
        [ShowNativeProperty]
        public float MaxStamina
        {
            get => maxStamina;
            set => maxStamina = value;
        }
        float maxStamina;
        [ShowNativeProperty]
        public float Stamina
        {
            get => stamina;
            set
            {
                stamina = value;
                SaveLoadManager.Instance.GameData.PlayerStamina = stamina;
            }
        }
        [SerializeField] float stamina; //Chi dung de hien thi de theo doi
        [SerializeField] float lowHealthRate;

        [SerializeField] LevelConfig levelSystem;
        

        [ShowNativeProperty]
        public int Level
        {
            get => level;
            set
            {
                level = value;
                SaveLoadManager.Instance.GameData.PlayerLevel = level;
            }
        }
        int level;
        [ShowNativeProperty]
        public int Exp
        {
            get => exp;
            set
            {
                exp = value;
                if (exp >= levelSystem.ExpToNextLevel(level))
                {
                    exp -= levelSystem.ExpToNextLevel(level);
                    level++;
                    LevelUp();
                    SliderManager.Instance.ExpText.text = "Lv" + level.ToString();
                }
                SaveLoadManager.Instance.GameData.PlayerExp = exp;
            }
        }
        int exp;
        [ShowNativeProperty]
        public int HpBonus
        {
            get => hpBonus;
            set
            {
                hpBonus = value;
                SaveLoadManager.Instance.GameData.HpBonus = hpBonus;
                maxHealth = InitMaxHealth + hpBonus;
            }
        }
        int hpBonus;
        [ShowNativeProperty]
        public int AtkBonus
        {
            get => atkBonus;
            set
            { 
                atkBonus = value;
                SaveLoadManager.Instance.GameData.AtkBonus = atkBonus;
                attack = (int)(InitAttack * buffAttackValue + equipAttack + atkBonus);
            }
        }
        int atkBonus;
        [ShowNativeProperty]
        public int DefBonus
        {
            get => defBonus;
            set
            {
                defBonus = value;
                SaveLoadManager.Instance.GameData.DefBonus = defBonus;
                defense = (int)(InitDefense * buffDefenseValue + equipDefense + defBonus);
            }
        }
        int defBonus;
        [ShowNativeProperty]
        public int StamBonus
        {
            get => stamBonus;
            set
            {
                stamBonus = value;
                SaveLoadManager.Instance.GameData.StamBonus = stamBonus;
                maxStamina = InitMaxStamina + stamBonus;
            }
        }
        int stamBonus;

        [HideInInspector]
        public int InitSpeed, BaseSpeed, InitAttack, InitDefense, InitMaxHealth;
        [HideInInspector]
        public float InitMaxStamina;
        float buffAttackValue = 1, buffDefenseValue = 1, buffSpeedValue = 1;
        [SerializeField] int equipSpeed;
        [ShowNativeProperty]
        public int EquipSpeed
        {
            get => equipSpeed;
            set
            {
                equipSpeed = value;
                speed = (int)(InitSpeed * buffSpeedValue + equipSpeed);
            }
        }
        [SerializeField] int equipAttack;
        [ShowNativeProperty]
        public int EquipAttack
        {
            get => equipAttack;
            set
            {
                equipAttack = value;
                attack = (int)(InitAttack * buffAttackValue + equipAttack + atkBonus);
            }
        }
        [SerializeField] int equipDefense;
        [ShowNativeProperty]
        public int EquipDefense
        {
            get => equipDefense;
            set
            {
                equipDefense = value;
                defense = (int)(InitDefense * buffDefenseValue + equipDefense + defBonus);
            }
        }
        [SerializeField] int equipMaxStamina;
        [ShowNativeProperty]
        public int EquipMaxStamina
        {
            get => equipMaxStamina;
            set
            {
                equipMaxStamina = value;
                maxStamina = (int)(InitMaxStamina + equipMaxStamina);
            }
        }
        private Dictionary<ePlayerEffectStatus, GameObject> effectDictInstance;
        [SerializeField] Transform effectContainer;
        PlayerStatConfig playerStatConfig;
        private GameObject sleepEffect, dizzyCircle, slowEffect, freezeEffect, poisonEffect, buffAtackkEffect, buffDefenseEffect, buffSpeedEffect, healEffect;
        Coroutine freezeCor, sleepCor, slowCor, poisonCor, buffAtkCor, buffDefCor, buffSpeCor;
        [Foldout("Effects")]
        [SerializeField] private bool isSleep, isSlow, isFreeze, isPoison, isBuffAtk, isBuffDef, isBuffSpe, isPreventSleep, isPreventPoison, isPreventSlow, isPreventFreeze;
        [Foldout("Effects")]
        public bool isCanMove, isCanAttack;
        [SerializeField, Foldout("Effects")] Animator levelUpEffect;
        bool isStartHurt, isDead;
        [HideInInspector] public bool isCanGetHit;
        [SerializeField] ObtainEffect obtainEffect;

        [SerializeField] Transform damageTextPopup;
        [SerializeField, ReadOnly, Foldout("Dash Info")] float dashDistance;
        [SerializeField, Foldout("Dash Info")] int numberOfClone = 3;
        [SerializeField, Foldout("Dash Info")] float fadeDuration;
        [SerializeField, Foldout("Dash Info")] float timeBetweenClone;
        [SerializeField, Foldout("Dash Info")] Transform dashCloneContainer;    
        Coroutine dashCloneCor;
        bool isDashing = false;
        Vector2 dashDirection;
        Coroutine dashCor,endDashCor;

        [SerializeField, Foldout("Dash Info")] DashAttack dashAttackHand;
        [SerializeField, Foldout("Dash Info")] float dashCost;
        [Header("Equipment")]
        public SO_Item HelmetItem;
        public SO_Item ArmorItem, WeaponItem, ShoeItem;

        [SerializeField, Foldout("Pick up pet")] float pickUpRadius;
        [SerializeField, Foldout("Pick up pet")] LayerMask petGrassMask;
        [SerializeField, Foldout("Pick up pet"), ReadOnly] PetGrass currentGrass = null;
        [SerializeField, Foldout("Pick up pet")] SceneType petScene;
        [SerializeField, Foldout("Pick up pet")] LayerMask petMask;
        [SerializeField, Foldout("Pick up pet"), ReadOnly] Pet currentPet = null;
        [SerializeField, Foldout("Cutscene")] GameObject cutSceneWeapon;
        [SerializeField, Foldout("Cutscene"), OnValueChanged("TestForCutscene")] bool inCutScene = false;
        [SerializeField, Foldout("Cutscene"), OnValueChanged("TestForCutscene")] bool useWeapon = false;
        public bool UseWeapon
        {
            get => useWeapon;
            set
            {
                if (value)
                {
                    cutSceneWeapon.GetComponent<SpriteRenderer>().sprite = HandMovement.Instance.Weapon;
                    cutSceneWeapon.SetActive(true);
                }
                else
                {
                    cutSceneWeapon.SetActive(false);
                }
                useWeapon = value;
            }
        }
        public bool InMinigame = false;

        public bool InCutScene
        {
            get => inCutScene;
            set
            {
                inCutScene = value;
                if (value)
                {
                    PlayerSpell.Instance.enabled = false;
                    if (PetManager.Instance.CurrentPet != null)
                        PetManager.Instance.CurrentPet.Idle();
                    return;
                }
                PlayerSpell.Instance.enabled = true;
                if (PetManager.Instance.CurrentPet!=null)
                    PetManager.Instance.CurrentPet.EndIdle();
            }
        }
        [SerializeField, Foldout("Cutscene"), OnValueChanged("TestForCutscene")] bool useHandMovement = false;
        public bool UseHandMovement
        {
            get => useHandMovement;
            set
            {
                if (value)
                {
                    graphicOfPlayer.SetBool("useWeapon", true);
                    graphicOfPlayer.Play("idle");
                }
                else
                {
                    graphicOfPlayer.SetBool("useWeapon", false);
                    graphicOfPlayer.Play("idle(noWeapon)");
                }
                useHandMovement = value;
            }
        }
        [SerializeField,Foldout("Mana")] Transform textContainer;
        [SerializeField, Foldout("Mana")] List<TMP_Text> textList = new List<TMP_Text>();
        [SerializeField, Foldout("Mana"),OnValueChanged("GenerateText")] int numberOfText;

        [SerializeField, Foldout("Footstep"), NaughtyAttributes.Tag] string landTag, grassTag, rockTag, iceTag;
        SoundID currentFootstepSound = SoundID.SFX_LAND_FOOTSTEP;
    }
}
