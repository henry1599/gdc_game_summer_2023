//using GDC.Enemies;
//using GDC.PlayerManager.Spell;
//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//using GDC.Gameplay.UI;

//public class PlayerAbsorb : MonoBehaviour
//{
//    [Header("Absorb")]
//    [SerializeField] float absorbRadius;
//    [SerializeField] float absorbSpeed;
//    [SerializeField] bool isAbsorbing;

//    // Update is called once per frame
//    void Update()
//    {
//        if (Input.GetKeyDown(KeyCode.X)) isAbsorbing = true;
//        if (Input.GetKeyUp(KeyCode.X)) isAbsorbing = false;
//        if (Input.GetKey(KeyCode.X)) AbsorbMonster(); //just for test
//    }
//    void AbsorbMonster()
//    {
//        Collider2D[] targetColls = Physics2D.OverlapCircleAll(transform.position, absorbRadius);      
//        if (targetColls != null)
//        {
//            foreach (var targetColl in targetColls)
//            {
//                if (targetColl.tag == "Enemy")
//                {
//                    Debug.Log("Detect" + targetColl.gameObject.name + "to absorb!");
//                    Enemy enemyScr = targetColl.GetComponent<Enemy>();
//                    if (enemyScr.Hp > 0) return;
//                    targetColl.transform.position = Vector3.MoveTowards(targetColl.transform.position, this.transform.position, absorbSpeed * Time.deltaTime);
//                }
//            }
//        }
//    }

//    private void OnCollisionEnter2D(Collision2D collision)
//    {
//        if (collision.transform.CompareTag("Enemy"))
//        {
//            Enemy enemyScr = collision.transform.GetComponent<Enemy>();
//            if (enemyScr != null)
//            {
//                if (enemyScr.Hp <= 0 && isAbsorbing)
//                {
//                    PlayerSpell.Instance.CurrentSpellType = enemyScr.spellType;
//                    CurrentSpellDisplay.Instance.Show();
//                    enemyScr.Disapear();
//                }
//            }
//        }
//    }

//    //private void OnDrawGizmos()
//    //{
//    //    Gizmos.DrawSphere(transform.position, absorbRadius);
//    //}
//}
