using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.Enums;
using DG.Tweening;
using GDC.Enemies;
using GDC.Gameplay.UI;
using GDC.PlayerManager.Hand;
using GDC.Configuration;
using NaughtyAttributes;
using GDC.Gameplay;
using GDC.PlayerManager.Spell;
using GDC.Managers;
using GDC.Pets;
using GDC.Gameplay.Effect;
using UnityEngine.SceneManagement;
using TMPro;

namespace GDC.PlayerManager
{
    public partial class Player
    {
        IEnumerator Cor_SpawnCloneDash(float speedUp = 1)
        {
            for (int i = 0; i < numberOfClone; i++)
            {
                GameObject clone = new GameObject("Clone (" + i.ToString() + ")");
                clone.transform.parent = dashCloneContainer;
                clone.transform.position = transform.position;
                clone.transform.localScale = graphicOfPlayer.transform.localScale;
                foreach (SpriteRenderer sprite in spriteRendererList)
                {
                    if (sprite == spriteRendererList[^1])
                        break;
                    SpriteRenderer cloneSprite = Instantiate(sprite, clone.transform);
                    cloneSprite.transform.position = sprite.transform.position;
                    cloneSprite.DOColor(Color.clear, fadeDuration).OnComplete(() => { Destroy(clone); });
                    for (int j = 0;j<cloneSprite.transform.childCount;j++)
                    {
                        cloneSprite.transform.GetChild(j).GetComponent<SpriteRenderer>().DOColor(Color.clear, fadeDuration);
                    }
                }

                yield return new WaitForSeconds(timeBetweenClone/speedUp);
            }
        }

        IEnumerator Cor_Dash(Vector2 velocity, float time = 2 / 3f, bool dashAttack = false )
        {
            if (dashAttack)
                isCanGetHit = false;
            rb.velocity = velocity/time;
            yield return new WaitForSeconds(time);
            rb.velocity = Vector2.zero;
            isDashing = false;
            if (dashAttack)
            {
                yield return new WaitForSeconds(0.5f);
                isCanGetHit = true;
            }
        }

        void Dash()
        {
            if (isOnTallGrass || inCutScene || !SaveLoadManager.Instance.GameData.isPlayerHaveDash)
                return;

            if (Input.GetKeyDown(KeyCode.LeftAlt) && Time.timeScale > 0)
            {
                graphicOfPlayer.Play("dash");
                rb.drag = 0;
                var horizontalDirection = Input.GetAxisRaw("Horizontal");
                var verticalDirection = Input.GetAxisRaw("Vertical");

                dashCloneCor = StartCoroutine(Cor_SpawnCloneDash());

                if (horizontalDirection == 0 && verticalDirection == 0)
                {
                    dashDirection = new Vector2((graphicOfPlayer.transform.localScale.x >= 0) ? dashDistance : -dashDistance, 0);
                    
                }
                else
                {
                    Vector2 direction = new Vector2(horizontalDirection, verticalDirection).normalized;
                    dashDirection = new Vector2(dashDistance * direction.x, dashDistance * direction.y);
                }
                dashCor = StartCoroutine(Cor_Dash(dashDirection));
                isDashing = true;
                SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_DASH);
                endDashCor = StartCoroutine(Cor_EndDash(2 / 3f));
                //dashTween = transform.DOMove((Vector2)transform.position + dashDirection, dashDuration);
                //dashTween.Play();

            }
        }

        IEnumerator Cor_EndDash(float duration)
        {
            yield return new WaitForSeconds(duration);
            EndDash();
        }

        void EndDash()
        {
            StopCoroutine(endDashCor);
            isDashing = false;
            StopCoroutine(dashCor);
            rb.velocity = Vector2.zero;
            StopCoroutine(dashCloneCor);
            rb.drag = 4;
            if (useWeapon)
                graphicOfPlayer.Play("idle");
            else
                graphicOfPlayer.Play("idle(noWeapon)");
        }

        void DashAttack()
        {
            if (WeaponItem.weaponType != WeaponType.SWORD||
                graphicOfPlayer.GetCurrentAnimatorStateInfo(0).IsName("dashAttack"))
                return;
            if (stamina < dashCost)
            {
                SpawnManaText();
                return;
            }
            StopCoroutine(dashCor);
            StopCoroutine(dashCloneCor);
            Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector2 centerPos = transform.position + new Vector3(0, 0.3125f);

            float angle = Vector2.Angle(mousePos - centerPos, Vector2.right);
            dashDirection = new Vector2(
                dashDistance * Mathf.Cos(angle * Mathf.Deg2Rad * ((mousePos.y < centerPos.y) ? -1 : 1)),
                dashDistance * Mathf.Sin(angle * Mathf.Deg2Rad * ((mousePos.y < centerPos.y) ? -1 : 1)));
            dashCor = StartCoroutine(Cor_Dash(dashDirection,1/3f,true));
            dashCloneCor = StartCoroutine(Cor_SpawnCloneDash(2));

            if (mousePos.x < centerPos.x)
                angle = 180 - angle;
            if (mousePos.y < centerPos.y)
                angle = 0 - angle;

            dashAttackHand.transform.localRotation = Quaternion.Euler(0, 0, angle/*((mousePos.x < centerPos.x)?(180-angle):angle) * ((mousePos.y < centerPos.y)? -1:1)*/);
            dashAttackHand.transform.localPosition = new Vector2(
                0.1875f * Mathf.Cos(angle * Mathf.Deg2Rad),
                0.3125f + 0.1875f * Mathf.Sin(angle * Mathf.Deg2Rad));
            dashAttackHand.Attack();
            graphicOfPlayer.transform.localScale = new Vector3((mousePos.x < centerPos.x)?-1:1, 1, 1);
            graphicOfPlayer.Play("dashAttack");
            endDashCor = StartCoroutine(Cor_EndDash(1 / 3f));
            SliderManager.Instance.DamageEffect(false, true);
            stamina -= dashCost;
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_DASH);
        }
    }
}
