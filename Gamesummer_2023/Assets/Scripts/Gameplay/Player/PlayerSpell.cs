using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.Managers;
using GDC.Configuration;
using GDC.Enums;
using NaughtyAttributes;
using GDC.PlayerManager.Hand;
using RotaryHeart.Lib.SerializableDictionary;
using DG.Tweening;
using GDC.Enemies;
using GDC.Gameplay.UI;
using TMPro;
using UnityEngine.UI;
using AudioPlayer;

namespace GDC.PlayerManager.Spell
{
    [System.Serializable]
    public class SpellPrefabByType : SerializableDictionaryBase<SpellType, Spell>{}
    public class PlayerSpell : MonoBehaviour
    {
        public static PlayerSpell Instance { get; private set; }
        #region spell info
        //Phan nay chi de nhin tren inspector
        [SerializeField,OnValueChanged("TestSpell")] SpellType currentSpellType;
        public SpellType CurrentSpellType 
        { 
            get => currentSpellType;
            set => currentSpellType = value;
        }
        

        //[SerializeField] private float level;
        [SerializeField,Foldout("Spell Info")] private float spellAtk;
        [SerializeField, Foldout("Spell Info")] private float staminaUsed;
        [SerializeField, Foldout("Spell Info")] private float spellEffectDuration;
        [SerializeField, Foldout("Spell Info")] Transform spellContainer;
        [SerializeField, Foldout("Spell Info"), ReadOnly] Spell currentSpell = null; 

        [SerializeField] GameObject castSpellAnimation;
        [Foldout("Spells"), SerializeField] SpellPrefabByType spellPrefabDict;
        //Dictionary<SpellType, Vector2> spellSpawmOffsetPositionDict;
        //IEnumerator Cor_GetSpellSpawnPosition()
        //{
        //    yield return new WaitUntil(() => HandMovement.Instance != null);
        //    this.spellSpawmOffsetPositionDict = new()
        //    {
        //        {SpellType.SPIKE_DART, new Vector2(0f, HandMovement.Instance.CenterOfHand)},
        //        {SpellType.POISON_BOMB, new Vector2(0f, HandMovement.Instance.CenterOfHand)},
        //        {SpellType.FIRE_BREATH, new Vector2(0f, HandMovement.Instance.CenterOfHand)},
        //        {SpellType.ICE_SPIT, new Vector2(0f, 0f)},
        //        {SpellType.SPIN_ATTACK, new Vector2(0f, 0f)},
        //        {SpellType.THUNDER_SHOCK,new Vector2(0f, 0f)},
        //        {SpellType.ENERGY_BALL,new Vector2(0f, 0f)},
        //        {SpellType.HYPER_BEAM, new Vector2(0f, HandMovement.Instance.CenterOfHand)},
        //        {SpellType.HYPNOSIS,new Vector2(0f, 0f)},
        //        {SpellType.TELEPORT,new Vector2(0f, 0f)},
        //        {SpellType.ATK_BUFF,new Vector2(0f, 0f)},
        //        {SpellType.DEF_BUFF,new Vector2(0f, 0f)},
        //        {SpellType.SPE_BUFF,new Vector2(0f, 0f)},
        //    };
        //}

        #endregion

        #region absorb spell
        [SerializeField,Foldout("Absorb")] float absorbRadius;
        [SerializeField, Foldout("Absorb")] float absorbDuration;
        [SerializeField, Foldout("Absorb")] LayerMask enemyMask;
        [SerializeField, Foldout("Absorb")] AnimationCurve absorbCurve;
        [SerializeField, Foldout("Absorb")] ParticleSystem vfxDust, vfxInhale;
        [SerializeField, Foldout("Absorb"),OnValueChanged("GenerateText")] int numberOfText = 1;
        [SerializeField, Foldout("Absorb")] Transform textContainer;
        [SerializeField, Foldout("Absorb")] List<Image> absorbText = new List<Image>();
        [SerializeField, Foldout("Absorb")] Vector2 absorbBookPos;
        [SerializeField, Foldout("Absorb")] float zoomOrtho,originOrtho;
        Tween absorbTween;
        bool isAbsorbing = false;
        Enemy absorbEnemy = null;
        Boss absorbBoss = null;

        void GenerateText()
        {
            if (numberOfText < 1)
                return;

            GameObject template = textContainer.GetChild(0).gameObject;

            absorbText.Clear();
            while (textContainer.childCount > 1)
                DestroyImmediate(textContainer.GetChild(1).gameObject);

            absorbText.Add(template.GetComponent<Image>());
            for (int i=1; i < numberOfText; i++)
            {
                GameObject clone = Instantiate(template, textContainer);
                clone.name = "Absorb Text " + i.ToString();
                absorbText.Add(clone.GetComponent<Image>());
            }
        }

        void SpawnText()
        {
            if (absorbText.Count==0)
            {
                //print("Not enough text");
                return;
            }
            SoundManager.Instance.PlaySound(SoundID.SFX_ERROR);
            Image text = absorbText[0];
            text.color = new Color(text.color.r, text.color.g, text.color.b, 1);
            absorbText.RemoveAt(0);

            text.DOFade(0, 0.5f);
            text.GetComponent<RectTransform>().DOAnchorPosY(80, 0.5f).OnComplete(() =>
            {
                text.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 50);
                absorbText.Add(text);
            });
        }    

        void AbsorbSpell()
        {
            Collider2D[] enemies = Physics2D.OverlapCircleAll(transform.position, absorbRadius,enemyMask);
            if (enemies.Length == 0)
            {
                SpawnText();
                return;
            }

            Enemy target = null;
            float range = absorbRadius;

            Vector2 playerPos = transform.position;
            foreach (Collider2D enemyCol in enemies)
            {
                if (enemyCol.GetComponent<Boss>()!=null)
                {
                    absorbBoss = enemyCol.GetComponent<Boss>();
                    if (absorbBoss.Hp > 0) 
                    {
                        absorbBoss = null;
                        break;
                    }
                    HandleAbsorb();
                    break;
                }
                else
                {
                    Enemy enemy = enemyCol.GetComponent<Enemy>() ?? enemyCol.GetComponentInParent<Enemy>();
                    if (enemy.Hp > 0 || (target!=null && enemy.spellType == SpellType.NONE && target?.spellType!=SpellType.NONE))
                    {
                        continue;
                    }
                    float distance = Vector2.Distance(enemy.transform.position, playerPos);
                    if (distance<range)
                    {
                        range = distance;
                        target = enemy;
                    }
                }
            }
            if (target != null)
            {
                absorbEnemy = target;
                HandleAbsorb();
            }
            else
                SpawnText();
        }


        void HandleAbsorb()
        {
            Vector2 tempAbsorbPos = absorbBookPos;
            if (Player.Instance.transform.localScale.x < 0)
                tempAbsorbPos = new Vector2(-tempAbsorbPos.x, tempAbsorbPos.y);
            Player.Instance.graphicOfPlayer.Play("obtain");
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_ABSORB);
            vfxInhale.gameObject.SetActive(true);
            vfxInhale.transform.localScale = Vector2.zero;
            vfxInhale.transform.DOScale(1, 0.5f);
            StartCoroutine(Cor_EnableSpell(absorbDuration));
            isAbsorbing = true;
            Camera.main.DOOrthoSize(zoomOrtho, 0.25f);
            if (absorbBoss!=null)
            {
                absorbTween = absorbBoss.transform.DOMove((Vector2)transform.position + tempAbsorbPos, absorbDuration).SetEase(absorbCurve).OnComplete(() =>
                {
                    currentSpellType = absorbBoss.spellType;
                    SaveLoadManager.Instance.GameData.CurrentSpell = currentSpellType;
                    SaveLoadManager.Instance.GameData.SetTrueSpell(currentSpellType);
                    if (currentSpellType != SpellType.NONE)
                        vfxDust.Play();
                    CurrentSpellDisplay.Instance.Show();
                    absorbBoss.Disappear();
                    Player.Instance.Heal(0, (int)absorbBoss.staminaGain);
                    PrepareSpell();
                    isAbsorbing = false;
                    vfxInhale.transform.DOScale(0, 0.5f).OnComplete(() => vfxInhale.gameObject.SetActive(false));
                    Camera.main.DOOrthoSize(originOrtho, 0.25f);
                    SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_GET_SPELL);
                });
                absorbTween.Play();
                absorbBoss.transform.DOScale(0, absorbDuration).SetEase(absorbCurve);
            }
            else
            {
                absorbTween = absorbEnemy.transform.DOMove((Vector2)transform.position + tempAbsorbPos, absorbDuration).SetEase(absorbCurve).OnComplete(() =>
                {
                    currentSpellType = absorbEnemy.spellType;
                    SaveLoadManager.Instance.GameData.CurrentSpell = currentSpellType;
                    SaveLoadManager.Instance.GameData.SetTrueSpell(currentSpellType);
                    if (currentSpellType != SpellType.NONE)
                        vfxDust.Play();
                    CurrentSpellDisplay.Instance.Show();
                    absorbEnemy.Disappear();
                    Player.Instance.Heal(0, (int)absorbEnemy.staminaGain);
                    PrepareSpell();
                    isAbsorbing = false;
                    vfxInhale.transform.DOScale(0, 0.5f).OnComplete(() => vfxInhale.gameObject.SetActive(false));
                    Camera.main.DOOrthoSize(originOrtho, 0.25f);
                    SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_GET_SPELL);
                });
                absorbTween.Play();
                absorbEnemy.transform.DOScale(0, absorbDuration).SetEase(absorbCurve);
            }
        }
        void CheckAbsorb()
        {
            if (absorbBoss!=null && absorbBoss.gameObject.activeInHierarchy)
                return;
            if (absorbEnemy != null && absorbEnemy.gameObject.activeInHierarchy)
                return;
            Camera.main.DOOrthoSize(originOrtho, 0.25f);
            absorbTween?.Kill();
            vfxInhale.transform.DOScale(0, 0.5f).OnComplete(() => vfxInhale.gameObject.SetActive(false));
            isAbsorbing = false;
        }

        IEnumerator Cor_EnableSpell(float duration)
        {
            yield return new WaitForSeconds(duration);
            canUseSpell = true;
            isAbsorbing = false;
        }
        #endregion

        #region cast spell
        IEnumerator castSpell = null;
        bool canUseSpell = false;
        IEnumerator Cor_CastSpell()
        {
            if (currentSpell == null)
            {
                print("You don't have spell");
                yield break;
            }
            yield return new WaitUntil(() => canUseSpell);
            yield return new WaitUntil(() => SaveLoadManager.Instance.GameData.isPlayerHaveSpell);
            yield return new WaitUntil(()=> Input.GetMouseButtonDown(1));
            currentSpell.gameObject.SetActive(true);
            yield return new WaitUntil(() => Input.GetMouseButtonUp(1));
            if (Player.Instance.Stamina < staminaUsed)
            {
                Player.Instance.SpawnManaText();
                currentSpell.gameObject.SetActive(false);
                Camera.main.DOOrthoSize(originOrtho, 0.25f);
                castSpell = Cor_CastSpell();
                StartCoroutine(castSpell);
                yield break;
            }
            currentSpell.IsCasting = true;
            SpellIntro.Instance.CastSpellIntro(currentSpell.introSprite, currentSpell.introColor);
            //yield return new WaitUntil(() => 
            //    SpellIntro.Instance.CastSpellIntro(currentSpell.introSprite, currentSpell.introColor)
            //    && !SpellIntro.Instance.IsInIntro);
            yield return new WaitUntil(() => !SpellIntro.Instance.IsInIntro);
            StartCoroutine(Cor_CastSpellAnimation());
            currentSpell.transform.position = (Vector2)transform.position;
            currentSpell.HandleCast();
            currentSpellType = SpellType.NONE;
            SaveLoadManager.Instance.GameData.CurrentSpell = SpellType.NONE;
            currentSpell = null;
            CurrentSpellDisplay.Instance.Show();
            canUseSpell = false;

            SliderManager.Instance.DamageEffect(false, true);
            Player.Instance.Stamina -= staminaUsed;
        }

        IEnumerator Cor_CastSpellAnimation()
        {
            castSpellAnimation.transform.position = transform.position;
            castSpellAnimation.SetActive(true);
            yield return new WaitForSeconds(1.5f);
            castSpellAnimation.SetActive(false);
        }
        void GetSpellData(SpellType spellType)
        {
            foreach (SO_Spell spellData in SpellManager.Instance.SO_SpellList)
            {
                if (spellData.SpellType == spellType)
                {
                    //this.level = spellData.Level;
                    this.spellAtk = spellData.SpellAtk();
                    this.staminaUsed = spellData.StaminaUsed();
                    this.spellEffectDuration = spellData.EffectDuration();
                    this.currentSpellType = spellType;

                    return;
                }
            }

            Debug.Log("Can't find spellType with: " + spellType);
        }

        void PrepareSpell()
        {
            if (currentSpellType == SpellType.NONE)
            {
                Debug.Log("This enemy didn't have spell skill");
                return;
            }
            GetSpellData(currentSpellType);
            Spell spellToCast = this.spellPrefabDict[currentSpellType];
            Spell spell = Instantiate(spellToCast, spellContainer);
            currentSpell = spell;
            currentSpell.gameObject.SetActive(false);
            castSpell = Cor_CastSpell();
            StartCoroutine(castSpell);
        }

        void TestSpell()
        {
            canUseSpell = true;
            PrepareSpell();
        }

        public void LoadCurrentSpell(SpellType spellType)
        {
            if (spellType == SpellType.NONE)
                return;

            currentSpellType = spellType;
            CurrentSpellDisplay.Instance.Show();
            canUseSpell = true;
            PrepareSpell();
        }

        public void CancelSpell()
        {
            Destroy(currentSpell.gameObject);
            StopCoroutine(castSpell);
        }
        #endregion

        [SerializeField, Foldout("Cancel Spell")] TMP_Text cancelText;
        bool cancelTextOn = false;
        public void SpawnCancelText()
        {
            if (cancelTextOn)
                return;
            cancelTextOn = true;
            cancelText.color = new Color(cancelText.color.r, cancelText.color.g, cancelText.color.b,1);
            cancelText.DOFade(0, 0.5f);
            cancelText.GetComponent<RectTransform>().DOAnchorPosY(80, 0.5f).OnComplete(() =>
            {
                cancelText.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 50);
                cancelTextOn = false;
            });
        }
        private void Awake()
        {
            //if (Instance != null)
            //    Destroy(gameObject);
            //else
                Instance = this;
        }
        private void Start()
        {
            castSpellAnimation.SetActive(false);
            StartCoroutine(Cor_SetOriginOrthoSize());
            //StartCoroutine(Cor_GetSpellSpawnPosition());
        }

        

        private void Update()
        {
            if (!SaveLoadManager.Instance.GameData.isPlayerHaveSpell)
                return;
            if (currentSpellType == SpellType.NONE && !isAbsorbing && Input.GetMouseButtonUp(1))
            {
                AbsorbSpell();
            }
            if (isAbsorbing)
                CheckAbsorb();

            if (currentSpell != null && Input.GetMouseButtonDown(0)&& !currentSpell.IsCasting)
            {
                CancelSpell();
                PrepareSpell();
            }
        }

        IEnumerator Cor_SetOriginOrthoSize()
        {
            yield return new WaitUntil(() => GameManager.Instance.isLoadSceneComplete);
            originOrtho = Camera.main.orthographicSize;
        }
    }
}
