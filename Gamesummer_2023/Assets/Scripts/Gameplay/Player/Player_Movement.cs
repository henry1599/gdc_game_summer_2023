using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.Enums;
using DG.Tweening;
using GDC.Enemies;
using GDC.Gameplay.UI;
using GDC.PlayerManager.Hand;
using GDC.Configuration;
using NaughtyAttributes;
using GDC.Gameplay;
using GDC.PlayerManager.Spell;
using GDC.Managers;
using GDC.Pets;
using GDC.Gameplay.Effect;
using UnityEngine.SceneManagement;

namespace GDC.PlayerManager
{
    public partial class Player
    {
        void Move()
        {
            var horizontalDirection = Input.GetAxisRaw("Horizontal");
            var verticalDirection = Input.GetAxisRaw("Vertical");

            if (horizontalDirection == 0 && verticalDirection == 0)
            {
                if (graphicOfPlayer != null)
                {
                    graphicOfPlayer.SetBool("isMoving", false);
                }
                return;
            }
            if (graphicOfPlayer != null)
            {
                graphicOfPlayer.SetBool("isMoving", true);
                if (BaseSpeed != 0)
                {
                    graphicOfPlayer.SetFloat("movingSpeedMultiplier", this.speed / BaseSpeed);
                }
            }
            if (!useHandMovement && horizontalDirection != 0)
                graphicOfPlayer.transform.localScale = new Vector3(horizontalDirection, 1, 1);

            Vector2 moveDistance = new Vector2(horizontalDirection, verticalDirection).normalized * Time.deltaTime * speed * speedScale;
            //rb.velocity = moveDistance;
            transform.position += (Vector3)moveDistance;
        }
        public void MoveTo(Vector2 destination, float duration)
        {
            graphicOfPlayer.transform.localScale = new Vector3((destination.x > transform.position.x) ? 1 : -1, 1, 1);
            graphicOfPlayer.SetBool("isMoving", true);
            transform.DOLocalMove(destination, duration).SetEase(Ease.Linear).OnComplete(() => graphicOfPlayer.SetBool("isMoving", false));
        }
    }
}
