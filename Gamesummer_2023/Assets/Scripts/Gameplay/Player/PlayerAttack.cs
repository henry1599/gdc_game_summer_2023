using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.PlayerManager;
using GDC.PlayerManager.Spell;
using DG.Tweening;
using GDC.Enemies;

namespace GDC.PlayerManager.Attack
{
    public class PlayerAttack : MonoBehaviour
    {
        Animator animator;

        [Header("Slash")]
        float currentAngleOfHand;
        [SerializeField] float angleSlash;

        private void Awake()
        {
            animator= GetComponent<Animator>();
        }
        private void Update()
        {
            
        }


        void Slash()
        {
            if (Input.GetMouseButtonUp(0))
            {
                animator.Play("slash");
            }
        }

    }
}