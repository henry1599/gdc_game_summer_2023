using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GDC.PlayerManager;
using GDC.Managers;

namespace GDC.PlayerManager.ChargingSlider
{
    public class ChargingSlider : MonoBehaviour
    {
        [SerializeField] Image[] fill;
        [SerializeField] Animator anim;
        [SerializeField] float evaluateValue;
        [SerializeField] Slider chargingSlider;
        [SerializeField] Gradient chargingSliderGradient;

        public void UpdateValue()
        {
            if (chargingSlider.value <= 0.95f)
            {
                anim.SetBool("rage", false);
            }
            else if (chargingSlider.value >= 1)
            {
                anim.SetBool("rage", true);
                SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_FULL_CHARGE);
            }
            
        }

        private void Update()
        {
            foreach (Image img in fill)
                img.color = chargingSliderGradient.Evaluate(evaluateValue);
        }
    }
}