using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GDC.PlayerManager
{
    public class DashAttack : MonoBehaviour
    {
        [SerializeField] SpriteRenderer dashEffect;
        [SerializeField,ShowAssetPreview] Sprite[] dashEffectSprite;
        [SerializeField] int loopTime;
        [SerializeField] float dashDuration = 2 / 3;

        IEnumerator Cor_DashEffect()
        {
            for (int loop = 0;loop<loopTime;loop++)
            {
                for (int i = 0;i< dashEffectSprite.Length; i++)
                {
                    dashEffect.sprite = dashEffectSprite[i];
                    yield return new WaitForSeconds(dashDuration / (loopTime * dashEffectSprite.Length));
                }
            }
            gameObject.SetActive(false);
        }

        public void Attack()
        {
            gameObject.SetActive(true);
            StartCoroutine(Cor_DashEffect());
        }
    }
}