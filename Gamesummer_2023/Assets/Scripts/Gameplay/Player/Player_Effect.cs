using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.Enums;
using DG.Tweening;
using GDC.Enemies;
using GDC.Gameplay.UI;
using GDC.PlayerManager.Hand;
using GDC.Configuration;
using NaughtyAttributes;
using GDC.Gameplay;
using GDC.PlayerManager.Spell;
using GDC.Managers;
using GDC.Pets;
using GDC.Gameplay.Effect;
using UnityEngine.SceneManagement;
using TMPro;
using GDC.Minigame;
using System;

namespace GDC.PlayerManager
{
    public partial class Player
    {
        public void Freeze(float duration)
        {
            if (isFreeze || isPreventFreeze)
            {
                //StopCoroutine(freezeCor);
                //Debug.Log(gameObject.name + " is freezed again");
                return;
            }
            Debug.Log(gameObject.name + " is freezed");
            graphicOfPlayer.speed = 0;
            isFreeze = true;
            //isCanMove = false;
            isCanAttack = false;
            freezeEffect.SetActive(true);
            freezeCor = StartCoroutine(Cor_EndStatus(freezeEffect, duration));
            FullScreenEffect.Instance.Freeze();
        }
        public void Sleep(float duration)
        {
            if (isFreeze)
            {
                Debug.Log(gameObject.name + " can't sleep because it is freezing!");
                return;
            }
            if (isSleep || isPreventSleep)
            {
                //StopCoroutine(sleepCor);
                //Debug.Log(gameObject.name + " is slept again");
                return;
            }
            Debug.Log(gameObject.name + " is slept");
            isSleep = true;
            graphicOfPlayer.Play("die"); //Actually, it's sleep
            //isCanMove = false;
            isCanAttack = false;
            sleepEffect.SetActive(true);
            sleepCor = StartCoroutine(Cor_EndStatus(sleepEffect, duration));
        }
        public void Slow(float duration)
        {
            if (isSlow || isPreventSlow)
            {
                //StopCoroutine(slowCor);
                //Debug.Log(gameObject.name + " is slowed again");
                return;
            }
            Debug.Log(gameObject.name + " is slowed");
            isSlow = true;
            slowEffect.SetActive(true);
            speed = InitSpeed / 2;
            slowCor = StartCoroutine(Cor_EndStatus(slowEffect, duration));
            FullScreenEffect.Instance.Slow();
        }
        public void Poison(float duration)
        {
            if (isPoison || isPreventPoison)
            {
                //StopCoroutine(poisonCor);
                //Debug.Log(gameObject.name + " is poisoned again");
                return;
            }
            Debug.Log(gameObject.name + " is poisoned");
            isPoison = true;
            poisonEffect.SetActive(true);
            poisonCor = StartCoroutine(Cor_EndStatus(poisonEffect, duration));
            StartCoroutine(Cor_DamageByPoison());
            FullScreenEffect.Instance.Poison();
        }
        IEnumerator Cor_DamageByPoison()
        {
            while(isPoison)
            {
                yield return new WaitForSeconds(2);
                Transform damagePopup = Instantiate(damageTextPopup, transform.position + Vector3.up*1.25f, Quaternion.identity);
                DamageTextPopup damagePopupScr = damagePopup.GetComponent<DamageTextPopup>();
                float damage = 0.07f * maxHealth;
                damagePopupScr.SetText((int)damage);

                this.currentHealth -= (int)damage;
                if (this.currentHealth <= 0)
                {
                    this.currentHealth = 0;
                    Die();
                }
            }
        }
        public void BuffAtk(float buffValue, float duration)
        {
            if (isBuffAtk)
            {
                StopCoroutine(buffAtkCor);
                Debug.Log(gameObject.name + " is buffed Atk again");
            }
            Debug.Log(gameObject.name + " is buffed Atk");
            buffAttackValue = buffValue;
            attack = (int)(InitAttack * buffAttackValue + equipAttack + atkBonus);
            isBuffAtk = true;
            buffAtackkEffect.SetActive(true);
            BuffEffectManager.Instance?.SetEffect(EffectType.ATK_BUFF, true);
            buffAtkCor = StartCoroutine(Cor_EndStatus(buffAtackkEffect, duration));
        }
        public void BuffDef(float buffValue, float duration)
        {
            if (isBuffDef)
            {
                StopCoroutine(buffDefCor);
                Debug.Log(gameObject.name + " is buffed Def again");
            }
            Debug.Log(gameObject.name + " is buffed Def");
            buffDefenseValue = buffValue;
            defense = (int)(InitDefense * buffDefenseValue + equipDefense + defBonus);
            isBuffDef = true;
            buffDefenseEffect.SetActive(true);
            BuffEffectManager.Instance?.SetEffect(EffectType.DEF_BUFF, true);
            buffDefCor = StartCoroutine(Cor_EndStatus(buffDefenseEffect, duration));
        }
        public void BuffSpe(float buffValue, float duration)
        {
            if (isBuffSpe)
            {
                StopCoroutine(buffSpeCor);
                Debug.Log(gameObject.name + " is buffed Spe again");
            }
            Debug.Log(gameObject.name + " is buffed Spe");
            buffSpeedValue = buffValue;
            speed = (int)(InitSpeed * buffSpeedValue + equipSpeed);
            isBuffSpe = true;
            buffSpeedEffect.SetActive(true);
            BuffEffectManager.Instance?.SetEffect(EffectType.SPE_BUFF, true);
            buffSpeCor = StartCoroutine(Cor_EndStatus(buffSpeedEffect, duration));
        }
        public void PreventEffect(EffectType effectType, float duration)
        {
            if (effectType == EffectType.SLEEP)
            {
                isPreventSleep = true;
                EndStatus(sleepEffect);
            }
            else if (effectType == EffectType.POISON)
            {
                isPreventPoison = true;
                EndStatus(poisonEffect);
            }
            else if (effectType == EffectType.SLOW)
            {
                isPreventSlow = true;
                EndStatus(slowEffect);
            }
            else if (effectType == EffectType.FREEZE)
            {
                isPreventFreeze = true;
                EndStatus(freezeEffect);
            }
            
            BuffEffectManager.Instance.SetEffect(effectType, true);
            healEffect.SetActive(true);
            healEffect.transform.GetChild(0).GetComponent<Animator>().Play("healEffect");
            StartCoroutine(Cor_EndPreventEffect(effectType, duration));
        }
        IEnumerator Cor_EndPreventEffect(EffectType effectType, float duration)
        {
            yield return new WaitForSeconds(duration);
            BuffEffectManager.Instance.SetEffect(effectType, false);
            if (effectType == EffectType.SLEEP) isPreventSleep = false;
            else if (effectType == EffectType.POISON) isPreventPoison = false;
            else if (effectType == EffectType.SLOW) isPreventSlow = false;
            else if (effectType == EffectType.FREEZE) isPreventFreeze = false;
        }
        public void Heal(int healHeartValue, int healStaminaValue, bool isHavePopupHeal = true)
        {
            healEffect.SetActive(true);
            healEffect.transform.GetChild(0).GetComponent<Animator>().Play("healEffect");
            SliderManager.Instance.HealEffect(healHeartValue > 0, healStaminaValue > 0);
            this.currentHealth += healHeartValue;
            this.stamina += healStaminaValue;
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_HEAL);

            if (currentHealth > MaxHealth) currentHealth = maxHealth;
            if (stamina > maxStamina) stamina = maxStamina;
            if (currentHealth > maxHealth * lowHealthRate)
                FullScreenEffect.Instance.EndLowHP();

            if (isHavePopupHeal)
            {
                if (healHeartValue != 0 && healStaminaValue == 0)
                {
                    Transform damagePopup = Instantiate(damageTextPopup, transform.position, Quaternion.identity);
                    DamageTextPopup damagePopupScr = damagePopup.GetComponent<DamageTextPopup>();
                    damagePopupScr.SetText(healHeartValue);
                    damagePopupScr.HealHeartEffect();
                }
                else if (healHeartValue == 0 && healStaminaValue != 0)
                {
                    Transform damagePopup = Instantiate(damageTextPopup, transform.position, Quaternion.identity);
                    DamageTextPopup damagePopupScr = damagePopup.GetComponent<DamageTextPopup>();
                    damagePopupScr.SetText(healStaminaValue);
                    damagePopupScr.HealStaminaEffect();
                }
                else if (healHeartValue != 0 && healStaminaValue != 0)
                {
                    Transform damagePopup1 = Instantiate(damageTextPopup, transform.position + new Vector3(-0.3f, 0, 0), Quaternion.identity);
                    DamageTextPopup damagePopupScr1 = damagePopup1.GetComponent<DamageTextPopup>();
                    Transform damagePopup2 = Instantiate(damageTextPopup, transform.position + new Vector3(0.3f, 0, 0), Quaternion.identity);
                    DamageTextPopup damagePopupScr2 = damagePopup2.GetComponent<DamageTextPopup>();
                    damagePopupScr1.SetText(healHeartValue);
                    damagePopupScr1.HealHeartEffect();
                    damagePopupScr2.SetText(healStaminaValue);
                    damagePopupScr2.HealStaminaEffect();
                }
            }
        }
        public void EndStatus(EffectType effectType)
        {
            if (effectType == EffectType.SLEEP)
            {
                EndStatus(sleepEffect);
            }
            else if (effectType == EffectType.FREEZE)
            {
                EndStatus(freezeEffect);
            }
            else if (effectType == EffectType.POISON)
            {
                EndStatus(poisonEffect);
            }
            else if (effectType == EffectType.SLOW)
            {
                EndStatus(slowEffect);
            }
            else if (effectType == EffectType.ATK_BUFF)
            {
                EndStatus(buffAtackkEffect);
            }
            else if (effectType == EffectType.DEF_BUFF)
            {
                EndStatus(buffDefenseEffect);
            }
            else if (effectType == EffectType.SPE_BUFF)
            {
                EndStatus(buffSpeedEffect);
            }
        }
        void EndStatus(GameObject effectObj)
        {
            effectObj.SetActive(false);
            if (effectObj == sleepEffect) { isSleep = false; /*isCanMove = true*/; isCanAttack = true; ToIdle(); }
            else if (effectObj == slowEffect)
            {
                FullScreenEffect.Instance.EndSlow();
                isSlow = false; speed = InitSpeed;
            }
            else if (effectObj == freezeEffect)
            {
                isFreeze = false; graphicOfPlayer.speed = 1; /*isCanMove = true;*/ isCanAttack = true;
                FullScreenEffect.Instance.EndFreeze();
            }
            else if (effectObj == poisonEffect)
            {
                isPoison = false;
                FullScreenEffect.Instance.EndPoison();
            }
            else if (effectObj == buffAtackkEffect)
            {
                isBuffAtk = false;
                buffAttackValue = 1;
                attack = (int)(InitAttack * buffAttackValue + equipAttack + atkBonus);
                buffAtackkEffect.SetActive(false);
                BuffEffectManager.Instance?.SetEffect(EffectType.ATK_BUFF, false);
            }
            else if (effectObj == buffDefenseEffect)
            {
                isBuffDef = false;
                buffDefenseValue = 1;
                defense = (int)(InitDefense * buffDefenseValue + equipDefense + defBonus);
                buffDefenseEffect.SetActive(false);
                BuffEffectManager.Instance?.SetEffect(EffectType.DEF_BUFF, false);
            }
            else if (effectObj == buffSpeedEffect)
            {
                isBuffSpe = false;
                buffSpeedValue = 1;
                speed = (int)(InitSpeed * buffSpeedValue + equipSpeed);
                buffSpeedEffect.SetActive(false);
                BuffEffectManager.Instance?.SetEffect(EffectType.SPE_BUFF, false);
            }
        }
        IEnumerator Cor_EndStatus(GameObject effectObj, float statusDuration = 0)
        {
            yield return new WaitForSeconds(statusDuration);
            EndStatus(effectObj);
        }
        public void SetIsCanGetHit()
        {
            isCanGetHit = false;
            StartCoroutine(Cor_RefreshGetHit(2.2f)); //Thoi gian de co the bi nhan hit lai la 2.2(s)
        }
        public void GetHit(Vector3 attackPos, float enemyAtk, float spellEffectDuration, LayerMask enemyAttackLayer)
        {
            if (isCanGetHit == false) return;

            //isCanGetHit = false;
            //StartCoroutine(Cor_RefreshGetHit(2.2f)); //Thoi gian de co the bi nhan hit lai la 2.2(s)
            SetIsCanGetHit();

            if (InMinigame)
            {
                Area1Minigame.Instance?.LostCoin();
                CameraEffects.Instance.ShakeOnce(0.5f, 3, camera: Camera.main);
                SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_HURT);
                Vector3 direct = (this.transform.position - attackPos).normalized;
                //StartCoroutine(Cor_EndStartHurt(1, direct));
                rb.velocity = direct*7;
                return;
            }

            Transform damagePopup = Instantiate(damageTextPopup, transform.position, Quaternion.identity);
            DamageTextPopup damagePopupScr = damagePopup.GetComponent<DamageTextPopup>();

            int criticalRate;
            // StartCoroutine(Cor_GetHitEffect());
            BlinkTo(new Color(1, 1, 1, 0.5f), 0.25f, 4);

            if (enemyAttackLayer == LayerMask.NameToLayer("Sleep")) Sleep(spellEffectDuration);
            else if (enemyAttackLayer == LayerMask.NameToLayer("Poison")) Poison(spellEffectDuration);
            else if (enemyAttackLayer == LayerMask.NameToLayer("Ice")) Freeze(spellEffectDuration);
            else if (enemyAttackLayer == LayerMask.NameToLayer("Electric")) Slow(spellEffectDuration);

            criticalRate = UnityEngine.Random.Range(0, 100) / 94 + 1; //co ti le 95% la critical Rate = 2. Con lai = 1
            if (criticalRate == 2)
            {
                Debug.Log(gameObject.name + " GET CRITICAL HIT!");
                damagePopupScr.CriticalEffect();
            }

            float damage = 3 * criticalRate * enemyAtk * UnityEngine.Random.Range(0.75f, 1.25f) / this.defense;
            damagePopupScr.SetText((int)damage);

            if ((int)damage > 0)
            {
                SliderManager.Instance.DamageEffect(true, false);
                CameraEffects.Instance.ShakeOnce(0.5f, 3, camera: Camera.main);
                //isStartHurt = true;
                Vector3 direct = (this.transform.position - attackPos).normalized;

                //StartCoroutine(Cor_EndStartHurt(1,direct));
                rb.velocity = direct*7;
            }

            this.currentHealth -= (int)damage;
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_HURT);
            if (this.currentHealth <= maxHealth*lowHealthRate)
            {
                FullScreenEffect.Instance.LowHP();
                SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_LOW_HP);
            }
            if (this.currentHealth <= 0)
            {
                this.currentHealth = 0;
                Die();
            }

            //Debug.Log(gameObject.name + " is gotten hit with damage = " + damage);
        }
    }
}
