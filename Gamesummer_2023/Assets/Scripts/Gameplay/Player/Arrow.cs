using GDC.Enemies;
using GDC.PlayerManager.Hand;
using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC;
using GDC.Objects;


namespace GDC.PlayerManager.Attack
{
    public enum ArrowType
    {
        NORMAL = 0,
        STRONG = 1,
    }
    public class Arrow : PlayerAttackObject
    {
        [ShowNonSerializedField]
        float angle;
        public float Angle { set => angle = value; }
        [ShowNonSerializedField]
        float range;
        public float Range { set => range = value; }
        [ShowNonSerializedField]
        float arrowSpeed;
        public float ArrowSpeed { set => arrowSpeed = value; }
        [ShowNonSerializedField]
        Vector2 startPoint;
        public Vector2 StartPoint { set => startPoint = value; }

        ArrowType arrowType;
        public ArrowType Type { set => arrowType = value; }

        void Update()
        {
            Move();
        }

        public override void Disappear()
        {
            if (arrowType == ArrowType.NORMAL)
                HandMovement.Instance.normalArrow.Add(this.gameObject);
            else if (arrowType == ArrowType.STRONG)
            {
                HandMovement.Instance.strongArrow.Add(this.gameObject);
            }
            gameObject.SetActive(false);
        }

        void Move()
        {
            if (Vector2.Distance(startPoint, transform.position) >= range)
            {

                Disappear();
                return;
            }
            transform.position += new Vector3(Time.deltaTime * arrowSpeed * Mathf.Sin(angle * Mathf.Deg2Rad), -Time.deltaTime * arrowSpeed * Mathf.Cos(angle * Mathf.Deg2Rad), 0);
        }

    }
}