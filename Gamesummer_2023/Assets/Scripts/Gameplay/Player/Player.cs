using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.Enums;
using DG.Tweening;
using GDC.Enemies;
using GDC.Gameplay.UI;
using GDC.PlayerManager.Hand;
using GDC.Configuration;
using NaughtyAttributes;
using GDC.Gameplay;
using GDC.PlayerManager.Spell;
using GDC.Managers;
using GDC.Pets;
using GDC.Gameplay.Effect;
using UnityEngine.SceneManagement;
using TMPro;
using AudioPlayer;

namespace GDC.PlayerManager
{

    public partial class Player : MonoBehaviour
    {
        void GenerateText()
        {
            GameObject template = textContainer.GetChild(0).gameObject;

            textList.Clear();
            while (textContainer.childCount > 1)
                DestroyImmediate(textContainer.GetChild(1).gameObject);

            textList.Add(template.GetComponent<TMP_Text>());
            for (int i = 1; i < numberOfText; i++)
            {
                GameObject clone = Instantiate(template, textContainer);
                clone.name = "Text " + i.ToString();
                textList.Add(clone.GetComponent<TMP_Text>());
            }
        }
        public void SpawnManaText()
        {
            if (textList.Count==0)
            {
                //print("Not enough text");
                return;
            }
            SoundManager.Instance.PlaySound(SoundID.SFX_ERROR);
            TMP_Text text = textList[0];
            text.color = new Color(text.color.r, text.color.g, text.color.b, 1);
            textList.RemoveAt(0);

            text.DOFade(0, 0.5f);
            text.GetComponent<RectTransform>().DOAnchorPosY(80, 0.5f).OnComplete(() =>
            {
                text.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 50);
                textList.Add(text);
            });
        }
        private void Awake()
        {
            //if (Instance != null)
            //{
            //    Destroy(gameObject);
            //    return;
            //}
            Instance = this;
            LoadPlayerStats();
        }

        void LoadPlayerStats()
        {
            StartCoroutine(Cor_LoadPlayerStats());
        }

        public void DetectLowHPWhenSceneChanged()
        {
            if (currentHealth <= maxHealth * lowHealthRate)
                FullScreenEffect.Instance.LowHP();
        }

        IEnumerator Cor_LoadPlayerStats()
        {
            yield return new WaitUntil(() => GDC.Managers.ConfigManager.Instance != null && SaveLoadManager.Instance != null);
            yield return new WaitUntil(() => SaveLoadManager.Instance.GameData.IsSaveLoadProcessing == false);
            GameData gamedata = SaveLoadManager.Instance.GameData;
            this.playerStatConfig = GDC.Managers.ConfigManager.Instance.PlayerStatConfig;

            this.level = gamedata.PlayerLevel;
            this.exp = gamedata.PlayerExp;

            InitAttack = this.playerStatConfig.Attack;
            InitDefense = this.playerStatConfig.Defense;
            InitSpeed = this.playerStatConfig.Speed;
            BaseSpeed = this.playerStatConfig.BaseSpeed;
            InitMaxHealth = this.playerStatConfig.MaxHealth;
            InitMaxStamina = this.playerStatConfig.MaxStamina;
            this.speedScale = this.playerStatConfig.SpeedScale;
            this.dashDistance = this.playerStatConfig.DashDistance;
            this.maxHealth = this.InitMaxHealth + gamedata.HpBonus;
            this.MaxStamina = this.InitMaxStamina + gamedata.StamBonus;       

            isCanMove = true;
            isCanAttack = true;
            isCanGetHit = true;

            speed = (int)(InitSpeed * buffSpeedValue + equipSpeed);
            attack = (int)(InitAttack * buffAttackValue + equipAttack + gamedata.AtkBonus);
            defense = (int)(InitDefense * buffDefenseValue + equipDefense + gamedata.DefBonus);
            maxStamina = (int)(InitMaxStamina + equipMaxStamina + gamedata.StamBonus);
            if (gamedata.PlayerHp > 0)
            {
                this.currentHealth = gamedata.PlayerHp;
                this.stamina = gamedata.PlayerStamina;
            }
            else
            {
                this.currentHealth = this.maxHealth;
                this.stamina = this.maxStamina;
            }

            this.effectDictInstance = this.playerStatConfig.LoadAllEffectInstances(this.effectContainer);
            this.sleepEffect = this.effectDictInstance[ePlayerEffectStatus.Sleep];
            this.dizzyCircle = this.effectDictInstance[ePlayerEffectStatus.Dizzy];
            this.slowEffect = this.effectDictInstance[ePlayerEffectStatus.Slow];
            this.freezeEffect = this.effectDictInstance[ePlayerEffectStatus.Freeze];
            this.poisonEffect = this.effectDictInstance[ePlayerEffectStatus.Poison];
            this.buffAtackkEffect = this.effectDictInstance[ePlayerEffectStatus.Attack_Buff];
            this.buffDefenseEffect = this.effectDictInstance[ePlayerEffectStatus.Defense_Buff];
            this.buffSpeedEffect = this.effectDictInstance[ePlayerEffectStatus.Speed_Buff];
            this.healEffect = this.effectDictInstance[ePlayerEffectStatus.Heal];
        }

        public float ShowExpInSlider()
        {
            return (float)exp / levelSystem.ExpToNextLevel(level);
        }
        [Button]
        void LevelUp()
        {
            StartCoroutine(Cor_LevelUp());
        }

        public void LoadLevelFromData()
        {
            exp = SaveLoadManager.Instance.GameData.PlayerExp;
            level = SaveLoadManager.Instance.GameData.PlayerLevel;
            atkBonus = SaveLoadManager.Instance.GameData.AtkBonus;
            defBonus = SaveLoadManager.Instance.GameData.DefBonus;
            hpBonus = SaveLoadManager.Instance.GameData.HpBonus;
            stamBonus = SaveLoadManager.Instance.GameData.StamBonus;
            SliderManager.Instance.ExpText.text = "Lv" + level.ToString();
        }

        IEnumerator Cor_LevelUp()
        {
            levelUpEffect.gameObject.SetActive(true);
            levelUpEffect.enabled = true;
            levelUpEffect.Play("levelUp", 0, 0);
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_LEVEL_UP);
            yield return new WaitForSeconds(1);
            levelUpEffect.enabled = false;
            levelUpEffect.gameObject.SetActive(false);
            LevelUpPanel.Instance.Show();
        }

        // Update is called once per frame
        void Update()
        {
            if (IsCanMove())
            {
                Move();
                Dash();
            }
            if (isDashing&&Input.GetMouseButtonDown(0))
            {
                DashAttack();
            }

            DetectPet();
            DetectPetGrass();

            EnableHandUsingWeapon();
        }

        void EnableHandUsingWeapon()
        {
            if (graphicOfPlayer.GetCurrentAnimatorStateInfo(0).IsName("idle")
                || graphicOfPlayer.GetCurrentAnimatorStateInfo(0).IsName("move"))
            {
                if (HandMovement.Instance.gameObject.activeInHierarchy)
                    return;
                HandMovement.Instance.transform.position = backHand.transform.position;
                HandMovement.Instance.gameObject.SetActive(true);
                return;
            }
            HandMovement.Instance.StopAttack();
            HandMovement.Instance.gameObject.SetActive(false);
        }
        
        
        [Button]
        void Die()
        {
            if (isDead) return;
            SoundManager.Instance.PlaySound(SoundID.SFX_DIE);
            isDead = true;
            Debug.Log("Player died!");
            graphicOfPlayer.Play("die");
            StartCoroutine(Cor_ShowGameOverPanel());
            //todo
        }

        public void ConvertToLayer(string layerID)
        {
            foreach (SpriteRenderer sprite in spriteRendererList)
            {
                sprite.sortingLayerName = layerID;
                for (int j = 0; j < sprite.transform.childCount; j++)
                {
                    SpriteRenderer childRend = sprite.transform.GetChild(j).GetComponent<SpriteRenderer>();
                    if (childRend!=null)
                        childRend.sortingLayerName = layerID;
                }
            }
        }

        IEnumerator Cor_ShowGameOverPanel()
        {
            yield return new WaitForSeconds(1);
            //GameObject clone = new GameObject("Dead player");
            //clone.transform.parent = dashCloneContainer;
            //clone.transform.position = transform.position;
            //clone.transform.localScale = graphicOfPlayer.transform.localScale;
            //foreach (SpriteRenderer sprite in spriteRendererList)
            //{
            //    SpriteRenderer cloneSprite = Instantiate(sprite,clone.transform);
            //    cloneSprite.transform.position = sprite.transform.position;
            //    cloneSprite.sortingLayerName = "UI_World";
            //    for (int j = 0; j < cloneSprite.transform.childCount; j++)
            //    {
            //        cloneSprite.transform.GetChild(j).GetComponent<SpriteRenderer>().sortingLayerName = "UI_World";
            //    }
            //}
            InCutScene = true;
            ConvertToLayer("UI_World");
            GameOverPanel.Instance.TurnOn();
        }

        public void ObtainItem(SO_Item so_item)
        {
            StartCoroutine(Cor_ObtainItem(so_item));
        }
        IEnumerator Cor_ObtainItem(SO_Item so_item)
        {
            SoundManager.Instance.PlaySound(SoundID.SFX_LOOT_SPECIAL);
            if (inCutScene)
            {
                obtainEffect.gameObject.SetActive(true);
                graphicOfPlayer.Play("obtain2");
                obtainEffect.ShowItem(so_item);
                yield return new WaitForSeconds(3);
                obtainEffect.HideItem();
            }
            else
            {
                inCutScene = true;
                obtainEffect.gameObject.SetActive(true);
                graphicOfPlayer.Play("obtain2");
                obtainEffect.ShowItem(so_item);
                yield return new WaitForSeconds(3);
                obtainEffect.HideItem();
                inCutScene = false;
            }
            graphicOfPlayer.Play("idle");
        }
        public void HandleSurprise(float duration, float animSpeed = 1)
        {
            StartCoroutine(Cor_Surprise(duration, animSpeed));
        }

        IEnumerator Cor_Surprise(float duration, float animSpeed = 1)
        {
            graphicOfPlayer.Play("surprise");
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_SURPRISE);
            graphicOfPlayer.speed = animSpeed;
            yield return new WaitForSeconds(duration);
            graphicOfPlayer.speed = 1;
            graphicOfPlayer.Play("idle");
        }

        IEnumerator Cor_RefreshGetHit(float sec)
        {
            yield return new WaitForSeconds(sec);
            isCanGetHit = true;
        }
        void BlinkTo(Color color, float blinkTime = 1f, int blinkCycle = 1)
        {
            foreach (var spriteRenderer in spriteRendererList)
            {
                spriteRenderer.material.DOColor(color, blinkTime).SetLoops(blinkCycle, LoopType.Yoyo);
            }
        }
        //IEnumerator Cor_GetHitEffect()
        //{
        //    //Hieu ung do nguoi
        //    foreach (var spriteRenderer in spriteRendererList)
        //    {
        //        spriteRenderer.material.DOColor(Color.red, 0.2f);
        //    }
        //    yield return new WaitForSeconds(0.2f);
        //    foreach (var spriteRenderer in spriteRendererList)
        //    {
        //        spriteRenderer.material.DOColor(Color.white, 0.2f);
        //    }

        //    //Hieu ung Nhap nhay nguoi sau khi bi hit
        //    Color blinkColor = new Color(1, 1, 1, 0.5f);
        //    yield return new WaitForSeconds(0.25f);
        //    foreach (var spriteRenderer in spriteRendererList)
        //    {
        //        spriteRenderer.material.DOColor(blinkColor, 0.2f);
        //    }
        //    yield return new WaitForSeconds(0.25f);
        //    foreach (var spriteRenderer in spriteRendererList)
        //    {
        //        spriteRenderer.material.DOColor(Color.white, 0.2f);
        //    }
        //    yield return new WaitForSeconds(0.25f);
        //    foreach (var spriteRenderer in spriteRendererList)
        //    {
        //        spriteRenderer.material.DOColor(blinkColor, 0.2f);
        //    }
        //    yield return new WaitForSeconds(0.25f);
        //    foreach (var spriteRenderer in spriteRendererList)
        //    {
        //        spriteRenderer.material.DOColor(Color.white, 0.2f);
        //    }
        //    yield return new WaitForSeconds(0.25f);
        //    foreach (var spriteRenderer in spriteRendererList)
        //    {
        //        spriteRenderer.material.DOColor(blinkColor, 0.2f);
        //    }
        //    yield return new WaitForSeconds(0.25f);
        //    foreach (var spriteRenderer in spriteRendererList)
        //    {
        //        spriteRenderer.material.DOColor(Color.white, 0.2f);
        //    }
        //    yield return new WaitForSeconds(0.25f);
        //    foreach (var spriteRenderer in spriteRendererList)
        //    {
        //        spriteRenderer.material.DOColor(blinkColor, 0.2f);
        //    }
        //    yield return new WaitForSeconds(0.25f);
        //    foreach (var spriteRenderer in spriteRendererList)
        //    {
        //        spriteRenderer.material.DOColor(Color.white, 0.2f);
        //    }
        //}
        //IEnumerator Cor_ToColor(Color color, float sec)
        //{
        //    yield return new WaitForSeconds(sec);
        //    foreach (var spriteRenderer in spriteRendererList)
        //    {
        //        spriteRenderer.material.DOColor(color, 0.2f);
        //    }
        //}
        //IEnumerator Cor_EndStartHurt(float sec, Vector2 direct)
        //{
        //    rb.velocity = direct * 7;
        //    yield return new WaitForSeconds(sec*0.1f);
        //    rb.velocity = direct * 5;
        //    yield return new WaitForSeconds(sec*0.1f);
        //    rb.velocity = direct*2;
        //    yield return new WaitForSeconds(sec*0.2f);
        //    //isStartHurt = false;
        //    rb.velocity = Vector2.zero;
        //}
        bool IsCanMove()
        {
            isCanMove = true;
            AnimatorStateInfo state = graphicOfPlayer.GetCurrentAnimatorStateInfo(0);
            //if (TriggerDialogue.Instance != null && TriggerDialogue.Instance.isEndDialogue == false) isCanMove = false;
            if (isFreeze || isSleep || isStartHurt) isCanMove = false;
            if (isTeleporting) isCanMove = false;
            if (inCutScene || InMinigame || GameManager.Instance.isLoadSceneComplete == false) isCanMove = false;
            if (isDashing) isCanMove = false;
            if (state.IsName("obtain")) isCanMove = false;
            if (state.IsName("pickUp")) isCanMove = false;
            if (state.IsName("lieDown")) isCanMove = false;
            if (PetPopupPanel.Instance != null && PetPopupPanel.Instance.gameObject.activeInHierarchy) isCanMove = false;
            //to do more here
            return isCanMove;
        }

        void SetFootstep(Collider2D collision) // used for trigger maybe...
        {
            SoundID tempSoundID = currentFootstepSound;
            if (collision.CompareTag(landTag))
                tempSoundID = SoundID.SFX_LAND_FOOTSTEP;
            else if (collision.CompareTag(rockTag))
                tempSoundID = SoundID.SFX_ROCK_FOOTSTEP;
            else if (collision.CompareTag(grassTag))
                tempSoundID = SoundID.SFX_GRASS_FOOTSTEP;
            else if (collision.CompareTag(iceTag))
                tempSoundID = SoundID.SFX_ICE_FOOTSTEP;

            if (tempSoundID == currentFootstepSound)
                return;
            SoundManager.Instance.StopSFX(currentFootstepSound);
            currentFootstepSound = tempSoundID;
            SoundManager.Instance.PlaySound(currentFootstepSound);
        }

        private void OnTriggerStay2D(Collider2D collision)
        {
            //SetFootstep(collision);
            if (collision.CompareTag("Enemy"))
            {
                if (InCutScene) return;

                Enemy enemyScr = collision.GetComponent<Enemy>();
                if (enemyScr != null)
                {
                    if (enemyScr.Hp <= 0) return;
                    GetHit(collision.transform.position, enemyScr.Atk, 0, collision.gameObject.layer);
                }
                else
                {
                    Boss bossScr = collision.GetComponent<Boss>();
                    if (bossScr == null || bossScr.Hp <= 0) return;
                    GetHit(collision.transform.position, bossScr.Atk, 0, collision.gameObject.layer);
                }
            }
            if (collision.CompareTag("Grass") || collision.CompareTag("SunFlower")) //Dang o trong bui co cao, ko the dash
            {
                isOnTallGrass = true;
            }
        }
        private void OnTriggerExit2D(Collider2D collision)
        {
            if (collision.CompareTag("Grass") || collision.CompareTag("SunFlower"))
            {
                isOnTallGrass = false;
            }
        }
        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.gameObject.CompareTag("Boulder"))
            {
                if (graphicOfPlayer.GetCurrentAnimatorStateInfo(0).IsName("dash"))
                {
                    EndDash();
                    collision.gameObject.GetComponent<Boulder>().Move(dashDirection, transform.position);
                }
            }
            
        }

        private void OnCollisionStay2D(Collision2D collision)
        {
            if (collision.gameObject.CompareTag("Enemy"))
            {
                if (InCutScene) return;

                Enemy enemyScr = collision.gameObject.GetComponent<Enemy>();
                if (enemyScr != null)
                {
                    if (enemyScr.Hp <= 0) return;
                    GetHit(collision.transform.position, enemyScr.Atk, 0, collision.gameObject.layer);
                }
                else
                {
                    Boss bossScr = collision.gameObject.GetComponent<Boss>();
                    if (bossScr == null || bossScr.Hp <= 0) return;
                    GetHit(collision.transform.position, bossScr.Atk, 0, collision.gameObject.layer);
                }
                if (isDashing)
                {
                    EndDash();
                }
                // Enemy enemyScr = collision.gameObject.GetComponent<Enemy>();
                // if (enemyScr.Hp <= 0) return;
                // GetHit(collision.transform.position, enemyScr.Atk, 0, collision.gameObject.layer);
            }
            else if (collision.gameObject.CompareTag("Ledge"))
            {
                int horizontalDirection = (int)Input.GetAxisRaw("Horizontal");
                int verticalDirection = (int)Input.GetAxisRaw("Vertical");

                Ledge ledge = collision.transform.GetComponent<Ledge>();
                ledge?.TryToJump(horizontalDirection, verticalDirection);
            }
            else if (collision.gameObject.CompareTag("Boulder"))
            {
                if (graphicOfPlayer.GetCurrentAnimatorStateInfo(0).IsName("dash"))
                {
                    EndDash();
                    collision.gameObject.GetComponent<Boulder>().Move(dashDirection, transform.position);
                }
            }
        }




        public void ChangeEquipmentItem(SO_Item equipItem)
        {
            if (equipItem.EquipmentType == EquipmentType.HELMET)
            {
                HelmetItem = equipItem;
                spriteRendererList[3].transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = equipItem.sprite;
            }
            else if (equipItem.EquipmentType == EquipmentType.ARMOR)
            {
                ArmorItem = equipItem;
                spriteRendererList[0].sprite = equipItem.sprite;
            }
            else if (equipItem.EquipmentType == EquipmentType.WEAPON)
            {
                WeaponItem = equipItem;
                spriteRendererList[5].transform.GetChild(1).GetComponent<SpriteRenderer>().sprite = equipItem.sprite;
                HandMovement.Instance.ChangeWeapon(equipItem.weaponType, equipItem.sprite);
            }
            else if (equipItem.EquipmentType == EquipmentType.SHOE)
            {
                ShoeItem = equipItem;
                spriteRendererList[1].sprite = equipItem.sprite;
                spriteRendererList[2].sprite = equipItem.sprite;
                spriteRendererList[8].sprite = equipItem.sprite;
            }
            else
            {
                Debug.LogError("Equipment item has EquipmentType == NONE!");
            }

            UpdateEquipStat();
            //LoadEquipmentSprite();
        }
        public void TakeOutEquipmentItem(EquipmentType equipmentType)
        {
            if (equipmentType == EquipmentType.HELMET)
            {
                HelmetItem = null;
                spriteRendererList[3].transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = null;
            }
            else if (equipmentType == EquipmentType.ARMOR)
            {
                ArmorItem = null;
                spriteRendererList[0].sprite = nullArmor;
            }
            else if (equipmentType == EquipmentType.WEAPON)
            {
                WeaponItem = null;
                HandMovement.Instance.ChangeWeapon();
            }
            else if (equipmentType == EquipmentType.SHOE)
            {
                ShoeItem = null;
                spriteRendererList[1].sprite = nullShoe;
                spriteRendererList[2].sprite = nullShoe;
                spriteRendererList[8].sprite = nullShoe;
            }
            else
            {
                Debug.LogError("warning: equipmentType == NONE!");
            }
            UpdateEquipStat();
            //LoadEquipmentSprite();
        }
        void UpdateEquipStat()
        {
            int hAtk = 0, hDef = 0, hSpe = 0, hStam = 0;
            int aAtk = 0, aDef = 0, aSpe = 0, aStam = 0;
            int wAtk = 0, wDef = 0, wSpe = 0, wStam = 0;
            int sAtk = 0, sDef = 0, sSpe = 0, sStam = 0;

            if (HelmetItem != null)
            {
                hAtk = HelmetItem.Attack;
                hDef = HelmetItem.Defense;
                hSpe = HelmetItem.Speed;
                hStam = HelmetItem.Stamina;
            }
            if (ArmorItem != null)
            {
                aAtk = ArmorItem.Attack;
                aDef = ArmorItem.Defense;
                aSpe = ArmorItem.Speed;
                aStam = ArmorItem.Stamina;
            }
            if (WeaponItem != null)
            {
                wAtk = WeaponItem.Attack;
                wDef = WeaponItem.Defense;
                wSpe = WeaponItem.Speed;
                wStam = WeaponItem.Stamina;
            }
            if (ShoeItem != null)
            {
                sAtk = ShoeItem.Attack;
                sDef = ShoeItem.Defense;
                sSpe = ShoeItem.Speed;
                sStam = ShoeItem.Stamina;
            }
            EquipAttack = hAtk + aAtk + wAtk + sAtk;
            EquipDefense = hDef + aDef + wDef + sDef;
            EquipSpeed = hSpe + aSpe + wSpe + sSpe;
            EquipMaxStamina = hStam + aStam + wStam + sStam;
        }

        [Button]
        void Undress()
        {
            TakeOutEquipmentItem(EquipmentType.HELMET);
            TakeOutEquipmentItem(EquipmentType.ARMOR);
            TakeOutEquipmentItem(EquipmentType.WEAPON);
            TakeOutEquipmentItem(EquipmentType.SHOE);
        }

        public void ToIdle()
        {
            graphicOfPlayer.SetBool("isMoving", false);
            if (useHandMovement)
                graphicOfPlayer.Play("idle");
            else
                graphicOfPlayer.Play("idle(noWeapon)");
        }
        [SerializeField, Foldout("Cutscene")] int timeTurnAround;

        [Button]
        public void LieDown()
        {
            LieDown(transform.position);
        }
        public void LieDown(Vector2 bedPos, bool isSleepHeadLeft = false)
        {
            graphicOfPlayer.Play("lieDown");
            transform.position = bedPos;
            StartCoroutine(Cor_WaitHand(isSleepHeadLeft));
        }
        IEnumerator Cor_WaitHand(bool isSleepHeadLeft)
        {
            yield return new WaitUntil(() => !HandMovement.Instance.gameObject.activeInHierarchy);
            if (isSleepHeadLeft)
                graphicOfPlayer.transform.localScale = new Vector3(1, 1, 1);
            else
                graphicOfPlayer.transform.localScale = new Vector3(1, -1, 1);
        }

        [Button]
        public void StandUp()
        {

            StandUp(transform.position);
        }
        public void StandUp(Vector2 standUpPos)
        {
            graphicOfPlayer.transform.localScale = new Vector3(1, 1, 1);
            transform.position = standUpPos;
            graphicOfPlayer.Play("standUp");
            //StartCoroutine(Cor_StandUpSurprise());
        }
        public void StandUpSurprise()
        {
            graphicOfPlayer.Play("standUp");
            StartCoroutine(Cor_StandUpSurprise());
        }

        IEnumerator Cor_StandUpSurprise()
        {
            yield return new WaitForSeconds(graphicOfPlayer.GetCurrentAnimatorStateInfo(0).length);
            Transform graphicTransform = graphicOfPlayer.transform;
            for (int i = 0; i < timeTurnAround; i++)
            {
                graphicTransform.localScale = new Vector3(-graphicTransform.localScale.x, 1, 1);
                yield return new WaitForSeconds(0.5f);
            }
            ToIdle();
        }
        void DetectPetGrass()
        {
            Collider2D[] petGrasses = Physics2D.OverlapCircleAll(transform.position, pickUpRadius, petGrassMask);
            if (petGrasses.Length == 0)
            {
                if (currentGrass != null)
                {
                    currentGrass.CloseBubbleChat();
                    currentGrass = null;
                }
                return;
            }
            SceneType curSceneType = SaveLoadManager.Instance.GameData.CurrentSceneType;
            if (curSceneType == petScene)
            {
                foreach (Collider2D petGrass in petGrasses)
                {
                    petGrass.GetComponent<PetGrass>().SpawnPet();
                }
                return;
            }

            Collider2D tempGrass = null;
            float minDistance = pickUpRadius;

            foreach (Collider2D petGrass in petGrasses)
            {
                float distance = Vector2.Distance(petGrass.transform.position, transform.position);
                if (distance < minDistance)
                {
                    minDistance = distance;
                    tempGrass = petGrass;
                }
            }
            PetGrass tempGrassScr = tempGrass?.GetComponent<PetGrass>();
            if (currentGrass != null && tempGrassScr != currentGrass)
            {
                currentGrass.CloseBubbleChat();
                currentGrass = null;
            }
            if (tempGrassScr == null)
                return;
            currentGrass = tempGrassScr;
            currentGrass.OpenBubbleChat();
            if (Input.GetKeyDown(KeyCode.F))
                StartCoroutine(Cor_PickUp());
        }

        IEnumerator Cor_PickUp()
        {

            if (currentGrass == null)
                yield break;
            graphicOfPlayer.Play("pickUp");
            StartCoroutine(Cor_PickUpPet());
            yield return new WaitUntil(() => !HandMovement.Instance.gameObject.activeInHierarchy);
            graphicOfPlayer.transform.localScale = new Vector3((transform.position.x > currentGrass.transform.position.x) ? -1 : 1, 1, 1);
        }

        IEnumerator Cor_PickUpPet()
        {
            yield return new WaitForSeconds(1/3f);
            currentGrass.SpawnPet();
            SoundManager.Instance.PlaySound(SoundID.SFX_PLUCK);
        }

        void DetectPet()
        {
            string curSceneName = SceneManager.GetActiveScene().name;
            SceneType curSceneType = GameSceneManager.TranslateToSceneType(curSceneName);
            if (curSceneType != petScene)
                return;
            Collider2D[] pets = Physics2D.OverlapCircleAll(transform.position, pickUpRadius, petMask);
            if (pets.Length == 0)
            {
                if (currentPet != null)
                {
                    currentPet.CloseBubbleChat();
                    currentPet = null;
                }
                return;
            }

            Collider2D tempPet = null;
            float minDistance = pickUpRadius;

            foreach (Collider2D pet in pets)
            {
                if (pet.GetComponent<Pet>().FollowPlayer)
                    continue;
                float distance = Vector2.Distance(pet.transform.position, transform.position);
                if (distance < minDistance)
                {
                    minDistance = distance;
                    tempPet = pet;
                }
            }
            Pet tempPetScr = tempPet?.GetComponent<Pet>();
            if (currentPet != null && tempPetScr != currentPet)
            {
                currentPet.CloseBubbleChat();
                currentPet = null;
            }
            if (tempPetScr == null)
                return;
            currentPet = tempPetScr;
            currentPet.OpenBubbleChat();
            if (Input.GetKeyDown(KeyCode.F))
                PetManager.Instance.AddPet(currentPet);
        }
    }
}