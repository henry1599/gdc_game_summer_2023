using DG.Tweening;
using GDC.Configuration;
using GDC.Enums;
using GDC.Managers;
using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;
using UnityEngine.UI;

namespace GDC.Pets
{
    public class PetInfoPanel : MonoBehaviour
    {
        public static PetInfoPanel Instance {  get; private set; }

        #region sign
        [SerializeField, Foldout("Sign")] Transform sign;
        [SerializeField, Foldout("Sign")] float signRadius;
        [SerializeField, Foldout("Sign")] LayerMask playerMask;
        [SerializeField, Foldout("Sign")] Transform bubbleChat;
        [SerializeField,Foldout("Sign"),ReadOnly]bool playerIn = false;

        void DetectPlayer()
        {
            Collider2D[] player = Physics2D.OverlapCircleAll(sign.position, signRadius, playerMask);
            if (player.Length == 0)
            {
                if (playerIn)
                    bubbleChat.DOScale(0, 0.5f);
                playerIn = false;
                return;
            }

            if (!playerIn)
                bubbleChat.DOScale(1, 0.5f);

            if (Input.GetKeyDown(KeyCode.F))
            {
                OpenPanel();
                bubbleChat.localScale = Vector3.zero;
            }
        }
        #endregion

        #region panel
        [SerializeField, Foldout("Panel")] Transform petInfoPanel;
        [SerializeField, Foldout("Panel")] Transform content;
        [SerializeField, Foldout("Panel")] string pathToSOPet;
        //[SerializeField, Foldout("Panel")] PetManager petManager;
        [SerializeField, Foldout("Panel")] ScrollRect scrollView;
        [SerializeField, Foldout("Panel")] List<PetInfo> petInfos = new List<PetInfo>();
        [SerializeField, Foldout("Panel")] TMPro.TMP_Text noPetText;
        public List<PetInfo> PetInfos
        {
            get => petInfos;
        }
        public void UpdatePetInUse(PetID petID)
        {
            foreach (PetInfo petInfo in petInfos)
            {
                if (petInfo.PetID == petID)
                    petInfo.InUse = true;
                else
                    petInfo.InUse = false;
            }
        }

        void OpenPanel()
        {
            DetectPetInfo();
            petInfoPanel.DOScale(1, 0.5f).SetEase(Ease.OutBack).SetUpdate(true).OnComplete(()=> scrollView.enabled = true);
            Time.timeScale = 0;
            content.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;
        }

        public void ClosePanel()
        {
            petInfoPanel.DOScale(0, 0.5f).SetEase(Ease.InBack).SetUpdate(true).OnComplete(()=> Time.timeScale = 1);
            scrollView.enabled = false;
            
        }

        //[Button]
        //void GeneratePanel()
        //{ 

        //    var petInfos = Resources.LoadAll(pathToSOPet);
        //    if (petInfos == null)
        //        return;
        //    this.petInfos.Clear();

        //    PetInfo template = Instantiate(content.GetChild(0).GetComponent<PetInfo>());
        //    while (content.childCount > 0)
        //    {
        //        DestroyImmediate(content.GetChild(0).gameObject);
        //    }
        //    PetInfoByType petDict = petManager.PetDict;
        //    Pet currentPet = petManager.CurrentPet;

        //    foreach (var petInfo in petInfos)
        //    {
        //        SO_Pet pet = (SO_Pet)petInfo;
        //        PetInfo clone = Instantiate(template, content);
        //        Color tempColor;
        //        if (ColorUtility.TryParseHtmlString(petDict[pet.PetID].Color, out tempColor))
        //        {
        //            clone.name = pet.PetID.ToString();
        //            clone.SetUp(pet, petDict[pet.PetID].Head, petDict[pet.PetID].Leaf, petDict[pet.PetID].Name, tempColor);
        //        }
        //        else
        //        {
        //            print("Invalid color");
        //            return;
        //        }
        //        clone.PetID = pet.PetID;
        //        this.petInfos.Add(clone);
        //    }
        //    if (currentPet != null)
        //    {
        //        foreach (PetInfo petInfo in this.petInfos)
        //        {
        //            if (petInfo.PetID == currentPet.PetID)
        //            {
        //                petInfo.InUse = true;
        //                break;
        //            }
        //        }
        //    }

        //    DestroyImmediate(template.gameObject);
        //}

        void DetectPetInfo()
        {
            List<PetID> petAtHome = SaveLoadManager.Instance.GameData.GetPetAtHomeList();
            for (int i=0;i<petInfos.Count;i++) 
            {
                PetInfo petInfo = petInfos[i];
                bool hasPet = false;
                foreach (PetID petID in petAtHome)
                {
                    if (petID == petInfo.PetID)
                    {
                        hasPet = true;
                        break;
                    }
                }
                if (!hasPet)
                {
                    i--;
                    petInfo.ReleasePet();
                }
            }

            if (petAtHome.Count == 0)
                noPetText.gameObject.SetActive(true);
        }

        #endregion

        #region confirm panel
        [SerializeField,Foldout("Confirm panel")] Transform confirmPanel;
        PetInfo currentPetInfo = null;
        public void OpenConfirmPanel(PetInfo petInfo)
        {

            confirmPanel.DOScale(1, 0.5f).SetEase(Ease.OutBack).SetUpdate(true);
            currentPetInfo = petInfo;
        }
        public void CloseConfirmPanel()
        {
            currentPetInfo = null;
            confirmPanel.DOScale(0, 0.5f).SetEase(Ease.InBack).SetUpdate(true);
        }

        public void LeavePet()
        {
            PetID currentPetID = currentPetInfo.PetID;
            SaveLoadManager.Instance.GameData.ReleasePet(currentPetID);
            currentPetInfo.ReleasePet();
            CloseConfirmPanel();

            List<Pet> petAtHome = GreenHouse.Instance.PetAtHome;
            foreach (Pet pet in petAtHome)
            {
                if (pet.PetID == currentPetID)
                {
                    petAtHome.Remove(pet);
                    Destroy(pet.gameObject);
                    return;
                }
            }
            List<PetGrass> petGrassAtHome = GreenHouse.Instance.PetGrassAtHome;
            foreach (PetGrass petGrass in petGrassAtHome)
            {
                if (petGrass.PetID == currentPetID)
                {   
                    petGrassAtHome.Remove(petGrass);
                    Destroy(petGrass.gameObject);
                    return;
                }
            }

            print("Pet not at home");
        }
        #endregion

        #region unuse pet
        [SerializeField, Foldout("Unuse pet")] Image unuseButton;
        [SerializeField, Foldout("Unuse pet")] Collider2D range;
        Tween currentButtonTween;
        public void ButtonEnter()
        {
            if (PetManager.Instance.CurrentPet == null)
                return;
            currentButtonTween.Kill();
            currentButtonTween = unuseButton.DOFade(1, 0.5f);
            currentButtonTween.Play();
        }
        public void ButtonExit()
        {
            if (PetManager.Instance.CurrentPet == null)
                return;
            currentButtonTween.Kill();
            currentButtonTween = unuseButton.DOFade(0.5f, 0.5f);
            currentButtonTween.Play();
        }
        public void ButtonDown()
        {
            if (PetManager.Instance.CurrentPet == null)
                return;
            currentButtonTween.Kill();
            unuseButton.color = new Color(0.5f, 0.5f, 0.5f, 1);
        }

        public void ButtonUp()
        {
            if (PetManager.Instance.CurrentPet == null)
                return;
            unuseButton.color = Color.white;
        }
        public void UnusePet()
        {
            PetManager.Instance.UnusePet(range);
        }
        #endregion
        private void Awake()
        {
            if (Instance != null)
                Destroy(gameObject);
            else
                Instance = this;
        }


        private void Start()
        {
            bubbleChat.GetComponent<Animator>().Play("PressF");
            bubbleChat.localScale = Vector3.zero;
        }

        // Update is called once per frame
        void Update()
        {
            DetectPlayer();
        }
    }
}