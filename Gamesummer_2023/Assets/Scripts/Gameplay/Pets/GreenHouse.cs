using GDC.Enums;
using GDC.Managers;
using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GDC.Pets
{
    public class GreenHouse : MonoBehaviour
    {
        public static GreenHouse Instance {  get; private set; }

        [SerializeField] Collider2D range,soil;
        //[SerializeField,ReadOnly] List<PetID> pets = new List<PetID>();
        [SerializeField] PetGrass template;
        [SerializeField, ReadOnly] List<PetGrass> petGrassAtHome = new List<PetGrass>();
        public List<PetGrass> PetGrassAtHome
        {
            get => petGrassAtHome;
        }

        [SerializeField, ReadOnly] List<Pet> petAtHome = new List<Pet>();
        public List<Pet> PetAtHome
        {
            get => petAtHome;
        }
        bool hasGenerate = false;

        private void Awake()
        {
            if (Instance != null)
                Destroy(gameObject);
            else
                Instance = this;
        }

        public void GeneratePetGrass(List<PetID> listPet)
        {
            if (hasGenerate)
                return;
            hasGenerate = true;

            Vector2 center = soil.bounds.center;
            Vector2 extent = soil.bounds.extents;

            foreach (PetID petID in listPet)
            {
                PetGrass petGrass = Instantiate(template);
                petGrass.name = "Grass";
                petGrass.PetID = petID;
                petGrass.SetUpInGarden(range.bounds);
                PetGrassAtHome.Add(petGrass);
                petGrass.transform.position = center + new Vector2(Random.Range(-extent.x, extent.x), Random.Range(-extent.y, extent.y));
            }
        }

    }
}