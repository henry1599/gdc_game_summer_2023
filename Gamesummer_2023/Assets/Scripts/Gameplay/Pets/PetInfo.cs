﻿using DG.Tweening;
using GDC.Configuration;
using GDC.Enums;
using GDC.Managers;
using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace GDC.Pets
{
    public class PetInfo : MonoBehaviour
    {
        [SerializeField] Image infoBackground;
        [HideInInspector] public Sprite[] Leaf;
        [SerializeField] TMP_Text statusText, petNameText, petDescriptionText;
        [SerializeField] Image head, leaf;
        [SerializeField, ReadOnly] PetID petID;
        [SerializeField] Image releaseButton;
        [SerializeField] GameObject inUseText;
        [SerializeField,ReadOnly] string petName;
        public PetID PetID
        {
            get => petID;
            set => petID = value;
        }
        bool inUse = false;
        public bool InUse
        {
            set
            {
                if (value)
                {
                    inUseText.SetActive(true);
                }
                else
                {
                    releaseButton.color = Color.white;
                    releaseButton.raycastTarget = true;
                    inUseText.SetActive(false);
                }
                inUse = value;
            }
        }

        IEnumerator Cor_LeafAnimation()
        {
            int length = Leaf.Length;
            if (length == 0)
            {
                print("Invalid leaf");
                yield break;
            }
            while (true)
            {
                for (int i = 0; i < length; i++)
                {
                    leaf.sprite = Leaf[i];
                    yield return new WaitForSeconds(0.35f / length);
                }
            }
        }

        public void SetUp(SO_Pet petInfo, Sprite headSprite, Sprite[] leafSprite, string petName, Color backgroundColor)
        {
            Leaf = leafSprite;
            head.sprite = headSprite;
            leaf.sprite = Leaf[0];
            this.petName = petName;
            petNameText.text = petName;
            petDescriptionText.text = petInfo.PetInfo;
            infoBackground.color = new Color(backgroundColor.r, backgroundColor.g, backgroundColor.b, 100 / 255f);
            petNameText.color = backgroundColor;

            switch (petInfo.PetType)
            {
                case PetType.MIX:
                    {
                        statusText.text = "Thời gian hồi : " + petInfo.Cooldown.ToString() + "s\n"
                            + "Hồi máu : " + petInfo.HealAmount.ToString() + "\n"
                            + "Tấn công : " + petInfo.Damage.ToString();
                        break;
                    }
                case PetType.ATTACK:
                    {
                        statusText.text = "Thời gian hồi : " + petInfo.Cooldown.ToString() + "s\n"
                            + "Tấn công : " + petInfo.Damage.ToString();
                        break;
                    }
                case PetType.BUFF:
                    {
                        statusText.text = "Thời gian hồi : " + petInfo.Cooldown.ToString() + "s\n"
                            + "Hồi máu : " + petInfo.HealAmount.ToString();
                        break;
                    }
            }


        }

        public void OpenConfirmPanel()
        {
            PetInfoPanel.Instance.OpenConfirmPanel(this);
        }
        public void ReleasePet()
        {
            PetInfoPanel.Instance.PetInfos.Remove(this);
            Destroy(gameObject);
        }

        // Start is called before the first frame update
        void Start()
        {
            StartCoroutine(Cor_LeafAnimation());
        }
    }
}