using DG.Tweening;
using GDC.Configuration;
using GDC.Enemies;
using GDC.Enums;
using GDC.Managers;
using GDC.Objects;
using GDC.PlayerManager;
using NaughtyAttributes;
using System.Collections;
using UnityEngine;

namespace GDC.Pets
{
    public class Pet : MonoBehaviour
    {
        [SerializeField, ShowAssetPreview] Sprite normalFace, healFace, attackFace;
        bool followPlayer = false;
        public bool FollowPlayer
        {
            get => followPlayer;
            set
            {
                if (value)
                {
                    bubbleChat.enabled = false;
                    bubbleChat.transform.DOScale(0, 0.5f);
                    StopWander();

                    MoveToPlayer();

                    corHeal = Cor_Heal();
                    StartCoroutine(corHeal);

                    corAttack = Cor_Attack();
                    StartCoroutine(corAttack);
                }
                else
                {
                    Wander();
                }
                followPlayer = value;
            }
        }

        public void Disappear()
        {
            transform.DOScale(0, 0.5f).SetEase(Ease.InBack).OnComplete(() => { Destroy(gameObject); });
        }
        #region components
        [SerializeField, Foldout("Components")] protected Transform graphic;
        [SerializeField, Foldout("Components")] SpriteRenderer face;
        [SerializeField, Foldout("Components")] Rigidbody2D rb;
        #endregion

        #region info
        [SerializeField, Foldout("Info"), Expandable] SO_Pet petStatus;
        [SerializeField, Foldout("Info"), ReadOnly] PetType petType;
        [SerializeField, Foldout("Info"), ReadOnly] bool buffPet = false, attackPet = false;
        [SerializeField, Foldout("Info"), ReadOnly] float speed, coolDown;
        [SerializeField, Foldout("Info"), ReadOnly] float healAmount;
        [SerializeField, Foldout("Info"), ReadOnly] float damage;
        [SerializeField, Foldout("Info"), ReadOnly] PetID petID;
        public PetID PetID
        {
            get => petID;
        }

        void LoadStatus()
        {
            petType = petStatus.PetType;

            switch (petType)
            {
                case PetType.NONE:
                    {
                        buffPet = false;
                        attackPet = false;
                        break;
                    }
                case PetType.BUFF:
                    {
                        buffPet = true;
                        attackPet = false;
                        break;
                    }
                case PetType.ATTACK:
                    {
                        buffPet = false;
                        attackPet = true;
                        break;
                    }
                case PetType.MIX:
                    {
                        buffPet = true;
                        attackPet = true;
                        break;
                    }
            }

            speed = petStatus.Speed;
            coolDown = petStatus.Cooldown;
            healAmount = petStatus.HealAmount;
            damage = petStatus.Damage;
            petID = petStatus.PetID;
        }
        #endregion

        #region floating
        [SerializeField, Foldout("Float")] float floatingHeight, floatingDuration, normalHeight;
        //[SerializeField, Foldout("Float")] AnimationCurve floatingCurve;
        void Floating()
        {
            graphic.DOLocalMoveY(floatingHeight, floatingDuration).SetEase(Ease.Linear).OnComplete(() =>
            {
                graphic.DOLocalMoveY(normalHeight, floatingDuration).SetEase(Ease.Linear).OnComplete(() =>
                {
                    Floating();
                });
            });
        }
        #endregion

        #region chase after player
        [SerializeField, Foldout("Chase")] Transform player;
        [SerializeField, Foldout("Chase")] float maxRangeFromPlayer, maxDuration;
        [SerializeField, Foldout("Chase")] float[] timeBetweenChase = new float[2];
        [SerializeField, Foldout("Chase")] float rangeForceChase, rangeAfterChase;
        Coroutine corEndMove;
        bool isMoving = false;
        bool isTeleporting = false;

        IEnumerator Cor_LoadPlayer()
        {
            yield return new WaitUntil(() => Player.Instance != null);
            player = Player.Instance.transform;
        }

        void Chase()
        {
            if (player == null || !followPlayer || isTeleporting)
                return;

            float distance = Vector2.Distance(transform.position, player.position);

            if (distance > maxRangeFromPlayer)
            {
                print("teleport");
                isMoving = false;
                StopCoroutine(corEndMove);
                TeleportToPlayer();
                return;
            }
            if (isMoving)
                return;

            if (distance > rangeForceChase)
            {
                if (corEndMove!=null)
                StopCoroutine(corEndMove);
                MoveToPlayer();
            }
        }

        void StartMove()
        {
            if (!followPlayer)
                return;
            float range = Random.Range(0.1f, rangeAfterChase);
            float angle = Random.Range(0, 360f);//angle from player
            Vector2 destination = Player.Instance.transform.position + new Vector3(range * Mathf.Cos(angle * Mathf.Deg2Rad), range * Mathf.Sin(angle * Mathf.Deg2Rad));
            if (!isAttacking)
                graphic.localScale = new Vector3((destination.x > transform.position.x) ? -1 : 1, 1, 1);
            isMoving = true;
            float petAngle = Vector2.Angle(destination - (Vector2)transform.position, Vector2.right);
            if (destination.y < transform.position.y)
                petAngle*=-1;
            rb.velocity = new Vector3(speed * Mathf.Cos(petAngle * Mathf.Deg2Rad), speed * Mathf.Sin(petAngle * Mathf.Deg2Rad));
            corEndMove = StartCoroutine(Cor_EndMove(destination));
        }

        IEnumerator Cor_EndMove(Vector2 destination)
        {
            yield return new WaitUntil(() => Vector2.Distance(transform.position, destination) < 0.01f*speed);
            rb.velocity = Vector2.zero;
            isMoving = false;
            yield return new WaitForSeconds(Random.Range(timeBetweenChase[0], timeBetweenChase[1]));
            StartMove();
        }

        void MoveToPlayer()//used in special case
        {
            if (!followPlayer) 
                return;
            Vector2 destination = Player.Instance.transform.position;
            if (!isAttacking)
                graphic.localScale = new Vector3((destination.x > transform.position.x) ? -1 : 1, 1, 1);
            isMoving = true;
            float petAngle = Vector2.Angle(destination - (Vector2)transform.position, Vector2.right);
            if (destination.y < transform.position.y)
                petAngle *= -1;
            rb.velocity = new Vector3(speed * Mathf.Cos(petAngle * Mathf.Deg2Rad), speed * Mathf.Sin(petAngle * Mathf.Deg2Rad));
            corEndMove = StartCoroutine(Cor_EndMove(destination));
        }

        void TeleportToPlayer()
        {
            isTeleporting = true;
            rb.velocity = Vector2.zero;
            transform.DOScale(0, 0.5f).OnComplete(() =>
            {
                ///Vector2 destination = Player.Instance.transform.position + new Vector3(range * Mathf.Cos(angle * Mathf.Deg2Rad), range * Mathf.Sin(angle * Mathf.Deg2Rad));
                transform.position = Player.Instance.transform.position;
                transform.DOScale(1, 0.5f).OnComplete(() => 
                {
                    isTeleporting = false;
                    StartMove();
                });
            });
            
        }
        #endregion

        #region heal
        [SerializeField, Foldout("Heal")] float healLimit = 1 / 3;
        [SerializeField, Foldout("Heal")] float healDelay;
        bool isHealing = false;
        IEnumerator corHeal;
        IEnumerator Cor_Heal()
        {
            if (!buffPet)
                yield break;
            Player playerScript = Player.Instance;
            while (true)
            {
                yield return new WaitUntil(() => followPlayer && !isTeleporting);
                yield return new WaitForSeconds(coolDown);
                if (corAttack == null)
                {
                    corAttack = Cor_Attack();
                    StartCoroutine(corAttack);
                }
                yield return new WaitUntil(() => playerScript.CurrentHealth < playerScript.MaxHealth * healLimit);
                if (corAttack != null)
                {
                    StopCoroutine(corAttack);
                    isAttacking = false;
                }
                playerScript.Heal((int)healAmount, 0);
                StartCoroutine(Cor_HealAnimation());
            }
        }

        IEnumerator Cor_HealAnimation()
        {
            isHealing = true;
            face.sprite = healFace;
            yield return new WaitForSeconds(healDelay);
            face.sprite = normalFace;
            isHealing = false;
        }
        #endregion

        #region attack
        [SerializeField, Foldout("Attack")] float attackRange;
        [SerializeField, Foldout("Attack")] LayerMask enemyMask;
        [SerializeField, Foldout("Attack"),ReadOnly] Enemy enemy;
        [SerializeField, Foldout("Attack"), ReadOnly] Boss boss;
        [SerializeField, Foldout("Attack")] GameObject bullet;
        [SerializeField, Foldout("Attack")] float attackDelay;
        [SerializeField, Foldout("Attack")] Transform spawnBullet;
        IEnumerator corAttack;
        bool isAttacking = false;

        void DetectAttack()
        {
            Collider2D[] enemies = Physics2D.OverlapCircleAll(transform.position, attackRange,enemyMask);
            if (enemies.Length == 0)
            {
                enemy = null;
                boss = null;
                return;
            }

            if (enemy != null && enemy.gameObject.activeInHierarchy)
                if (enemy.Hp > 0)
                    return;
                else if (boss != null && boss.gameObject.activeInHierarchy && boss.Hp > 0)
                    return;

            Enemy tempEnemy = null;
            float minDistance = attackRange;
            foreach (Collider2D enemy in enemies)
            {
                Enemy enemyScr = enemy.GetComponent<Enemy>();
                if (enemyScr == null)
                {
                    Boss bossScr = enemy.GetComponent<Boss>();
                    if (bossScr == null || bossScr.Hp <= 0)
                        continue;
                    boss = bossScr;
                    return;
                }
                if (enemyScr.Hp <= 0)
                    continue;
                float distanceFromCenter = Vector2.Distance(enemy.transform.position, transform.position);
                if (distanceFromCenter < minDistance)
                {
                    minDistance = distanceFromCenter;
                    tempEnemy = enemyScr;
                }
            }
            enemy = tempEnemy;
        }

        IEnumerator Cor_Attack()
        {
            if (!attackPet || !followPlayer)
                yield break;
            while (true)
            {
                yield return new WaitUntil(() => followPlayer && !isTeleporting);
                DetectAttack();
                
                if (enemy == null && boss == null)
                {
                    yield return new WaitForEndOfFrame();
                    continue;
                }
                if (enemy == null)
                    StartCoroutine(Cor_AttackAnimation(boss.transform));
                else
                    StartCoroutine(Cor_AttackAnimation(enemy.transform));

                yield return new WaitForSeconds(coolDown);
            }
        }

        IEnumerator Cor_AttackAnimation(Transform target)
        {
            isAttacking = true;
            graphic.localScale = new Vector3((target.position.x > transform.position.x) ? -1 : 1, 1, 1);
            face.sprite = healFace;
            graphic.transform.DOScale(1.2f, attackDelay).SetEase(Ease.Linear);
            yield return new WaitForSeconds(attackDelay);
            graphic.transform.DOScale(1, 0.1f);
            face.sprite = attackFace;

            float angle = Vector2.Angle(target.position - spawnBullet.position, Vector2.right); ;
            if (target.position.y < transform.position.y)
                angle = 0 - angle;

            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_BOW_ATTACK);
            GameObject clone = Instantiate(bullet, spawnBullet.position, Quaternion.Euler(0, 0, angle));
            PetAttackObject cloneScr = clone.GetComponent<PetAttackObject>();
            cloneScr.Damage = damage;
            cloneScr.Angle = angle;
            cloneScr.Center = spawnBullet.position;

            yield return new WaitForSeconds(0.2f);
            face.sprite = normalFace;
            isAttacking = false;

        }
        #endregion

        #region garden
        Coroutine corResetWander;
        [SerializeField, Foldout("Garden")] Animator bubbleChat;
        Vector2 center, extent;
        bool isWandering = false;

        public void SetUpRange(Vector2 center,Vector2 extent)
        {
            this.center = center;
            this.extent = extent;
        }

        public void Wander()
        {
            Vector2 destination = center + new Vector2(Random.Range(-extent.x, extent.x), Random.Range(-extent.y, extent.y));
            float petAngle = Vector2.Angle(destination - (Vector2)transform.position, Vector2.right);
            if (destination.y < transform.position.y)
                petAngle *= -1;
            rb.velocity = new Vector3(speed * Mathf.Cos(petAngle * Mathf.Deg2Rad), speed * Mathf.Sin(petAngle * Mathf.Deg2Rad));
            corResetWander = StartCoroutine(Cor_ResetWander(destination));
        }

        public void StopWander()
        {
            if (corResetWander!=null)
                StopCoroutine(corResetWander);
            isWandering = false;
        }

        IEnumerator Cor_ResetWander(Vector2 destination)
        {
            yield return new WaitUntil(() => Vector2.Distance(transform.position, destination) < 0.01f * speed);
            rb.velocity = Vector2.zero;
            isWandering = false;
            yield return new WaitForSeconds(Random.Range(timeBetweenChase[0], timeBetweenChase[1]));
            Wander();
        }
        #endregion

        public void Idle()
        {
            StopCoroutine(corEndMove);
            rb.velocity = Vector2.zero;
            isMoving = false;
            StopCoroutine(corHeal);
            StopCoroutine(corAttack);
        }   
        
        public void EndIdle()
        {
            StartMove();

            corHeal = Cor_Heal();
            StartCoroutine(corHeal);

            corAttack = Cor_Attack();
            StartCoroutine(corAttack);
        }    

        private void Awake()
        {
            StartCoroutine(Cor_LoadPlayer());
            LoadStatus();
        }
        void Start()
        {
            StartMove();

            corHeal = Cor_Heal();
            StartCoroutine(corHeal);

            corAttack = Cor_Attack();
            StartCoroutine(corAttack);

            Floating();
        }
        void Update()
        {
            Chase();
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.CompareTag("Wall"))
            {
                if (followPlayer)
                {
                    MoveToPlayer();
                }
            }    
        }

        public void OpenBubbleChat()
        {
            if (followPlayer)
                return;
            bubbleChat.enabled = true;
            bubbleChat.Play("PressF");
            bubbleChat.transform.DOScale(1, 0.5f);
            StopWander();
        }

        public void CloseBubbleChat()
        {
            if (followPlayer)
                return;
            bubbleChat.enabled = false;
            bubbleChat.transform.DOScale(0, 0.5f);
            Wander();
        }
    }
}