using DG.Tweening;
using GDC.Enums;
using GDC.Managers;
using GDC.Pets;
using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

namespace GDC.Gameplay.UI
{
    public class PetPopupPanel : MonoBehaviour
    {
        public static PetPopupPanel Instance { get; private set; }
        [SerializeField] TMP_Text text;
        [SerializeField] Image head,leaf;
        [SerializeField] Sprite[] leafSprite;
        [SerializeField] float animDuration;
        IEnumerator corLeafAnimation;
        private void Awake()
        {
            Instance = this;

            transform.localScale = Vector2.zero;
            gameObject.SetActive(false);
        }

        public void OpenPanel(string text, Sprite head, Sprite[] leafSprite)
        {
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_START_UI);
            gameObject.SetActive(true);
            this.text.text = text;
            this.head.sprite = head;
            this.leafSprite = leafSprite;
            transform.DOScale(1, 0.5f).SetEase(Ease.OutBack);
            corLeafAnimation = Cor_LeafAnimation();
            StartCoroutine(corLeafAnimation);
        }    

        IEnumerator Cor_LeafAnimation()
        {
            int length = leafSprite.Length;
            while (true)
            {
                for (int i=0;i<length;i++)
                {
                    leaf.sprite = leafSprite[i];
                    yield return new WaitForSeconds(animDuration/length);
                }
            }
        }

        public void UsePet()
        {
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_RELEASE_ITEM);
            PetManager.Instance.UsePet();
            transform.DOScale(0, 0.5f).SetEase(Ease.InBack).OnComplete(()=> { gameObject.SetActive(false); });
        }

        public void StorePet()
        {
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_RELEASE_ITEM);
            PetManager.Instance.StorePet();
            transform.DOScale(0, 0.5f).SetEase(Ease.InBack).OnComplete(() => { gameObject.SetActive(false); });
        }
    }
}