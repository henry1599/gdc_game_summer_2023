﻿using DG.Tweening;
using GDC.Enums;
using GDC.Managers;
using NaughtyAttributes;
using RotaryHeart.Lib.SerializableDictionary;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GDC.Pets
{
    [System.Serializable]
    public class PetGrassInfoByType : SerializableDictionaryBase<PetID, PetGrassInfo> { }

    [System.Serializable]
    public class PetGrassInfo
    {
        public RuntimeAnimatorController Controller;
        public Sprite InitialSprite;
    }
    public class PetGrass : MonoBehaviour
    {

        //#region dust
        //[SerializeField, Foldout("Dust")] Sprite[] dust;
        //[SerializeField, Foldout("Dust")] List<SpriteRenderer> dustSprite = new List<SpriteRenderer>();
        //[SerializeField, Foldout("Dust")] float dustDuration;
        //[SerializeField, Foldout("Dust")] Transform dustContainer;
        //[SerializeField, Foldout("Dust")] int numberOfDust;
        //[SerializeField, Foldout("Dust")] float range;
        //[SerializeField, Foldout("Dust")] Vector2 center;

        //[Button]
        //void GenerateDust()
        //{
        //    if (numberOfDust == 0)
        //        return;

        //    dustSprite.Clear();
        //    GameObject template = Instantiate(dustContainer.GetChild(0).gameObject);
        //    while (dustContainer.childCount > 0)
        //    {
        //        DestroyImmediate(dustContainer.GetChild(0).gameObject);
        //    }

        //    for (int i = 0; i < numberOfDust; i++)
        //    {
        //        GameObject clone = Instantiate(template, dustContainer);
        //        clone.name = "Dust (" + i.ToString() + ")";
        //        clone.transform.localPosition = center + new Vector2(
        //            range * Mathf.Cos(360 / numberOfDust * i * Mathf.Deg2Rad),
        //             range * Mathf.Sin(360 / numberOfDust * i * Mathf.Deg2Rad));
        //        dustSprite.Add(clone.GetComponent<SpriteRenderer>());
        //    }

        //    DestroyImmediate(template);
        //}
        //#endregion
        [SerializeField] ParticleSystem vfxDust;
        [SerializeField] Animator anim;
        [SerializeField] SpriteRenderer spriteRenderer;
        [SerializeField] float[] timeBetweenShake = new float[2];
        [SerializeField] int[] shakeTime = new int[2];
        [SerializeField,OnValueChanged("UpdateAnimation")] PetID petID;
        public PetID PetID
        {
            get => petID;
            set
            {
                petID = value;
                UpdateAnimation();
            }
        }
        [SerializeField] PetGrassInfoByType petGrassInfoDict;
        bool hasbeenCaptured = false;
        bool inGarden = false;
        Bounds greenHouseBound;
        [SerializeField] Animator bubbleChat;

        public void SetUpInGarden(Bounds greenHouseBound)
        {
            inGarden = true;
            this.greenHouseBound = greenHouseBound;
        }

        void UpdateAnimation()
        {
            if (petID!=PetID.NONE)
            {
                anim.runtimeAnimatorController = petGrassInfoDict[petID].Controller;
                spriteRenderer.sprite = petGrassInfoDict[petID].InitialSprite;
            }    
        }
        private void Start()
        {
            StartCoroutine(Cor_Shake());
            DetectPet();
        }

        void DetectPet()
        {
            if (inGarden)
                return;
            List<PetID> petAtHome = SaveLoadManager.Instance.GameData.GetPetAtHomeList();
            foreach (PetID id in petAtHome)
            {
                if (petID == id)
                {
                    gameObject.SetActive(false);
                    break;
                }
            }
        }

        IEnumerator Cor_Shake()
        {
            float clipLength = anim.GetCurrentAnimatorClipInfo(0)[0].clip.length;
            while (true)
            {
                anim.speed = 1;
                yield return new WaitForSeconds(clipLength * Random.Range(shakeTime[0], shakeTime[1]+1));
                anim.speed = 0;
                yield return new WaitForSeconds(Random.Range(timeBetweenShake[0], timeBetweenShake[1]));
            }
        }

        IEnumerator Cor_EndGrass()
        {
            yield return new WaitForSeconds(vfxDust.main.startLifetime.constantMax);
            Destroy(gameObject);
        }

        public void SpawnPet()
        {
            if (hasbeenCaptured)
                return;

            Pet tempPet = Instantiate(PetManager.Instance.PetDict[petID].Pet);
            tempPet.transform.localScale = Vector2.zero;
            tempPet.transform.DOScale(1, 0.5f);
            tempPet.transform.position = transform.position;
            hasbeenCaptured = true;
            if (!inGarden)
                PetManager.Instance.AddPet(tempPet);
            else
            {
                tempPet.SetUpRange(greenHouseBound.center, greenHouseBound.extents);
                tempPet.Wander();
                GreenHouse.Instance.PetGrassAtHome.Remove(this);
                GreenHouse.Instance.PetAtHome.Add(tempPet);
            }
            anim.gameObject.SetActive(false);
            bubbleChat.gameObject.SetActive(false);
            vfxDust.Play();
            StartCoroutine(Cor_EndGrass());

        }

        public void OpenBubbleChat()
        {
            if (inGarden)
                return;
            bubbleChat.enabled = true;
            bubbleChat.Play("PressF");
            bubbleChat.transform.DOScale(1, 0.5f);
        }

        public void CloseBubbleChat()
        {
            if (inGarden)
                return;
            bubbleChat.enabled = false;
            bubbleChat.transform.DOScale(0, 0.5f);
        }
    }
}