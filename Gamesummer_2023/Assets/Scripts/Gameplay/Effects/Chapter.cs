using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using GDC.PlayerManager;
using DG.Tweening;
using Gameplay;
using GDC.Managers;
using GDC.Enums;
using GDC.PlayerManager.Hand;

public class Chapter : MonoBehaviour
{
    [SerializeField] SceneType sceneType;
    [SerializeField] int chapterNumber;
    [SerializeField] string chapterTitle;
    [SerializeField] TMP_Text chapterNumberText;
    [SerializeField] TMP_Text chapterTitleText;
    [SerializeField] Image backgroundPanel;
    [SerializeField] List<GameObject> upGameObjects, downGameObjects, leftGameObjects, rightGameObjects;
    Dictionary<GameObject, Vector3> dictionaryUpGameObjects, dictionaryDownGameObjects, dictionaryLeftGameObjects, dictionaryRightGameObjects;
    void Awake()
    {
        chapterNumberText.text = "Chương " + chapterNumber.ToString();
        chapterTitleText.text = chapterTitle;

        dictionaryUpGameObjects = new();
        dictionaryDownGameObjects = new();
        dictionaryLeftGameObjects = new();
        dictionaryRightGameObjects = new();

        foreach (GameObject go in upGameObjects)
        {
            dictionaryUpGameObjects.Add(go, go.transform.position);
            go.transform.position = new Vector3(go.transform.position.x, 7, 0);
        }
        foreach (GameObject go in downGameObjects)
        {
            dictionaryDownGameObjects.Add(go, go.transform.position);
            go.transform.position = new Vector3(go.transform.position.x, -7, 0);
        }
        foreach (GameObject go in leftGameObjects)
        {
            dictionaryLeftGameObjects.Add(go, go.transform.position);
            go.transform.position = new Vector3(-8, go.transform.position.y, 0);
        }
        foreach (GameObject go in rightGameObjects)
        {
            dictionaryRightGameObjects.Add(go, go.transform.position);
            go.transform.position = new Vector3(8, go.transform.position.y, 0);
        }
    }
    void Start()
    {
        if (SaveLoadManager.Instance.GameData.FindSceneTypePass(sceneType))
        {
            //Da tung di vao area1 roi
            gameObject.SetActive(false);
            return;
        }

        chapterNumberText.color = Color.clear;
        chapterTitleText.color = Color.clear;

        CameraController.Instance.isFollowPlayer = false;
        backgroundPanel.color = Color.clear;

        var shadow = Player.Instance.transform.Find("Shadow");
        shadow.gameObject.SetActive(false);

        Player.Instance.InCutScene = true;
        Player.Instance.UseHandMovement = false;
        Player.Instance.UseWeapon = false;
        StartCoroutine(Cor_SetPlayerDirect());

        var backhand = FindObjectOfType<HandMovement>();
        if (backhand != null) {
            var player_graphics_hand = backhand.GetComponentsInChildren<SpriteRenderer>(true);
            foreach (var sprite in player_graphics_hand)
                sprite.sortingLayerName = "UI_World";
        }
        var player_graphics = Player.Instance.GetComponentsInChildren<SpriteRenderer>(true);
        foreach (var sprite in player_graphics)
            sprite.sortingLayerName = "UI_World";

        backgroundPanel.transform.position = new (CameraController.Instance.transform.position.x, CameraController.Instance.transform.position.y, 0);
        backgroundPanel.DOColor(Color.white, 1).OnComplete(() => {
            CameraController.Instance.transform.position = Vector3.zero;
            backgroundPanel.transform.position = Vector3.zero;
            Player.Instance.transform.position = Vector3.zero;
            CameraController.Instance.transform.position = new Vector3(0,0,-10);
            Player.Instance.transform.DOMoveY(-1.5f, 0.5f).OnComplete(() => {
                StartCoroutine(Cor_GameObjectsMove());
                chapterNumberText.DOColor(Color.black, 1)
                    .OnComplete(() => chapterTitleText.DOColor(Color.black, 1));
            });

        });
    }
    IEnumerator Cor_SetPlayerDirect()
    {
        yield return new WaitUntil(() => !HandMovement.Instance.gameObject.activeInHierarchy);
        Player.Instance.graphicOfPlayer.transform.localScale = Vector3.one;
    }
    IEnumerator Cor_GameObjectsMove()
    {
        SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_START_UI);
        foreach (GameObject go in upGameObjects)
        {
            go.transform.DOMoveY(dictionaryUpGameObjects[go].y, 2)
                .OnComplete(() => StartCoroutine(Cor_LittleMove(go)));
        }
        foreach (GameObject go in downGameObjects)
        {
            go.transform.DOMoveY(dictionaryDownGameObjects[go].y, 2)
                .OnComplete(() => StartCoroutine(Cor_LittleMove(go)));
        }
        foreach (GameObject go in leftGameObjects)
        {
            go.transform.DOMoveX(dictionaryLeftGameObjects[go].x, 2)
                .OnComplete(() => StartCoroutine(Cor_LittleMove(go)));
        }
        foreach (GameObject go in rightGameObjects)
        {
            go.transform.DOMoveX(dictionaryRightGameObjects[go].x, 2)
                .OnComplete(() => StartCoroutine(Cor_LittleMove(go)));
        }
        yield return new WaitForSeconds(5);

        Player.Instance.MoveTo(new Vector2(8,-1.5f), 1);
        chapterNumberText.DOColor(Color.clear, 1);
        chapterTitleText.DOColor(Color.clear, 1);

        SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_END_UI);
        foreach (GameObject go in upGameObjects)
        {
            go.transform.DOMoveY(7, 1);
        }
        foreach (GameObject go in downGameObjects)
        {
            go.transform.DOMoveY(-7, 1);
        }
        foreach (GameObject go in leftGameObjects)
        {
            go.transform.DOMoveX(-8, 1);
        }
        foreach (GameObject go in rightGameObjects)
        {
            go.transform.DOMoveX(8, 1);
        }
    }
    IEnumerator Cor_LittleMove(GameObject go)
    {
        for (int i=0; i<3; i++)
        {
            go.transform.DOMoveY(go.transform.position.y + 0.1f, 0.5f)
                .OnComplete(() => go.transform.DOMoveY(go.transform.position.y - 0.1f, 0.5f));
            yield return new WaitForSeconds(1);
        }
    }
}
