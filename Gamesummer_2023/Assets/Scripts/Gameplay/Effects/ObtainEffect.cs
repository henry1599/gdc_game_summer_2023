using DG.Tweening;
using GDC.Configuration;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GDC.Gameplay.Effect
{
    public class ObtainEffect : MonoBehaviour
    {
        [SerializeField] Transform effect1, effect2;
        [SerializeField] SpriteRenderer spriteRenderer;

        private void Update()
        {
            effect1.Rotate(0, 0, 25 * Time.deltaTime);
            effect2.Rotate(0, 0, -25 * Time.deltaTime);
        }
        public void ShowItem(SO_Item so_item)
        {
            transform.localScale = Vector3.zero;
            transform.DOScale(1, 0.5f);
            spriteRenderer.sprite = so_item.sprite;
            if (so_item.itemType == Enums.ItemType.WEAPON)
            {
                spriteRenderer.transform.localPosition = new Vector3(-0.35f, -0.27f, 0);
            }    
            else
            {
                if (so_item.ID == "k000") // new la branch
                {
                    spriteRenderer.transform.localPosition = new Vector3(-0.35f, -0.27f, 0);
                }
                else
                {
                    spriteRenderer.transform.localPosition = Vector3.zero;
                }
            }
        }
        public void HideItem()
        {
            transform.DOScale(0, 0.5f).OnComplete(() => gameObject.SetActive(false));
        }
    }
}