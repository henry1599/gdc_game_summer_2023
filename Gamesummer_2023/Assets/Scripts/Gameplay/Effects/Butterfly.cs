using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.PlayerManager;
using DG.Tweening;

public class Butterfly : MonoBehaviour
{
    [SerializeField] float cornerX, cornerY, speed;
    Vector2 firstPos, currentPos;
    bool isMoving, isMovingToAnotherPos, previousTouch;
    Coroutine cor;
    float angle = 0f;
    Vector2 center;
    int clockwise = 1;
    void Start()
    {
        firstPos = transform.position;
        currentPos = firstPos;

        Animator anim = GetComponentInChildren<Animator>();
        int animCount = anim.runtimeAnimatorController.animationClips.Length;
        int ranIdx = Random.Range(0, animCount);
        string temp = anim.runtimeAnimatorController.animationClips[ranIdx].ToString();
        string animName = temp.Split(' ')[0];
        anim.Play(animName);
    }
    void Update()
    {
        bool isTouching = Vector2.Distance(Player.Instance.transform.position, firstPos) <= 2;
        CheckTouch(isTouching);
        MoveAround();
    }
    void CheckTouch(bool isTouching)
    {
        if (!isMovingToAnotherPos && isTouching != previousTouch)
        {
            if (cor != null) StopCoroutine(cor);
            FlyToAnotherPos(isTouching);
            previousTouch = isTouching;
        }
    }
    void MoveAround()
    {
        if (!isMoving)
        {
            float x = Random.Range(currentPos.x - cornerX, currentPos.x + cornerX);
            float y = Random.Range(currentPos.y - cornerY, currentPos.y + cornerY);
            cor = StartCoroutine(MoveTo(new(x,y)));
        }
    }
    void FlyToAnotherPos(bool touch)
    {
        isMovingToAnotherPos = true;
        if (touch)
        {
            currentPos = firstPos + Random.insideUnitCircle.normalized * 3;
            cor = StartCoroutine(MoveTo(currentPos));
        }
        else 
        {
            currentPos = firstPos;
            cor = StartCoroutine(MoveTo(currentPos));
        }
    }
    IEnumerator MoveTo(Vector2 pos)
    {
        float radius = 0;
        FindCenter(transform.position, pos, ref radius);
        isMoving = true;
        clockwise *= -1;

        float deltaX = transform.position.x - center.x;
        float deltaY = transform.position.y - center.y;
        angle = Mathf.Atan2(deltaY, deltaX);

        while (Vector2.Distance(pos, transform.position) > 0.05f)
        {
            float x = center.x + radius * Mathf.Cos(angle);
            float y = center.y + radius * Mathf.Sin(angle);
            transform.position = new(x, y);
            yield return new WaitForSeconds(Time.deltaTime);
            angle += speed / radius * Time.deltaTime * clockwise;
        }
        transform.position = pos;
        isMoving = false;
        isMovingToAnotherPos = false;
    }
    void FindCenter(Vector2 pointA, Vector2 pointB, ref float radius)
    {
        float distance = Vector2.Distance(pointA, pointB);
        while (distance > 2f * radius)
        {
            radius += 0.1f;
        }
        Vector2 midpoint = (pointA + pointB) / 2f;
        Vector2 direction = (pointB - pointA).normalized;

        Vector2 perpendicular = new(-direction.y, direction.x);
        Vector2 displacement = perpendicular * Mathf.Sqrt(radius*radius - distance*distance/4f);
        center = midpoint + displacement;
    }
}
