using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DamageTextPopup : MonoBehaviour
{
    [SerializeField] private Animator anim;
    [SerializeField] private TextMeshPro textMP;
    
    public void SetText(int damageValue)
    {
        textMP.text = damageValue.ToString();
    }

    public void NoEffect()
    {
        anim.Play("noEffect");
    }

    public void NormalEffect()
    {
        anim.Play("normalEffect");
    }

    public void SuperEffect()
    {
        anim.Play("superEffect");
    }

    public void CriticalEffect()
    {
        anim.Play("criticalEffect");
    }

    public void HealHeartEffect()
    {
        anim.Play("healHeartEffect");
    }
    public void HealStaminaEffect()
    {
        anim.Play("healStaminaEffect");
    }
}
