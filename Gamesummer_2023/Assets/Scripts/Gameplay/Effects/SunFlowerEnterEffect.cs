using GDC.PlayerManager;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GDC.Gameplay.Effect
{
    public class SunFlowerEnterEffect : MonoBehaviour
    {
        Vector3 lastpos;
        [SerializeField] SpriteRenderer spriteRenderer;
        [SerializeField] bool isFollowPlayer;
        bool isSprite1 = true;
        [SerializeField] Sprite sprite1, sprite2;
        [SerializeField] float distanceChangeSprite;

        // Start is called before the first frame update
        void Start()
        {
            if (isFollowPlayer)
            {
                lastpos = Player.Instance.transform.position;
            }    
            else
            {
                lastpos = transform.position;
            }
            spriteRenderer.color = Color.clear;
        }

        // Update is called once per frame
        void Update()
        {
            if (isFollowPlayer)
            {
                transform.position = Player.Instance.transform.position + new Vector3(0,-0.05f, 0);
            }    
            if (Vector2.SqrMagnitude(transform.position - lastpos)>distanceChangeSprite)
            {
                lastpos = transform.position;
                if (isSprite1)
                    spriteRenderer.sprite = sprite2;
                else
                    spriteRenderer.sprite = sprite1;
                isSprite1 = !isSprite1;
            }    
        }
        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.CompareTag("SunFlower"))
            {
                spriteRenderer.color = Color.white;
            }
        }
        private void OnTriggerExit2D(Collider2D collision)
        {
            if (collision.CompareTag("SunFlower"))
            {
                spriteRenderer.color = Color.clear;
            }
        }
    }
}
