using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.PlayerManager;
using GDC.Managers;

namespace Gameplay
{
    public class CameraController : MonoBehaviour
    {
        public static CameraController Instance { get; private set; }
        public float maxX = 0, minX = 0, maxY = 0, minY = 0;
        public float moveSpeed;

        public bool isFollowPlayer, isNotCheckBound;
        public Collider2D coll;
        [HideInInspector] public bool isMoveToNewBound;
        //bool isScoll = false;

        Rigidbody2D rigi;

        Vector3 target;
        // Start is called before the first frame update
        void Awake()
        {
            Instance = this;
            coll = GetComponent<Collider2D>();
            //anim = GetComponent<Animator>();
            //rigi = GetComponent<Rigidbody2D>();
        }

        // Update is called once per frame
        void Update()
        {
            if (isMoveToNewBound == false)
            {
                if (Player.Instance != null && isFollowPlayer)
                {
                    // Tao camera di chuyen theo targetObj
                    target = Player.Instance.transform.position;
                    target.z -= 12;

                    if (target.x < minX) target.x = minX;
                    if (target.x >= maxX) target.x = maxX;
                    if (target.y < minY) target.y = minY;
                    if (target.y >= maxY) target.y = maxY;

                    transform.position = Vector3.Lerp(transform.position, target, moveSpeed);
                }
                else if (isFollowPlayer == false)
                {
                    CheckPos();
                }
            }
        }
        public void CheckPos()
        {
            if (isNotCheckBound) return;

            Vector3 curPos = transform.position;
            if (transform.position.x < minX) curPos.x = minX;
            if (transform.position.x >= maxX) curPos.x = maxX;
            if (transform.position.y < minY) curPos.y = minY;
            if (transform.position.y >= maxY) curPos.y = maxY;
            curPos.z = -12;
            transform.position = curPos;
        }
        public void MoveToNewBound(float minX, float maxX, float minY, float maxY, float x = -99999, float y = -99999, float duration = 0.5f)
        {
            isMoveToNewBound = true;
            this.minX = minX;
            this.maxX = maxX;
            this.minY = minY;
            this.maxY = maxY;

            if (x > -99990 && y > -99990)
            {
                Vector3 targetPos = new Vector3(x, y, 0);
                if (transform.position.x < minX) targetPos.x = minX;
                if (transform.position.x >= maxX) targetPos.x = maxX;
                if (transform.position.y < minY) targetPos.y = minY;
                if (transform.position.y >= maxY) targetPos.y = maxY;
                targetPos.z = -12;
                transform.DOMove(targetPos, duration).OnComplete(() => isMoveToNewBound = false);
            }
            else
            {
                isMoveToNewBound = false;
            }

            SaveLoadManager.Instance.GameData.CamMaxX = this.maxX;
            SaveLoadManager.Instance.GameData.CamMaxY = this.maxY;
            SaveLoadManager.Instance.GameData.CamMinX = this.minX;
            SaveLoadManager.Instance.GameData.CamMinY = this.minY;
        }
        public Vector2 CameraPos
        {
            get { return transform.position; }
            set
            {
                transform.position = value;
                transform.position -= new Vector3(0, 0, 10);
            }
        }

        IEnumerator Cor_FollowPlayer(float sec)
        {
            yield return new WaitForSeconds(sec);
            isFollowPlayer = true;
        }
    }
}
