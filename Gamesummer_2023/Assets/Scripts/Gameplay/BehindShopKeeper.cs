using GDC.Gameplay;
using GDC.Gameplay.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BehindShopKeeper : MonoBehaviour
{
    [SerializeField] Dialogue defaultDialogue, newDialogue;
    [SerializeField] NPC npc;
    // Start is called before the first frame update
    void Start()
    {
        defaultDialogue = npc.GetDialogue();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            npc.SetDialogue(newDialogue);
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            npc.SetDialogue(defaultDialogue);
        }
    }
}
