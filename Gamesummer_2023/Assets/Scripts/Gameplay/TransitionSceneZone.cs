using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.Common;
using GDC.Enums;
using GDC.Managers;
using NaughtyAttributes;
using Gameplay;
using GDC.PlayerManager;
using GDC.Gameplay.UI;
using AudioPlayer;

namespace GDC.Gameplay
{
    public class TransitionSceneZone : MonoBehaviour
    {
        [SerializeField] SceneType sceneType;
        [SerializeField] TransitionType transitionType;
        [SerializeField] TransitionLoadSceneType transitionLoadSceneType;
        [SerializeField, ShowIf("transitionLoadSceneType", TransitionLoadSceneType.RELOAD_WITHOUT_TRANSITION)]
        float newMinX, newMaxX, newMinY, newMaxY, oldMinX, oldMaxX, oldMinY, oldMaxY;
        [SerializeField, ShowIf("transitionLoadSceneType", TransitionLoadSceneType.RELOAD_WITH_TRANSITION)] SoundID musicIDBegin, musicID;

        [SerializeField] bool isChangeSoundMap;
        [SerializeField, ShowIf("isChangeSoundMap")] SoundType soundType;
        [SerializeField] Vector2 playerNewPos;

        [Header("Only for change camera bound")]
        [SerializeField] bool isChangeCameraBound;
        [SerializeField, ShowIf("isChangeCameraBound")] float camMinX, camMaxX, camMinY, camMaxY;

        [SerializeField] GameObject startAreaIntro;

        bool isTransition = false;

        [Button]
        public void TransitionScene() //Tru truong hop RELOAD_WITHOUT_TRANSITION
        {
            if (Player.Instance != null)
            {
                Player.Instance.InCutScene = true; //just freeze player movement
            }

            isTransition = true;
            if (startAreaIntro != null)
            {
                StartCoroutine(Cor_StartAreaIntro());
            }
            else
            {
                SetupTransition();
            }
        }
        void SetupTransition()
        {
            SaveLoadManager.Instance.CacheData.playerInitPos = playerNewPos;
            SaveLoadManager.Instance.CacheData.isChangeCameraBound = isChangeCameraBound;
            if (isChangeCameraBound)
            {
                SaveLoadManager.Instance.CacheData.camMaxX = camMaxX;
                SaveLoadManager.Instance.CacheData.camMinX = camMinX;
                SaveLoadManager.Instance.CacheData.camMaxY = camMaxY;
                SaveLoadManager.Instance.CacheData.camMinY = camMinY;
                SaveLoadManager.Instance.GameData.CamMaxX = camMaxX;
                SaveLoadManager.Instance.GameData.CamMaxY = camMaxY;
                SaveLoadManager.Instance.GameData.CamMinX = camMinX;
                SaveLoadManager.Instance.GameData.CamMinY = camMinY;
            }
            SaveLoadManager.Instance.GameData.PlayerHp = Player.Instance.CurrentHealth;
            SaveLoadManager.Instance.GameData.PlayerStamina = Player.Instance.Stamina;

            if (transitionLoadSceneType == TransitionLoadSceneType.NEW_SCENE)
            {
                if (isChangeSoundMap)
                {
                    GameManager.Instance.LoadSceneAsyncManually(
                        sceneType, 
                        transitionType, 
                        soundType,
                        cb: ()=>
                        {
                            GameManager.Instance.UnloadSceneManually(
                                SaveLoadManager.Instance.GameData.CurrentSceneType,
                                cb: () => GameManager.Instance.SetInitData()
                            ) ;
                        },
                        true);
                }
                else GameManager.Instance.LoadSceneAsyncManually(
                        sceneType,
                        transitionType,
                        cb: () =>
                        {
                            GameManager.Instance.UnloadSceneManually(
                                SaveLoadManager.Instance.GameData.CurrentSceneType,
                                cb: () => GameManager.Instance.SetInitData()
                            );
                        });
            }
            if (transitionLoadSceneType == TransitionLoadSceneType.RELOAD_WITH_TRANSITION)
            {
                GameManager.Instance.ReLoadSceneManually(
                    transitionType,
                    cb: () =>
                    {
                        GameManager.Instance.SetInitData();
                        if (musicIDBegin != SoundID._____COMMON_____)
                        {
                            SoundManager.Instance.PlayMusicWithIntro(musicIDBegin, musicID);
                        }
                        else
                        {
                            if (musicID != SoundID._____COMMON_____)
                            {
                                SoundManager.Instance.PlayMusic(musicID);
                            }
                        }
                    }
                    );
            }
        }
        IEnumerator Cor_StartAreaIntro()
        {
            startAreaIntro.SetActive(true);
            if (!SaveLoadManager.Instance.GameData.FindSceneTypePass(sceneType))
            {
                yield return new WaitForSeconds(8);
            }
            else yield return null;
            SetupTransition();
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.CompareTag("Player") && GameManager.Instance.isLoadSceneComplete && transitionLoadSceneType != TransitionLoadSceneType.RELOAD_WITHOUT_TRANSITION)
            {
                TransitionScene();
            }
        }
        private void OnTriggerExit2D(Collider2D collision)
        {
            if (collision.CompareTag("Player"))
            {
                if (transitionLoadSceneType != TransitionLoadSceneType.RELOAD_WITHOUT_TRANSITION)
                        return;

                if (transitionType == TransitionType.UP || transitionType == TransitionType.DOWN)
                {
                    if (collision.transform.position.y > transform.position.y)
                    {
                        CameraController.Instance.MoveToNewBound(newMinX, newMaxX, newMinY, newMaxY);
                    }
                    else
                    {
                        CameraController.Instance.MoveToNewBound(oldMinX, oldMaxX, oldMinY, oldMaxY);
                    }
                }
                else if (transitionType == TransitionType.RIGHT || transitionType == TransitionType.LEFT)
                {
                    if (collision.transform.position.x > transform.position.x)
                    {
                        CameraController.Instance.MoveToNewBound(newMinX, newMaxX, newMinY, newMaxY);
                    }
                    else
                    {
                        CameraController.Instance.MoveToNewBound(oldMinX, oldMaxX, oldMinY, oldMaxY);
                    }
                }
            }
        }
    }
}
