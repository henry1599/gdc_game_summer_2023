using DG.Tweening;
using GDC.Gameplay.UI;
using GDC.Managers;
using GDC.PlayerManager;
using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Bed : MonoBehaviour
{

    [SerializeField] float sleepDuration,fadeDuration;
    [SerializeField] Image blackScreen;
    [SerializeField] Vector2 sleepPos, wakeUpPos;
    [SerializeField] BoxCollider2D col;
    [SerializeField] Transform bubbleChat;
    [SerializeField] bool isHeadInLeft;
    bool playerIn = false;
    public void Sleep()
    {
        //bubbleChat.localScale = Vector3.zero;
        playerIn = false;
        SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_INTERACT);
        bubbleChat.DOScale(0, 0.5f);
        Player player = Player.Instance;
        player.LieDown(sleepPos, isHeadInLeft);
        col.isTrigger = true;
        blackScreen.DOFade(1, fadeDuration).OnComplete(() =>
        {
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_HEAL);
            player.CurrentHealth = player.MaxHealth;
            player.Stamina = player.MaxStamina;
            FullScreenEffect.Instance.EndLowHP();
            blackScreen.DOFade(0, fadeDuration).SetDelay(sleepDuration).OnComplete(() =>
            {
                player.StandUp(wakeUpPos);
                col.isTrigger = false;
            });
        });
    }

    private void Start()
    {
        bubbleChat.GetComponent<Animator>().Play("PressF");
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.CompareTag("Player"))
        {
            playerIn = true;
            bubbleChat.DOScale(1, 0.5f); 
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.transform.CompareTag("Player"))
        {
            playerIn = false;
            bubbleChat.DOScale(0, 0.5f);
        }
    }
    private void Update()
    {
        if (Input.GetKeyUp(KeyCode.F) && playerIn)
            Sleep();
    }
}
