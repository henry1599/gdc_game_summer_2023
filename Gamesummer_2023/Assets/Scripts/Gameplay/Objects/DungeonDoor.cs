using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Gameplay;
using GDC.Gameplay;
using NaughtyAttributes;
using GDC.PlayerManager;
using DG.Tweening;
using GDC.PlayerManager.Hand;
using GDC.Managers;
using GDC.Gameplay.UI;
using System.Linq;

public enum DungeonDoorType
{
    LEFT,
    RIGHT,
    UP,
    DOWN
}

public class DungeonDoor : MonoBehaviour
{
    public int ID;
    [SerializeField] bool haveChest;
    [SerializeField] Animator[] unlockSignals;
    [SerializeField, HideIf("haveChest")] float minXNext, maxXNext, minYNext, maxYNext, xNext, yNext, playerOffset;
    [SerializeField, HideIf("haveChest")] DungeonDoorType dungeonDoorType;
    [SerializeField, HideIf("haveChest")] DungeonDoor[] nextDungeonDoors;
    [SerializeField, HideIf("haveChest")] DungeonDoor[] thisDungeonDoors;
    [SerializeField] List<GameObject> enemies;
    [Header("Camera settings")]
    [SerializeField] int delayIdx;
    [SerializeField] bool lastDelay, isNotFade;
    Animator anim;
    bool finishing, done, appear;
    SpriteRenderer[] player_graphics, player_graphics_hand;
    List<float> fade = new(), fade_hand = new ();

    void Start()
    {
        anim = GetComponent<Animator>();
    }

    void Update()
    {
        if (!finishing)
        {
            if (CheckFinishDungeon())
            {
                finishing = true;
                StartCoroutine(Unlock());
            }
        }
    }
    public void AlreadyOpen()
    {
        anim = GetComponent<Animator>();
        finishing = true;
        foreach (var signal in unlockSignals)
            signal.Play("unlock");
        if (!haveChest)
        {
            if (anim != null) 
                anim.Play("door_open");
        }
        done = true;
    }
    bool CheckFinishDungeon()
    {
        if (enemies.Count == 0) 
        {
            done = true;
            return false;
        }
        foreach(var enemy in enemies)
        {
            if (enemy.activeInHierarchy) return false;
        }
        return true;
    }
    IEnumerator Unlock()
    {
        CameraController.Instance.isFollowPlayer = false;
        Player.Instance.ToIdle();
        Player.Instance.InCutScene = true;
        yield return new WaitForSeconds(delayIdx * 2.5f);
        CameraController.Instance.transform.DOMove(transform.position, 1.5f);
        yield return new WaitForSeconds(1.5f);
        foreach (var signal in unlockSignals)
        {
            signal.Play("unlock");
        }
        if (unlockSignals.Length > 0)
            SoundManager.Instance.PlaySoundRelatedToCamera(transform.position, AudioPlayer.SoundID.SFX_LOOT);
        yield return new WaitForSeconds(0.5f);
        if (!haveChest)
        {
            if (anim != null) anim.Play("door_open");
            SoundManager.Instance.PlaySoundRelatedToCamera(transform.position, AudioPlayer.SoundID.SFX_DOOR_OPEN);
            SaveLoadManager.Instance.GameData.SetDoor(UIGameplay.Instance.areaType, ID);
        }
        else 
        {
            if (!appear)
            {
                appear = true;
                GetComponent<Chest>()?.Appear();
            }
        }
        yield return new WaitForSeconds(0.5f);
        done = true;
        if (lastDelay) 
        {
            yield return new WaitForSeconds(0.5f);
            CameraController.Instance.isFollowPlayer = true;
            Player.Instance.InCutScene = false;
        }
    }
    void OnCollisionStay2D(Collision2D collision)
    {
        if (haveChest) return;
        var isMoveToNewBound = CameraController.Instance.isMoveToNewBound;

        var newPos = dungeonDoorType switch
        {
            DungeonDoorType.UP => (Vector2)(transform.position + new Vector3(0, playerOffset)),
            DungeonDoorType.DOWN => (Vector2)(transform.position - new Vector3(0, playerOffset)),
            DungeonDoorType.LEFT => (Vector2)(transform.position - new Vector3(playerOffset, 0)),
            _ => (Vector2)(transform.position + new Vector3(playerOffset, 0))
        };

        if (collision.gameObject.CompareTag("Player") && done && !isMoveToNewBound)
        {
            CameraController.Instance.MoveToNewBound(minXNext, maxXNext, minYNext, maxYNext, xNext, yNext, 1.5f);
            Player.Instance.MoveTo(newPos, 1.5f);
            if (!isNotFade) StartCoroutine(Cor_FadeShowPlayer());
            foreach (var dungeon in nextDungeonDoors)
                dungeon.enabled = true;
            foreach (var dungeon in thisDungeonDoors)
                dungeon.enabled = false;
        }
    }
    IEnumerator Cor_FadeShowPlayer()
    {
        FadePlayer();
        yield return new WaitForSeconds(1.32f);
        ShowPlayer();
    }
    void FadePlayer()
    {
        player_graphics = Player.Instance.GetComponentsInChildren<SpriteRenderer>();
        fade = new List<float>();
        foreach (var graphic in player_graphics)
        {
            fade.Add(graphic.color.a);
            graphic.DOFade(0f, 0.25f);
        }

        var backhand = FindObjectOfType<HandMovement>();
        if (backhand != null)
        {
            player_graphics_hand = backhand.GetComponentsInChildren<SpriteRenderer>();
            fade_hand = new List<float>();
            foreach (var graphic_hand in player_graphics_hand)
            {
                fade_hand.Add(graphic_hand.color.a);
                graphic_hand.DOFade(0f, 0.25f);
            }
        }
    }
    void ShowPlayer()
    {
        for (int i=0; i<fade.Count; i++)
            player_graphics[i].DOFade(fade[i], 0.18f);
        for (int i=0; i<fade_hand.Count; i++)
            player_graphics_hand[i].DOFade(fade_hand[i], 0.18f);
    }
}