using DG.Tweening;
using GDC.Configuration;
using GDC.Gameplay.UI;
using GDC.Managers;
using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using UnityEngine;

namespace GDC.Gameplay
{
    public class Chest : MonoBehaviour
    {
        public int ID;
        [SerializeField] List<GameObject> items;
        [SerializeField] Animator graphicAnim, bubbleChatAnim;
        [SerializeField] Collider2D objCollision, objCollider;
        [SerializeField] GameObject vfx_open;
        [SerializeField] bool isHide, isBossDoor, isLock;
        [SerializeField, ShowIf("isLock")] SO_Item keyItem;
        bool isPlayerNear, opened;
        [ReadOnly]
        public bool isAlreayGet;

        //private void Start()
        //{
        //    if (isHide)
        //    {
        //        graphicAnim.gameObject.SetActive(false);
        //        objCollision.enabled = false;
        //    }
        //}
        public void Setup()
        {
            if (isHide)
            {
                graphicAnim.gameObject.SetActive(false);
                objCollision.enabled = false;
                if (objCollider!=null)
                    objCollider.enabled = false;
            }
        }
        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.F))
            {
                CheckOpen();
            }
        }

        void CheckOpen()
        {
            if (isPlayerNear == false) return;
            if (isLock)
            {
                if (keyItem == null)
                {
                    Debug.LogError("Chua gan keyItem!");
                    return;
                }
                if (SaveLoadManager.Instance.GameData.GetAmountOfItem(keyItem)>0)
                {
                    SaveLoadManager.Instance.GameData.ReduceItem(keyItem, 1);
                    if (isBossDoor) OpenBossDoor();
                    else Open();
                    return;
                }
                else
                {
                    SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_UNLOCK);
                    graphicAnim.Play("lockShake");
                }
            }
            else
            {
                Open();
            }    
        }
        public void Open()
        {
            if (graphicAnim.gameObject.activeSelf == false) return;
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_APPEAR);
            SaveLoadManager.Instance.GameData.SetChest(UIGameplay.Instance.areaType, ID);
            Instantiate(vfx_open, transform.position + new Vector3(0, 0.165f, 0), Quaternion.identity);
            foreach (var item in items)
            {
                Instantiate(item, transform.position, Quaternion.identity);
            }

            graphicAnim.gameObject.SetActive(false);
            objCollision.enabled = false;
            if (objCollider != null)
                objCollider.enabled = false;
            BubbleChatHide();
        }
        void OpenBossDoor()
        {
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_UNLOCK);
            graphicAnim.Play("unlock");
            opened = true;
            objCollision.enabled = false;
            if (objCollider != null)
                objCollider.enabled = false;
            // GetComponent<DungeonDoor>().enabled = true;
            BubbleChatHide();
        }
        // [Button]
        public void Appear()
        {
            if (isAlreayGet)
            {
                Debug.Log(gameObject.name + ": Chest nay da duoc mo roi!");
                gameObject.SetActive(false);
                return;
            }

            graphicAnim.gameObject.SetActive(true);
            objCollision.enabled = true;
            if (objCollider != null)
                objCollider.enabled = true;

            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_APPEAR);
            Instantiate(vfx_open, transform.position + new Vector3(0, 0.165f, 0), Quaternion.identity);
            graphicAnim.transform.localScale = Vector2.zero;
            graphicAnim.transform.DOScaleY(1.2f, 0.2f);
            graphicAnim.transform.DOScaleX(0.8f, 0.2f).OnComplete(() =>
            {
                graphicAnim.transform.DOScaleX(1, 0.1f);
                graphicAnim.transform.DOScaleY(1, 0.1f);
            });
        }
        void BubbleChatShow()
        {
            bubbleChatAnim.gameObject.SetActive(true);
            bubbleChatAnim.Play("PressF");
            bubbleChatAnim.transform.DOScaleX(0.6f, 0.15f);
            bubbleChatAnim.transform.DOScaleY(1.4f, 0.15f).OnComplete(() =>
            {
                bubbleChatAnim.transform.DOScale(1f, 0.1f);
            });
        }
        void BubbleChatHide()
        {
            bubbleChatAnim.transform.DOScaleX(0.6f, 0.1f);
            bubbleChatAnim.transform.DOScaleY(1.4f, 0.1f).OnComplete(() =>
            {
                bubbleChatAnim.transform.DOScale(0f, 0.15f).OnComplete(() =>
                {
                    if (isPlayerNear == false)
                    {
                        bubbleChatAnim.transform.gameObject.SetActive(false);
                    }
                });
            });
        }
        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.CompareTag("Player") && graphicAnim.gameObject.activeSelf && !opened)
            {
                isPlayerNear = true;
                BubbleChatShow();
            }
        }

        private void OnTriggerExit2D(Collider2D collision)
        {
            if (collision.CompareTag("Player"))
            {
                BubbleChatHide();
                isPlayerNear = false;
            }
        }
    }
}
