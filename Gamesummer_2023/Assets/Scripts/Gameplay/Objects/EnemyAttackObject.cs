using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.PlayerManager;

public class EnemyAttackObject : MonoBehaviour
{
    [SerializeField] private float atk;
    [SerializeField] private float spellEffectDuration;

    [SerializeField] float gizmosRadius;
    [SerializeField] bool isUseGizmos, isDisappearWhenHit;

    private void Update()
    {
        DetectGizmos();
    }
    void DetectGizmos()
    {
        if (isUseGizmos)
        {
            Collider2D hit = Physics2D.OverlapCircle(transform.position, gizmosRadius);
            if (hit.CompareTag("Player"))
            {
                Player.Instance.GetHit(transform.position, atk, spellEffectDuration, gameObject.layer);
            }
        }
    }
    public void Disappear()
    {
        //May be add some effect here
        Destroy(gameObject);
        // gameObject.SetActive(false); //hoac co the setActive(false) neu pool
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (!this.enabled) return;
        if (collision.gameObject.CompareTag("Wall") || collision.gameObject.CompareTag("Boulder"))
        {
            if (isDisappearWhenHit == true) Disappear();
        }
        if (collision.CompareTag("Player"))
        {
            Player.Instance.GetHit(transform.position, atk, spellEffectDuration, gameObject.layer);
            if (isDisappearWhenHit == true) Disappear();
        }
        if (collision.CompareTag("PlayerAttack"))
        {
            if (isDisappearWhenHit == true) Disappear();
        }
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (!this.enabled) return;
        if (collision.gameObject.CompareTag("Wall") || collision.gameObject.CompareTag("Boulder"))
        {
            if (isDisappearWhenHit == true) Disappear();
        }    
        if (collision.gameObject.CompareTag("Player"))
        {
            Player.Instance.GetHit(transform.position, atk, spellEffectDuration, gameObject.layer);
            if (isDisappearWhenHit == true) Disappear();
        }
        if (collision.gameObject.CompareTag("PlayerAttack"))
        {
            if (isDisappearWhenHit == true) Disappear();
        }
    }
    private void OnDrawGizmos()
    {
        Gizmos.DrawSphere(transform.position, gizmosRadius);
    }
}
