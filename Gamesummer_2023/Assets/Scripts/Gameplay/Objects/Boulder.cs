using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using GDC.Managers;

namespace GDC.Gameplay
{
    public class Boulder : MonoBehaviour
    {
        [SerializeField] Animator anim;
        [SerializeField] LayerMask stuckLayerMask, waterLayerMask;
        [SerializeField] GameObject vfx_waterSplash;
        [SerializeField] SpriteRenderer shadow;
        [SerializeField] bool isMoving;
        public void Move(Vector2 dashVelo, Vector3 playerPos) //dashVelo la toc do cua player dash vao tang da
        {
            if (isMoving) return;

            float absX = Mathf.Abs(playerPos.x - transform.position.x);
            float absY = Mathf.Abs(playerPos.y - transform.position.y);
            if (dashVelo.x > 0 && absX > absY) 
            {
                if (Physics2D.OverlapBox((Vector2)transform.position + Vector2.right + Vector2.up/2, new Vector2(0.9f, 0.9f), 0, stuckLayerMask) == null)
                {
                    SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_HIT_BOULDER);
                    isMoving = true;
                    anim.Play("MoveRight");
                    transform.DOMoveX(transform.position.x + 1, 0.33f).OnComplete(() =>
                    {
                        if (CheckWater())
                        {
                            transform.DOMoveX(transform.position.x + 0.5f, 0.1f);
                            Drown();
                        }
                        isMoving = false;
                    });
                }
            }
            if (dashVelo.x < 0 && absX > absY) 
            {
                if (Physics2D.OverlapBox((Vector2)transform.position + Vector2.left + Vector2.up/2, new Vector2(0.9f, 0.9f), 0, stuckLayerMask) == null)
                {
                    SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_HIT_BOULDER);
                    isMoving = true;
                    anim.Play("MoveLeft");
                    transform.DOMoveX(transform.position.x - 1, 0.33f).OnComplete(() =>
                    {
                        if (CheckWater())
                        {
                            transform.DOMoveX(transform.position.x - 0.5f, 0.1f);
                            Drown();
                        }
                        isMoving = false;
                    });
                }
            }
            if (dashVelo.y > 0 && absY > absX) 
            {
                if (Physics2D.OverlapBox((Vector2)transform.position + Vector2.up * 1.5f, new Vector2(0.9f, 0.9f), 0, stuckLayerMask) == null)
                {
                    SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_HIT_BOULDER);
                    isMoving = true;
                    anim.Play("MoveUp");
                    transform.DOMoveY(transform.position.y + 1, 0.33f).OnComplete(() =>
                    {
                        if (CheckWater())
                        {
                            transform.DOMoveY(transform.position.y + 0.5f, 0.1f);
                            Drown();
                        }
                        isMoving = false;
                    });
                }
            }
            if (dashVelo.y < 0 && absY > absX) 
            {
                if (Physics2D.OverlapBox((Vector2)transform.position + Vector2.down/2, new Vector2(0.9f, 0.9f), 0, stuckLayerMask) == null)
                {
                    SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_HIT_BOULDER);
                    isMoving = true;
                    anim.Play("MoveDown");
                    transform.DOMoveY(transform.position.y - 1, 0.33f).OnComplete(() =>
                    {
                        if (CheckWater())
                        {
                            transform.DOMoveY(transform.position.y - 0.5f, 0.1f);
                            Drown();
                        }
                        isMoving = false;
                    });
                }
            }
        }
        bool CheckWater()
        {
            return Physics2D.OverlapBox((Vector2)transform.position + Vector2.up / 2, new Vector2(0.3f, 0.3f), 0, waterLayerMask);
        }
        void Drown()
        {
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_WATER_SPLASH);
            anim.Play("Drown");
            //Instantiate(vfx_waterSplash, transform.position + Vector3.up / 2, Quaternion.identity);
            vfx_waterSplash.SetActive(true);
            shadow.DOColor(Color.clear, 0.5f);
            StartCoroutine(Cor_ActiveFalse());
        }
        IEnumerator Cor_ActiveFalse()
        {
            yield return new WaitForSeconds(1);
            gameObject.SetActive(false);
        }    
        //private void OnDrawGizmos()
        //{
        //    Gizmos.DrawCube((Vector2)transform.position + Vector2.up * 1.5f, new Vector2(0.9f, 0.9f));
        //}
    }
}
