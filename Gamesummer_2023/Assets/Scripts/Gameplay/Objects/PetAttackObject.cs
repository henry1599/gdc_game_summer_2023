using GDC.Enemies;
using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

namespace GDC.Objects
{
    public class PetAttackObject : MonoBehaviour
    {
        [SerializeField] float speed = 3;
        [SerializeField] float range = 4;
        [HideInInspector] public Vector2 Center;

        [SerializeField,ReadOnly] float damage;
        public float Damage { set => damage = value; }

        [HideInInspector] public float Angle; 
        //[SerializeField, ReadOnly] Enemy target = null;
        //public Enemy Target { set => target = value; }

        private void Update()
        {
            ChaseTarget();
        }

        void ChaseTarget()
        {
            //if (target == null || !target.gameObject.activeInHierarchy)
            //    Destroy(gameObject);

            //float angle = Vector2.Angle(target.transform.position - transform.position, Vector2.right); ;
            //if (target.transform.position.y < transform.position.y)
            //    angle = 0 - angle;
            if (Vector2.Distance(Center, transform.position) > range)
                Destroy(gameObject);

            transform.rotation = Quaternion.Euler(0, 0, Angle);
            transform.position += new Vector3(
                speed * Mathf.Cos(Angle * Mathf.Deg2Rad) * Time.deltaTime,
                speed * Mathf.Sin(Angle * Mathf.Deg2Rad) * Time.deltaTime);
        }

        private void OnTriggerStay2D(Collider2D collision)
        {
            if (collision.gameObject.CompareTag("Wall"))
                Destroy(gameObject);
            if (collision.CompareTag("Enemy"))
            {
                if (collision.GetComponent<Enemy>() == null)
                {
                    var parentEnemyComponent = collision.GetComponentInParent<Enemy>();

                    if (parentEnemyComponent != null)
                        parentEnemyComponent.GetHit(transform.position, damage, 0, gameObject.layer);
                    else 
                        collision.GetComponent<Boss>().GetHit(transform.position, damage, 0, gameObject.layer);
                }
                else
                    collision.GetComponent<Enemy>().GetHit(transform.position, damage, 0, gameObject.layer);
                Destroy(gameObject);
            }
        }
    }
}