using DG.Tweening;
using GDC.PlayerManager;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

namespace GDC.Gameplay
{
    public class ShowWhenNear : MonoBehaviour
    {
        [SerializeField] SpriteRenderer graphic;
        [SerializeField] float range;
        Transform player;
        bool isPlayerNear;
        Color whiteClearColor = Color.white;
        private void Start()
        {
            player = Player.Instance.transform;
            whiteClearColor.a = 0;
            graphic.color = whiteClearColor;
        }
        private void Update()
        {
            if (Vector2.SqrMagnitude(transform.position - player.position)<range)
            {
                if (isPlayerNear == false)
                {
                    isPlayerNear = true;
                    Show();
                }
            }
            else
            {
                if (isPlayerNear)
                {
                    isPlayerNear = false;
                    Hide();
                }
            }
        }
        void Show()
        {
            graphic.DOColor(Color.white, 0.3f);
        }
        void Hide()
        {
            Color whiteClearColor = Color.white;
            whiteClearColor.a = 0;
            graphic.DOColor(whiteClearColor, 0.3f);
        }
    }
}
