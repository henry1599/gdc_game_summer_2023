using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GDC.Gameplay
{
    public class SpikePlant : MonoBehaviour
    {
        [SerializeField] Animator anim;
        [SerializeField] Collider2D coll;
        public void Grow()
        {
            anim.Play("grow");
            coll.enabled = true;
        }
        public void Hide()
        {
            anim.Play("hide");
            coll.enabled = false;
        }
    }
}
