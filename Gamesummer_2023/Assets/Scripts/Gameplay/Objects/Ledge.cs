using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using DG.Tweening;
using GDC.Enums;
using GDC.PlayerManager;
using GDC.Managers;

namespace GDC.Gameplay
{
    public class Ledge : MonoBehaviour
    {
        [SerializeField] LedgeType ledgeType;
        Collider2D coll;

        private void Awake()
        {
            this.coll = GetComponent<Collider2D>();
        }
        public void TryToJump(int xDir, int yDir)
        {
            Vector2 playerPos = Player.Instance.transform.position;
            if (ledgeType == LedgeType.UP && yDir > 0 && playerPos.y < transform.position.y + coll.offset.y)
            {
                StartCoroutine(Cor_Jump(0, 1));
            }
            else if (ledgeType == LedgeType.DOWN && yDir < 0 && playerPos.y > transform.position.y + coll.offset.y)
            {
                StartCoroutine(Cor_Jump(0, -1));
            }
            else if (ledgeType == LedgeType.LEFT && xDir < 0 && playerPos.x > transform.position.x + coll.offset.x)
            {
                StartCoroutine(Cor_Jump(-1, 0));
            }
            else if (ledgeType == LedgeType.RIGHT && xDir > 0 && playerPos.x < transform.position.x + coll.offset.x)
            {
                StartCoroutine(Cor_Jump(1, 0));
            }
        }

        IEnumerator Cor_Jump(float xDis, float yDis)
        {
            //this.coll.enabled = false;
            Player.Instance.isCanMove = false;
            Player.Instance.coll.enabled = false;

            var jumpDis = Player.Instance.transform.position + new Vector3(xDis, yDis)*1.2f;
            Player.Instance.transform.DOMove(jumpDis, 0.6f);
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_JUMP);
            yield return Player.Instance.graphicContainTransform.transform.DOJump(jumpDis, 1f, 1, 0.6f).WaitForCompletion();

            //this.coll.enabled = true;
            Player.Instance.isCanMove = true;
            Player.Instance.coll.enabled = true;
        }
    }
}
