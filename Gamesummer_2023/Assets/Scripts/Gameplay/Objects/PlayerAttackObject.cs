using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.Enemies;
using GDC.Configuration;
using GDC.PlayerManager.Hand;
using NaughtyAttributes;

namespace GDC.Objects
{
    public class PlayerAttackObject : MonoBehaviour
    {
        [SerializeField,Expandable] private SO_Spell SO_Spell;
        private float spellAtk;
        private float spellEffectDuration;

        [Header("He so nhan them vao ATK")]
        [SerializeField] float multiValue = 1;
        public float MultiValue
        {
            get => multiValue; set => multiValue = value;
        }

        [Space(10)]
        [SerializeField] bool isUseGizmos, isDisappearWhenHit;

        [ShowIf("isUseGizmos"), SerializeField] bool circleGizmos = false, cubeGizmos = false;
        [ShowIf("isUseGizmos"), SerializeField] LayerMask enemyMask;
        [ShowIf("circleGizmos")]
        [SerializeField] float gizmosRadius;
        [ShowIf("cubeGizmos")]
        [SerializeField] Vector2 cubeSize;
        
        
        public void Awake()
        {
            SetData();
        }
        void SetData()
        {
            if (SO_Spell == null) spellAtk = 1;
            else
            {
                spellAtk = SO_Spell.SpellAtk();
                spellEffectDuration = SO_Spell.EffectDuration();
                //Debug.Log(spellAtk + " " + spellEffectDuration);
            }
        }
        protected virtual void DetectGizmos()
        {
            
            if (isUseGizmos)
            {
            

                Collider2D[] hitArray = new Collider2D[0];
                if (circleGizmos)
                    hitArray = Physics2D.OverlapCircleAll(transform.position, gizmosRadius,enemyMask);
                if (cubeGizmos)
                    hitArray = Physics2D.OverlapBoxAll(transform.position, cubeSize,transform.localRotation.z,enemyMask);

                
                if (hitArray.Length == 0)
                    return;
                foreach (var hit in hitArray)
                {

                    if (hit.CompareTag("Enemy"))
                    {
                        Enemy enemyScr = hit.GetComponent<Enemy>();
                        if (enemyScr != null)
                        {
                            if (enemyScr.Hp <= 0) return;
                            enemyScr.GetHit(transform.position, spellAtk * multiValue, spellEffectDuration, gameObject.layer, gameObject.GetInstanceID());
                        }
                        else 
                        {
                            Boss bossScr = hit.GetComponent<Boss>();
                            if (bossScr == null || bossScr.Hp <= 0) return;
                            bossScr.GetHit(transform.position, spellAtk * multiValue, spellEffectDuration, gameObject.layer);
                        }
                        // Enemy enemyScr = hit.GetComponent<Enemy>();
                        // if (enemyScr == null) return;
                        // enemyScr.GetHit(transform.position, spellAtk * multiValue, spellEffectDuration, gameObject.layer, gameObject.GetInstanceID());
                    }
                }
            }
        }    
        public virtual void Disappear()
        {
            //Instantiate Effect here (chua lam)
            gameObject.SetActive(false);
        }
        private void OnTriggerStay2D(Collider2D collision)
        {
            if (collision.gameObject.CompareTag("Wall") || collision.gameObject.CompareTag("Boulder"))
            {
                if (isDisappearWhenHit == true) Disappear();
            }
            if (collision.CompareTag("Enemy"))
            {
                Enemy enemyScr = collision.GetComponent<Enemy>();
                if (enemyScr != null)
                {
                    enemyScr.GetHit(transform.position, spellAtk * multiValue, spellEffectDuration, gameObject.layer, gameObject.GetInstanceID());
                }
                else 
                {
                    Boss bossScr = collision.GetComponent<Boss>();
                    if (bossScr == null) return;
                    bossScr.GetHit(transform.position, spellAtk * multiValue, spellEffectDuration, gameObject.layer);
                }
                // Enemy enemyScr = collision.GetComponent<Enemy>();
                // if (enemyScr == null) return;
                // enemyScr.GetHit(transform.position, spellAtk * multiValue, spellEffectDuration, gameObject.layer, gameObject.GetInstanceID());
                if (isDisappearWhenHit == true) Disappear();
            }
            if (collision.CompareTag("EnemyAttack"))
            {
                if (isDisappearWhenHit == true) Disappear();
            }
        }

        private void OnCollisionStay2D(Collision2D collision)
        {
            if (collision.gameObject.CompareTag("Wall") || collision.gameObject.CompareTag("Boulder"))
            {
                if (isDisappearWhenHit == true) Disappear();
            }
            if (collision.gameObject.CompareTag("Enemy"))
            {
                Enemy enemyScr = collision.gameObject.GetComponent<Enemy>();
                if (enemyScr != null)
                {
                    enemyScr.GetHit(transform.position, spellAtk * multiValue, spellEffectDuration, gameObject.layer, gameObject.GetInstanceID());
                }
                else 
                {
                    Boss bossScr = collision.gameObject.GetComponent<Boss>();
                    if (bossScr == null) return;
                    bossScr.GetHit(transform.position, spellAtk * multiValue, spellEffectDuration, gameObject.layer);
                }
                // Enemy enemyScr = collision.gameObject.GetComponent<Enemy>();
                // if (enemyScr == null) return;
                // enemyScr.GetHit(transform.position, spellAtk * multiValue, spellEffectDuration, gameObject.layer, gameObject.GetInstanceID());
                if (isDisappearWhenHit == true) Disappear();
            }
            if (collision.gameObject.CompareTag("EnemyAttack"))
            {
                if (isDisappearWhenHit == true) Disappear();
            }
        }
        private void OnDrawGizmos()
        {
            if (circleGizmos)
                Gizmos.DrawWireSphere(transform.position, gizmosRadius);
            if (cubeGizmos)
                Gizmos.DrawWireCube(transform.position, cubeSize);
        }


        
    }
}
