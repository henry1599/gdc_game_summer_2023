using AudioPlayer;
using GDC.Managers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GDC.Gameplay
{
    public class AnimWhenTouch : MonoBehaviour
    {
        [SerializeField] SpriteRenderer spriteRenderer;
        [SerializeField] Sprite idleSprite, touchSprite;
        [SerializeField] SoundID touchSoundID, idleSoundID;

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.CompareTag("Player"))
            {
                SoundManager.Instance.PlaySound(touchSoundID);
                spriteRenderer.sprite = touchSprite;
            }
        }
        private void OnTriggerExit2D(Collider2D collision)
        {
            if (collision.CompareTag("Player"))
            {
                SoundManager.Instance.PlaySound(idleSoundID);
                spriteRenderer.sprite = idleSprite;
            }
        }
    }
}
