using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.Universal;
using DG.Tweening;

public class GlowTree : MonoBehaviour
{
    Light2D treeLight;
    float time = 1;
    void Start()
    {
        treeLight = GetComponentInChildren<Light2D>();
        treeLight.intensity = 1;
    }
    void FixedUpdate()
    {
        time -= Time.fixedDeltaTime;
        if (time <= 0) {
            time = 1;
            treeLight.intensity = 1;
        }
        else if (time <= 0.5f) {
            treeLight.intensity -= 0.6f * Time.fixedDeltaTime;
        }
        else {
            treeLight.intensity += 0.6f * Time.fixedDeltaTime;
        }
    }
}
