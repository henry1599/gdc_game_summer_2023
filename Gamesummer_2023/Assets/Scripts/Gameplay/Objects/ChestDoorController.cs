using GDC.Enums;
using GDC.Gameplay;
using GDC.Gameplay.UI;
using GDC.Managers;
using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class ChestDoorController : MonoBehaviour
{
    AreaType areaType;
    [SerializeField] List<Chest> chestList;
    [SerializeField] List<DungeonDoor> doorList;
    void Start()
    {
        SetupChest();
        SetupDoor();
    }
    void SetupChest()
    {
        SaveLoadManager.Instance.GameData.InitChestAndDoorList();
        foreach (var chest in chestList)
        {
            areaType = UIGameplay.Instance.areaType;
            if (SaveLoadManager.Instance.GameData.GetOpenChest(UIGameplay.Instance.areaType, chest.ID) == true)
            {
                chest.isAlreayGet = true;
                chest.gameObject.SetActive(false);
            }
            else
            {
                chest.Setup();
            }
        }
    }
    void SetupDoor()
    {
        foreach (var door in doorList)
        {
            areaType = UIGameplay.Instance.areaType;
            if (SaveLoadManager.Instance.GameData.GetOpenDoor(UIGameplay.Instance.areaType, door.ID) == true)
            {
                door.AlreadyOpen();
            }
        }
    }
}
