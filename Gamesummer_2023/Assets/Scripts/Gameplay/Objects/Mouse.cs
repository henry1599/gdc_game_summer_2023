using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GDC.Objects
{
    public class Mouse : MonoBehaviour
    {
        [SerializeField] RectTransform rect;
        [SerializeField] Canvas gameplayCanvas;
        void Start()
        {
            if (gameplayCanvas.worldCamera == null)
                print("UIGameplayCanvas doesnt have camera");
            else
            {
                GetComponent<ParticleSystemRenderer>().sortingLayerID = gameplayCanvas.sortingLayerID;
                GetComponent<ParticleSystemRenderer>().sortingOrder = gameplayCanvas.sortingOrder + 1;
            }
        }

        void Update()
        {
            transform.position = (Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition);
        }
    }
}