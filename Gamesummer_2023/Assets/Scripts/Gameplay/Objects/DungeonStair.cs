using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.PlayerManager;
using GDC.PlayerManager.Hand;
using UnityEngine.Rendering.Universal;
using GDC.Events;
using GDC.Managers;
using GDC.Gameplay;
using DG.Tweening;
using NaughtyAttributes;

public class DungeonStair : MonoBehaviour
{
    enum DungeonStairType {
        UP, 
        DOWN
    }
    enum DungeonStairDirection {
        RIGHT,
        LEFT
    }
    [SerializeField] DungeonStairType dungeonStairType;
    [SerializeField] DungeonStairDirection dungeonStairDirection;
    [InfoBox("PlayerManager transform.position must be (0,0,0)")]
    [SerializeField] Animator bubble;
    Light2D playerLight;
    bool isNear, isShow, isMoving;
    TransitionSceneZone transitionSceneZone;
    Vector3 direction;
    SpriteRenderer[] player_graphics, player_graphics_hand;
    List<float> fade = new(), fade_hand = new ();
    void Start()
    {
        if (dungeonStairDirection == DungeonStairDirection.RIGHT) transform.localScale = new Vector3(-1,1,1);
        else transform.localScale = Vector3.one;

        transitionSceneZone = GetComponentInParent<TransitionSceneZone>();
        transitionSceneZone.enabled = false;
        direction = Vector3.zero;
        playerLight = Player.Instance.GetComponentInChildren<Light2D>(true);
        // if (playerLight == null) enabled = false;
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F) && isShow)
        {
            transitionSceneZone.TransitionScene();
            EnterStair();
            isNear = false;
        }
        if (isNear && !isShow) BubbleChatShow();
        else if (!isNear && isShow) BubbleChatHide();
    }
    void EnterStair()
    {
        if (dungeonStairType == DungeonStairType.UP) 
        {
            StartCoroutine(Cor_TurnLight(false));
            direction += Vector3.up * 0.2f;
        }
        else 
        {
            StartCoroutine(Cor_TurnLight(true));
            direction += Vector3.down * 0.2f;
        }
        if (dungeonStairDirection == DungeonStairDirection.RIGHT) direction += Vector3.right;
        else direction += Vector3.left;

        SetPlayerDirection();
    }
    void BubbleChatShow()
    {
        isShow = true;
        bubble.gameObject.SetActive(true);
        bubble.Play("PressF");
        bubble.transform.DOScaleX(0.6f, 0.15f);
        bubble.transform.DOScaleY(1.4f, 0.15f).OnComplete(() =>
        {
            bubble.transform.DOScale(1f, 0.1f);
        });
    }
    void BubbleChatHide()
    {
        isShow = false;
        bubble.transform.DOScaleX(0.6f, 0.1f);
        bubble.transform.DOScaleY(1.4f, 0.1f).OnComplete(() =>
        {
            bubble.transform.DOScale(0f, 0.15f).OnComplete(() =>
            {
                if (!isNear) bubble.transform.gameObject.SetActive(false);
            });
        });
    }
    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
            isNear = true;
    }
    void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
            isNear = false;
    }
    IEnumerator Cor_TurnLight(bool setActive)
    {
        FadePlayer();

        yield return new WaitForSeconds(GameConstants.TRANSITION_TIME);
        if (playerLight != null) playerLight.gameObject.SetActive(setActive);
        yield return new WaitUntil(() => GameManager.Instance.isLoadSceneComplete);
        if (!isMoving) 
        {
            Player.Instance.MoveTo(Player.Instance.transform.position + direction * 1.5f, 1);
            isMoving = true;
        }
        ShowPlayer();
        yield return new WaitForSeconds(1);
        Player.Instance.UseHandMovement = true;
        Player.Instance.InCutScene = false;
        
        isMoving = false;
        direction = Vector3.zero;
    }
    void FadePlayer()
    {
        player_graphics = Player.Instance.GetComponentsInChildren<SpriteRenderer>(true);
        fade = new List<float>();
        foreach (var graphic in player_graphics)
        {
            fade.Add(graphic.color.a);
            graphic.DOFade(0f, 0.5f);
        }

        var backhand = FindObjectOfType<HandMovement>();
        if (backhand != null)
        {
            player_graphics_hand = backhand.GetComponentsInChildren<SpriteRenderer>(true);
            fade_hand = new List<float>();
            foreach (var graphic_hand in player_graphics_hand)
            {
                fade_hand.Add(graphic_hand.color.a);
                graphic_hand.DOFade(0f, 0.5f);
            }
        }
    }
    void ShowPlayer()
    {
        for (int i=0; i<fade.Count; i++)
            player_graphics[i].DOFade(fade[i], 0.5f);
        for (int i=0; i<fade_hand.Count; i++)
            player_graphics_hand[i].DOFade(fade_hand[i], 0.5f);
    }
    void SetPlayerDirection()
    {
        Player.Instance.InCutScene = true;
        Player.Instance.UseHandMovement = false;
        StartCoroutine(Cor_SetPlayerDirect());
    }
    IEnumerator Cor_SetPlayerDirect()
    {
        yield return new WaitUntil(() => !HandMovement.Instance.gameObject.activeInHierarchy);

        if (dungeonStairDirection == DungeonStairDirection.RIGHT)
            Player.Instance.graphicOfPlayer.transform.localScale = Vector3.one;
        else 
            Player.Instance.graphicOfPlayer.transform.localScale = new Vector3(-1,1,1);

        Player.Instance.MoveTo(Player.Instance.transform.position + direction, 0.75f);
    }
}
