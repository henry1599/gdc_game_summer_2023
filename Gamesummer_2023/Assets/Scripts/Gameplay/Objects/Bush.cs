using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using DG.Tweening;
using GDC.Managers;
using AudioPlayer;

namespace GDC.Gameplay
{
    public class Bush : MonoBehaviour
    {
        [SerializeField] SpriteRenderer spriteRenderer;
        [SerializeField] Collider2D coll;
        [SerializeField] LayerMask cuttingLayerMask;
        [SerializeField] GameObject vfx_bushCutting;
        [SerializeField] Sprite afterCutSprite;

        [SerializeField] bool isHaveAnimWhenTouch;
        [SerializeField] Depth depthScr;

        [SerializeField] SoundID cutSoundID;

        [Header("ItemDrop")]
        [SerializeField] private int maxCoinDrop;
        [SerializeField] private int coinRate; //coinRate theo %
        [SerializeField] private GameObject coinPreFab;
        [SerializeField] private List<GameObject> commonItemDrop, rareItemDrop, epicItemDrop;

        [Header("ObjectWhenCut")]
        [SerializeField] GameObject objWhenCut;

        void Cut()
        {
            SoundManager.Instance.PlaySound(cutSoundID);
            Instantiate(vfx_bushCutting, transform.position, Quaternion.identity);
            DropItem();
            if (depthScr!= null)
                depthScr.enabled = false;
            if (afterCutSprite != null)
                spriteRenderer.sprite = afterCutSprite;
            else
                spriteRenderer.enabled = false;
            spriteRenderer.sortingLayerName = "Ground";
            spriteRenderer.sortingOrder = -2;
            coll.enabled = false;
            if (objWhenCut != null)
            {
                //Instantiate(objWhenCut, transform.position, Quaternion.identity);
                objWhenCut.SetActive(true);
            }
        }

        public void DropItem()
        {
            for (int i = 1; i <= maxCoinDrop; i += 5)
            {
                if (Random.Range(0, 100) <= coinRate)
                {
                    Instantiate(coinPreFab, transform.position, Quaternion.identity);
                }
            }
            foreach (var itemDrop in commonItemDrop)
            {
                Instantiate(itemDrop, transform.position, Quaternion.identity);
            }
            foreach (var itemDrop in rareItemDrop)
            {
                if (Random.Range(0, 100) < 40)
                    Instantiate(itemDrop, transform.position, Quaternion.identity);
            }
            foreach (var itemDrop in epicItemDrop)
            {
                if (Random.Range(0, 100) < 10)
                    Instantiate(itemDrop, transform.position, Quaternion.identity);
            }
        }

        void Shake()
        {
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_TALL_GRASS);
            spriteRenderer.transform.DORotate(new Vector3(0, 0, 10), 0.1f).OnComplete(()=>
            {
                spriteRenderer.transform.DORotate(new Vector3(0, 0, -10), 0.3f).OnComplete(()=>
                {
                    spriteRenderer.transform.DORotate(new Vector3(0, 0, 0), 0.4f);
                });
            });
            
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (((1 << collision.gameObject.layer) & cuttingLayerMask) != 0)
            {
                Cut();
            }
            if (isHaveAnimWhenTouch && collision.CompareTag("Grass") == false)
            {
                Shake();
            }
        }
    }
}
