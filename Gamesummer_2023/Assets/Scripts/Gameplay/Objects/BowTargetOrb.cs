using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;
using UnityEngine;
using DG.Tweening;

public enum BowTargetOrbType
{
    GRAY, BLUE_OR_RED
}

public class BowTargetOrb : MonoBehaviour
{
    [SerializeField] BowTargetOrbType bowTargetOrbType;
    [SerializeField] List<GameObject> boxes;
    [SerializeField, ShowIf("bowTargetOrbType", BowTargetOrbType.BLUE_OR_RED)] 
    List<BowTargetOrb> thisOrbs, otherOrbs;
    [SerializeField] SpriteRenderer graphic;
    [SerializeField] Animator anim;
    bool isOrbUp, isOrbOpen;
    void Start()
    {
        if (bowTargetOrbType == BowTargetOrbType.BLUE_OR_RED)
            isOrbOpen = false;
        else
            isOrbOpen = true;
    }
    void Update()
    {
        if (!isOrbUp)
        {
            isOrbUp = true;
            graphic.transform.DOLocalMoveY(0.5f, 1.5f).OnComplete(() => 
                graphic.transform.DOLocalMoveY(0, 1.5f).OnComplete(() => isOrbUp = false)
            );
        }
    }
    void OpenChest()
    {
        if (isOrbOpen) return;
        isOrbOpen = true;
        anim.Play("orb");
        if (bowTargetOrbType == BowTargetOrbType.BLUE_OR_RED)
        {
            HandleChest(1, false);
            foreach (var go in thisOrbs) {
                go.OpenChest();
            }
            foreach (var go in otherOrbs) {
                go.CloseChest();
            }
        }
        else 
        {
            HandleChest(1, false);
        }
    }
    void CloseChest()
    {
        if (!isOrbOpen) return;
        isOrbOpen = false;
        anim.Play("orb_close");
        // if (bowTargetOrbType == BowTargetOrbType.BLUE_OR_RED)
        // {
            HandleChest(0.5f, true);
        // }
    }
    void HandleChest(float fade, bool trigger)
    {
        foreach (var go in boxes)
        {
            go.GetComponent<SpriteRenderer>().DOFade(fade, 0.5f);
            go.GetComponent<BoxCollider2D>().isTrigger = trigger;
        }
    }
    void OnTriggerEnter2D(Collider2D collision)
    {
        // if "rangeAttack", the layer is 9
        if (collision.gameObject.layer == 8)
        {
            if (bowTargetOrbType == BowTargetOrbType.BLUE_OR_RED)
            {
                OpenChest();
            }
            else
            {
                if (isOrbOpen)
                {
                    CloseChest();
                }
                else 
                {
                    OpenChest();
                }
            }
        }
    }
}
