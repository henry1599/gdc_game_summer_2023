using DG.Tweening;
using GDC.Enums;
using GDC.Managers;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace GDC.Gameplay.UI
{
    public class AreaIntroPanel : MonoBehaviour
    {
        RectTransform rect;
        [SerializeField] Image[] images;
        [SerializeField] RectTransform cloudRect, treeRect;
        [SerializeField] TMP_Text nameText;

        private void Awake()
        {
            rect = GetComponent<RectTransform>();
            gameObject.SetActive(false);
        }
        public void Show()
        {
            StartCoroutine(Cor_Show());
        }

        IEnumerator Cor_Show()
        {
            rect.anchoredPosition = new Vector2(0, -250);
            rect.DOAnchorPosY(-100, 1);
            cloudRect.DOAnchorPosX(0, 2);
            treeRect.DOAnchorPosX(0, 2);
            for (int i = 0; i<images.Length; i++)
            {
                Color imageColor = images[i].color;
                images[i].color = Color.clear;
                images[i].DOColor(imageColor, 1);
            }
            Color textColor = nameText.color;
            nameText.color = Color.clear;
            nameText.DOColor(textColor, 1);

            yield return new WaitForSeconds(3);

            rect.DOAnchorPosY(50, 1);
            cloudRect.DOAnchorPosX(300, 2);
            treeRect.DOAnchorPosX(-300, 2);
            foreach (var image in images)
            {
                image.DOColor(Color.clear, 1);
            }
            nameText.DOColor(Color.clear, 1);

            yield return new WaitForSeconds(2);
            SaveLoadManager.Instance.GameData.TargetSceneType = SceneType.AREA1_DUNGEON;
            gameObject.SetActive(false);
        }
    }
}
