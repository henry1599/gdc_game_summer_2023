using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using GDC.Gameplay.UI;
using GDC.PlayerManager;
using GDC.Managers;

namespace GDC.Gameplay.UI
{
    public class SpellUpgradeManager : MonoBehaviour
    {
        public static SpellUpgradeManager Instance { get; private set; }
        RectTransform rectTransform;
        public bool isOpening = false;

        [SerializeField] BookUI bookUI;

        Tween hideTween;

        private void Awake()
        {
            //if (Instance != null)
            //{
            //    Destroy(gameObject);
            //    return;
            //}
            Instance = this;

            rectTransform = gameObject.GetComponent<RectTransform>();

            gameObject.SetActive(false);
        }
        public void Show()
        {
            if (Player.Instance.InCutScene) return;
            if (InventoryManager.Instance?.isOpening == true)
            {
                InventoryManager.Instance.Hide();
            }
            if (MapManager.Instance?.isOpening == true)
            {
                MapManager.Instance.Hide();
            }
            if (isOpening == true)
            {
                Hide();
                return;
            }

            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_START_UI);
            hideTween.Kill();
            gameObject.SetActive(true);
            Time.timeScale = 0;
            isOpening = true;
            rectTransform.DOAnchorPosY(0, 0.5f).SetUpdate(true);
            Load();
        }
        public void Hide()
        {
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_END_UI);
            Time.timeScale = 1;
            isOpening = false;
            hideTween = rectTransform.DOAnchorPosY(-1000, 0.5f).SetUpdate(true).OnComplete(() => gameObject.SetActive(false));
            CarrySlotManager.Instance?.Hide();
        }

        [NaughtyAttributes.Button]
        public void Load()
        {
            bookUI.ReloadCurrentPage();
        }
    }
}
