using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.Configuration;
using DG.Tweening;
using UnityEngine.UI;
using TMPro;
using GDC.Managers;

namespace GDC.Gameplay.UI
{
    public class ExplainPanel : MonoBehaviour
    {
        public static ExplainPanel Instance { get; private set; }
        public RectTransform rectTransform;
        [SerializeField] Image icon;
        [SerializeField] TMP_Text nameText, explainText;


        private void Awake()
        {
            //if (Instance != null)
            //{
            //    Destroy(gameObject);
            //    return;
            //}
            Instance = this;

            rectTransform = gameObject.GetComponent<RectTransform>();
            gameObject.SetActive(false);
        }

        public void Show(SO_Item so_item)
        {
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_START_UI);
            rectTransform.DOAnchorPosX(-557, 0.3f).SetUpdate(true);
            icon.sprite = so_item.sprite;
            nameText.text = so_item.itemName;
            explainText.text = so_item.detail;
        }

        public void Hide()
        {
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_END_UI);
            rectTransform.DOAnchorPosX(0, 0.3f).SetUpdate(true);
        }
    }
}
