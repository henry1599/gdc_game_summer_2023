using DG.Tweening;
using GDC.Enums;
using GDC.Managers;
using GDC.PlayerManager;
using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace GDC.Gameplay.UI
{
    public class UIGameplay : MonoBehaviour
    {
        [SerializeField] RectTransform slidersRect, inventoryBtnRect, spellUpgradeBtnRect, mapBtnRect;
        public AreaIntroPanel areaIntroPanel;
        public TMP_Text nameText;
        public static UIGameplay Instance { get; private set; }
        public AreaType areaType;
        public SceneType sceneType;
        public bool isPause;
        private void Awake()
        {
            //if (Instance != null)
            //{
            //    Destroy(gameObject);
            //    return;
            //}
            Instance = this;
        }

        public void ShowSliders()
        {
            slidersRect.DOAnchorPosY(-94, 1);
        }
        public void HideSliders()
        {
            slidersRect.DOAnchorPosY(150,1);
        }
        public void HideSliders(float duration)
        {
            slidersRect.DOAnchorPosY(150, duration);
        }
        public void LoadBottomLeftButton()
        {
            if (SaveLoadManager.Instance.GameData.isPlayerHaveWeapon)
            {
                ShowInventoryButton();      
            }
            else
            {
                HideInventoryButton(0);
            }

            if(SaveLoadManager.Instance.GameData.isPlayerHaveMap)
            {
                ShowMapButton();
            }
            else
            {
                HideMapButton(0);
            }

            if (SaveLoadManager.Instance.GameData.isPlayerHaveSpell)
            {
                ShowSpellUpgradeButton();
            }
            else
            {
                HideSpellUpgradeButton(0);
            }
        }
        public void HideBottomLeftButton(float duration)
        {
            HideInventoryButton(duration);
            HideMapButton(duration);
            HideSpellUpgradeButton(duration);
        }

        public void HideInventoryButton(float duration)
        {
            inventoryBtnRect.DOScale(0, duration).OnComplete(() => inventoryBtnRect.gameObject.SetActive(false));
        }
        public void ShowInventoryButton()
        {
            inventoryBtnRect.gameObject.SetActive(true);
            inventoryBtnRect.DOScale(1.2f, 0.8f).OnComplete(() =>
            {
                inventoryBtnRect.DOScale(1, 0.2f);
            });
        }
        public void HideSpellUpgradeButton(float duration)
        {
            spellUpgradeBtnRect.DOScale(0, duration).OnComplete(() => spellUpgradeBtnRect.gameObject.SetActive(false));          
        }
        [Button]
        public void ShowSpellUpgradeButton()
        {
            spellUpgradeBtnRect.gameObject.SetActive(true);
            spellUpgradeBtnRect.DOScale(1.2f, 0.8f).OnComplete(()=>
            {
                spellUpgradeBtnRect.DOScale(1, 0.2f);
            });
        }
        public void HideMapButton(float duration)
        {
            mapBtnRect.DOScale(0, duration).OnComplete(() => mapBtnRect.gameObject.SetActive(false));
        }
        public void ShowMapButton()
        {
            mapBtnRect.gameObject.SetActive(true);
            mapBtnRect.DOScale(1.2f, 0.8f).OnComplete(() =>
            {
                mapBtnRect.DOScale(1, 0.2f);
            });
        }

        public void LoadUI()
        {
            StartCoroutine(Cor_LoadUI());
        }
        IEnumerator Cor_LoadUI()
        {
            yield return new WaitUntil(() => SaveLoadManager.Instance.GameData.IsSaveLoadProcessing == false);

            nameText.text = SaveLoadManager.Instance.GameData.PlayerName;
            LoadBottomLeftButton();

            yield return new WaitUntil(() => CoinDisplay.Instance != null);
            CoinDisplay.Instance.UpdateCoin();

            yield return new WaitUntil(() => InventoryManager.Instance != null);
            InventoryManager.Instance.InitLoad();
            
            yield return new WaitUntil(() => SpellUpgradeManager.Instance != null);
            SpellUpgradeManager.Instance.Load();

            yield return new WaitUntil(() => CurrentSpellDisplay.Instance != null);
            CurrentSpellDisplay.Instance.Setup();

            if (areaType != SaveLoadManager.Instance.GameData.CurrentAreaType 
                || sceneType == SceneType.AREA1_DUNGEON)
            {
                ShowAreaIntroPanel();
                SaveLoadManager.Instance.GameData.CurrentAreaType = areaType;
            }

            Debug.Log("Load UI success!");
        }    
        public void ShowAreaIntroPanel()
        {
            areaIntroPanel.gameObject.SetActive(true);
            areaIntroPanel.Show();
        }
    }
}
