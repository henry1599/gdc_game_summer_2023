using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GDC.Enums;
using UnityEngine.Rendering;

namespace GDC.Gameplay.UI
{
    public class BuffEffectManager : MonoBehaviour
    {
        public static BuffEffectManager Instance { get; private set; }
        [SerializeField] Image buffAtk, buffDef, buffSpe, sleep, poison, slow, freeze;

        private void Awake()
        {
            //if (Instance != null)
            //{
            //    Destroy(gameObject);
            //    return;
            //}
            Instance = this;

            buffAtk.gameObject.SetActive(false);
            buffDef.gameObject.SetActive(false);
            buffSpe.gameObject.SetActive(false);
            sleep.gameObject.SetActive(false);
            poison.gameObject.SetActive(false);
            slow.gameObject.SetActive(false) ; 
            freeze.gameObject.SetActive(false);
        }
        public void SetEffect(EffectType effectType, bool isShow)
        {
            if (effectType == EffectType.ATK_BUFF)
            {
                buffAtk.gameObject.SetActive(isShow);
            }    
            else if (effectType == EffectType.DEF_BUFF)
            {
                buffDef.gameObject.SetActive(isShow);
            }    
            else if (effectType == EffectType.SPE_BUFF)
            {
                buffSpe.gameObject.SetActive(isShow);
            }    
            else if (effectType == EffectType.SLEEP)
            {
                sleep.gameObject.SetActive(isShow);
            }    
            else if (effectType == EffectType.POISON)
            {
                poison.gameObject.SetActive(isShow);
            }    
            else if (effectType == EffectType.SLOW)
            {
                slow.gameObject.SetActive(isShow);
            }
            else if (effectType == EffectType.FREEZE)
            {
                slow.gameObject.SetActive(isShow);
            }
        }    
        //public void ShowBuffAtk()
        //{
        //    buffAtk.gameObject.SetActive(true);
        //}
        //public void ShowBuffDef()
        //{
        //    buffDef.gameObject.SetActive(true);
        //}
        //public void ShowBuffSpe()
        //{
        //    buffSpe.gameObject.SetActive(true);
        //}
        //public void HideBuffAtk()
        //{
        //    buffAtk.gameObject.SetActive(false);
        //}
        //public void HideBuffDef()
        //{
        //    buffDef.gameObject.SetActive(false);
        //}
        //public void HideBuffSpe()
        //{
        //    buffSpe.gameObject.SetActive(false);
        //}
    }
}
