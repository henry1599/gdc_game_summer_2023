using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.PlayerManager;
using TMPro;
using GDC.Configuration;
using GDC.Enums;

namespace GDC.Gameplay.UI
{
    public class EquipmentManager : MonoBehaviour
    {
        public static EquipmentManager Instance { get; private set; }
        public RectTransform rectTransform;
        [SerializeField] TMP_Text HP_ValueText, ATK_ValueText, DEF_ValueText, SPE_ValueText, STAM_ValueText;
        [SerializeField] TMP_Text HP_ChangeText, ATK_ChangeText, DEF_ChangeText, SPE_ChangeText, STAM_ChangeText;

        private void Awake()
        {
            //if (Instance != null)
            //{
            //    Destroy(gameObject);
            //    return;
            //}
            Instance = this;

            rectTransform = GetComponent<RectTransform>();
        }

        public void SetData()
        {
            //StartCoroutine(Cor_SetData());
            HP_ValueText.text = Player.Instance.MaxHealth.ToString();
            ATK_ValueText.text = Player.Instance.Attack.ToString();
            DEF_ValueText.text = Player.Instance.Defense.ToString();
            SPE_ValueText.text = Player.Instance.Speed.ToString();
            STAM_ValueText.text = Player.Instance.MaxStamina.ToString();
        }
        
        public void SetChangeValue(SO_Item SO_item)
        {
            if (SO_item != null && SO_item.EquipmentType != EquipmentType.NONE)
            {
                int attackChange = SO_item.Attack;
                int defenseChange = SO_item.Defense;
                int speedChange = SO_item.Speed;
                int staminaChange = SO_item.Stamina;

                if (SO_item.EquipmentType == EquipmentType.HELMET)
                {
                    SO_Item playerItem = Player.Instance.HelmetItem;
                    if (playerItem != null)
                    {
                        attackChange = SO_item.Attack - playerItem.Attack;
                        defenseChange = SO_item.Defense - playerItem.Defense;
                        speedChange = SO_item.Speed - playerItem.Speed;
                        staminaChange = SO_item.Stamina - playerItem.Stamina;
                    }
                }
                else if (SO_item.EquipmentType == EquipmentType.WEAPON)
                {
                    SO_Item playerItem = Player.Instance.WeaponItem;
                    if (playerItem != null)
                    {
                        attackChange = SO_item.Attack - playerItem.Attack;
                        defenseChange = SO_item.Defense - playerItem.Defense;
                        speedChange = SO_item.Speed - playerItem.Speed;
                        staminaChange = SO_item.Stamina - playerItem.Stamina;
                    }
                }
                else if (SO_item.EquipmentType == EquipmentType.ARMOR)
                {
                    SO_Item playerItem = Player.Instance.ArmorItem;
                    if (playerItem != null)
                    {
                        attackChange = SO_item.Attack - playerItem.Attack;
                        defenseChange = SO_item.Defense - playerItem.Defense;
                        speedChange = SO_item.Speed - playerItem.Speed;
                        staminaChange = SO_item.Stamina - playerItem.Stamina;
                    }
                }
                else if (SO_item.EquipmentType == EquipmentType.SHOE)
                {
                    SO_Item playerItem = Player.Instance.ShoeItem;
                    if (playerItem != null)
                    {
                        attackChange = SO_item.Attack - playerItem.Attack;
                        defenseChange = SO_item.Defense - playerItem.Defense;
                        speedChange = SO_item.Speed - playerItem.Speed;
                        staminaChange = SO_item.Stamina - playerItem.Stamina;
                    }
                }

                if (attackChange > 0)
                {
                    ATK_ChangeText.color = Color.green;
                    ATK_ChangeText.text = "+" + attackChange.ToString();
                }
                else if (attackChange < 0)
                {
                    ATK_ChangeText.color = Color.red;
                    ATK_ChangeText.text = attackChange.ToString();
                }
                else
                {
                    ATK_ChangeText.text = "";
                }

                if (defenseChange > 0)
                {
                    DEF_ChangeText.color = Color.green;
                    DEF_ChangeText.text = "+" + defenseChange.ToString();
                }
                else if (defenseChange < 0)
                {
                    DEF_ChangeText.color = Color.red;
                    DEF_ChangeText.text = defenseChange.ToString();
                }
                else
                {
                    DEF_ChangeText.text = "";
                }

                if (speedChange > 0)
                {
                    SPE_ChangeText.color = Color.green;
                    SPE_ChangeText.text = "+" + speedChange.ToString();
                }
                else if (speedChange < 0)
                {
                    SPE_ChangeText.color = Color.red;
                    SPE_ChangeText.text = speedChange.ToString();
                }
                else
                {
                    SPE_ChangeText.text = "";
                }

                if (staminaChange > 0)
                {
                    STAM_ChangeText.color = Color.green;
                    STAM_ChangeText.text = "+" + staminaChange.ToString();
                }
                else if (staminaChange < 0)
                {
                    STAM_ChangeText.color = Color.red;
                    STAM_ChangeText.text = staminaChange.ToString();
                }
                else
                {
                    STAM_ChangeText.text = "";
                }

                //no MaxHP change
            }
            else
            {
                ATK_ChangeText.text = "";
                DEF_ChangeText.text = "";
                SPE_ChangeText.text = "";
                STAM_ChangeText.text = "";
            }
        }
        //IEnumerator Cor_SetData()
        //{
        //    yield return new WaitUntil(() => Player.Instance != null);
        //    HP_ValueText.text = Player.Instance.MaxHealth.ToString();
        //    ATK_ValueText.text = Player.Instance.Attack.ToString();
        //    DEF_ValueText.text = Player.Instance.Defense.ToString();
        //    SPE_ValueText.text = Player.Instance.Speed.ToString();
        //    STAM_ValueText.text = Player.Instance.MaxStamina.ToString();
        //    //gameObject.SetActive(false);
        //}
    }
}
