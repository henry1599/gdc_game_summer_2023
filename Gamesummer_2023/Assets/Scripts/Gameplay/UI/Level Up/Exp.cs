using DG.Tweening;
using GDC.Managers;
using GDC.PlayerManager.Spell;
using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GDC.Gameplay.UI
{
    public class Exp : MonoBehaviour
    {
        public static Exp Instance { get; private set; }

        private void Awake()
        {
            Instance = this;
        }

        [SerializeField] Transform expBallContainer;
        [SerializeField] int expBallNumber;
        [SerializeField] List<RectTransform> expBallList = new List<RectTransform>();
        [SerializeField] Vector2 destination;

        [SerializeField] float heightBeforeFlyToSlider;

        [Button]
        void GenerateExpBall()
        {
            if (expBallNumber == 0)
                return;

            GameObject template = Instantiate(expBallContainer.GetChild(0).gameObject);
            expBallList.Clear();

            while (expBallContainer.childCount > 0)
                DestroyImmediate(expBallContainer.GetChild(0).gameObject);

            for (float i = 0; i < expBallNumber; i++)
            {
                GameObject clone = Instantiate(template, expBallContainer);
                clone.name = "ExpBall";
                expBallList.Add(clone.GetComponent<RectTransform>());
            }

            DestroyImmediate(template);
        }    

        public void SpawnExp(Vector2 originPos)
        {
            RectTransform expBall = expBallList[0];
            expBallList.RemoveAt(0);

            expBall.transform.position = originPos;
            expBall.GetComponent<Image>().color = Color.white;
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_EXP_UP);
            expBall.transform.DOMoveY(expBall.transform.position.y + heightBeforeFlyToSlider, 1).OnComplete(() =>
            {
                expBall.DOAnchorPos(destination, 2).OnComplete(() =>
                {
                    expBall.GetComponent<Image>().DOFade(0, 0.5f).OnComplete(() =>
                    {
                        expBallList.Add(expBall);
                    });
                });
            });
        }
    }
}