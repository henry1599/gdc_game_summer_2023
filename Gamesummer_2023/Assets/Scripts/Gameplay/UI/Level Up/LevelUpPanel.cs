using DG.Tweening;
using GDC.PlayerManager;
using GDC.PlayerManager.Hand;
using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GDC.Gameplay.UI
{
    public class LevelUpPanel : MonoBehaviour
    {
        public static LevelUpPanel Instance {  get; private set; }
        private void Awake()
        {
            Instance = this;
        }

        [SerializeField] Image background;
        [SerializeField] float backgroundAlpha;
        [SerializeField] float fadeDuration;
        [SerializeField] LevelUpButton[] buttons;
        [SerializeField] Image textBorder,text;
        [SerializeField] ParticleSystem vfxStar;
        Tween textTween;
        Vector2 playerOriginPos;


        private void Start()
        {
            foreach (LevelUpButton button in buttons)
                button.SetUp();
            background.color = new Color(background.color.r, background.color.g, background.color.b, 0);
            gameObject.SetActive(false);
        }
        [Button]
        public void Show()
        {
            Time.timeScale = 0;
            HandMovement.Instance.EnableWhenStopTime = true;
            playerOriginPos = Player.Instance.transform.position;
            Player.Instance.InCutScene = true;
            Player.Instance.ConvertToLayer("UI_World");
            gameObject.SetActive(true);
            textBorder.color = new Color(1, 0, 0, 0);//red
            textBorder.DOFade(1, fadeDuration).SetUpdate(true);
            text.DOFade(1, fadeDuration).SetUpdate(true);
            Player.Instance.transform.DOMove((Vector2)Camera.main.transform.position + Vector2.down, fadeDuration).SetUpdate(true);
            background.DOFade(backgroundAlpha, fadeDuration).SetUpdate(true).OnComplete(() =>
            {
                foreach (LevelUpButton button in buttons)
                {
                    button.UpdateInfo();
                    button.Show();
                    vfxStar.Play();
                    textBorder.GetComponent<Animator>().enabled = true;
                    textBorder.GetComponent<Animator>().Play("levelUpText", 0, 0);
                }
            });
        }

        public void Hide()
        {
            foreach (LevelUpButton button in buttons)
                button.Hide();
            vfxStar.Stop();
            textBorder.DOFade(0, fadeDuration).SetUpdate(true);
            text.DOFade(0, fadeDuration).SetUpdate(true);
            Player.Instance.transform.DOMove(playerOriginPos,fadeDuration);
            background.DOFade(0, fadeDuration).SetUpdate(true).OnComplete(() =>
            {
                Time.timeScale = 1;
                Player.Instance.InCutScene = false;
                Player.Instance.ConvertToLayer("Default");
                gameObject.SetActive(false);
            });
        }
    }
}