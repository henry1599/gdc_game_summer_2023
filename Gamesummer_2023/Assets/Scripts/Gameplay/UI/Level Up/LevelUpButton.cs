using DG.Tweening;
using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using GDC.PlayerManager;
using GDC.Managers;

namespace GDC.Gameplay.UI
{
    public class LevelUpButton : MonoBehaviour
    {
        enum ButtonType
        {
            STAMINA,
            DEFEND,
            ATTACK,
            HEALTH,
        }
        [SerializeField] ButtonType type;
        [SerializeField] RectTransform rect;

        [SerializeField] Animator glow;
        [SerializeField] Image[] glowBack, glowFront;
        [SerializeField] float defaultAnimSpeed, speedMultiplier;

        [SerializeField] TMP_Text levelUpText;
        [SerializeField] string statusName;
        [SerializeField] float levelUpAmount;

        [SerializeField] bool enableButton = false;
        
        public void SetUp()
        {
            rect.localScale = Vector3.zero;
            foreach (Image img in glowBack)
                img.color = new Color(img.color.r, img.color.g, img.color.b, 0);
            foreach (Image img in glowFront)
                img.color = new Color(img.color.r, img.color.g, img.color.b, 0);
            levelUpText.color = new Color(levelUpText.color.r, levelUpText.color.g, levelUpText.color.b, 0);
        }

        public void UpdateInfo()
        {
            float currentStat = 0;
            switch (type)
            {
                case ButtonType.STAMINA:
                    currentStat = Player.Instance.Stamina; break;
                case ButtonType.DEFEND:
                    currentStat = Player.Instance.Defense; break;
                case ButtonType.ATTACK:
                    currentStat = Player.Instance.Attack; break;
                case ButtonType.HEALTH:
                    currentStat = Player.Instance.MaxHealth; break;
            }

            levelUpText.text = statusName + ":" + currentStat.ToString() + " -> " + (currentStat + levelUpAmount).ToString();
            glow.speed = defaultAnimSpeed;
            enableButton = false;
        }

        public void Show()
        {
            rect.DOScale(1, 0.5f).SetUpdate(true).OnComplete(() =>
            {
                enableButton = true;
            });
            foreach (Image img in glowBack)
                img.DOFade(0.25f, 0.5f).SetUpdate(true);
            foreach (Image img in glowFront)
                img.DOFade(0.5f, 0.5f).SetUpdate(true);
        }

        public void Hide()
        {
            rect.DOScale(0, 0.5f).SetUpdate(true);
            foreach (Image img in glowBack)
                img.DOFade(0, 0.5f).SetUpdate(true);
            foreach (Image img in glowFront)
                img.DOFade(0, 0.5f).SetUpdate(true);
            levelUpText.DOFade(0, 0.5f).SetUpdate(true);
        }

        public void Enter()
        {
            if (!enableButton)
                return;
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_HOLD_ITEM);
            rect.DOScale(1.2f, 0.5f).SetUpdate(true);
            glow.speed *= speedMultiplier;
            levelUpText.DOFade(1, 0.5f).SetUpdate(true);

            foreach (Image img in glowBack)
                img.DOFade(0.5f, 0.5f).SetUpdate(true);
            foreach (Image img in glowFront)
                img.DOFade(1, 0.5f).SetUpdate(true);
        }

        public void Exit()
        {
            if (!enableButton)
                return;
            rect.DOScale(1, 0.5f).SetUpdate(true);
            glow.speed = defaultAnimSpeed;
            levelUpText.DOFade(0, 0.5f).SetUpdate(true);

            foreach (Image img in glowBack)
                img.DOFade(0.25f, 0.5f).SetUpdate(true);
            foreach (Image img in glowFront)
                img.DOFade(0.5f, 0.5f).SetUpdate(true);
        }

        public void Down()
        {
            if (!enableButton)
                return;
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_RELEASE_ITEM);
            switch (type)
            {
                case ButtonType.STAMINA:
                    Player.Instance.StamBonus+=(int)levelUpAmount;
                    Player.Instance.Stamina += levelUpAmount;
                    break;
                case ButtonType.DEFEND:
                    Player.Instance.DefBonus+=(int)levelUpAmount; break;
                case ButtonType.ATTACK:
                    Player.Instance.AtkBonus+=(int)levelUpAmount; break;
                case ButtonType.HEALTH:
                    Player.Instance.HpBonus+=(int)levelUpAmount;
                    Player.Instance.CurrentHealth+=levelUpAmount;
                    break;
            }

            enableButton = false;
            LevelUpPanel.Instance.Hide();
        }
    }
}