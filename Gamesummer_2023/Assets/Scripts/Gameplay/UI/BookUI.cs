using GDC.Managers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GDC.Gameplay.UI
{
    public class BookUI : MonoBehaviour
    {
        [SerializeField] SpellUpgradePage[] spellUpgradePages;
        int curPageIndex = 0;
        [SerializeField] Button nextPageButton, prePageButton;

        public void ReloadCurrentPage()
        {
            spellUpgradePages[curPageIndex].ReloadPage();
            setButtonInteractable();
        }
        public void NextPage()
        {
            if (curPageIndex < spellUpgradePages.Length - 1)
            {
                SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_BOOK_PAGE);
                spellUpgradePages[curPageIndex].gameObject.SetActive(false);
                spellUpgradePages[curPageIndex + 1].gameObject.SetActive(true);
                spellUpgradePages[curPageIndex + 1].ReloadPage();
                curPageIndex++;

                setButtonInteractable();
            }
        }
        public void PrePage()
        {
            if (curPageIndex > 0)
            {
                SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_BOOK_PAGE);
                spellUpgradePages[curPageIndex].gameObject.SetActive(false);
                spellUpgradePages[curPageIndex - 1].gameObject.SetActive(true);
                spellUpgradePages[curPageIndex - 1].ReloadPage();
                curPageIndex--;

                setButtonInteractable();
            }
        }    

        void setButtonInteractable()
        {
            if (curPageIndex == spellUpgradePages.Length - 1)
            {
                nextPageButton.interactable = false;
            }
            else
            {
                nextPageButton.interactable = true;
            }

            if (curPageIndex == 0)
            {
                prePageButton.interactable = false;
            }
            else
            {
                prePageButton.interactable = true;
            }
        }
    }
}
