using DG.Tweening;
using GDC.Configuration;
using GDC.Managers;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace GDC.Gameplay.UI
{
    public class ExplainMapPanel : MonoBehaviour
    {
        public static ExplainMapPanel Instance { get; private set; }
        public RectTransform rectTransform;
        [SerializeField] TMP_Text nameText, explainText;


        private void Awake()
        {
            //if (Instance != null)
            //{
            //    Destroy(gameObject);
            //    return;
            //}
            Instance = this;

            rectTransform = gameObject.GetComponent<RectTransform>();
            gameObject.SetActive(false);
        }

        public void Show(string areaName, string info)
        {
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_START_UI);
            rectTransform.DOAnchorPosX(-557, 0.3f).SetUpdate(true);
            nameText.text = areaName;
            explainText.text = info;
        }

        public void Hide()
        {
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_END_UI);
            rectTransform.DOAnchorPosX(0, 0.3f).SetUpdate(true);
        }
    }
}
