using DG.Tweening;
using GDC.Enums;
using GDC.Managers;
using GDC.PlayerManager;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace GDC.Gameplay.UI
{
    public class GameOverPanel : MonoBehaviour
    {
        public static GameOverPanel Instance { get; private set; }

        private void Awake()
        {
            Instance = this;
        }

        [SerializeField] Image background,yesBtn,noBtn;
        [SerializeField] TMP_Text gameOverText,continueText;
        [SerializeField] float glowValue;
        [SerializeField] float tweenTime;
        [SerializeField] float blackDuration;
        [SerializeField] float delayToContinue;
        Material myMat;
        float currentGlowValue;
        Tween textTween;

        private void Start()
        {
            myMat = Instantiate(gameOverText.fontSharedMaterial);
            gameOverText.fontSharedMaterial = myMat;
            myMat.EnableKeyword("GLOW_ON");
        }

        public void TurnOn()
        {
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_GAME_OVER);
            textTween = DOTween.To(() => this.currentGlowValue, value => this.currentGlowValue = value, glowValue, tweenTime).SetLoops(-1, LoopType.Yoyo).OnUpdate(() =>
            {
                gameOverText.fontSharedMaterial.SetFloat("_GlowOuter", currentGlowValue);
            }).SetEase(Ease.InOutSine);

            background.DOFade(1, blackDuration);
            gameOverText.DOFade(1, blackDuration).OnComplete(() =>
            {
                textTween.Play();
                continueText.DOFade(1, 0.5f).SetDelay(delayToContinue);
                yesBtn.DOFade(1, 0.5f).SetDelay(delayToContinue).OnComplete(() =>
                {
                    yesBtn.raycastTarget = true;
                });
                noBtn.DOFade(1, 0.5f).SetDelay(delayToContinue).OnComplete(() =>
                {
                    noBtn.raycastTarget = true;
                });
            });
        }

        public void Continue()
        {
            SaveLoadManager.Instance.Load();
            StartCoroutine(Cor_Continue());
        }

        IEnumerator Cor_Continue()
        {
            yield return new WaitUntil(() => !SaveLoadManager.Instance.GameData.IsSaveLoadProcessing);
            Player.Instance.CurrentHealth = Player.Instance.MaxHealth;
            Player.Instance.Stamina = Player.Instance.MaxStamina;
            SaveLoadManager.Instance.CacheData.playerInitPos = SaveLoadManager.Instance.GameData.PlayerPosition;
            SceneType oldScneType = UIGameplay.Instance.sceneType;
            GameManager.Instance.LoadSceneAsyncManually(
                SaveLoadManager.Instance.GameData.CurrentSceneType,
                TransitionType.FADE,
                SaveLoadManager.Instance.GameData.CurrentSoundMapType(),
                cb: () =>
                {
                    GameManager.Instance.UnloadSceneManually(
                        oldScneType,
                        cb: () => GameManager.Instance.SetInitData()
                    );

                });
        }

        public void BackToMainMenu()
        {
            GameManager.Instance.LoadSceneManually(Enums.SceneType.MAIN, Enums.TransitionType.FADE);
        }
    }
}