using DG.Tweening;
using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;


namespace GDC.Gameplay.UI
{
    public class EchoTextUI : MonoBehaviour
    {
        public static EchoTextUI Instance { get; private set; }

        private void Awake()
        {
            Instance = this;
            Material myMat = Instantiate(redText.fontSharedMaterial);
            redText.fontSharedMaterial = myMat;
            myMat.SetColor("_OutlineColor", Color.red);
            myMat.SetFloat(ShaderUtilities.ID_OutlineWidth, 0.1f);
        }

        [SerializeField,ResizableTextArea] List<string> creepyTexts = new List<string>();
        [SerializeField] Vector2 duration,fontSize,timeBetweenText;
        [SerializeField] AnimationCurve fadeCurve;
        [SerializeField] TMP_Text redText,whiteText;
        [SerializeField] bool whiteTextInArea2 = false;

        public void SpawnTextInArea2()
        {
            StartCoroutine(Cor_SpawnTextInArea2());
        }    

        IEnumerator Cor_SpawnTextInArea2()
        {
            while (true)
            {
                if (whiteTextInArea2)
                    SpawnText(isBless: true);
                else
                    SpawnText();
                yield return new WaitForSeconds(Random.Range(timeBetweenText.x,timeBetweenText.y));
            }
        }    

        public void SpawnText(string specifiedText = null,bool isBless = false)
        {
            TMP_Text text = Instantiate(isBless? whiteText:redText, transform);
            text.GetComponent<RectTransform>().anchoredPosition = new Vector2(Random.Range(-900, 600f), Random.Range(-450, 450f));
            text.name = "Creepy Text";

            StartCoroutine(Cor_RunText(text,(specifiedText!=null)?specifiedText:creepyTexts[Random.Range(0, creepyTexts.Count)]));
            //text.color = textColor;
            text.fontSize = Random.Range(fontSize.x, fontSize.y);
            text.DOFade(1,1).SetEase(Ease.Linear);
        }

        IEnumerator Cor_RunText(TMP_Text text, string textInput)
        {
            foreach (char c in textInput)
            {
                text.text += c;
                yield return new WaitForSeconds(0.1f);
            }
            yield return new WaitForSeconds(1);
            text.DOFade(0, 0.5f).SetEase(Ease.Linear).OnComplete(() => Destroy(text.gameObject));
        }
    }
}