using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;
using UnityEngine.UI;

public class GetItemDisplay : MonoBehaviour
{
    RectTransform rectTransform;
    [SerializeField] RectTransform elementContainRect;
    [SerializeField] Image bgImage, itemImage;
    [SerializeField] TMP_Text itemNameText;

    float timeAnim = 0.5f;
    public int growLevel = 0;

    Tween rectTween;
    Coroutine hideCor;

    private void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
    }
    public void Show(Sprite itemSprite, string itemName)
    {
        if (itemImage.sprite != null)
        {
            itemImage.sprite = itemSprite;
        }
        if (itemNameText.text != null)
        {
            itemNameText.text = itemName;
        }
        growLevel = 0;
        elementContainRect.DOAnchorPosX(0, timeAnim);
        bgImage.DOColor(Color.white, timeAnim);
        itemImage.DOColor(Color.white, timeAnim);
        itemNameText.DOColor(Color.white, timeAnim);

        hideCor = StartCoroutine(Cor_Hide(2));
    }

    [NaughtyAttributes.Button]
    public void Hide()
    {
        elementContainRect.DOAnchorPosX(200, timeAnim);
        bgImage.DOColor(Color.clear, timeAnim);
        itemImage.DOColor(Color.clear, timeAnim);
        itemNameText.DOColor(Color.clear, timeAnim);

        if (hideCor != null)
        {
            StopCoroutine(hideCor);
        }
    }

    [NaughtyAttributes.Button]
    public void Grow()
    {
        growLevel = (growLevel + 1) % 4;
        if (rectTween != null)
        {
            rectTween.Kill();
        }

        rectTween = rectTransform.DOAnchorPosY(growLevel*120, timeAnim);
    }

    IEnumerator Cor_Hide(float sec)
    {
        yield return new WaitForSeconds(sec);
        Hide();
    }
}
