using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GDC.PlayerManager;
using DG.Tweening;
using TMPro;

namespace GDC.Gameplay.UI
{
    public class SliderManager : MonoBehaviour
    {
        public static SliderManager Instance { get; private set; }
        [SerializeField] private Slider heartSlider, staminaSlider,expSlider;
        public TMP_Text ExpText;
        [SerializeField] RectTransform heartIcon, staminaIcon;
        [SerializeField] Image heartFill, heartHandle, staminaFill, staminaHandle;
        Sprite heartFillSprite, heartHandleSprite, staminaFillSprite, staminaHandleSprite;
        [SerializeField] Sprite damageFillSprite, damageHandleSprite;
        private void Awake()
        {
            //if (Instance != null)
            //{
            //    Destroy(gameObject);
            //    return;
            //}
            Instance = this;
            UpdateSliderMaxValue();
        }
        private void Start()
        {
            heartFillSprite = heartFill.sprite;
            heartHandleSprite = heartHandle.sprite;
            staminaFillSprite = staminaFill.sprite;
            staminaHandleSprite = staminaHandle.sprite;
        }

        // Update is called once per frame
        void Update()
        {
            heartSlider.value = Player.Instance.CurrentHealth;
            staminaSlider.value = Player.Instance.Stamina;
            expSlider.value = Player.Instance.ShowExpInSlider();
        }

        public void UpdateSliderMaxValue() //co the dung trong cac script khac
        {
            StartCoroutine(Cor_UpdateSliderMaxValue());
        }

        IEnumerator Cor_UpdateSliderMaxValue()
        {
            yield return new WaitUntil(() => Player.Instance != null);
            yield return new WaitUntil(() => Player.Instance.MaxHealth > 0 && Player.Instance.MaxStamina > 0);
            heartSlider.maxValue = Player.Instance.MaxHealth;
            staminaSlider.maxValue = Player.Instance.MaxStamina;
            expSlider.value = Player.Instance.ShowExpInSlider();
        }

        public void HealEffect(bool healHeart, bool healStamina)
        {
            if(healHeart)
            {
                heartIcon.DOScale(0.8f, 0.1f).OnComplete
                (() =>
                    heartIcon.DOScale(1.2f, 0.2f).OnComplete
                    (() =>
                        heartIcon.DOScale(1, 0.1f)
                    )
                );
            }

            if (healStamina)
            {
                staminaIcon.DOScale(0.8f, 0.1f).OnComplete
                (() =>
                    staminaIcon.DOScale(1.2f, 0.2f).OnComplete
                    (() =>
                        staminaIcon.DOScale(1, 0.1f)
                    )
                );
            }    
        }
        public void DamageEffect(bool damageHeart, bool damageStamina)
        {
            if(damageHeart)
            {
                StartCoroutine(Cor_DamageEffect(heartFill, heartHandle, heartFillSprite, heartHandleSprite));
            }
            if(damageStamina)
            {
                StartCoroutine(Cor_DamageEffect(staminaFill, staminaHandle, staminaFillSprite, staminaHandleSprite));
            }
        }
        IEnumerator Cor_DamageEffect(Image fillImage, Image handleImage, Sprite fillSprite, Sprite handleSprite)
        {
            fillImage.sprite = damageFillSprite;
            handleImage.sprite = damageHandleSprite;
            yield return new WaitForSeconds(0.2f);
            fillImage.sprite = fillSprite;
            handleImage.sprite = handleSprite;
        }
    }
}
