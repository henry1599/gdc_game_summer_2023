using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GDC.Configuration;
using GDC.Managers;

namespace GDC.Gameplay.UI
{
    public class ItemChoosing : MonoBehaviour
    {
        public SO_Item SO_item;
        [SerializeField] Image image;

        // Update is called once per frame
        void Update()
        {
            if (SO_item!=null)
            {
                Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                mousePos.z = 0;
                transform.position = mousePos;
            }
        }

        public void SetChoosingItem(SO_Item SO_item)
        {
            this.SO_item = SO_item;
            if (SO_item != null)
            {
                image.color = Color.white;
                image.sprite = SO_item.sprite;

                if (SO_item.EquipmentType != Enums.EquipmentType.NONE)
                {
                    EquipmentManager.Instance.SetChangeValue(SO_item);
                }
            }
            else
            {
                image.color = Color.clear;
            }
        }
    }
}
