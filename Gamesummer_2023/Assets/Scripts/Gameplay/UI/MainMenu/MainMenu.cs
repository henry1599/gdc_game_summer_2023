using DG.Tweening;
using GDC.Enums;
using GDC.Managers;
using NaughtyAttributes;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using RotaryHeart.Lib.SerializableDictionary;
using GDC.Gameplay.Cutscene;
using GDC.Gameplay;

namespace GDC.MainMenu
{
    [Serializable]
    public class AreaBackground : SerializableDictionaryBase<AreaType,Texture2D> { }

    public class MainMenu : MonoBehaviour
    {
        #region main menu
        [SerializeField, Foldout("MainMenu")] AnimationCurve glowCurve;
        [SerializeField, Foldout("MainMenu")] GameObject blockContinue;
        [SerializeField, Foldout("MainMenu")] CustomButton continueButton;
        [SerializeField, Foldout("MainMenu")] Transform creditPanel,quitPanel;
        [SerializeField, Foldout("MainMenu")] AreaBackground backgroundDict;
        [SerializeField, Foldout("MainMenu")] Image background;
        [SerializeField, Foldout("MainMenu")] GameObject mainMenu;
        [SerializeField, Foldout("MainMenu")] RectTransform buttons;
        [SerializeField, Foldout("MainMenu")] RectTransform upBorder, downBorder;
        [SerializeField, Foldout("MainMenu")] Transform settingPanel;
        [SerializeField, Foldout("MainMenu")] RectTransform confirmPanel;
        bool isLoading = false;

        IEnumerator Cor_LoadBackground()
        {
            yield return new WaitUntil(() => SaveLoadManager.Instance != null);
            SaveLoadManager.Instance.Load();
            yield return new WaitUntil(() => SaveLoadManager.Instance.GameData.IsSaveLoadProcessing == false);
            if (!SaveLoadManager.Instance.GameData.IsHaveSaveData)
            {
                blockContinue.SetActive(true);
                continueButton.FadeSetUp();
            }
            else
                blockContinue.SetActive(false);
            background.material.SetTexture("_MainTex",backgroundDict[SaveLoadManager.Instance.GameData.CurrentAreaType]);
        }

        
        //void TextAnimation()
        //{
        //    gameName.ForceMeshUpdate();
        //    var textInfo = gameName.textInfo;
        //    for (int i = 0; i < textInfo.characterCount; ++i)
        //    {
        //        var charInfo = textInfo.characterInfo[i];
        //        if (!charInfo.isVisible)
        //            continue;

        //        var verts = textInfo.meshInfo[charInfo.materialReferenceIndex].vertices;

        //        for (int j = 0; j < 4; ++j)
        //        {
        //            var orig = verts[charInfo.vertexIndex + j];
        //            verts[charInfo.vertexIndex + j] = orig + new Vector3(0, Mathf.Sin(Time.time * 2 + orig.x * 0.01f) * 10, 0);
        //        }
        //    }

        //    for (int i = 0; i < textInfo.meshInfo.Length; ++i)
        //    {
        //        var meshInfo = textInfo.meshInfo[i];
        //        meshInfo.mesh.vertices = meshInfo.vertices;
        //        gameName.UpdateGeometry(meshInfo.mesh, i);
        //    }
        //}

        public void NewGame()
        {
            if (isLoading)
                return;
            isLoading = true;
            SaveLoadManager.Instance.ResetData();
            GameManager.Instance.LoadSceneAsyncManually(
                SceneType.INTRO, 
                TransitionType.FADE, 
                SoundType.INTRO,
                cb: () => GameManager.Instance.UnloadSceneManually(SceneType.MAIN),
                true
            );
        }

        public void ContinueGame()
        {
            if (isLoading)
                return;
            isLoading = true;
            StartCoroutine(Cor_Continue());
        }

        public void OpenConfirm()
        {
            if (!SaveLoadManager.Instance.GameData.IsHaveSaveData)
                NewGame();
            else
                confirmPanel.DOScale(1, 0.5f).SetEase(Ease.OutBack);
        }

        public void CloseConfirm()
        {
            confirmPanel.DOScale(0, 0.5f).SetEase(Ease.InBack);
        }
        
        IEnumerator Cor_Continue()
        {
            yield return new WaitUntil(() => SaveLoadManager.Instance.GameData.IsSaveLoadProcessing == false);
            Debug.Log("Start load into game");
            SaveLoadManager.Instance.CacheData.playerInitPos = SaveLoadManager.Instance.GameData.PlayerPosition;   

            GameManager.Instance.LoadSceneAsyncManually(
                SaveLoadManager.Instance.GameData.CurrentSceneType,
                TransitionType.FADE,
                SaveLoadManager.Instance.GameData.CurrentSoundMapType(),
                cb: () =>
                {
                    GameManager.Instance.UnloadSceneManually(
                        SceneType.MAIN,
                        cb: () => GameManager.Instance.SetInitData()
                    );

                },
                true);
        }

        public void OpenCreditPanel()
        {
            creditPanel.DOScale(1, 0.5f).SetEase(Ease.OutBack);
        }


        public void CloseCreditPanel()
        {
            creditPanel.DOScale(0, 0.5f);
        }

        public void OpenQuitPanel()
        {
            quitPanel.DOScale(1, 0.5f).SetEase(Ease.OutBack);
        }

        public void CloseQuitPanel()
        {
            quitPanel.DOScale(0, 0.5f);
        }

        public void QuitGame()
        {
            Application.Quit();
        }   

        public void ShowButtons()
        {
            buttons.DOAnchorPosX(0, 0.5f).SetEase(Ease.InBack).SetEase(Ease.OutBack);
            upBorder.DOAnchorPosY(490, 0.5f);
            downBorder.DOAnchorPosY(-490, 0.5f);
        }

        public void HideButtons()
        {
            buttons.DOAnchorPosX(1620, 0.5f).SetEase(Ease.InBack).SetEase(Ease.OutBack);
            upBorder.DOAnchorPosY(590, 0.5f);
            downBorder.DOAnchorPosY(-590, 0.5f);
        }

        public void OpenSettingPanel()
        {
            settingPanel.DOScale(1, 0.5f).SetEase(Ease.OutBack);
        }
        public void CloseSettingPanel()
        {
            settingPanel.DOScale(0, 0.5f).SetEase(Ease.InBack);
        }
        #endregion

        #region before main menu
        [SerializeField, Foldout("Before main menu")] Animator smallBook;
        [SerializeField, Foldout("Before main menu")] AnimationClip bookClip;
        [SerializeField, Foldout("Before main menu")] SpriteRenderer largeBook;
        [SerializeField, Foldout("Before main menu")] Sprite[] bookSprites;
        [SerializeField, Foldout("Before main menu")] ParticleSystem dust;
        [SerializeField, Foldout("Before main menu")] Transform circle;
        [SerializeField, Foldout("Before main menu")] float bookAnimStep;
        [SerializeField, Foldout("Before main menu")] GameObject cutsceneBeforeMainMenu;
        IEnumerator BookAnimation()
        {
            yield return new WaitForSeconds(1);
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_APPEAR);
            dust.Play();
            smallBook.transform.DOScale(1, 0.5f).SetEase(Ease.OutBack);
            yield return new WaitForSeconds(0.5f);
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_BOOK_PAGE);
            smallBook.enabled = true;
            yield return new WaitForSeconds(bookClip.length);
            smallBook.gameObject.SetActive(false);
            largeBook.gameObject.SetActive(true);
            yield return new WaitForSeconds(bookAnimStep);
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_LIGHT_TRANSITION);
            for (int i=7;i< bookSprites.Length;i++)
            {
                float scaleUp = bookSprites[i].textureRect.size.magnitude / largeBook.sprite.textureRect.size.magnitude;
                largeBook.transform.DOScale(scaleUp, bookAnimStep).SetEase(Ease.Linear).OnComplete(() =>
                {
                    largeBook.sprite = bookSprites[i];
                    largeBook.transform.localScale = Vector3.one;
                });
                yield return new WaitForSeconds(bookAnimStep);
            }

            largeBook.transform.DOScale(1.5f, bookAnimStep);
            circle.DOScale(30,bookAnimStep);
            circle.GetComponent<SpriteRenderer>().DOFade(1, bookAnimStep).OnComplete(()=>
            {
                mainMenu.SetActive(true);
                largeBook.gameObject.SetActive(false);
                circle.GetComponent<SpriteRenderer>().DOFade(0, 1).SetDelay(1);
            });
            yield return new WaitForSeconds(1.5f);
            SoundManager.Instance.PlayMusicWithIntro(AudioPlayer.SoundID.MUSIC_MAIN_MENU_BEGIN, AudioPlayer.SoundID.MUSIC_MAIN_MENU);
        }

        void AnimBeforeMainMenu()
        {
            //dust.Play();
            //book.transform.DOScale(1, 0.5f).SetEase(Ease.OutBack).OnComplete(() =>
            //{
                StartCoroutine(BookAnimation());
            //});
        }

        #endregion
        void Start()
        {
            StartCoroutine(Cor_LoadBackground());
            AnimBeforeMainMenu();

            // SoundManager.Instance.LoadSoundMap(SoundType.MAIN_MENU);
            // SoundManager.Instance.LoadSoundMap(SoundType.COMMON);
        }
    }
}