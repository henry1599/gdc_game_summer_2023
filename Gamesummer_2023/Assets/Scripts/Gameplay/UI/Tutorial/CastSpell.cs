using DG.Tweening;
using GDC.PlayerManager.Spell;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GDC.Gameplay.UI
{
    public class CastSpell : TutorialPanel
    {
        [SerializeField] Animator graphic;
        [SerializeField] Transform slime;
        [SerializeField] Vector2 slimeInitialPos;
        [SerializeField] SpriteRenderer mouse;
        [SerializeField] Sprite[] mouseSprite;
        [SerializeField] float clickDuration, duration,obtainDuration;
        [SerializeField] GameObject spellShow;
        [SerializeField] GameObject disappearEffect;
        [SerializeField] Spell template;
        [SerializeField] GameObject castSpellAnimation;
        [SerializeField] ParticleSystem dust;
        Spell clone = null;

        void SlimeAnimation()
        {
            slime.DORotate(new Vector3(0, 0, 5), 0.2f).SetUpdate(true).OnComplete(() => 
            {
                slime.DORotate(new Vector3(0, 0, -5), 0.2f).SetUpdate(true).OnComplete(() =>
                {
                    SlimeAnimation();
                });
            });
        }

        IEnumerator Cor_CastSpellAnimation()
        {
            castSpellAnimation.SetActive(true);
            yield return new WaitForSecondsRealtime(1.5f);
            castSpellAnimation.SetActive(false);
        }

        IEnumerator Cor_MouseAnimation()
        {
            yield return new WaitForSecondsRealtime(clickDuration);
            mouse.sprite = mouseSprite[1];
            if (clone!=null)
            {
                clone.gameObject.SetActive(true);
                yield return new WaitForSecondsRealtime(2);
                mouse.sprite = mouseSprite[0];
                spellShow.SetActive(false);
                clone.HandleCast();
                if (gameObject.activeSelf) StartCoroutine(Cor_CastSpellAnimation());
            }
            else
            {
                yield return new WaitForSecondsRealtime(clickDuration);
                mouse.sprite = mouseSprite[0];
            }
        }

        IEnumerator Cor_SpellAnimation()
        {

            slime.transform.localPosition = slimeInitialPos;
            SlimeAnimation();
            slime.gameObject.SetActive(true);

            yield return new WaitForSecondsRealtime(0.5f);
            if (gameObject.activeSelf) StartCoroutine(Cor_MouseAnimation());
            graphic.Play("obtain");
            slime.DOMove(graphic.transform.position, obtainDuration / 2).SetUpdate(true).OnComplete(() => 
            {
                clone = Instantiate(template, transform);
                slime.gameObject.SetActive(false);
                dust.Play();
                if (gameObject.activeSelf) StartCoroutine(Cor_Disappear());
            });
            yield return new WaitForSecondsRealtime(obtainDuration);
            graphic.Play("idle");
            spellShow.SetActive(true);

            

            yield return new WaitForSecondsRealtime(duration);
            if (gameObject.activeSelf) StartCoroutine(Cor_MouseAnimation());
            yield return new WaitUntil(() => clone == null);
            yield return new WaitForSecondsRealtime(1);
            if (gameObject.activeSelf) StartCoroutine(Cor_SpellAnimation());
        }

        IEnumerator Cor_Disappear()
        {
            disappearEffect.transform.position = slime.position;
            disappearEffect.SetActive(true);
            yield return new WaitForSecondsRealtime(2);
            disappearEffect.SetActive(false);
        }

        public override void OpenPanel()
        {
            gameObject.SetActive(true);
            if (gameObject.activeSelf) StartCoroutine(Cor_SpellAnimation());
        }

        public override void ClosePanel()
        {
            slime.transform.localPosition = slimeInitialPos;
            spellShow.SetActive(false);
            mouse.sprite = mouseSprite[0];
            castSpellAnimation.SetActive(false);
            graphic.Play("idle");
            
            if (clone != null) Destroy(clone.gameObject);
            gameObject.SetActive(false);
        }

        protected override void Start()
        {
            base.Start();
            template.FixedCamera = true;
        }
    }
}