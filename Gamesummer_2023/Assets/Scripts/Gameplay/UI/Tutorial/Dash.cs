using DG.Tweening;
using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GDC.Gameplay.UI
{
    public class Dash : TutorialPanel
    {
        [SerializeField] Transform graphic;
        [SerializeField] Animator anim;
        [SerializeField] Vector2 initialPos;
        [SerializeField] float dashDistance;
        [SerializeField] SpriteRenderer altButton;
        [SerializeField] Sprite[] altButtonSprites;
        [SerializeField] float clickDuration, dashDuration, durationBetweenClick;

        #region clone
        [SerializeField,Foldout("Clone")] SpriteRenderer[] sprites;
        [SerializeField, Foldout("Clone")] int numberOfClone;
        [SerializeField, Foldout("Clone")] float fadeDuration, timeBetweenClone;

        IEnumerator Cor_SpawnCloneDash()
        {

            GameObject template = new();
            for (int i = 0; i < numberOfClone; i++)
            {
                GameObject clone = Instantiate(template, transform);
                clone.name = "Clone (" + i.ToString() + ")";
                clone.transform.position = graphic.transform.position;
                clone.transform.localScale = graphic.transform.localScale;
                foreach (SpriteRenderer sprite in sprites)
                {
                    SpriteRenderer cloneSprite = Instantiate(sprite, clone.transform);
                    cloneSprite.transform.position = sprite.transform.position;
                    cloneSprite.DOColor(Color.clear, fadeDuration).SetUpdate(true).OnComplete(() => { Destroy(clone); });
                }

                yield return new WaitForSecondsRealtime(timeBetweenClone);
            }

            Destroy(template);
        }
        #endregion


        IEnumerator Cor_Dash()
        {
            yield return new WaitForSecondsRealtime(0.5f);
            altButton.sprite = altButtonSprites[1];
            yield return new WaitForSecondsRealtime(clickDuration);
            altButton.sprite = altButtonSprites[2];

            anim.Play("dash");
            if (gameObject.activeSelf) StartCoroutine(Cor_SpawnCloneDash());
            graphic.DOLocalMoveX(initialPos.x + dashDistance, dashDuration).SetUpdate(true).OnComplete(()=>
            {
                
                if (gameObject.activeSelf) StartCoroutine(Cor_SetUpDash());
                
            });

            yield return new WaitForSecondsRealtime(clickDuration);
            altButton.sprite = altButtonSprites[1];
            yield return new WaitForSecondsRealtime(clickDuration);
            altButton.sprite = altButtonSprites[0];
        }

        IEnumerator Cor_SetUpDash()
        {
            anim.Play("idle");
            graphic.localPosition = initialPos;
            yield return new WaitForSecondsRealtime(durationBetweenClick);
            if (gameObject.activeSelf) StartCoroutine(Cor_Dash());
            //yield return new WaitForSecondsRealtime(durationBetweenClick-0.5f);
        }

        public override void OpenPanel()
        {
            gameObject.SetActive(true);
            if (gameObject.activeSelf) StartCoroutine(Cor_SetUpDash());
        }

        public override void ClosePanel()
        {
            StopAllCoroutines();
            anim.Play("idle");
            graphic.localPosition = initialPos;
            gameObject.SetActive(false);
        }
    }
}