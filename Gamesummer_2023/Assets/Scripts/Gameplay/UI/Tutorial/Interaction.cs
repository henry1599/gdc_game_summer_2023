using DG.Tweening;
using GDC.Enums;
using GDC.Managers;
using NaughtyAttributes;
using RotaryHeart.Lib.SerializableDictionary;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GDC.Gameplay.UI
{
    public class Interaction : TutorialPanel
    {
        [SerializeField] Transform bubbleChat;
        [SerializeField] float duration, clickDuration;
        [SerializeField] SpriteRenderer fButton;
        [SerializeField] Sprite[] fButtonSprites;

        IEnumerator Cor_Interact()
        {
            fButton.sprite = fButtonSprites[1];
            yield return new WaitForSecondsRealtime(clickDuration);
            bubbleChat.DOScale(0, 0.5f).SetUpdate(true);
            fButton.sprite = fButtonSprites[2];
            yield return new WaitForSecondsRealtime(duration);
            fButton.sprite = fButtonSprites[1];
            bubbleChat.DOScale(1, 0.5f).SetUpdate(true).OnComplete(()=> { if (gameObject.activeSelf) StartCoroutine(Cor_Interact()); });
            yield return new WaitForSecondsRealtime(clickDuration);
            fButton.sprite = fButtonSprites[0];
        }


        public override void OpenPanel()
        {
            gameObject.SetActive(true);
            bubbleChat.GetComponent<Animator>().Play("PressF");
            if (gameObject.activeSelf) StartCoroutine(Cor_Interact());
        }

        public override void ClosePanel()
        {
            gameObject.SetActive(false);
            bubbleChat.localScale = Vector3.one;
        }
    }
}