using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GDC.Gameplay.UI
{
    public class Attack : TutorialPanel
    {
        [SerializeField] Transform hand,pointer,graphic,weapon;
        [SerializeField] float duration, clickDuration,rotateStep,handRadius,pointerRadius;
        float currentAngle = 90;
        [SerializeField,ReadOnly] float angleSlash = 0;
        [SerializeField] Animator attackAnim;
        [SerializeField] SpriteRenderer mouse;
        [SerializeField] Sprite[] mouseSprite;
        [HideInInspector] public bool canRotate;

        void Rotate()
        {
            currentAngle += duration / 360 / rotateStep;
            if (currentAngle >= 360)
                currentAngle -= 360;

            float angle = currentAngle * ((currentAngle >= 180) ? -1 : 1);
            graphic.localScale = new Vector3((currentAngle >= 180) ? -1 : 1, 1, 1);
            pointer.localScale = new Vector3((currentAngle >= 180) ? -1f : 1f, 1f, 1);

            hand.localPosition = new Vector3(handRadius * Mathf.Sin((angle+angleSlash) * Mathf.Deg2Rad), -handRadius * Mathf.Cos((angle+angleSlash) * Mathf.Deg2Rad));
            hand.localRotation = Quaternion.Euler(0, 0, angle+angleSlash);

            pointer.localPosition = new Vector3(pointerRadius * Mathf.Sin(angle * Mathf.Deg2Rad), -pointerRadius * Mathf.Cos(angle * Mathf.Deg2Rad));
        }

        IEnumerator Cor_AttackAnimation()
        {
            yield return new WaitForSecondsRealtime(clickDuration);
            attackAnim.enabled = true;
            attackAnim.Play("attack", -1, 0);
            mouse.sprite = mouseSprite[1];
            yield return new WaitForSecondsRealtime(attackAnim.GetCurrentAnimatorStateInfo(0).length);
            mouse.sprite = mouseSprite[0];
            if (gameObject.activeSelf) StartCoroutine(Cor_AttackAnimation());
        }

        public override void OpenPanel()
        {
            gameObject.SetActive(true);
            InvokeRepeating(nameof(Rotate), 0, rotateStep);
            if (gameObject.activeSelf) StartCoroutine(Cor_AttackAnimation());

            canRotate = true;
            StartCoroutine(Cor_RepeatRotateHelper());
        }
        public IEnumerator Cor_RepeatRotateHelper()
        {
            CancelInvoke();
            while (canRotate)
            {
                Rotate();
                yield return new WaitForSecondsRealtime(0.01f);
            }
        }

        public override void ClosePanel()
        {
            canRotate = false;
            gameObject.SetActive(false);
            CancelInvoke();
            currentAngle = 90;
        }
    }
}