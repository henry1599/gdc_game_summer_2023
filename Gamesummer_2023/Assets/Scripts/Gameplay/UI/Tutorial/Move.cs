using DG.Tweening;
using NaughtyAttributes;
using RotaryHeart.Lib.SerializableDictionary;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GDC.Gameplay.UI
{
    public enum ButtonType
    {
        W,
        A,
        S,
        D,
        UP_ARROW,
        LEFT_ARROW,
        RIGHT_ARROW,
        DOWN_ARROW,
    }

    [System.Serializable]
    public class ButtonInfo
    {
        public SpriteRenderer Button;
        [ShowAssetPreview] public Sprite[] ButtonSprite = new Sprite[3];
    }

    [System.Serializable]
    public class ButtonDict : SerializableDictionaryBase<ButtonType, ButtonInfo> { }
    public class Move : TutorialPanel
    {
        [SerializeField] ButtonDict buttonDict;
        [SerializeField] Vector2[] graphicPos = new Vector2[4];
        [SerializeField] Transform graphic;
        [SerializeField] Transform buttonContainer;
        [SerializeField] float distance, duration, clickDuration;

        List<Coroutine> coroutines = new();

        IEnumerator Cor_MoveButton(ButtonType type)
        {
            ButtonInfo info = buttonDict[type];
            info.Button.sprite = info.ButtonSprite[1];
            yield return new WaitForSecondsRealtime(clickDuration);
            info.Button.sprite = info.ButtonSprite[2];
            yield return new WaitForSecondsRealtime(duration);
            info.Button.sprite = info.ButtonSprite[1];
            yield return new WaitForSecondsRealtime(clickDuration);
            info.Button.sprite = info.ButtonSprite[0];
        }
        Tween currentTween = null;

        void MoveAnimation()
        {
            graphic.localScale = new Vector3(1, 1, 1);
            if (gameObject.activeSelf) coroutines.Add(StartCoroutine(Cor_MoveButton(ButtonType.D)));
            currentTween = graphic.DOLocalMove(graphicPos[1], duration).SetEase(Ease.Linear).SetDelay(clickDuration).SetUpdate(true);
            currentTween.Play().OnComplete(() =>
            {
                if (gameObject.activeSelf) coroutines.Add(StartCoroutine(Cor_MoveButton(ButtonType.W)));
                currentTween = graphic.DOLocalMove(graphicPos[2], duration).SetEase(Ease.Linear).SetDelay(clickDuration).SetUpdate(true);
                currentTween.Play().OnComplete(() =>
                {
                    graphic.localScale = new Vector3(-1, 1, 1);
                    if (gameObject.activeSelf) coroutines.Add(StartCoroutine(Cor_MoveButton(ButtonType.A)));
                    currentTween = graphic.DOLocalMove(graphicPos[3], duration).SetEase(Ease.Linear).SetDelay(clickDuration).SetUpdate(true);
                    currentTween.Play().OnComplete(() =>
                    {
                        if (gameObject.activeSelf) coroutines.Add(StartCoroutine(Cor_MoveButton(ButtonType.S)));
                        currentTween = graphic.DOLocalMove(graphicPos[0], duration).SetEase(Ease.Linear).SetDelay(clickDuration).SetUpdate(true);
                        currentTween.Play().OnComplete(() =>
                        {
                            graphic.localScale = new Vector3(1, 1, 1);
                            currentTween = buttonContainer.DOLocalMoveX(-distance, 0.5f).SetUpdate(true);
                            currentTween.Play().OnComplete(() =>
                            {
                                if (gameObject.activeSelf) coroutines.Add(StartCoroutine(Cor_MoveButton(ButtonType.RIGHT_ARROW)));
                                graphic.localScale = new Vector3(1, 1, 1);
                                currentTween = graphic.DOLocalMove(graphicPos[1], duration).SetEase(Ease.Linear).SetDelay(clickDuration).SetUpdate(true);
                                currentTween.Play().OnComplete(() =>
                                {
                                    if (gameObject.activeSelf) coroutines.Add(StartCoroutine(Cor_MoveButton(ButtonType.UP_ARROW)));
                                    currentTween = graphic.DOLocalMove(graphicPos[2], duration).SetEase(Ease.Linear).SetDelay(clickDuration).SetUpdate(true);
                                    currentTween.Play().OnComplete(() =>
                                    {
                                        graphic.localScale = new Vector3(-1, 1, 1);
                                        if (gameObject.activeSelf) coroutines.Add(StartCoroutine(Cor_MoveButton(ButtonType.LEFT_ARROW)));
                                        currentTween = graphic.DOLocalMove(graphicPos[3], duration).SetEase(Ease.Linear).SetDelay(clickDuration).SetUpdate(true);
                                        currentTween.Play().OnComplete(() =>
                                        {
                                            if (gameObject.activeSelf) coroutines.Add(StartCoroutine(Cor_MoveButton(ButtonType.DOWN_ARROW)));
                                            graphic.DOLocalMove(graphicPos[0], duration).SetEase(Ease.Linear).SetDelay(clickDuration).SetUpdate(true);
                                            currentTween.Play().OnComplete(() =>
                                            {
                                                graphic.localScale = new Vector3(1, 1, 1);
                                                currentTween = buttonContainer.DOLocalMoveX(0, 0.5f).SetUpdate(true);
                                                currentTween.Play().OnComplete(() =>
                                                {
                                                    MoveAnimation();
                                                });
                                            });
                                        });

                                    });
                                });
                            });
                        });
                    });

                });
            });

        }

        public override void OpenPanel()
        {
            gameObject.SetActive(true);
            MoveAnimation();
        }

        public override void ClosePanel()
        {
            graphic.localPosition = graphicPos[0];
            currentTween.Kill();
            StopAllCoroutines();
            gameObject.SetActive(false);
        }
    }
}