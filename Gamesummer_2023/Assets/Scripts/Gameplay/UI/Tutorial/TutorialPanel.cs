using AudioPlayer;
using DG.Tweening;
using Gameplay;
using GDC.Managers;
using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GDC.Gameplay.UI
{
    public abstract class TutorialPanel : MonoBehaviour
    {
        [SerializeField] Transform fBtn;
        [HideInInspector] public bool isCanPressF;

        protected virtual void Start()
        {
            Vector3 cameraPos = CameraController.Instance.transform.position;
            cameraPos.z = 0;
            transform.position = cameraPos;
            transform.localScale = Vector3.zero;
            isCanPressF = false;
            if (fBtn != null) fBtn.localScale = Vector3.zero;
            transform.DOScale(1.2f, 0.3f).SetUpdate(true).OnComplete(() =>
            {
                transform.DOScale(1f, 0.1f).SetUpdate(true).OnComplete(() => 
                {
                    if (gameObject.activeSelf) 
                        if (fBtn != null) StartCoroutine(Cor_ShowFButton());
                });
            });
            Open();
            //Debug.Log("A");
        }
        private void Update()
        {
            if (isCanPressF)
            {
                if (Input.GetKeyUp(KeyCode.F))
                {
                    SoundManager.Instance.PlaySound(SoundID.SFX_INTERACT);
                    SoundManager.Instance.PlaySound(SoundID.SFX_HIDE_CARRY_SLOT);
                    isCanPressF = false;
                    transform.DOScale(0, 0.4f).SetUpdate(true).OnComplete(() => Close());
                }
            }
        }
        IEnumerator Cor_ShowFButton()
        {
            yield return new WaitForSecondsRealtime(1f);
            fBtn.DOScale(1.2f, 0.2f).SetUpdate(true).OnComplete(() =>
            {
                fBtn.DOScale(1f, 0.1f).SetUpdate(true).OnComplete(() => isCanPressF = true);
            });
        }

        public abstract void OpenPanel();
        public abstract void ClosePanel();

        [Button]
        void Open()
        {
            OpenPanel();
        }

        [Button]
        void Close()
        {
            ClosePanel();
        }
    }
}
