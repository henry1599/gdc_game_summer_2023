using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GDC.Managers;
using TMPro;
using GDC.Configuration;
using DG.Tweening;

namespace GDC.Gameplay.UI
{

    public class ShopSlot : MonoBehaviour
    {
        [SerializeField] Image slotImage, iconImage;
        [SerializeField] TMP_Text nameText, amountText, valueText;
        [SerializeField] Button increaseBtn, decreaseBtn;

        int value, amount;

        [SerializeField] SO_Item SO_item;

        RectTransform rectTranform;

        public int slotIndex;

        private void Awake()
        {
            slotIndex = transform.GetSiblingIndex();
            rectTranform = GetComponent<RectTransform>();
        }

        public void Setup(SO_Item SO_item)
        {
            if (SO_item == null)
            {
                gameObject.SetActive(false);
                return;
            }

            gameObject.SetActive(true);
            iconImage.sprite = SO_item.sprite;
            nameText.text = SO_item.itemName;

            amount = 1;
            value = SO_item.cost;

            amountText.text = amount.ToString();
            valueText.text = value.ToString();

            this.SO_item = SO_item;
        }

        public void IncreaseAmount()
        {
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_RELEASE_ITEM);
            amount++;
            if (amount >= 99)
            {
                amount = 99;
                increaseBtn.interactable = false;
            }
            decreaseBtn.interactable = true;

            value = amount * SO_item.cost;
            amountText.text = amount.ToString();
            valueText.text = value.ToString();
        }
        public void DecreaseAmount()
        {
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_RELEASE_ITEM);
            amount--;
            if (amount <= 1)
            {
                amount = 1;
                decreaseBtn.interactable = false;
            }
            increaseBtn.interactable = true;

            value = amount * SO_item.cost;
            amountText.text = amount.ToString();
            valueText.text = value.ToString();
        }

        public void Buy()
        {
            if (SaveLoadManager.Instance.GameData.Coin < value)
            {
                TriggerDialogue.Instance.BuyDialogue(false);
            }
            else
            {
                SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_PURCHASE);
                TriggerDialogue.Instance.BuyDialogue(true);
                SaveLoadManager.Instance.GameData.Coin -= value;
                CoinDisplay.Instance.UpdateCoin();
                SaveLoadManager.Instance.GameData.AddItem(SO_item);
            }
        }
        public void OnEnter()
        {
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_HOLD_ITEM);
            ExplainPanel.Instance.Show(SO_item);
            EquipmentManager.Instance.SetChangeValue(SO_item);
        }
        public void OnExit()
        {
            ExplainPanel.Instance.Hide();
            EquipmentManager.Instance.SetChangeValue(null);
        }

        public void OnDown()
        {
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_RELEASE_ITEM);
            rectTranform.DOScale(new Vector3(0.8f, 0.8f, 1), 0.05f).SetUpdate(true);
        }
        public void OnUp()
        {
            rectTranform.DOScale(new Vector3(1f, 1f, 1), 0.05f).SetUpdate(true);
        }
    }
}
