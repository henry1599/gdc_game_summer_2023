using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using GDC.Gameplay.UI;
using GDC.Configuration;
using System;
using GDC.Constants;
using GDC.Managers;

namespace GDC.Gameplay
{
    public class ShopManager : MonoBehaviour
    {
        public static ShopManager Instance { get; private set; }
        RectTransform rectTransform;
        [SerializeField] Transform itemContent;

        public bool isOpening;

        Tween hideTween, explainHideTween, equipHideTween;

        [SerializeField] List<SO_Item> so_items; // just for test

        private void Awake()
        {
            //if (Instance != null)
            //{
            //    Destroy(gameObject);
            //    return;
            //}
            Instance = this;

            rectTransform = gameObject.GetComponent<RectTransform>();

            gameObject.SetActive(false);
        }

        public void LoadShopItem(List<SO_Item> so_items)
        {
            int index = 0;
            foreach (var data in so_items)
            {
                itemContent.GetChild(index).GetComponent<ShopSlot>().Setup(data);
                index++;
                if (index >= GameConstants.MAX_SHOP_SLOT)
                {
                    Debug.Log("Full slot");
                    return;
                }
            }

            for (int i = index; i < GameConstants.MAX_SHOP_SLOT; i++)
            {
                itemContent.GetChild(i).gameObject.SetActive(false);
            }
        }

        [NaughtyAttributes.Button]
        public void Show()
        {
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_START_UI);
            hideTween.Kill();
            explainHideTween.Kill();
            equipHideTween.Kill();

            gameObject.SetActive(true);
            EquipmentManager.Instance.gameObject.SetActive(true);
            ExplainPanel.Instance.gameObject.SetActive(true);
            EquipmentManager.Instance.gameObject.SetActive(true);
            Time.timeScale = 0;
            isOpening = true;
            rectTransform.DOAnchorPosY(0, 0.5f).SetUpdate(true);
            ExplainPanel.Instance.rectTransform.DOAnchorPosY(-209f, 0.5f).SetUpdate(true);
            EquipmentManager.Instance.rectTransform.DOAnchorPosY(93f, 0.5f).SetUpdate(true);
        }

        [NaughtyAttributes.Button]
        public void Hide()
        {
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_END_UI);
            Time.timeScale = 1;
            isOpening = false;
            hideTween = rectTransform.DOAnchorPosY(-1000, 0.5f).SetUpdate(true).OnComplete(() => gameObject.SetActive(false));
            explainHideTween = ExplainPanel.Instance.rectTransform.DOAnchorPosY(-1209f, 0.5f).SetUpdate(true).OnComplete(
                () => ExplainPanel.Instance.gameObject.SetActive(false));
            equipHideTween = EquipmentManager.Instance.rectTransform.DOAnchorPosY(-907f, 0.5f).SetUpdate(true).OnComplete(
                () => EquipmentManager.Instance.gameObject.SetActive(false));

            TriggerDialogue.Instance.DisplayDialogue();
        }
    }
}
