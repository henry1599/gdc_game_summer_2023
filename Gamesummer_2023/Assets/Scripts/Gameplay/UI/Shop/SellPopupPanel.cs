using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GDC.Managers;
using TMPro;
using GDC.Configuration;
using DG.Tweening;

namespace GDC.Gameplay.UI
{
    public class SellPopupPanel : MonoBehaviour
    {
        public static SellPopupPanel Instance { get; private set; }
        [SerializeField] Image slotImage, iconImage;
        [SerializeField] TMP_Text nameText, amountText, valueText;
        [SerializeField] Button increaseBtn, decreaseBtn, sellButton;

        int value, amount, maxAmount;
        public bool isOpening;

        [SerializeField] SO_Item SO_item;

        RectTransform rectTransform;

        private void Awake()
        {
            //if (Instance != null)
            //{
            //    Destroy(gameObject);
            //    return;
            //}
            Instance = this;

            rectTransform = GetComponent<RectTransform>();
            gameObject.SetActive(false);
        }

        public void Setup(SO_Item SO_item)
        {
            if (SO_item == null)
            {
                gameObject.SetActive(false);
                return;
            }

            maxAmount = SaveLoadManager.Instance.GameData.GetAmountOfItem(SO_item);
            Debug.Log(SO_item.itemName + " " + maxAmount);
            iconImage.sprite = SO_item.sprite;
            nameText.text = SO_item.itemName;

            amount = 1;

            this.SO_item = SO_item;

            SetupAmountButton();
        }

        void SetupAmountButton()
        {
            if (maxAmount < 1)
            {
                amount = 0;
                increaseBtn.interactable = false;
                decreaseBtn.interactable = false;
                sellButton.interactable = false;
            }
            else
            {
                sellButton.interactable = true;
                if (amount >= maxAmount)
                {
                    amount = maxAmount;
                    increaseBtn.interactable = false;
                }
                else
                {
                    increaseBtn.interactable = true;
                }

                if (amount <= 1)
                {
                    amount = 1;
                    decreaseBtn.interactable = false;
                }
                else
                {
                    decreaseBtn.interactable = true;
                }
            }

            value = amount * SO_item.cost/2;
            amountText.text = amount.ToString();
            valueText.text = value.ToString();
        }
        public void IncreaseAmount()
        {
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_RELEASE_ITEM);
            amount++;
            SetupAmountButton();
        }
        public void DecreaseAmount()
        {
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_RELEASE_ITEM);
            amount--;
            SetupAmountButton();
        }

        public void Sell()
        {
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_PURCHASE);
            SaveLoadManager.Instance.GameData.ReduceItem(SO_item, amount);
            maxAmount = SaveLoadManager.Instance.GameData.GetAmountOfItem(SO_item);
            Debug.Log(maxAmount);
            SaveLoadManager.Instance.GameData.Coin += value;
            CoinDisplay.Instance.UpdateCoin();
            SetupAmountButton();
            InventoryManager.Instance.ReloadCurrentPanel();
            TriggerDialogue.Instance.SellDialogue(true);
            //Hide();
        }
        public void OnEnter()
        {
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_HOLD_ITEM);
            ExplainPanel.Instance.Show(SO_item);
            EquipmentManager.Instance.SetChangeValue(SO_item);
        }
        public void OnExit()
        {
            ExplainPanel.Instance.Hide();
            EquipmentManager.Instance.SetChangeValue(null);
        }

        public void OnDown()
        {
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_RELEASE_ITEM);
            rectTransform.DOScale(new Vector3(0.8f, 0.8f, 1), 0.05f).SetUpdate(true);
        }
        public void OnUp()
        {
            rectTransform.DOScale(new Vector3(1f, 1f, 1), 0.05f).SetUpdate(true);
        }

        public void Show()
        {
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_START_UI);
            isOpening = true;
            gameObject.SetActive(true);
            rectTransform.localScale = Vector2.zero;
            rectTransform.DOScale(1f, 0.5f).SetUpdate(true);
        }

        public void Hide()
        {
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_END_UI);
            rectTransform.DOScale(0, 0.5f).SetUpdate(true).OnComplete(
                () =>
                {
                    gameObject.SetActive(false);
                    isOpening = false;
                }
                );
        }
    }
}
