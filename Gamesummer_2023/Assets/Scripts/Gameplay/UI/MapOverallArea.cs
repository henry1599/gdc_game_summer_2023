using GDC.Managers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GDC.Gameplay.UI
{
    public class MapOverallArea : MonoBehaviour
    {
        [SerializeField] List<MapAreaImage> mapAreaImages;
        [SerializeField] RectTransform playerIcon, targetIcon;
        bool isHaveTargetOnMap = false;
        public void Setup()
        {
            foreach(var mapAreaImage in mapAreaImages)
            {
                mapAreaImage.Setup();
                foreach (var sceneType in mapAreaImage.sceneTypes)
                {
                    if (sceneType == SaveLoadManager.Instance.GameData.CurrentSceneType)
                    {
                        playerIcon.position = mapAreaImage.GetComponent<RectTransform>().position;
                    }
                    if (sceneType == SaveLoadManager.Instance.GameData.TargetSceneType)
                    {
                        isHaveTargetOnMap = true;
                        targetIcon.gameObject.SetActive(true);
                        targetIcon.position = mapAreaImage.GetComponent<RectTransform>().position;
                    }
                }
            }
            if (SaveLoadManager.Instance.GameData.TargetSceneType == Enums.SceneType.MAIN || isHaveTargetOnMap == false)
            {
                targetIcon.gameObject.SetActive(false);
            }    
        }
    }
}
