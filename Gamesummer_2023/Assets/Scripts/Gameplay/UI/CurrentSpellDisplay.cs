using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using GDC.PlayerManager.Spell;
using GDC.Enums;
using GDC.Managers;

namespace GDC.Gameplay.UI
{
    public class CurrentSpellDisplay : MonoBehaviour
    {
        public static CurrentSpellDisplay Instance { get; private set; }

        RectTransform rectTransform;
        [SerializeField] Image slotImage;
        [SerializeField] Image cancelBtn;
        [SerializeField] RectTransform cancelBtnRect;

        [Space(10)]

        [SerializeField] Sprite spikeDartSlot;
        [SerializeField]
        Sprite poisonBombSlot, fireBreathSlot, iceSpitSlot, enegryBallSlot, thunderShockSlot, spinAttackSlot, hypnosisSlot,
            hyperBeamSlot, teleportSlot, atkBuffSlot, defBuffSlot, speBuffSlot;
        private void Awake()
        {
            //if (Instance != null)
            //{
            //    Destroy(gameObject);
            //    return;
            //}
            Instance = this;
            this.rectTransform = gameObject.GetComponent<RectTransform>();
        }

        public void Setup()
        {
            Debug.Log("Setup spell display");
            if (SaveLoadManager.Instance.GameData.CurrentSpell == SpellType.NONE)
            {
                Hide();
            }
            else
            {
                Show();
            }
            HideCancelButton(true);
        }
        public void Show()
        {
            ChangeSprite();
            this.rectTransform.DOAnchorPosX(-200, 0.5f);
        }
        [NaughtyAttributes.Button]
        public void Hide()
        {
            this.rectTransform.DOAnchorPosX(200, 0.5f);
        }
        public void ShowCancelButton()
        {
            StopAllCoroutines();
            cancelBtnRect.DOAnchorPosX(-44,0.5f);
            cancelBtn.DOColor(Color.white, 0.5f);
        }
        public void HideCancelButton(bool hideImmediately = false)
        {
            if (hideImmediately)
            {
                cancelBtnRect.anchoredPosition = new Vector2(-120, 47);
                cancelBtn.color = Color.clear;
            }
            else
            {
                StartCoroutine(Cor_HideCancelButton());
            }
        }
        IEnumerator Cor_HideCancelButton()
        {
            yield return new WaitForSeconds(3);
            cancelBtnRect.DOAnchorPosX(-120, 0.5f);
            cancelBtn.DOColor(Color.clear, 0.5f);
        }
        public void CancelSpell()
        {
            Hide();
            SaveLoadManager.Instance.GameData.CurrentSpell = SpellType.NONE;
            PlayerSpell.Instance.CurrentSpellType = SpellType.NONE;
            PlayerSpell.Instance.CancelSpell();
            PlayerSpell.Instance.SpawnCancelText();
            //todo more here
        }

        public void ChangeSprite()
        {
            slotImage.color = Color.white;
            switch (SaveLoadManager.Instance.GameData.CurrentSpell)
            {
                case SpellType.NONE:
                    slotImage.color = Color.clear;
                    break;
                case SpellType.SPIKE_DART:
                    slotImage.sprite = spikeDartSlot;
                    break;
                case SpellType.POISON_BOMB:
                    slotImage.sprite = poisonBombSlot;
                    break;
                case SpellType.FIRE_BREATH:
                    slotImage.sprite = fireBreathSlot;
                    break;
                case SpellType.ICE_SPIT:
                    slotImage.sprite = iceSpitSlot;
                    break;
                case SpellType.ENERGY_BALL:
                    slotImage.sprite = enegryBallSlot;
                    break;
                case SpellType.THUNDER_SHOCK:
                    slotImage.sprite = thunderShockSlot;
                    break;
                case SpellType.SPIN_ATTACK:
                    slotImage.sprite = spinAttackSlot;
                    break;
                case SpellType.TELEPORT:
                    slotImage.sprite = teleportSlot;
                    break;
                case SpellType.HYPNOSIS:
                    slotImage.sprite = hypnosisSlot;
                    break;
                case SpellType.HYPER_BEAM:
                    slotImage.sprite = hyperBeamSlot;
                    break;
                case SpellType.ATK_BUFF:
                    slotImage.sprite = atkBuffSlot;
                    break;
                case SpellType.DEF_BUFF:
                    slotImage.sprite = defBuffSlot;
                    break;
                case SpellType.SPE_BUFF:
                    slotImage.sprite = speBuffSlot;
                    break;
            }
        }
    }
}
