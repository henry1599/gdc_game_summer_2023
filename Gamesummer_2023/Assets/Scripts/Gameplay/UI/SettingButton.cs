using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using GDC.Managers;
using UnityEngine.UI;
using GDC.PlayerManager.Spell;
using GDC.Enums;

namespace GDC.Gameplay.UI
{
    public class SettingButton : MonoBehaviour
    {
        [SerializeField] RectTransform rectTransform;
        [SerializeField] RectTransform settingPanel, creditPanel, quitPanel, guidePanel;
        [Header("Guide Panel")]
        [SerializeField] List<TutorialPanel> panels;
        [SerializeField] List<TutorialPanel> tempPanels;
        [SerializeField] Button rightButton, leftButton;
        bool isOpening;
        int idx = 0;
        void Start()
        {
            Setup();
        }
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                OnClick();
            }
        }
        void Setup()
        {
            idx = 0;

            if (!SaveLoadManager.Instance.GameData.isPlayerHaveWeapon) {
                if (panels[2]) 
                {
                    panels[2].gameObject.SetActive(false);
                    panels[2] = null;
                }
            }
            else panels[2] = tempPanels[0];

            if (!SaveLoadManager.Instance.GameData.isPlayerHaveSpell) {
                if (panels[3]) 
                {
                    panels[3].gameObject.SetActive(false);
                    panels[3] = null;
                }
            }
            else panels[3] = tempPanels[1];

            if (!SaveLoadManager.Instance.GameData.isPlayerHaveDash) {
                if (panels[4]) 
                {
                    panels[4].gameObject.SetActive(false);
                    panels[4] = null;
                }
            }
            else panels[4] = tempPanels[2];

            foreach (var panel in panels)
            {
                if (panel != null) 
                {
                    panel.transform.localPosition = Vector3.zero;
                    panel.isCanPressF = false;
                    panel.transform.localScale = Vector3.zero;
                    panel.gameObject.SetActive(false);
                }
            }
        }
        public void OnEnter()
        {
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_HOLD_ITEM);
            rectTransform.DOScale(new Vector3(1.3f, 1.3f, 1.3f), 0.5f).SetUpdate(true);
        }    
        public void OnExit()
        {
            rectTransform.DOScale(Vector3.one, 0.5f).SetUpdate(true);
        }
        public void OnClick()
        {
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_PRESS_BUTTON);      
            if (!isOpening)
            {
                Setup();
                SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_START_UI);
                UIGameplay.Instance.isPause = true;
                settingPanel.DOAnchorPosY(0, 0.5f).SetUpdate(true);
                isOpening = true;
                Time.timeScale = 0;
            }
            else OnClickExitSettingPanel();
        }
        public void OnClickExitSettingPanel()
        {
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_RELEASE_ITEM);
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_END_UI);
            Time.timeScale = 1;
            UIGameplay.Instance.isPause = false;

            settingPanel.DOAnchorPosY(-1100, 0.5f).SetUpdate(true);
            creditPanel.DOAnchorPosY(-1150, 0.5f).SetUpdate(true);
            quitPanel.DOAnchorPosY(-1150, 0.5f).SetUpdate(true);
            guidePanel.DOAnchorPosY(-1150, 0.5f).SetUpdate(true).OnComplete(() => 
            {
                panels[idx].ClosePanel();
                idx = 0;
            });
            
            isOpening = false;
        }
        public void OnClickCreditButton()
        {
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_RELEASE_ITEM);
            creditPanel.DOAnchorPosY(-50, 0.5f).SetUpdate(true);
        }
        public void OnClickQuitButton()
        {
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_RELEASE_ITEM);
            quitPanel.DOAnchorPosY(-50, 0.5f).SetUpdate(true);
        }
        public void OnClickGuideButton()
        {
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_RELEASE_ITEM);
            guidePanel.gameObject.SetActive(true);
            guidePanel.DOAnchorPosY(-50, 0.5f).SetUpdate(true);

            panels[0].gameObject.SetActive(true);
            panels[0].OpenPanel();

            panels[idx].transform.localScale = Vector3.zero;
            panels[0].transform.localScale = Vector3.one * 75;
        }
        public void YesQuitGame()
        {
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_RELEASE_ITEM);
            Application.Quit();
            print("QUIT");
        }
        public void NoQuitGame()
        {
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_RELEASE_ITEM);
            quitPanel.DOAnchorPosY(-1150, 0.5f).SetUpdate(true);
        }
        public void OnClickExitCreditPanel()
        {
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_RELEASE_ITEM);
            creditPanel.DOAnchorPosY(-1150, 0.5f).SetUpdate(true);
        }
        public void OnClickExitGuidePanel()
        {
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_RELEASE_ITEM);
            guidePanel.DOAnchorPosY(-1150, 0.5f).SetUpdate(true).OnComplete(() => 
            {
                panels[idx].ClosePanel();
                idx = 0;
            });

        }
        public void ChangeTutorialButton(int num)
        {
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_RELEASE_ITEM);
            panels[idx].transform.localScale = Vector3.zero;
            panels[idx].ClosePanel();
            idx = (idx + num + panels.Count) % panels.Count;
            while (panels[idx] == null)
                idx = (idx + num + panels.Count) % panels.Count;

            panels[idx].transform.localScale = Vector3.one * 75;
            panels[idx].OpenPanel();
        }
    }
}
