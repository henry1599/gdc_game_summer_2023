using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.Managers;
using GDC.Constants;
using GDC.Configuration;
using DG.Tweening;
using GDC.Enums;
using NaughtyAttributes;
using TMPro;
using GDC.PlayerManager;

namespace GDC.Gameplay.UI
{
    public class InventoryManager : MonoBehaviour
    {
        public static InventoryManager Instance { get; private set; }
        RectTransform rectTransform;
        [Foldout("Item content")]
        [SerializeField] Transform weaponContent, clothesContent, useableItemContent, ingredientContent, keyItemContent;

        [Foldout("Craft content")]
        [SerializeField] Transform weaponCraftContent, clothesCraftContent, useableItemCraftContent, ingredientCraftContent, keyItemCraftContent;

        [Foldout("Item panel")]
        [SerializeField] GameObject weaponPanel, clothesPanel, useableItemPanel, ingredientPanel, keyItemPanel;

        [Foldout("Craft panel")]
        [SerializeField] GameObject weaponCraftPanel, clothesCraftPanel, useableItemCraftPanel, ingredientCraftPanel, keyItemCraftPanel;

        [SerializeField] InventoryButtonDisplay weaponButtonScr;
        public InventoryButtonController inventoryButtonController;

        public SO_Item ChoosingItem = null;
        public int CurrentPointerSlotIndex = 0;
        public int CurrentPointerCarrySlotIndex = -1;
        public bool isOpening = false, isOnCraftMode = false;
        public EquipmentType CurrentPointerEquipSlotType;

        [SerializeField] ItemChoosing itemChoosingScr;
        public TMP_Text InventoryText;

        public List<CarrySlot> CarrySlotList = new List<CarrySlot>(GameConstants.MAX_CARRY_SLOT);
        
        [Foldout("Equipment slot")]
        public EquipSlot HelmetSlot, ArmorSlot, WeaponSlot, ShoeSlot;

        Tween hideTween, explainHideTween, equipHideTween;

        public bool isSelling;

        private void Awake()
        {
            //if (Instance != null)
            //{
            //    Destroy(gameObject);
            //    return;
            //}
            Instance = this;

            rectTransform = gameObject.GetComponent<RectTransform>();
            SetChoosingItem(null);

            gameObject.SetActive(false);
        }

        public void InitLoad()
        {
            LoadWeaponItem();
            weaponButtonScr.OnDown();

            LoadCarrySlot();
            LoadEquipSlot();
        }

        public void Show(bool isIgnoreCondition = false)
        {
            if (isIgnoreCondition == false)
            {
                if (Player.Instance.InCutScene) return;
                if (SpellUpgradeManager.Instance?.isOpening == true)
                {
                    SpellUpgradeManager.Instance.Hide();
                }
                if (MapManager.Instance?.isOpening == true)
                {
                    MapManager.Instance.Hide();
                }
                if (isOpening == true)
                {
                    Hide();
                    return;
                }
            }

            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_START_UI);
            hideTween.Kill();
            explainHideTween.Kill();
            equipHideTween.Kill();

            gameObject.SetActive(true);
            inventoryButtonController.SetupButton();
            EquipmentManager.Instance.gameObject.SetActive(true);
            ExplainPanel.Instance.gameObject.SetActive(true);
            EquipmentManager.Instance.gameObject.SetActive(true);
            Time.timeScale = 0;
            isOpening = true;
            if (isSelling)
            {
                rectTransform.DOAnchorPosY(-150, 0.5f).SetUpdate(true);
            }
            else
            {
                rectTransform.DOAnchorPosY(0, 0.5f).SetUpdate(true);
            }
            ExplainPanel.Instance.rectTransform.DOAnchorPosY(-209f, 0.5f).SetUpdate(true);
            EquipmentManager.Instance.rectTransform.DOAnchorPosY(93f, 0.5f).SetUpdate(true);
            ReloadCurrentPanel();
        }    
        public void Hide()
        {
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_END_UI);
            Time.timeScale = 1;
            isOpening = false;
            hideTween = rectTransform.DOAnchorPosY(-1000, 0.5f).SetUpdate(true).OnComplete(()=>gameObject.SetActive(false));
            explainHideTween = ExplainPanel.Instance.rectTransform.DOAnchorPosY(-1209f, 0.5f).SetUpdate(true).OnComplete(
                () => ExplainPanel.Instance.gameObject.SetActive(false));
            equipHideTween = EquipmentManager.Instance.rectTransform.DOAnchorPosY(-907f, 0.5f).SetUpdate(true).OnComplete(
                () => EquipmentManager.Instance.gameObject.SetActive(false));

            CarrySlotManager.Instance?.Hide();

            if (isSelling == true)
            {
                isSelling = false;
                TriggerDialogue.Instance.DisplayDialogue();
            }
        }
        void HideAllInventoryPanel()
        {
            if (weaponPanel.activeSelf)
            {
                weaponPanel.SetActive(false);
                inventoryButtonController.SetupWeaponItem();
            }
            if (clothesPanel.activeSelf)
            {
                clothesPanel.SetActive(false);
                inventoryButtonController.SetupClothesItem();
            }
            if (useableItemPanel.activeSelf)
            {
                useableItemPanel.SetActive(false);
                inventoryButtonController.SetupUseableItemItem();
            }
            if (ingredientPanel.activeSelf)
            {
                ingredientPanel.SetActive(false);
                inventoryButtonController.SetupIngredientItem();
            }
            if (keyItemPanel.activeSelf)
            {
                keyItemPanel.SetActive(false);
                inventoryButtonController.SetupKeyItemItem();
            }

            if (weaponCraftPanel.activeSelf)
            {
                weaponCraftPanel.SetActive(false);
                inventoryButtonController.SetupWeaponCraft();
            }
            if (clothesCraftPanel.activeSelf)
            {
                clothesCraftPanel.SetActive(false);
                inventoryButtonController.SetupClothesCraft();
            }
            if (useableItemCraftPanel.activeSelf)
            {
                useableItemCraftPanel.SetActive(false);
                inventoryButtonController.SetupUseableItemCraft();
            }
            if (ingredientCraftPanel.activeSelf)
            {
                ingredientCraftPanel.SetActive(false);
                inventoryButtonController.SetupIngredientCraft();
            }
            if (keyItemCraftPanel.activeSelf)
            {
                keyItemCraftPanel.SetActive(false);
                inventoryButtonController.SetupKeyItemCraft();
            }
            weaponCraftPanel?.SetActive(false);
            clothesCraftPanel?.SetActive(false);
            useableItemCraftPanel?.SetActive(false);
            ingredientCraftPanel?.SetActive(false);
            keyItemCraftPanel?.SetActive(false);

            CarrySlotManager.Instance?.Hide();
        }
        #region Open Each Inventory
        public void LoadIngredientItem()
        {
            if (isOnCraftMode)
            {
                LoadIngredientCraftItem();
                return;
            }
            HideAllInventoryPanel();
            inventoryButtonController.HideNewIngredient();
            ingredientPanel.SetActive(true);
            int index = 0;
            if (SaveLoadManager.Instance.GameData.PlayerIngredientList != null)
            {
                foreach (var data in SaveLoadManager.Instance.GameData.PlayerIngredientList)
                {
                    ingredientContent.GetChild(index).GetComponent<InventorySlot>().Setup(data);
                    index++;
                    if (index >= GameConstants.MAX_INVENTORY_SLOT)
                    {
                        Debug.Log("Full slot");
                    }
                }
            }
            for (int i=index; i<GameConstants.MAX_INVENTORY_SLOT; i++)
            {
                ingredientContent.GetChild(i).GetComponent<InventorySlot>().Setup(null);
            }    
        }    
        public void LoadWeaponItem()
        {
            if (isOnCraftMode)
            {
                LoadWeaponCraftItem();
                return;
            }
            HideAllInventoryPanel();
            inventoryButtonController.HideNewWeapon();
            weaponPanel.SetActive(true);
            int index = 0;
            if (SaveLoadManager.Instance.GameData.PlayerWeaponList != null)
            {
                foreach (var data in SaveLoadManager.Instance.GameData.PlayerWeaponList)
                {
                    weaponContent.GetChild(index).GetComponent<InventorySlot>().Setup(data);
                    index++;
                    if (index >= GameConstants.MAX_INVENTORY_SLOT)
                    {
                        Debug.Log("Full slot");
                    }
                }
            }
            for (int i = index; i < GameConstants.MAX_INVENTORY_SLOT; i++)
            {
                weaponContent.GetChild(i).GetComponent<InventorySlot>().Setup(null);
            }
        }
        public void LoadClothesItem()
        {
            if (isOnCraftMode)
            {
                LoadClothesCraftItem();
                return;
            }
            HideAllInventoryPanel();
            inventoryButtonController.HideNewClothes();
            clothesPanel.SetActive(true);
            int index = 0;
            if (SaveLoadManager.Instance.GameData.PlayerClothesList != null)
            {
                foreach (var data in SaveLoadManager.Instance.GameData.PlayerClothesList)
                {
                    clothesContent.GetChild(index).GetComponent<InventorySlot>().Setup(data);
                    index++;
                    if (index >= GameConstants.MAX_INVENTORY_SLOT)
                    {
                        Debug.Log("Full slot");
                    }
                }
            }
            for (int i = index; i < GameConstants.MAX_INVENTORY_SLOT; i++)
            {
                clothesContent.GetChild(i).GetComponent<InventorySlot>().Setup(null);
            }
        }
        public void LoadUseableItem()
        {
            if (isOnCraftMode)
            {
                LoadUseableItemCraftItem();
                return;
            }
            HideAllInventoryPanel();
            inventoryButtonController.HideNewUseableItem();
            CarrySlotManager.Instance?.Show();
            useableItemPanel.SetActive(true);
            int index = 0;
            if (SaveLoadManager.Instance.GameData.PlayerUseableItemList != null)
            {
                foreach (var data in SaveLoadManager.Instance.GameData.PlayerUseableItemList)
                {
                    //Debug.Log("index : " + index + " Itemdata: " + data.ToString());
                    useableItemContent.GetChild(index).GetComponent<InventorySlot>().Setup(data);
                    index++;
                    if (index >= GameConstants.MAX_INVENTORY_SLOT)
                    {
                        Debug.Log("Full slot");
                    }
                }
            }
            for (int i = index; i < GameConstants.MAX_INVENTORY_SLOT; i++)
            {
                useableItemContent.GetChild(i).GetComponent<InventorySlot>().Setup(null);
            }
        }
        public void LoadKeyItem()
        {
            if (isOnCraftMode)
            {
                LoadKeyItemCraftItem();
                return;
            }
            HideAllInventoryPanel();
            inventoryButtonController.HideNewKeyItem();
            keyItemPanel.SetActive(true);
            int index = 0;
            if (SaveLoadManager.Instance.GameData.PlayerKeyItemList != null)
            {
                foreach (var data in SaveLoadManager.Instance.GameData.PlayerKeyItemList)
                {
                    keyItemContent.GetChild(index).GetComponent<InventorySlot>().Setup(data);
                    index++;
                    if (index >= GameConstants.MAX_INVENTORY_SLOT)
                    {
                        Debug.Log("Full slot");
                    }
                }
            }
            for (int i = index; i < GameConstants.MAX_INVENTORY_SLOT; i++)
            {
                keyItemContent.GetChild(i).GetComponent<InventorySlot>().Setup(null);
            }
        }

        public void ReloadCurrentPanel()
        {
            if (weaponPanel.activeSelf == true)
            {
                LoadWeaponItem();
            }
            else if (clothesPanel.activeSelf == true)
            {
                LoadClothesItem();
            }
            else if (useableItemPanel.activeSelf == true)
            {
                LoadUseableItem();
            }
            else if (ingredientPanel.activeSelf == true)
            {
                LoadIngredientItem();
            }
            else if (keyItemPanel.activeSelf == true)
            {
                LoadKeyItem();
            }
            //--------------craft-------------
            else if (weaponCraftPanel.activeSelf == true)
            {
                if (isOnCraftMode)
                    LoadWeaponCraftItem();
                else
                    LoadWeaponItem();
            }
            else if (clothesCraftPanel.activeSelf == true)
            {
                if (isOnCraftMode)
                    LoadClothesCraftItem();
                else
                    LoadClothesItem();
            }
            else if (useableItemCraftPanel.activeSelf == true)
            {
                if (isOnCraftMode)
                    LoadUseableItemCraftItem();
                else
                    LoadUseableItem();
            }
            else if (ingredientCraftPanel.activeSelf == true)
            {
                if (isOnCraftMode)
                    LoadIngredientCraftItem();
                else
                    LoadIngredientItem();
            }
            else if (keyItemCraftPanel.activeSelf == true)
            {
                if (isOnCraftMode)
                    LoadKeyItemCraftItem();
                else
                    LoadKeyItem();
            }
            else
            {
                Debug.LogError("Khong co inventory panel nao dang mo!");
            }
        }
        #endregion

        #region Open each craft
        public void LoadIngredientCraftItem()
        {
            HideAllInventoryPanel();
            inventoryButtonController.HideNewIngredient();
            ingredientCraftPanel.SetActive(true);
            int index = 0;
            if (SaveLoadManager.Instance.GameData.PlayerIngredientCraftList != null)
            {
                foreach (var data in SaveLoadManager.Instance.GameData.PlayerIngredientCraftList)
                {
                    ingredientCraftContent.GetChild(index).GetComponent<CraftSlot>().Setup(data);
                    index++;
                    if (index >= GameConstants.MAX_CRAFT_SLOT)
                    {
                        Debug.Log("Full craft slot");
                    }
                }
            }
            for (int i = index; i < GameConstants.MAX_CRAFT_SLOT; i++)
            {
                ingredientCraftContent.GetChild(i).GetComponent<CraftSlot>().Setup(null);
            }
        }
        public void LoadWeaponCraftItem()
        {
            HideAllInventoryPanel();
            inventoryButtonController.HideNewWeapon();
            weaponCraftPanel.SetActive(true);
            int index = 0;
            if (SaveLoadManager.Instance.GameData.PlayerWeaponCraftList != null)
            {
                foreach (var data in SaveLoadManager.Instance.GameData.PlayerWeaponCraftList)
                {
                    weaponCraftContent.GetChild(index).GetComponent<CraftSlot>().Setup(data);
                    index++;
                    if (index >= GameConstants.MAX_CRAFT_SLOT)
                    {
                        Debug.Log("Full craft slot");
                    }
                }
            }
            for (int i = index; i < GameConstants.MAX_CRAFT_SLOT; i++)
            {
                weaponCraftContent.GetChild(i).GetComponent<CraftSlot>().Setup(null);
            }
        }
        public void LoadClothesCraftItem()
        {
            HideAllInventoryPanel();
            inventoryButtonController.HideNewClothes();
            clothesCraftPanel.SetActive(true);
            int index = 0;
            if (SaveLoadManager.Instance.GameData.PlayerClothesCraftList != null)
            {
                foreach (var data in SaveLoadManager.Instance.GameData.PlayerClothesCraftList)
                {
                    clothesCraftContent.GetChild(index).GetComponent<CraftSlot>().Setup(data);
                    index++;
                    if (index >= GameConstants.MAX_CRAFT_SLOT)
                    {
                        Debug.Log("Full craft slot");
                    }
                }
            }
            for (int i = index; i < GameConstants.MAX_CRAFT_SLOT; i++)
            {
                clothesCraftContent.GetChild(i).GetComponent<CraftSlot>().Setup(null);
            }
        }
        public void LoadUseableItemCraftItem()
        {
            HideAllInventoryPanel();
            inventoryButtonController.HideNewUseableItem();
            useableItemCraftPanel.SetActive(true);
            int index = 0;
            Debug.Log("LoadUseableItemCraftItem with PlayerUseableItemCraftList.Count = " + SaveLoadManager.Instance.GameData.PlayerUseableItemCraftList.Count);
            if (SaveLoadManager.Instance.GameData.PlayerUseableItemCraftList != null)
            {
                foreach (var data in SaveLoadManager.Instance.GameData.PlayerUseableItemCraftList)
                {
                    Debug.Log(SaveLoadManager.Instance.GameData.PlayerUseableItemCraftList[0]);
                    Debug.Log("data at index " + index + " is " + data);
                    useableItemCraftContent.GetChild(index).GetComponent<CraftSlot>().Setup(data);
                    index++;
                    if (index >= GameConstants.MAX_CRAFT_SLOT)
                    {
                        Debug.Log("Full craft slot");
                    }
                }
            }
            for (int i = index; i < GameConstants.MAX_CRAFT_SLOT; i++)
            {
                useableItemCraftContent.GetChild(i).GetComponent<CraftSlot>().Setup(null);
            }
        }
        public void LoadKeyItemCraftItem()
        {
            HideAllInventoryPanel();
            inventoryButtonController.HideNewKeyItem();
            keyItemCraftPanel.SetActive(true);
            int index = 0;
            if (SaveLoadManager.Instance.GameData.PlayerKeyItemCraftList != null)
            {
                foreach (var data in SaveLoadManager.Instance.GameData.PlayerKeyItemCraftList)
                {
                    keyItemCraftContent.GetChild(index).GetComponent<CraftSlot>().Setup(data);
                    index++;
                    if (index >= GameConstants.MAX_CRAFT_SLOT)
                    {
                        Debug.Log("Full craft slot");
                    }
                }
            }
            for (int i = index; i < GameConstants.MAX_CRAFT_SLOT; i++)
            {
                keyItemCraftContent.GetChild(i).GetComponent<CraftSlot>().Setup(null);
            }
        }
        #endregion

        [NaughtyAttributes.Button]
        public void LoadCarrySlot()
        {
            Debug.Log("Load carry slot");
            int i = 0;
            foreach (var carrySlot in CarrySlotList)
            {
                carrySlot.Setup(SaveLoadManager.Instance.GameData.CarrySlotList[i]);
                //Debug.Log("Gamedata.CarrySlotList at index " + i + " = " + SaveLoadManager.Instance.GameData.CarrySlotList[i]);
                i++;
            }
        }

        [NaughtyAttributes.Button]
        public void LoadEquipSlot()
        {
            Debug.Log("Load equip slot");
            HelmetSlot.Setup(SaveLoadManager.Instance.GameData.HelmetEquipItem);
            ArmorSlot.Setup(SaveLoadManager.Instance.GameData.ArmorEquipItem);
            WeaponSlot.Setup(SaveLoadManager.Instance.GameData.WeaponEquipItem);
            ShoeSlot.Setup(SaveLoadManager.Instance.GameData.ShoeEquipItem);

            EquipmentManager.Instance.SetData();
            PlayerDisplay.Instance.UpdateSprite();
        }

        public void SetChoosingItem(SO_Item SO_item)
        {
            if (SO_item != null)
            {
                SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_HOLD_ITEM);
            }
            else if (ChoosingItem != null)
            {
                SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_RELEASE_ITEM);
            }

            ChoosingItem = SO_item;
            itemChoosingScr.SetChoosingItem(SO_item);        
        }

        public void PayBackChoosingItem()
        {
            Debug.Log("A");
            if (ChoosingItem.itemType == ItemType.WEAPON)
            {
                weaponContent.GetChild(CurrentPointerSlotIndex).GetComponent<InventorySlot>().IncreaseAmount();
            }
            else if (ChoosingItem.itemType == ItemType.CLOTHES)
            {
                clothesContent.GetChild(CurrentPointerSlotIndex).GetComponent<InventorySlot>().IncreaseAmount();
            }
            else if (ChoosingItem.itemType == ItemType.USEABLE_ITEM)
            {
                useableItemContent.GetChild(CurrentPointerSlotIndex).GetComponent<InventorySlot>().IncreaseAmount();
            }
            else if (ChoosingItem.itemType == ItemType.INGREDIENT)
            {
                ingredientContent.GetChild(CurrentPointerSlotIndex).GetComponent<InventorySlot>().IncreaseAmount();
            }
            else if (ChoosingItem.itemType == ItemType.KEY_ITEM)
            {
                keyItemContent.GetChild(CurrentPointerSlotIndex).GetComponent<InventorySlot>().IncreaseAmount();
            }

            itemChoosingScr.SetChoosingItem(null);
        }

        public void SortList(ref List<ItemData> playerItemList)
        {
            for (int i = 0; i < playerItemList.Count - 1; i++)
            {
                for (int j = i; j < playerItemList.Count; j++)
                {
                    if (playerItemList[i].SO_item.ID.CompareTo(playerItemList[j].SO_item.ID)>0)
                    {
                        var sub = playerItemList[i];
                        playerItemList[i] = playerItemList[j];
                        playerItemList[j] = sub;
                    }
                }
            }
        }
        public void SortCraftList(ref List<SO_Craft> playerCraftList)
        {
            for (int i = 0; i < playerCraftList.Count - 1; i++)
            {
                for (int j = i; j < playerCraftList.Count; j++)
                {
                    if (playerCraftList[i].SO_item.ID.CompareTo(playerCraftList[j].SO_item.ID) > 0)
                    {
                        var sub = playerCraftList[i];
                        playerCraftList[i] = playerCraftList[j];
                        playerCraftList[j] = sub;
                    }
                }
            }
        }

        [NaughtyAttributes.Button]
        public void SortItem()
        {
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_RELEASE_ITEM);
            if (weaponPanel.activeSelf == true)
            {
                SortList(ref SaveLoadManager.Instance.GameData.PlayerWeaponList);
                LoadWeaponItem();
            }
            else if (clothesPanel.activeSelf == true)
            {
                SortList(ref SaveLoadManager.Instance.GameData.PlayerClothesList);
                LoadClothesItem();
            }
            else if (useableItemPanel.activeSelf == true)
            {
                SortList(ref SaveLoadManager.Instance.GameData.PlayerUseableItemList);
                LoadUseableItem();
            }
            else if (ingredientPanel.activeSelf == true)
            {
                SortList(ref SaveLoadManager.Instance.GameData.PlayerIngredientList);
                LoadIngredientItem();
            }
            else if (keyItemPanel.activeSelf == true)
            {
                SortList(ref SaveLoadManager.Instance.GameData.PlayerKeyItemList);
                LoadKeyItem();
            }
            //------------craft-----------
            else if (weaponCraftPanel.activeSelf == true)
            {
                SortCraftList(ref SaveLoadManager.Instance.GameData.PlayerWeaponCraftList);
                LoadWeaponCraftItem();
            }
            else if (clothesCraftPanel.activeSelf == true)
            {
                SortCraftList(ref SaveLoadManager.Instance.GameData.PlayerClothesCraftList);
                LoadClothesCraftItem();
            }
            else if (useableItemCraftPanel.activeSelf == true)
            {
                SortCraftList(ref SaveLoadManager.Instance.GameData.PlayerUseableItemCraftList);
                LoadUseableItemCraftItem();
            }
            else if (ingredientCraftPanel.activeSelf == true)
            {
                SortCraftList(ref SaveLoadManager.Instance.GameData.PlayerIngredientCraftList);
                LoadIngredientCraftItem();
            }
            else if (keyItemCraftPanel.activeSelf == true)
            {
                SortCraftList(ref SaveLoadManager.Instance.GameData.PlayerKeyItemCraftList);
                LoadKeyItemCraftItem();
            }
            else
            {
                Debug.LogError("Khong co inventory panel nao dang mo!");
            }
        }
    }


}
