using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.Configuration;
using UnityEngine.UI;
using DG.Tweening;
using GDC.Managers;
using GDC.PlayerManager;

namespace GDC.Gameplay.UI
{
    public class CarrySlot : MonoBehaviour
    {
        public SO_Item SO_item;
        public int slotIndex;
        [SerializeField] Image iconImage;

        private void Awake()
        {
            slotIndex = transform.GetSiblingIndex();
        }
        public void Setup(SO_Item SO_item)
        {
            if (SO_item == null)
            {
                this.SO_item = null;
                iconImage.sprite = null;
                iconImage.color = Color.clear;
                return;
            }

            this.SO_item = SO_item;
            iconImage.color = Color.white;
            iconImage.sprite = SO_item.sprite;
        }
        public void OnEnter()
        {
            InventoryManager.Instance.CurrentPointerCarrySlotIndex = slotIndex;
            if (SO_item == null) return;

            if (InventoryManager.Instance.isOpening)
            {
                ExplainPanel.Instance.Show(SO_item);
            }
        }
        public void OnExit()
        {
            InventoryManager.Instance.CurrentPointerCarrySlotIndex = -1;
            if (InventoryManager.Instance.isOpening)
            {
                if (SO_item != null)
                    ExplainPanel.Instance.Hide();
            }
        }
        public void OnDown()
        {
            if (InventoryManager.Instance.isOpening)
            {
                if (SO_item != null && InventoryManager.Instance.ChoosingItem == null)
                {
                    InventoryManager.Instance.SetChoosingItem(SO_item);
                    PutUpItem();
                }
            }
            else
            {
                if (SO_item != null)
                {
                    Debug.Log("use item");
                    if (SO_item.HealthIncrease != 0 || SO_item.StaminaIncrease != 0)
                    {
                        Player.Instance.Heal(SO_item.HealthIncrease, SO_item.StaminaIncrease);
                    }

                    if (SO_item.ID.CompareTo("u010") == 0)
                    {
                        //Player.Instance.EndStatus(Enums.EffectType.SLEEP);
                        Player.Instance.PreventEffect(Enums.EffectType.SLEEP, 20);
                    }
                    else if (SO_item.ID.CompareTo("u011") == 0)
                    {
                        Player.Instance.PreventEffect(Enums.EffectType.POISON, 20);
                    }
                    else if (SO_item.ID.CompareTo("u012") == 0)
                    {
                        Player.Instance.PreventEffect(Enums.EffectType.SLOW, 20);
                    }
                    else if (SO_item.ID.CompareTo("u013") == 0)
                    {
                        Player.Instance.PreventEffect(Enums.EffectType.FREEZE, 20);
                    }
                    else if (SO_item.ID.CompareTo("u014") == 0)
                    {
                        Player.Instance.PreventEffect(Enums.EffectType.SLEEP, 20);
                        Player.Instance.PreventEffect(Enums.EffectType.POISON, 20);
                        Player.Instance.PreventEffect(Enums.EffectType.SLOW, 20);
                        Player.Instance.PreventEffect(Enums.EffectType.FREEZE, 20);
                    }
                    PutUpItem();
                }
            }
        }
        public void OnUp()
        {
            if (InventoryManager.Instance.isOpening)
            {
                Debug.Log("Drop Item");
                if (InventoryManager.Instance.ChoosingItem != null && InventoryManager.Instance.CurrentPointerCarrySlotIndex == -1) //Co the cap nhat them neu keo tha vao sot rac...v.v
                {
                    if (InventoryManager.Instance.CurrentPointerSlotIndex != -1)
                    {
                        SaveLoadManager.Instance.GameData.AddItem(InventoryManager.Instance.ChoosingItem);
                        InventoryManager.Instance.ReloadCurrentPanel();
                    }
                    else
                    {
                        PutDownItem();
                    }
                }
                else if (InventoryManager.Instance.ChoosingItem != null && InventoryManager.Instance.CurrentPointerCarrySlotIndex != -1)
                {
                    SwapCarrySlot(InventoryManager.Instance.CurrentPointerCarrySlotIndex);
                }
                else if (InventoryManager.Instance.ChoosingItem != null)
                {
                    PutDownItem();
                }

                InventoryManager.Instance.SetChoosingItem(null);
            }
        }

        public void PutDownItem()
        {
            //Setup(InventoryManager.Instance.ChoosingItem);
            SO_item = InventoryManager.Instance.ChoosingItem;
            SaveLoadManager.Instance.GameData.CarrySlotList[slotIndex] = SO_item;
            //Debug.Log("Gamedata.CarrySlotList at index " + slotIndex + " = " + SaveLoadManager.Instance.GameData.CarrySlotList[slotIndex]);
            InventoryManager.Instance.LoadCarrySlot();
        }

        public void PutUpItem()
        {
            //Debug.Log("Put Up Carry Item at index " + slotIndex);
            //Setup(null);
            SO_item = null;
            SaveLoadManager.Instance.GameData.CarrySlotList[slotIndex] = null;
            InventoryManager.Instance.LoadCarrySlot();
        }

        void SwapCarrySlot(int otherIndex)
        {
            //Debug.Log("Swap Carry Item at index " + slotIndex + " and index " + otherIndex);
            SaveLoadManager.Instance.GameData.CarrySlotList[slotIndex] = SaveLoadManager.Instance.GameData.CarrySlotList[otherIndex];
            SaveLoadManager.Instance.GameData.CarrySlotList[otherIndex] = InventoryManager.Instance.ChoosingItem;
            InventoryManager.Instance.LoadCarrySlot();
        }
    }
}