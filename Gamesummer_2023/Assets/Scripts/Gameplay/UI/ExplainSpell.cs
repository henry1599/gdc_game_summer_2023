using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.Configuration;
using DG.Tweening;
using UnityEngine.UI;
using TMPro;
using GDC.Managers;

namespace GDC.Gameplay.UI
{
    public class ExplainSpell : MonoBehaviour
    {
        public static ExplainSpell Instance { get; private set; }
        RectTransform rectTransform;
        [SerializeField] TMP_Text nameText, explainText;
        bool isEndShow, isEndHide;

        private void Awake()
        {
            //if (Instance != null)
            //{
            //    Destroy(gameObject);
            //    return;
            //}
            Instance = this;

            rectTransform = gameObject.GetComponent<RectTransform>();
            isEndShow = true;
            isEndHide = true;
        }

        public void Show(SO_Spell so_spell, bool isShowRight)
        {
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_START_PAPER);
            StartCoroutine(Cor_ShowAnim(isShowRight));
            nameText.text = so_spell.Name;
            explainText.text = so_spell.Explain;
        }

        IEnumerator Cor_ShowAnim(bool isShowRight)
        {
            yield return new WaitUntil(() => isEndHide == true);
            isEndShow = false;
            if (isShowRight)
            {
                rectTransform.DOAnchorPosX(950, 0.15f).SetUpdate(true);
                yield return new WaitForSecondsRealtime(0.15f);
                transform.SetSiblingIndex(3);
                rectTransform.DOAnchorPosX(500, 0.15f).SetUpdate(true);
            }
            else
            {
                rectTransform.DOAnchorPosX(-950, 0.15f).SetUpdate(true);
                yield return new WaitForSecondsRealtime(0.15f);
                transform.SetSiblingIndex(3);
                rectTransform.DOAnchorPosX(-500, 0.15f).SetUpdate(true);
            }
            yield return new WaitForSecondsRealtime(0.15f);
            isEndShow = true;
        }    

        public void Hide(bool isShowRight)
        {
            StartCoroutine(Cor_HideAnim(isShowRight));
        }

        IEnumerator Cor_HideAnim(bool isShowRight)
        {
            yield return new WaitUntil(() => isEndShow == true);
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_START_PAPER);
            isEndHide = false;
            if (isShowRight)
            {
                rectTransform.DOAnchorPosX(950, 0.15f).SetUpdate(true);
                yield return new WaitForSecondsRealtime(0.15f);
                transform.SetSiblingIndex(0);
                rectTransform.DOAnchorPosX(300, 0.15f).SetUpdate(true);
            }
            else
            {
                rectTransform.DOAnchorPosX(-950, 0.15f).SetUpdate(true);
                yield return new WaitForSecondsRealtime(0.15f);
                transform.SetSiblingIndex(0);
                rectTransform.DOAnchorPosX(-300, 0.15f).SetUpdate(true);
            }
            yield return new WaitForSecondsRealtime(0.15f);
            isEndHide = true;
        }
    }
}
