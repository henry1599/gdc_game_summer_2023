﻿using GDC.Managers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GDC.Gameplay.UI
{
    public class CraftModeButton : MonoBehaviour
    {
        [SerializeField] bool isPushing;
        [SerializeField] Image buttonImage;
        [SerializeField] Sprite defaultSprite, enterSprite, pushSprite;

        public void OnEnter()
        {
            if (isPushing == false)
            {
                buttonImage.sprite = enterSprite;
            }
        }
        public void OnExit()
        {
            if (isPushing == false)
            {
                buttonImage.sprite = defaultSprite;
            }
        }
        public void OnClick()
        {
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_RELEASE_ITEM);
            if (isPushing == false)
            {
                isPushing = true;
                InventoryManager.Instance.InventoryText.text = "CHẾ TẠO";
                InventoryManager.Instance.isOnCraftMode = true;
                InventoryManager.Instance.inventoryButtonController.SetupCraftButton();
                InventoryManager.Instance.inventoryButtonController.HideNewCraft();
                buttonImage.sprite = pushSprite;
            }
            else
            {
                isPushing = false;
                InventoryManager.Instance.InventoryText.text = "TÚI ĐỒ";
                InventoryManager.Instance.isOnCraftMode = false;
                InventoryManager.Instance.inventoryButtonController.SetupCraftButton(false);
                InventoryManager.Instance.inventoryButtonController.SetupButton();
                buttonImage.sprite = defaultSprite;
            }
        }
        public void ToNormal() //Quay tro lai trang thai mac dinh
        {
            isPushing = false;
            buttonImage.sprite = defaultSprite;
        }
    }
}
