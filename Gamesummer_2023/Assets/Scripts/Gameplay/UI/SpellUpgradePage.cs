using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GDC.Gameplay.UI
{
    public class SpellUpgradePage : MonoBehaviour
    {
        [SerializeField] SpellUpgradeSlot[] spellUpgradeSlots;

        [NaughtyAttributes.Button]
        public void ReloadPage()
        {
            foreach(var spellUpgradeSlot in spellUpgradeSlots)
            {
                spellUpgradeSlot.Setup();
            }    
        }
    }
}
