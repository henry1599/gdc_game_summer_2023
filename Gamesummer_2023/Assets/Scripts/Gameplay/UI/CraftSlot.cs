using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GDC.Managers;
using TMPro;
using GDC.Configuration;
using DG.Tweening;

namespace GDC.Gameplay.UI
{
    public class CraftSlot : MonoBehaviour
    {
        [SerializeField] Image slotImage, iconImage;
        [SerializeField] Image[] itemRequireImages;
        [SerializeField] TMP_Text[] itemRequireTexts;
        [SerializeField] TMP_Text newItemTextTMP, nameText;
        [SerializeField] GameObject lockObject, informationObject;
        [SerializeField] Animator lockAnim;

        SO_Craft SO_craft;

        RectTransform rectTranform;

        public bool isNewItem = false;
        public bool isLocking = true, isCanCraft = false;
        public int slotIndex;

        private void Awake()
        {
            slotIndex = transform.GetSiblingIndex();
            rectTranform = GetComponent<RectTransform>();
        }

        public void Setup(SO_Craft SO_craft)
        {
            if (SO_craft == null)
            {
                lockObject.gameObject.SetActive(true);
                informationObject.SetActive(false);
                isLocking = true;
                return;
            }

            lockObject.SetActive(false);
            informationObject.SetActive(true);
            isLocking = false;

            iconImage.sprite = SO_craft.SO_item.sprite;
            nameText.text = SO_craft.SO_item.itemName;

            for(int i=0; i<3; i++)
            {
                itemRequireImages[i].gameObject.SetActive(false);
                itemRequireTexts[i].gameObject.SetActive(false);
            }
            isCanCraft = true;
            for(int i=0; i<SO_craft.ItemRequires.Length; i++)
            {
                itemRequireImages[i].gameObject.SetActive(true);
                itemRequireTexts[i].gameObject.SetActive(true);

                itemRequireImages[i].sprite = SO_craft.ItemRequires[i].sprite;
                int currentAmount = SaveLoadManager.Instance.GameData.GetAmountOfItem(SO_craft.ItemRequires[i]);
                itemRequireTexts[i].text = currentAmount.ToString() + " / " + SO_craft.numberRequires[i];
                if (currentAmount<SO_craft.numberRequires[i])
                {
                    itemRequireTexts[i].color = Color.red;
                    isCanCraft = false;
                }
                else
                {
                    itemRequireTexts[i].color = Color.green;
                }
            }

            if (IsNewItem(SO_craft)) isNewItem = true;
            else isNewItem = false;
            if (isNewItem == true)
                newItemTextTMP.gameObject.SetActive(true);
            else
                newItemTextTMP.gameObject.SetActive(false);

            this.SO_craft = SO_craft;
        }

        bool IsNewItem(SO_Craft SO_craft)
        {
            if (SaveLoadManager.Instance.GameData.SeenCraftIDList == null) SaveLoadManager.Instance.GameData.SeenCraftIDList = new List<string>();
            foreach (string data in SaveLoadManager.Instance.GameData.SeenCraftIDList)
            {
                if (SO_craft.SO_item.ID.Equals(data))
                {
                    return false;
                }
            }
            return true;
        }
        public void OnEnter()
        {
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_HOLD_ITEM);
            InventoryManager.Instance.CurrentPointerSlotIndex = slotIndex;
            InventoryManager.Instance.CurrentPointerCarrySlotIndex = -1;
            if (isLocking == true) return;

            if (isNewItem) SaveLoadManager.Instance.GameData.AddToSeenCraftIDList(this.SO_craft.SO_item.ID);
            isNewItem = false;
            newItemTextTMP.gameObject.SetActive(false);

            ExplainPanel.Instance.Show(SO_craft.SO_item);
            if (InventoryManager.Instance.ChoosingItem == null)
                EquipmentManager.Instance.SetChangeValue(SO_craft.SO_item);
        }
        public void OnExit()
        {
            InventoryManager.Instance.CurrentPointerSlotIndex = -1;
            if (isLocking == false)
            {
                ExplainPanel.Instance.Hide();
                if (InventoryManager.Instance.ChoosingItem == null)
                    EquipmentManager.Instance.SetChangeValue(null);
            }

        }
        public void OnClick()
        {
            if (isLocking == false && InventoryManager.Instance.ChoosingItem == null)
            {
                if (isCanCraft == false)
                {
                    Debug.Log("You don't have ennough material!");
                }
                else
                {
                    SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_PRESS_BUTTON);
                    for (int i = 0; i < SO_craft.ItemRequires.Length; i++)
                    {
                        SaveLoadManager.Instance.GameData.ReduceItem(SO_craft.ItemRequires[i], SO_craft.numberRequires[i]);
                    }
                    SaveLoadManager.Instance.GameData.AddItem(SO_craft.SO_item);
                    Setup(SO_craft);
                }
            }  
        }

        public void OnDown()
        {
            if (isLocking)
            {
                lockAnim.Play("lockShake");
                SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_UNLOCK);
            }
            else
            {
                SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_RELEASE_ITEM);
                rectTranform.DOScale(new Vector3(0.8f, 0.8f, 1), 0.05f).SetUpdate(true);
            }
        }
        public void OnUp()
        {
            rectTranform.DOScale(new Vector3(1f, 1f, 1), 0.05f).SetUpdate(true);
        }
    }
}