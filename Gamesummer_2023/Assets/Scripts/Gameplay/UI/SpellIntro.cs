using GDC.Managers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GDC.Gameplay.UI
{
    public class SpellIntro : MonoBehaviour
    {
        public static SpellIntro Instance { get;private set; }

        [SerializeField] Animator anim;
        [SerializeField] Image backGraphic;
        [SerializeField] Image graphic;
        [HideInInspector] public bool IsInIntro = false;

        private void Awake()
        {
            //if (Instance != null)
            //    Destroy(gameObject);
            //else
                Instance = this;
            anim.enabled = false;
        }
        
        public bool CastSpellIntro(Sprite graphicSprite,Color backGraphicColor)
        {
            IsInIntro = true;
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_SPELL_INTRO);
            Time.timeScale = 0;
            graphic.sprite = graphicSprite;
            backGraphic.color = backGraphicColor;

            anim.enabled = true;
            anim.Play("spellIntro", 0, 0);
            return true;
        }

        void EndIntro()
        {
            Time.timeScale = 1;
            IsInIntro = false;
        }

    }
}