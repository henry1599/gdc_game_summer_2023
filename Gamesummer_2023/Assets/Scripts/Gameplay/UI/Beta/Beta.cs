using GDC.Managers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.Enums;
using DG.Tweening;
using TMPro;
using static RotaryHeart.Lib.DataBaseExample;
using UnityEngine.UIElements;
using NaughtyAttributes;

public class Beta : MonoBehaviour
{
    [SerializeField] RectTransform betaPanel;
    [SerializeField] TMP_Text congratulationText;
    [SerializeField] float glowValue;
    [SerializeField] float tweenTime;
    Material myMat;
    float currentGlowValue;
    [SerializeField] ParticleSystem vfxStar;

    [Button]
    public void OpenBetaPanel()
    {
        gameObject.SetActive(true);
        Setup();
        betaPanel.DOScale(1, 0.5f).SetEase(Ease.OutBack).OnComplete(() => vfxStar.Play());
    }
    public void CloseBetaPanel()
    {
        vfxStar.Stop();
        betaPanel.DOScale(0, 0.5f).SetEase(Ease.InBack).OnComplete(() => gameObject.SetActive(false));
    }

    public void BackToMainMenu()
    {
        GameManager.Instance.LoadSceneAsyncManually(
                        SceneType.MAIN,
                        TransitionType.FADE,
                        SoundType.MAIN_MENU,
                        cb: () =>
                        {
                            GameManager.Instance.UnloadSceneManually(
                                SaveLoadManager.Instance.GameData.CurrentSceneType
                                //cb: () => GameManager.Instance.SetInitData()
                            );
                        });
    }

    // Start is called before the first frame update
    //void Start()
    //{
    //    myMat = Instantiate(congratulationText.fontSharedMaterial);
    //    congratulationText.fontSharedMaterial = myMat;
    //    myMat.EnableKeyword("GLOW_ON");
    //    DOTween.To(() => this.currentGlowValue, value => this.currentGlowValue = value, glowValue, tweenTime).SetLoops(-1, LoopType.Yoyo).OnUpdate(() =>
    //    {
    //        congratulationText.fontSharedMaterial.SetFloat("_GlowOuter", currentGlowValue);
    //    }).SetEase(Ease.InOutSine);

    //}

    void Setup()
    {
        myMat = Instantiate(congratulationText.fontSharedMaterial);
        congratulationText.fontSharedMaterial = myMat;
        myMat.EnableKeyword("GLOW_ON");
        DOTween.To(() => this.currentGlowValue, value => this.currentGlowValue = value, glowValue, tweenTime).SetLoops(-1, LoopType.Yoyo).OnUpdate(() =>
        {
            congratulationText.fontSharedMaterial.SetFloat("_GlowOuter", currentGlowValue);
        }).SetEase(Ease.InOutSine);
    }    

}
