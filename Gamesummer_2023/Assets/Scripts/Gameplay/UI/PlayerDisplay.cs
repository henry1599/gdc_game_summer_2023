using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.Enums;

namespace GDC.Gameplay.UI
{
    public class PlayerDisplay : MonoBehaviour
    {
        [Header("Player display sprite renderer")]
        [SerializeField] SpriteRenderer hat;
        [SerializeField] SpriteRenderer armor, weapon, shoeFront, shoeBack, shoeBackContain;

        [Header("Default sprite")]
        [SerializeField] Sprite defaultArmor;
        [SerializeField] Sprite defaultWeapon, defaultShoe;

        public static PlayerDisplay Instance { get; private set; }

        private void Awake()
        {
            //if (Instance != null)
            //{
            //    Destroy(gameObject);
            //    return;
            //}
            Instance = this;
        }

        public void UpdateSprite()
        {
            if (InventoryManager.Instance.HelmetSlot.SO_item != null)
            {
                hat.sprite = InventoryManager.Instance.HelmetSlot.SO_item.sprite;
            }
            else
            {
                hat.sprite = null;
            }
            
            if (InventoryManager.Instance.ArmorSlot.SO_item != null)
            {
                armor.sprite = InventoryManager.Instance.ArmorSlot.SO_item.sprite;
            }
            else
            {
                armor.sprite = defaultArmor;
            }

            if (InventoryManager.Instance.WeaponSlot.SO_item != null)
            {
                if (InventoryManager.Instance.WeaponSlot.SO_item.weaponType == WeaponType.SWORD)
                    weapon.transform.rotation = Quaternion.Euler(0, 0, 0);
                else if (InventoryManager.Instance.WeaponSlot.SO_item.weaponType == WeaponType.BOW)
                    weapon.transform.rotation = Quaternion.Euler(0, 0, 180);
                else
                    Debug.LogError("Chua set weaponType cua SO_item trong weaponSlot!");
                weapon.sprite = InventoryManager.Instance.WeaponSlot.SO_item.sprite;
            }
            else
            {
                weapon.transform.rotation = Quaternion.Euler(0, 0, 0);
                weapon.sprite = defaultWeapon;
            }
            
            if (InventoryManager.Instance.ShoeSlot.SO_item != null)
            {
                shoeFront.sprite = InventoryManager.Instance.ShoeSlot.SO_item.sprite;
                shoeBack.sprite = InventoryManager.Instance.ShoeSlot.SO_item.sprite;
                shoeBackContain.sprite = InventoryManager.Instance.ShoeSlot.SO_item.sprite;
            }
            else
            {
                shoeFront.sprite = defaultShoe;
                shoeBack.sprite = defaultShoe;
                shoeBackContain.sprite = defaultShoe;
            }
        }
    }
}
