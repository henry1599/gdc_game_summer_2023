using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using GDC.Managers;

public class CoinDisplay : MonoBehaviour
{
    public static CoinDisplay Instance { get; private set; }
    [SerializeField] TMP_Text coinText;
    private void Awake()
    {
        //if (Instance != null)
        //{
        //    Destroy(gameObject);
        //    return;
        //}
        Instance = this;
    }

    [NaughtyAttributes.Button]
    public void UpdateCoin()
    {
        coinText.text = SaveLoadManager.Instance.GameData.Coin.ToString();
    }
}
