using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GDC.Gameplay.UI
{
    public class InventoryButtonDisplay : MonoBehaviour
    {
        [SerializeField] bool isPushing;
        [SerializeField] Image buttonImage;
        [SerializeField] Sprite defaultSprite, enterSprite, pushSprite;

        public void OnEnter()
        {
            if (isPushing == false)
            {
                buttonImage.sprite = enterSprite;
            }
        }
        public void OnExit()
        {
            if (isPushing == false)
            {
                buttonImage.sprite = defaultSprite;
            }
        }
        public void OnDown()
        {
            isPushing = true;
            buttonImage.sprite = pushSprite;
        }
        public void ToNormal() //Quay tro lai trang thai mac dinh
        {
            isPushing = false;
            buttonImage.sprite = defaultSprite;
        }
    }
}