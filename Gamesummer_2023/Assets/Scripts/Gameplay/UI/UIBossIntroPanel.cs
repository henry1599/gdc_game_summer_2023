using DG.Tweening;
using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace GDC.Gameplay.UI
{
    public class UIBossIntroPanel : MonoBehaviour
    {       
        [SerializeField] Image panelImage;
        [SerializeField] TMP_Text bgName1Text, bgName2Text, nameText, detailText;
        [SerializeField] Vector2 bgName1Old, bgName1New, bgName2Old, bgName2New;
        [SerializeField] Color bgName1Color, bgName2Color;

        [Button]
        public void Show()
        {
            gameObject.SetActive(true);
            panelImage.color = Color.clear;
            bgName1Text.color = Color.clear;
            bgName2Text.color = Color.clear;
            nameText.color = Color.clear;
            detailText.color = Color.clear;
            bgName1Text.rectTransform.anchoredPosition = bgName1Old;
            bgName2Text.rectTransform.anchoredPosition = bgName2Old;

            panelImage.DOColor(Color.white, 1);
            bgName1Text.DOColor(bgName1Color, 1.5f);
            bgName2Text.DOColor(bgName2Color, 1.5f);
            nameText.DOColor(Color.white, 1.5f);
            detailText.DOColor(Color.white, 1.5f);

            bgName1Text.rectTransform.DOAnchorPos(bgName1New, 1.5f);
            bgName2Text.rectTransform.DOAnchorPos(bgName2New, 1.5f);
        }
        [Button]
        public void Hide()
        {           
            bgName1Text.DOColor(Color.clear, 1);
            bgName2Text.DOColor(Color.clear, 1);
            nameText.DOColor(Color.clear, 1);
            detailText.DOColor(Color.clear, 1);
            panelImage.DOColor(Color.clear, 1.5f).OnComplete(() => gameObject.SetActive(false));
        }
    }
}