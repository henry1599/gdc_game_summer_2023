using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetItemDisplayManager : MonoBehaviour
{
    public static GetItemDisplayManager Instance { get; private set; }
    [SerializeField] GetItemDisplay[] getItemDisplays;

    int currentIndex = 3;

    private void Awake()
    {
        //if (Instance != null)
        //{
        //    Destroy(gameObject);
        //    return;
        //}
        Instance = this;
    }

    [NaughtyAttributes.Button]
    public void ShowItemButton()
    {
        getItemDisplays[currentIndex].GetComponent<RectTransform>().anchoredPosition = new Vector3(0, 0, 0);
        getItemDisplays[currentIndex].Show(null, null);
        for (int i = 0; i < 3; i++)
        {
            getItemDisplays[(i + currentIndex) % 4].Grow();
        }
        getItemDisplays[(currentIndex + 3) % 4].Hide();
        currentIndex = (currentIndex + 3) % 4;
    }
    public void ShowItem(Sprite itemSprite, string itemName)
    {
        getItemDisplays[currentIndex].GetComponent<RectTransform>().anchoredPosition = new Vector3(0, 0, 0);
        getItemDisplays[currentIndex].Show(itemSprite, itemName);
        for (int i =0; i<3; i++)
        {
            getItemDisplays[(i + currentIndex) % 4].Grow();
        }
        getItemDisplays[(currentIndex + 3) % 4].Hide();
        currentIndex = (currentIndex + 3) % 4;
    }
}
