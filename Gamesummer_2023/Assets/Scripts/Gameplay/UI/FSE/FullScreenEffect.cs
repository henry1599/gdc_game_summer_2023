using DG.Tweening;
using GDC.Enums;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GDC.Gameplay.UI
{

    public class FullScreenEffect : MonoBehaviour
    {
        public static FullScreenEffect Instance { get; private set; }
        [SerializeField] Image lowHPEffects,freezeEffects,slowEffects,poisonEffects;
        [SerializeField] AnimationCurve tweenCurve;
        Tween lowHPTween/*,freezeTween*/,slowTween,poisonTween;
        private void Awake()
        {
            //if (Instance != null)
            //    Destroy(gameObject);
            //else
                Instance = this;
        }
        public void LowHP()
        {
            EndLowHP();
            lowHPTween = lowHPEffects.DOFade(1, 1).SetEase(tweenCurve).SetLoops(-1);
            lowHPTween.Play();
        }
        public void EndLowHP()
        {
            lowHPTween?.Kill();
            lowHPEffects.color = new Color(1, 1, 1, 0);
        }
        public void Freeze()
        {
            freezeEffects.color = Color.white;
        }
        public void EndFreeze()
        {
            freezeEffects.color = Color.clear;
        }

        public void Slow()
        {
            EndSlow();
            slowTween = slowEffects.DOFade(1, 1).SetEase(tweenCurve);
            slowTween.Play();
        }

        public void EndSlow()
        {
            slowTween?.Kill();
            slowEffects.color = new Color(1, 1, 1, 0);
        }
        public void Poison()
        {
            EndPoison();
            poisonTween = poisonEffects.DOFade(1, 1).SetEase(tweenCurve).SetLoops(-1);
            poisonTween.Play();
        }

        public void EndPoison()
        {
            poisonTween?.Kill();
            poisonEffects.color = new Color(1, 1, 1, 0);
        }
    }
}