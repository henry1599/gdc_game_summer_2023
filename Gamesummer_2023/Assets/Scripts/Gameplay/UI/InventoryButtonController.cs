using GDC.Configuration;
using GDC.Managers;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks.Sources;
using UnityEngine;
using UnityEngine.UI;

namespace GDC.Gameplay.UI
{
    public class InventoryButtonController : MonoBehaviour
    {
        GameData gameData;
        [SerializeField] Image newWeaponTick, newClothesTick, newUseableItemTick, newIngredientTick, newKeyItemTick, newCraftTick;

        public void PlaySoundMouseDown()
        {
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_RELEASE_ITEM);
        }
        void CheckNewItem(List<ItemData> itemDatas, ref Image newTick)
        {
            if (itemDatas != null)
            {
                foreach (var item in itemDatas)
                {
                    bool isSeen = false;
                    foreach (var id in gameData.SeenItemIDList)
                    {
                        if (id.CompareTo(item.SO_item.ID) == 0)
                        {
                            isSeen = true;
                            break;
                        }
                    }

                    if (isSeen == false)
                    {
                        newTick.gameObject.SetActive(true);
                        return;
                    }
                }
            }

            newTick.gameObject.SetActive(false);
        }
        public void SetupButton()
        {
            gameData = SaveLoadManager.Instance.GameData;
            CheckNewItem(gameData.PlayerWeaponList, ref newWeaponTick);
            CheckNewItem(gameData.PlayerClothesList, ref newClothesTick);
            CheckNewItem(gameData.PlayerIngredientList, ref newIngredientTick);
            CheckNewItem(gameData.PlayerUseableItemList, ref newUseableItemTick);
            CheckNewItem(gameData.PlayerKeyItemList, ref newKeyItemTick);

            SetupCraftButton(false);
        }
        public void SetupWeaponItem()
        {
            gameData = SaveLoadManager.Instance.GameData;
            CheckNewItem(gameData.PlayerWeaponList, ref newWeaponTick);
        }
        public void SetupClothesItem()
        {
            gameData = SaveLoadManager.Instance.GameData;
            CheckNewItem(gameData.PlayerClothesList, ref newClothesTick);
        }
        public void SetupIngredientItem()
        {
            gameData = SaveLoadManager.Instance.GameData;
            CheckNewItem(gameData.PlayerIngredientList, ref newIngredientTick);
        }
        public void SetupUseableItemItem()
        {
            gameData = SaveLoadManager.Instance.GameData;
            CheckNewItem(gameData.PlayerUseableItemList, ref newUseableItemTick);
        }
        public void SetupKeyItemItem()
        {
            gameData = SaveLoadManager.Instance.GameData;
            CheckNewItem(gameData.PlayerKeyItemList, ref newKeyItemTick);
        }

        //---------------------------------------------------------------------

        bool CheckNewCraft(List<SO_Craft> crafts, ref Image newTick, bool isSet = true)
        {
            foreach (var craft in crafts)
            {
                bool isSeen = false;
                foreach (var id in gameData.SeenCraftIDList)
                {
                    if (id.CompareTo(craft.SO_item.ID) == 0)
                    {
                        isSeen = true;
                        break;
                    }
                }

                if (isSeen == false)
                {
                    if (isSet)
                        newTick.gameObject.SetActive(true);
                    return true;
                }
            }

            if (isSet)
                newTick.gameObject.SetActive(false);
            return false;
        }

        public void SetupCraftButton(bool isSet = true)
        {
            gameData = SaveLoadManager.Instance.GameData;
            bool kt1, kt2, kt3, kt4, kt5;
            kt1 = CheckNewCraft(gameData.PlayerWeaponCraftList, ref newWeaponTick, isSet);
            kt2 = CheckNewCraft(gameData.PlayerClothesCraftList, ref newClothesTick, isSet);
            kt3 = CheckNewCraft(gameData.PlayerIngredientCraftList, ref newIngredientTick, isSet);
            kt4 = CheckNewCraft(gameData.PlayerUseableItemCraftList, ref newUseableItemTick, isSet);
            kt5 = CheckNewCraft(gameData.PlayerKeyItemCraftList, ref newKeyItemTick, isSet);
            
            if (kt1 || kt2 || kt3 || kt4 || kt5)
            {
                newCraftTick.gameObject.SetActive(true);
            }
            else
            {
                newCraftTick.gameObject.SetActive(false);
            }
        }
        public void SetupWeaponCraft()
        {
            gameData = SaveLoadManager.Instance.GameData;
            CheckNewCraft(gameData.PlayerWeaponCraftList, ref newWeaponTick);
        }
        public void SetupClothesCraft()
        {
            gameData = SaveLoadManager.Instance.GameData;
            CheckNewCraft(gameData.PlayerClothesCraftList, ref newClothesTick);
        }
        public void SetupUseableItemCraft()
        {
            gameData = SaveLoadManager.Instance.GameData;
            CheckNewCraft(gameData.PlayerUseableItemCraftList, ref newUseableItemTick);
        }
        public void SetupIngredientCraft()
        {
            gameData = SaveLoadManager.Instance.GameData;
            CheckNewCraft(gameData.PlayerIngredientCraftList, ref newIngredientTick);
        }
        public void SetupKeyItemCraft()
        {
            gameData = SaveLoadManager.Instance.GameData;
            CheckNewCraft(gameData.PlayerKeyItemCraftList, ref newKeyItemTick);
        }

        //---------------------------------------------------------------------

        public void HideNewWeapon()
        {
            newWeaponTick.gameObject.SetActive(false);
        }
        public void HideNewClothes()
        {
            newClothesTick.gameObject.SetActive(false);
        }
        public void HideNewUseableItem()
        {
            newUseableItemTick.gameObject.SetActive(false);
        }
        public void HideNewIngredient()
        {
            newIngredientTick.gameObject.SetActive(false);
        }
        public void HideNewKeyItem()
        {
            newKeyItemTick.gameObject.SetActive(false);
        }
        public void HideNewCraft()
        {
            newCraftTick.gameObject.SetActive(false);
        }
    }
}
