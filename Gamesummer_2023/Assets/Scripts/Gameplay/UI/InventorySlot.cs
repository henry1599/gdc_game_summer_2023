using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GDC.Managers;
using TMPro;
using GDC.Configuration;
using GDC.Enums;

namespace GDC.Gameplay.UI
{
    public class InventorySlot : MonoBehaviour
    {
        [SerializeField] Image slotImage, iconImage, amountImage;
        [SerializeField] TMP_Text amountTextTMP, newItemTextTMP;

        ItemData itemData;

        public bool isNewItem = false;

        public int slotIndex;

        private void Awake()
        {
            slotIndex = transform.GetSiblingIndex();
        }
        
        public void Setup(ItemData itemData)
        {
            if (itemData == null) 
            {
                this.itemData = null;
                iconImage.sprite = null;
                iconImage.color = Color.clear;
                amountImage.gameObject.SetActive(false);
                newItemTextTMP.gameObject.SetActive(false);
                return;
            }    

            iconImage.sprite = itemData.SO_item.sprite;
            iconImage.color = Color.white;
            if (itemData.Amount > 1)
            {
                amountImage.gameObject.SetActive(true);
                amountTextTMP.text = itemData.Amount.ToString();
            }
            else
            {
                amountImage.gameObject.SetActive(false);
            }
            if (IsNewItem(itemData)) isNewItem = true;
            else isNewItem = false;
            if (isNewItem == true) 
                newItemTextTMP.gameObject.SetActive(true);
            else 
                newItemTextTMP.gameObject.SetActive(false);

            this.itemData = itemData;
        }  

        bool IsNewItem(ItemData itemData)
        {
            if (SaveLoadManager.Instance.GameData.SeenItemIDList == null) SaveLoadManager.Instance.GameData.SeenItemIDList = new List<string>();
            foreach (string data in SaveLoadManager.Instance.GameData.SeenItemIDList)
            {
                if (itemData.SO_item.ID.Equals(data))
                {
                    return false;
                }
            }
            return true;
        }    

        public void OnEnter()
        {
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_HOLD_ITEM);
            InventoryManager.Instance.CurrentPointerSlotIndex = slotIndex;
            InventoryManager.Instance.CurrentPointerCarrySlotIndex = -1;
            if (itemData == null) return;

            if (isNewItem) SaveLoadManager.Instance.GameData.AddToSeenItemIDList(this.itemData.SO_item.ID);
            isNewItem = false;
            newItemTextTMP.gameObject.SetActive(false);

            ExplainPanel.Instance.Show(itemData.SO_item);
            if (InventoryManager.Instance.ChoosingItem == null)
                EquipmentManager.Instance.SetChangeValue(itemData.SO_item);
        }    
        public void OnExit()
        {
            InventoryManager.Instance.CurrentPointerSlotIndex = -1;
            if (itemData != null)
            {
                ExplainPanel.Instance.Hide();
                if (InventoryManager.Instance.ChoosingItem == null)
                    EquipmentManager.Instance.SetChangeValue(null);
            }

        }
        public void OnDown()
        {
            if (itemData != null && InventoryManager.Instance.ChoosingItem == null)
            {
                InventoryManager.Instance.SetChoosingItem(itemData.SO_item);
                DescreaseAmount();        
            }               
        }
        public void OnUp()
        {
            if (InventoryManager.Instance.ChoosingItem != null)
            {
                if (InventoryManager.Instance.ChoosingItem.Equals(itemData.SO_item) && InventoryManager.Instance.CurrentPointerSlotIndex == slotIndex)
                {
                    IncreaseAmount();
                }
                else if (InventoryManager.Instance.CurrentPointerCarrySlotIndex != -1 && InventoryManager.Instance.ChoosingItem.itemType == ItemType.USEABLE_ITEM)
                {
                    int carryIndex = InventoryManager.Instance.CurrentPointerCarrySlotIndex;
                    //DescreaseAmount(); //da descrease amount luc ondown
                    if (InventoryManager.Instance.CarrySlotList[carryIndex].SO_item != null)
                    {
                        SaveLoadManager.Instance.GameData.AddItem(InventoryManager.Instance.CarrySlotList[carryIndex].SO_item);
                        InventoryManager.Instance.ReloadCurrentPanel();
                    }

                    InventoryManager.Instance.CarrySlotList[carryIndex].PutDownItem();

                }
                else if (InventoryManager.Instance.CurrentPointerEquipSlotType != EquipmentType.NONE)
                {
                    EquipmentType slotType = InventoryManager.Instance.CurrentPointerEquipSlotType;
                    if (slotType == itemData.SO_item.EquipmentType)
                    {
                        if (slotType == EquipmentType.HELMET) InventoryManager.Instance.HelmetSlot.PutDownItem();
                        else if (slotType == EquipmentType.ARMOR) InventoryManager.Instance.ArmorSlot.PutDownItem();
                        else if (slotType == EquipmentType.WEAPON) InventoryManager.Instance.WeaponSlot.PutDownItem();
                        else if (slotType == EquipmentType.SHOE) InventoryManager.Instance.ShoeSlot.PutDownItem();
                        //Player.Instance.ChangeEquipmentItem(itemData.SO_item);
                        //InventoryManager.Instance.LoadEquipSlot();
                    }
                    else
                    {
                        IncreaseAmount();
                    }
                }
                else
                {
                    IncreaseAmount();
                }
                
                EquipmentManager.Instance.SetChangeValue(null);
            }

            InventoryManager.Instance.SetChoosingItem(null);

            if (itemData != null)
            {
                if (itemData.Amount <= 0)
                {
                    SaveLoadManager.Instance.GameData.DeleteItem(itemData.SO_item.itemType, slotIndex);
                    itemData = null;
                    InventoryManager.Instance.ReloadCurrentPanel();
                }
            }
        }
        public void OnClick()
        {
            if (InventoryManager.Instance.isSelling == false) return;

            if (itemData != null && SellPopupPanel.Instance.isOpening == false)
            {
                if (itemData.SO_item.itemType == ItemType.KEY_ITEM)
                {
                    TriggerDialogue.Instance.SellDialogue(false);
                }
                else
                {
                    SellPopupPanel.Instance.Setup(itemData.SO_item);
                    SellPopupPanel.Instance.Show();
                }
            }
        }
        public void IncreaseAmount()
        {
            itemData.Amount++;
            iconImage.color = Color.white;
            if (itemData.Amount>1)
            {
                amountImage.gameObject.SetActive(true);
            }
            amountTextTMP.text = itemData.Amount.ToString();
            //SaveLoadManager.Instance.GameData.AddItem(itemData.SO_item);
        }
        public void DescreaseAmount()
        {
            itemData.Amount--;
            amountTextTMP.text = itemData.Amount.ToString();

            if (itemData.Amount == 0)
            {
                iconImage.color = Color.clear;
            }
            if (itemData.Amount < 2)
            {
                amountImage.gameObject.SetActive(false);
            }
            //SaveLoadManager.Instance.GameData.ReduceItem(itemData.SO_item.itemType, slotIndex);
        }
    }
}
