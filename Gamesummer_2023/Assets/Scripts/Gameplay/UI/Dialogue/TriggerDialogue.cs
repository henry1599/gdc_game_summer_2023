using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;
using GDC.Enums;
using GDC.Managers;
using UnityEngine.Events;
//using Unity.VisualScripting;

namespace GDC.Gameplay.UI
{
    public class TriggerDialogue : MonoBehaviour
    {
        public static TriggerDialogue Instance { get; private set; }
        RectTransform rectTransform;
        DialogueState dialogueState;

        string nameStr;
        Queue<string> sentences = new Queue<string>();

        Queue<string> sentencesBranch1 = new Queue<string>();
        Queue<string> sentencesBranch2 = new Queue<string>();
        Queue<string> sentencesTail = new Queue<string>();

        string buySentence, cannotBuySentence, sellSentence, cannotSellSentence;

        [SerializeField] TMP_Text sentenceText, choosingText1, choosingText2, choosingSymbol;
        [SerializeField] Image messageBoxImage;

        string str; //trich sentence tu sentences
        Color nameColor;
        float sec;
        public bool isTalking;
        public bool isEndDialogue;
        bool isChoosingBranch1, isBranchDialogue, isBlackChoosingText;
        bool isHoldF;

        int sentenceIndex, shopSentenceIndex, sellSentenceIndex;
        List<int> eventIndexs;
        List<UnityEvent> eventTalks;

        private void Awake()
        {
            //if (Instance != null)
            //{
            //    Destroy(gameObject);
            //    return;
            //}
            Instance = this;

            rectTransform = GetComponent<RectTransform>();
            isTalking = false;
            isEndDialogue = true;

            gameObject.SetActive(false);
        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.F) && isTalking == false && isEndDialogue == false)
            {
                if (ShopManager.Instance.isOpening == false)
                {
                    SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_INTERACT);
                    DisplayDialogue();
                }
            }

            if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.S))
            {
                if (choosingSymbol.gameObject.activeSelf)
                    SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_INTERACT);
                isChoosingBranch1 = !isChoosingBranch1;
                if (isChoosingBranch1)
                    choosingSymbol.rectTransform.anchoredPosition = new Vector2(0,choosingText1.rectTransform.anchoredPosition.y);
                else
                    choosingSymbol.rectTransform.anchoredPosition = new Vector2(0, choosingText2.rectTransform.anchoredPosition.y);
            }    

            if (Input.GetKey(KeyCode.F))
            {
                //sec = 0.003f;
                isHoldF = true;
            }
            if (Input.GetKeyUp(KeyCode.F))
            {
                //sec = 0.02f;
                isHoldF = false;
            }
            //if (isEndDialogue && Time.timeScale == 0 && isTalking == false)
            //{
            //    Time.timeScale = 1;
            //}
        }

        public void SetDialogue(Dialogue dialogue)
        {
            gameObject.SetActive(true);
            isTalking = true;
            DOTween.Kill(rectTransform);

            messageBoxImage.sprite = dialogue.messageBoxSprite;
            sentenceText.color = dialogue.textColor;
            nameColor = dialogue.nameColor;
            nameStr = dialogue.name;
            sentenceText.text = "";
            buySentence = dialogue.buySentence;
            cannotBuySentence = dialogue.cannotBuySentence;
            sellSentence = dialogue.sellSentence;
            cannotSellSentence = dialogue.cannotSellSentence;
            isBlackChoosingText = dialogue.isBlackChoosingText;

            sentences.Clear();
            sentencesBranch1.Clear();
            sentencesBranch2.Clear();
            sentencesTail.Clear();
            foreach (string sentence in dialogue.sentencesHead)
            {
                sentences.Enqueue(sentence);
            }
            foreach (string sentence in dialogue.sentencesBranch1)
            {
                sentencesBranch1.Enqueue(sentence);
            }
            foreach (string sentence in dialogue.sentencesBranch2)
            {
                sentencesBranch2.Enqueue(sentence);
            }
            foreach (string sentence in dialogue.sentencesTail)
            {
                sentencesTail.Enqueue(sentence);
            }
            dialogueState = DialogueState.HEAD;
            isBranchDialogue = dialogue.isBranchDialogue;
            choosingText1.text = dialogue.sentenceChoosing1;
            choosingText2.text = dialogue.sentenceChoosing2;
            if (isBlackChoosingText)
            {
                choosingSymbol.color = Color.black;
                choosingText1.color = Color.black;
                choosingText2.color = Color.black;
            }    
            else
            {
                choosingSymbol.color = Color.white;
                choosingText1.color = Color.white;
                choosingText2.color = Color.white;
            }

            sentenceIndex = -1;
            shopSentenceIndex = dialogue.shopSentenceIndex;
            sellSentenceIndex = dialogue.sellSentenceIndex;
            eventIndexs = new List<int>();
            eventTalks = new List<UnityEvent>();
            if (dialogue.eventIndexs != null)
            {
                for (int i = 0; i < dialogue.eventIndexs.Length; i++)
                {
                    eventIndexs.Add(dialogue.eventIndexs[i]);
                    eventTalks.Add(dialogue.eventTalks[i]);
                }
            }
            sec = 0.02f;
            isEndDialogue = false;
            //DisplayDialogue();

            //rectTransform.DOAnchorPosY(-400, 0.5f).SetUpdate(true).OnComplete(() => DisplayDialogue());
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_INTERACT);
            rectTransform.anchoredPosition = new Vector2(0, -400);
            rectTransform.localScale = Vector2.zero;
            rectTransform.DOScale(1.2f, 0.3f).SetUpdate(true).OnComplete(() => DisplayDialogue());
        }

        public void DisplayDialogue()
        {
            if (sentences.Count == 0)
            {
                if (dialogueState == DialogueState.HEAD)
                {
                    if (isBranchDialogue)
                    {
                        if (isChoosingBranch1)
                        {
                            sentenceIndex = 99;
                            foreach (string sentence in sentencesBranch1)
                            {
                                sentences.Enqueue(sentence);
                            }
                        }
                        else
                        {
                            sentenceIndex = 199;
                            foreach (string sentence in sentencesBranch2)
                            {
                                sentences.Enqueue(sentence);
                            }
                        }
                        dialogueState = DialogueState.BRANCH;
                        choosingText1.gameObject.SetActive(false);
                        choosingText2.gameObject.SetActive(false);
                        choosingSymbol.gameObject.SetActive(false);
                    }
                    else
                    {
                        EndDialogue();
                        return;
                    }
                }
                else if (dialogueState == DialogueState.BRANCH)
                {
                    if (sentencesTail.Count > 0)
                    {
                        foreach (string sentence in sentencesTail)
                        {
                            sentences.Enqueue(sentence);
                        }
                    }
                    else
                    {
                        //sentences.Enqueue("");
                        EndDialogue();
                        return;
                    }
                    sentenceIndex = 299;
                    dialogueState = DialogueState.TAIL;
                }
                else if (dialogueState == DialogueState.TAIL)
                {
                    EndDialogue();
                    return;
                }
            }
            str = sentences.Dequeue();
            StopAllCoroutines();
            StartCoroutine(TypeSentence(str));
            sentenceIndex++;

            if (shopSentenceIndex>=0 && sentenceIndex == shopSentenceIndex)
            {
                ShopManager.Instance.Show();
                rectTransform.DOAnchorPosY(383, 0.5f).SetUpdate(true);
            }        
            else if (sellSentenceIndex >= 0 && sentenceIndex == sellSentenceIndex)
            {
                InventoryManager.Instance.isSelling = true;
                InventoryManager.Instance.Show(true);
                rectTransform.DOAnchorPosY(383, 0.5f).SetUpdate(true);
            }
            else
            {
                rectTransform.DOAnchorPosY(-400, 0.5f).SetUpdate(true);
            }

            for (int i = 0; i < eventTalks.Count; i++)
            {
                if (eventIndexs[i] == sentenceIndex)
                {
                    eventTalks[i]?.Invoke();
                }
            }
        }

        public void EndDialogue()
        {
            isEndDialogue = true;
            isTalking = false;
            //print("End Dialogue");
            //rectTransform.DOAnchorPosY(-700, 0.5f).SetUpdate(true).OnComplete(() => gameObject.SetActive(false));
            rectTransform.DOScale(0, 0.3f).SetUpdate(true).OnComplete(() => gameObject.SetActive(false));
        }
        void ActiveChoosing()
        {
            choosingText1.gameObject.SetActive(true);
            choosingText2.gameObject.SetActive(true);
            choosingSymbol.gameObject.SetActive(true);
            choosingSymbol.rectTransform.anchoredPosition = new Vector2(0, choosingText1.rectTransform.anchoredPosition.y);
            isChoosingBranch1 = true;
        }
        public void BuyDialogue(bool canBuy)
        {
            StopAllCoroutines();
            if (canBuy)
            {
                StartCoroutine(TypeSentence(buySentence));
            }
            else
            {
                SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_ERROR);
                StartCoroutine(TypeSentence(cannotBuySentence));
            }
        }
        public void SellDialogue(bool canSell)
        {
            StopAllCoroutines();
            if (canSell)
            {
                StartCoroutine(TypeSentence(sellSentence));
            }
            else
            {
                SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_ERROR);
                StartCoroutine(TypeSentence(cannotSellSentence));
            }
        }
        IEnumerator TypeSentence(string str)
        {
            isTalking = true;
            if (nameStr != "")
            {
                string hexColor = ColorUtility.ToHtmlStringRGB(nameColor);
                sentenceText.text = $"<color=#{hexColor}>{nameStr}</color>" + ": ";
            }
            else
            {
                sentenceText.text = "";
            }

            int i = 0;
            foreach (char letter in str.ToCharArray())
            {
                if (letter == '&')
                {
                    string hexColor = ColorUtility.ToHtmlStringRGB(nameColor);
                    sentenceText.text += $"<color=#{hexColor}>{SaveLoadManager.Instance.GameData.PlayerName}</color>";
                }
                else
                {
                    sentenceText.text += letter;
                }
                if (isHoldF == false || (i%4==0 && isHoldF))
                {
                    if (letter == '.' || letter == ',' || letter == '!' || letter == '?')
                        yield return new WaitForSecondsRealtime(sec * 5);
                    else
                        yield return new WaitForSecondsRealtime(sec);
                }
                i++;
            }
            isTalking = false;

            if (isBranchDialogue && dialogueState == DialogueState.HEAD && sentences.Count == 0)
            {
                ActiveChoosing();
            }
        }
    }
}
