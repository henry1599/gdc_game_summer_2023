using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace GDC.Gameplay.UI
{
    [System.Serializable]
    public class Dialogue
    {
        public Sprite messageBoxSprite;
        public Color textColor, nameColor;
        public bool isBranchDialogue;
        public bool isBlackChoosingText;

        public string name;

        [Header("Sentence index from 0->99")]
        public string[] sentencesHead;
        [Header("Sentence index from 100->199")]
        public string[] sentencesBranch1;
        [Header("Sentence index from 200->299")]
        public string[] sentencesBranch2;
        [Header("Sentence index from 300+")]
        public string[] sentencesTail;

        public string sentenceChoosing1, sentenceChoosing2;
        public string buySentence, cannotBuySentence, sellSentence, cannotSellSentence;
        [Header("If this NPC is a shopkeeper, change this value")]
        [Header("to index of the sentence which shop will display")]
        public int shopSentenceIndex = -1;
        public int sellSentenceIndex = -1;
        public int[] eventIndexs;
        public UnityEvent[] eventTalks;
    }
}
