﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GDC.Configuration;
using GDC.Managers;
using GDC.Constants;
using TMPro;
using DG.Tweening;

namespace GDC.Gameplay.UI
{
    public class SpellUpgradeSlot : MonoBehaviour
    {
        [SerializeField] Image iconImage, hoverImage, flashImage;
        [SerializeField] Image[] itemRequireImages;
        [SerializeField] TMP_Text[] itemRequireTexts;
        [SerializeField] TMP_Text newItemTextTMP, nameText, levelText, spellAtkText, StaminaText;
        [SerializeField] Button levelUpButton;
        [SerializeField] GameObject lockObject, infoObject;

        [SerializeField] GameObject glowEffect, fireEffect;

        public SO_Spell SO_spell;
        public bool isNewItem = false;

        bool isCanUpgrade;
        
        [NaughtyAttributes.Button]
        public void Setup()
        {
            if (SO_spell == null)
            {
                Debug.LogError("Chua set SO_Spell cho " + gameObject.name);
                return;
            }
            if (SaveLoadManager.Instance.GameData.GetSpellList[SO_spell.Index] == false)
            {
                lockObject.SetActive(true);
                infoObject.SetActive(false);
                return;
            }
            lockObject.SetActive(false);
            infoObject.SetActive(true);

            iconImage.sprite = SO_spell.Icon;
            nameText.text = SO_spell.Name;
            int level = SaveLoadManager.Instance.GameData.SpellLevelList[SO_spell.Index];
            if (level >=3 && level < 6)
                glowEffect.SetActive(true);
            else if (level >=6)
            {
                glowEffect.SetActive(true);
                fireEffect.SetActive(true);
            }
            else
            {
                glowEffect.SetActive(false);
                fireEffect.SetActive(false);
            }
            levelText.text = "Cấp độ: " + level.ToString();
            spellAtkText.text = "ATK: x" + SO_spell.SpellAtk();
            StaminaText.text = "Stamina: -" + SO_spell.StaminaUsed();

            for (int i = 0; i < 3; i++)
            {
                itemRequireImages[i].gameObject.SetActive(false);
                itemRequireTexts[i].gameObject.SetActive(false);
            }
            isCanUpgrade = true;
            for (int i = 0; i < SO_spell.ItemRequires.Length; i++)
            {
                itemRequireImages[i].gameObject.SetActive(true);
                itemRequireTexts[i].gameObject.SetActive(true);

                itemRequireImages[i].sprite = SO_spell.ItemRequires[i].sprite;
                int currentAmount = SaveLoadManager.Instance.GameData.GetAmountOfItem(SO_spell.ItemRequires[i]);
                itemRequireTexts[i].text = currentAmount.ToString() + " / " + SO_spell.numberRequires[i];
                if (currentAmount < SO_spell.numberRequires[i])
                {
                    itemRequireTexts[i].color = Color.red;
                    isCanUpgrade = false;
                }
                else
                {
                    itemRequireTexts[i].color = Color.green;
                }
            }

            if (level >= GameConstants.MAX_SPELL_LEVEL || isCanUpgrade == false)
            {
                levelUpButton.interactable = false;
            }
            else
            {
                levelUpButton.interactable = true;
            }

            if (IsNewItem()) isNewItem = true;
            else isNewItem = false;
            if (isNewItem == true)
                newItemTextTMP.gameObject.SetActive(true);
            else
                newItemTextTMP.gameObject.SetActive(false);

        }

        bool IsNewItem()
        {
            if (SaveLoadManager.Instance.GameData.SeenSpellList == null)
            {
                SaveLoadManager.Instance.GameData.SpellLevelList = new List<int>();
                SaveLoadManager.Instance.GameData.SeenSpellList = new List<bool>();
                for (int i = 0; i < GameConstants.NUMBER_OF_SPELL; i++)
                {
                    SaveLoadManager.Instance.GameData.SpellLevelList.Add(1);
                    SaveLoadManager.Instance.GameData.SeenSpellList.Add(false);
                }
            }
            return !(SaveLoadManager.Instance.GameData.SeenSpellList[SO_spell.Index]);
        }

        public void OnEnter()
        {
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_HOLD_ITEM);
            hoverImage.gameObject.SetActive(true);
            if (lockObject.activeSelf == true) return;

            if (SO_spell.Index/2 % 2 == 0)
                ExplainSpell.Instance.Show(SO_spell, true);
            else
                ExplainSpell.Instance.Show(SO_spell, false);

            if (isNewItem) SaveLoadManager.Instance.GameData.SeenSpellList[SO_spell.Index] = true;
            isNewItem = false;
            newItemTextTMP.gameObject.SetActive(false);

            //ExplainPanel.Instance.Show(SO_craft.SO_item);
        }
        public void OnExit()
        {
            hoverImage.gameObject.SetActive(false);

            if (lockObject.activeSelf == true) return;

            if (SO_spell.Index/2 % 2 == 0)
                ExplainSpell.Instance.Hide(true);
            else
                ExplainSpell.Instance.Hide(false);
            //if (isLocking == false)
            //{
            //    ExplainPanel.Instance.Hide();
            //}
        }

        public void LevelUp()
        {
            if (isCanUpgrade == false)
            {
                Debug.Log("You don't have ennough material!");
            }
            else
            {
                SoundManager.Instance.PlaySound(AudioPlayer.SoundID.BUTTON_CLICK);
                StartCoroutine(Cor_FlashEffect());
                for (int i = 0; i < SO_spell.ItemRequires.Length; i++)
                {
                    SaveLoadManager.Instance.GameData.ReduceItem(SO_spell.ItemRequires[i], SO_spell.numberRequires[i]);
                }

                int level = SaveLoadManager.Instance.GameData.SpellLevelList[SO_spell.Index];
                level++;
                if (level > GameConstants.MAX_SPELL_LEVEL) 
                    level = GameConstants.MAX_SPELL_LEVEL;
                SaveLoadManager.Instance.GameData.SpellLevelList[SO_spell.Index] = level;
                Setup();
            }
        }

        IEnumerator Cor_FlashEffect()
        {
            flashImage.DOColor(Color.white, 0.2f);
            yield return new WaitForSeconds(0.2f);
            flashImage.DOColor(Color.clear, 0.2f);
        }
    }
}