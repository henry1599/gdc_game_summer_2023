using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using GDC.Managers;

public class UIButton : MonoBehaviour
{
    RectTransform rectTransform;

    private void Awake()
    {
        rectTransform = gameObject.GetComponent<RectTransform>();
    }
    public void OnEnter()
    {
        SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_HOLD_ITEM);
        rectTransform.DOScale(new Vector3(1.3f, 1.3f, 1.3f), 0.5f).SetUpdate(true);
    }    
    public void OnExit()
    {
        rectTransform.DOScale(Vector3.one, 0.5f).SetUpdate(true);
    }    
    public void OnEnterSound()
    {
        SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_HOLD_ITEM);
    }
    public void OnClickSound()
    {
        SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_RELEASE_ITEM);
    }
}
