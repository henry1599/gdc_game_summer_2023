using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using GDC.Managers;

namespace GDC.Gameplay.UI
{
    public class CarrySlotManager : MonoBehaviour
    {
        public static CarrySlotManager Instance { get; private set; }
        RectTransform rectTransform;
        bool isShowing;

        private void Awake()
        {
            //if (Instance != null)
            //{
            //    Destroy(gameObject);
            //    return;
            //}
            Instance = this;

            rectTransform = gameObject.GetComponent<RectTransform>();
            Hide();
        }
        public void Show()
        {
            if (isShowing == false)
            {
                SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_SHOW_CARRY_SLOT);
            }
            isShowing = true;
            rectTransform.DOAnchorPosY(50, 0.5f).SetUpdate(true);
        }

        public void Hide()
        {
            if (isShowing)
            {
                SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_HIDE_CARRY_SLOT);
            }
            isShowing = false;
            rectTransform.DOAnchorPosY(-50,0.5f).SetUpdate(true);
        }

        public void OnEnter()
        {
            if (InventoryManager.Instance.isOpening == false)
            {
                Show();
            }
        }

        public void OnExit()
        {
            if (InventoryManager.Instance.isOpening == false)
            {
                Hide();
            }
        }
    }
}
