using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.Managers;
using GDC.Configuration;
using GDC.Enums;
using UnityEngine.UI;
using GDC.PlayerManager;

namespace GDC.Gameplay.UI
{
    public class EquipSlot : MonoBehaviour
    {
        public SO_Item SO_item;
        [SerializeField] Image iconImage;
        [SerializeField] EquipmentType slotType;

        public void Setup(SO_Item SO_item)
        {
            if (SO_item == null)
            {
                this.SO_item = null;
                iconImage.sprite = null;
                iconImage.color = Color.clear;
                return;
            }

            this.SO_item = SO_item;
            iconImage.color = Color.white;
            iconImage.sprite = SO_item.sprite;

            //StartCoroutine(Cor_SetPlayerData());
            Player.Instance.ChangeEquipmentItem(SO_item);
        }

        //IEnumerator Cor_SetPlayerData()
        //{
        //    yield return new WaitUntil(() => Player.Instance != null);
        //    Player.Instance.ChangeEquipmentItem(SO_item);
        //}

        public void OnEnter()
        {
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_HOLD_ITEM);
            InventoryManager.Instance.CurrentPointerEquipSlotType = slotType;
            if (SO_item == null) return;

            ExplainPanel.Instance.Show(SO_item);
        }
        public void OnExit()
        {
            InventoryManager.Instance.CurrentPointerEquipSlotType = EquipmentType.NONE;
            if (SO_item != null)
                ExplainPanel.Instance.Hide();
        }
        public void OnDown()
        {
            if (SO_item != null && InventoryManager.Instance.ChoosingItem == null)
            {
                InventoryManager.Instance.SetChoosingItem(SO_item);
                PutUpItem();
            }
        }
        public void OnUp()
        {
            Debug.Log("Drop Item");
            if (InventoryManager.Instance.ChoosingItem != null)
            {
                if (InventoryManager.Instance.CurrentPointerEquipSlotType == EquipmentType.NONE) //Co the cap nhat them neu keo tha vao sot rac...v.v
                {
                    if (InventoryManager.Instance.CurrentPointerSlotIndex != -1)
                    {
                        SaveLoadManager.Instance.GameData.AddItem(InventoryManager.Instance.ChoosingItem);
                        InventoryManager.Instance.ReloadCurrentPanel();
                    }
                    else
                    {
                        PutDownItem();
                    }
                }
                else
                {
                    PutDownItem();
                }
            }

                InventoryManager.Instance.SetChoosingItem(null);
        }

        public void PutDownItem()
        {
            Debug.Log("Put Down Equipment Item at equipmentType " + slotType);
            //Setup(InventoryManager.Instance.ChoosingItem);
            if (SO_item != null)
            {
                SaveLoadManager.Instance.GameData.AddItem(SO_item);
                InventoryManager.Instance.ReloadCurrentPanel();
            }

            SO_item = InventoryManager.Instance.ChoosingItem;
            if (SO_item.EquipmentType == EquipmentType.HELMET) SaveLoadManager.Instance.GameData.HelmetEquipItem = SO_item;
            else if (SO_item.EquipmentType == EquipmentType.ARMOR) SaveLoadManager.Instance.GameData.ArmorEquipItem = SO_item;
            else if (SO_item.EquipmentType == EquipmentType.WEAPON) SaveLoadManager.Instance.GameData.WeaponEquipItem = SO_item;
            else if (SO_item.EquipmentType == EquipmentType.SHOE) SaveLoadManager.Instance.GameData.ShoeEquipItem = SO_item;
            Player.Instance.ChangeEquipmentItem(SO_item);
            InventoryManager.Instance.LoadEquipSlot();        
        }

        public void PutUpItem()
        {
            Debug.Log("AAA");
            //Setup(null);
            SO_item = null;
            if (slotType == EquipmentType.HELMET) SaveLoadManager.Instance.GameData.HelmetEquipItem = null;
            else if (slotType == EquipmentType.ARMOR) SaveLoadManager.Instance.GameData.ArmorEquipItem = null;
            else if (slotType == EquipmentType.WEAPON) SaveLoadManager.Instance.GameData.WeaponEquipItem = null;
            else if (slotType == EquipmentType.SHOE) SaveLoadManager.Instance.GameData.ShoeEquipItem = null;
            Player.Instance.TakeOutEquipmentItem(slotType);
            InventoryManager.Instance.LoadEquipSlot();
        }
    }
}