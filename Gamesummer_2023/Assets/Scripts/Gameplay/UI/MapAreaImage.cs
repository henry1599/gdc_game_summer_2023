using DG.Tweening;
using GDC.Configuration;
using GDC.Enums;
using GDC.Managers;
using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GDC.Gameplay.UI
{
    public class MapAreaImage : MonoBehaviour
    {
        [SerializeField] RectTransform rectTransform;
        [SerializeField] string areaName, areaInfo;
        public List<SceneType> sceneTypes;
        [SerializeField] List<Image> pathImages;

        public void Setup()
        {
            if (CheckSceneType())
            {
                gameObject.SetActive(true);
                foreach(var pathImage in pathImages)
                {
                    pathImage.gameObject.SetActive(true);
                }
            }
            else
            {
                gameObject.SetActive(false);
            }
        }
        bool CheckSceneType()
        {
            foreach (var sceneTypePass in SaveLoadManager.Instance.GameData.SceneTypePassList)
            {
                foreach (var sceneType in sceneTypes)
                {
                    if (sceneTypePass == sceneType)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        public void OnEnter()
        {
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_HOLD_ITEM);
            ExplainMapPanel.Instance.Show(areaName, areaInfo);
            rectTransform.DOScale(1.1f, 0.5f).SetUpdate(true);
        }
        public void OnExit()
        {
            ExplainMapPanel.Instance.Hide();
            rectTransform.DOScale(1f, 0.5f).SetUpdate(true);
        }
    }
}
