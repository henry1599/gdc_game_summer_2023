using DG.Tweening;
using GDC.Managers;
using GDC.PlayerManager;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GDC.Gameplay.UI
{
    public class MapManager : MonoBehaviour
    {
        public static MapManager Instance { get; private set; }
        RectTransform rectTransform;
        [SerializeField] MapOverallArea mapOverallArea;
        public bool isOpening = false;
        Tween hideTween, explainHideTween;

        private void Awake()
        {
            //if (Instance != null)
            //{
            //    Destroy(gameObject);
            //    return;
            //}
            Instance = this;

            rectTransform = gameObject.GetComponent<RectTransform>();
            gameObject.SetActive(false);
        }
        public void Show()
        {
            if (Player.Instance.InCutScene) return;
            if (SpellUpgradeManager.Instance?.isOpening == true)
            {
                SpellUpgradeManager.Instance.Hide();
            }
            if (InventoryManager.Instance?.isOpening == true)
            {
                InventoryManager.Instance.Hide();
            }
            if (isOpening == true)
            {
                Hide();
                return;
            }

            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_START_UI);
            hideTween.Kill();
            explainHideTween.Kill();

            gameObject.SetActive(true);
            rectTransform.DOAnchorPosY(0, 0.5f).SetUpdate(true);
            ExplainMapPanel.Instance.gameObject.SetActive(true);
            ExplainMapPanel.Instance.rectTransform.DOAnchorPosY(-209f, 0.5f).SetUpdate(true);
            Time.timeScale = 0;
            isOpening = true;
            if (mapOverallArea != null)
            {
                mapOverallArea.Setup();
            }
        }
        public void Hide()
        {
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_END_UI);
            Time.timeScale = 1;
            isOpening = false;
            hideTween = rectTransform.DOAnchorPosY(-1000, 0.5f).SetUpdate(true).OnComplete(() => gameObject.SetActive(false));
            explainHideTween = ExplainMapPanel.Instance.rectTransform.DOAnchorPosY(-1209f, 0.5f).SetUpdate(true)
                .OnComplete(() => ExplainPanel.Instance.gameObject.SetActive(false));
        }
    }
}
