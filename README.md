# GDC_Game_Summer_2023



## Getting started

Đây là một project do CLB GameDev Club phát triển trong nửa năm, trong đó mình là leader với team gồm 3 người kể cả mình. Cho tới nay đã ra được bản beta, tuy vẫn còn cần phải hoàn thiện rất nhiều mới có thể hoàn thiện hoàn toàn con game.

Thư Giới, đây là một con game mà bạn hóa thân vào cậu bé bị hút vào thế giới trong một cuốn sách. Ở trong đó, cậu phải đi thám hiểm và đánh bại tất cả các thủ lĩnh của quái vật để có thể thực hiện điều ước trở về với thế giới của cậu. Người chơi sẽ lần lượt khám phá các màn chơi đầy thú vị, tươi sáng và cũng không kém phần đặc sắc. Game cũng có rất nhiều sự kiện đặc biệt ẩn trong game.


## Link

- [ ] [Video demo](https://youtu.be/N9dM9c5q40M)
- [ ] [Game build](https://drive.google.com/file/d/1xoKISENcaHMecXgpeM9Q3jDVNpO2OJD1/view?usp=sharing)
